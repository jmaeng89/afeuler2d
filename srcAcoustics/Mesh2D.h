/*!\file Mesh2D.h
  \brief Header file defining 2D Unstructed Mesh Class. */

#ifndef _MESH2D_INCLUDED
#define _MESH2D_INCLUDED

#include "State2D.h"

#define    NP    7
#define    NQ    3
#define    VORTICITY_ON    1111
#define    VORTICITY_OFF   1112
#define    NONLINEAR_ON    2111
#define    NONLINEAR_OFF   2112

/*!
 * Class: Mesh2D
 *
 * Definition of an element in unstructured mesh for solution of
 * barotropic flow.
 *
 * Member functions
 *     Q[0-5]               -- Return nodal states.
 *     Q[6]                 -- Return bubble function coefficient.
 *     Qavg                 -- Return average cell state.
 */

class Mesh2D{
 private:
 public:
        //@{ @name Primitive variables and associated constants:
        Euler2D               *Q; //!< Solution states.
        Euler2D             Qavg; //!< Average linearized solution state.
        double               *nx; //!< Node x-coordinates.
        double               *ny; //!< Node y-coordinates.
        int                 *nid; //!< Global id of all nodes.
        double            *angle; //!< Angle associated with each vertex node only.
        //@}

        //@{ @name Creation, copy, and assignment constructors.
        //! Creation constructor.
        Mesh2D(void) {
                Qavg = Euler2D(ZERO,ZERO,ZERO,ZERO);
                Q = new Euler2D [NP];
                for (int i = 0; i < NP; i++) {
                        Q[i] = Euler2D(ZERO,ZERO,ZERO,ZERO);
                }
                nx = new double [NQ];
                ny = new double [NQ];
                nid = new int [NQ];
                angle = new double [NQ];
                for (int i = 0; i < NQ; i++) {
                        nx[i] = ZERO;
                        ny[i] = ZERO;
                        nid[i] = 0;
                        angle[i] = ZERO;
                }
        }

        //! Copy constructor.
        Mesh2D(const Mesh2D &Elem) {
                Qavg = Elem.Qavg;
                Q = new Euler2D [NP];
                for (int i = 0; i < NP; i++) {
                        Q[i] = Elem.Q[i];
                }
                nx = new double [NQ];
                ny = new double [NQ];
                nid = new int [NQ];
                angle = new double [NQ];
                for (int i = 0; i < NQ; i++) {
                        nx[i] = Elem.nx[i];
                        ny[i] = Elem.ny[i];
                        nid[i] = Elem.nid[i];
                        angle[i] = Elem.angle[i];
                }
        }

        //! Assignment constructor.
        Mesh2D(Euler2D *Qi) {
                Qavg = 9.0/20.0*Qi[6] + 1.0/3.0*(Qi[1] + Qi[3] + Qi[5]);
                Q = new Euler2D [NP];
                for (int i = 0; i < NP; i++) {
                        Q[i] = Qi[i];
                }
        }

        //! Assignment constructor.
        Mesh2D(Euler2D *Qi,
               double *nodex, double *nodey,
               int *nodeid) {
                Qavg = 9.0/20.0*Qi[6] + 1.0/3.0*(Qi[1] + Qi[3] + Qi[5]);
                Q = new Euler2D [NP];
                for (int i = 0; i < NP; i++) {
                        Q[i] = Qi[i];
                }
                nx = new double [NQ];
                ny = new double [NQ];
                nid = new int [NQ];
                for (int i = 0; i < NQ; i++) {
                        nx[i] = nodex[i];
                        ny[i] = nodey[i];
                        nid[i] = nodeid[i];
                }
                angle = new double [NQ];
                // Determine the angle corresponding with each vertex
                angle[0] = acos( (l2sq() + l3sq() - l1sq())/(2.0*l2()*l3()) );
                angle[1] = acos( (l1sq() + l3sq() - l2sq())/(2.0*l1()*l3()) );
                angle[2] = acos( (l1sq() + l2sq() - l3sq())/(2.0*l1()*l2()) );
        }

        /* Destructor. */
        ~Mesh2D(void) {
                delete[] Q;
                delete[] nx; delete [] ny;
                delete[] nid;
                delete[] angle;
        };
        // Use automatically generated destructor.
        //@}
 
        //@{ @name Useful operators.
        //! Allocate
        void allocate() {
                Euler2D Q0(ZERO,ZERO,ZERO,ZERO);
                Q = new Euler2D [NP];
                for (int i = 0; i < NP; i++) {
                        Q[i] = Q0;
                }
                Qavg = Q0;
    
                nx = new double [NQ];
                ny = new double [NQ];
                nid = new int [NQ];
                angle = new double [NQ];
                for (int i = 0; i < NQ; i++) {
                        nx[i] = ZERO;
                        ny[i] = ZERO;
                        nid[i] = 0;
                        angle[i] = ZERO;
                }
        }
  
        //! Copy operator.
        void Copy(const Mesh2D &Elem) {
                Qavg = Elem.Qavg;
                for (int i = 0; i < NP; i++) {
                        Q[i] = Elem.Q[i];
                }
                for (int j = 0; j < NQ; j++) {
                        nx[j] = Elem.nx[j];
                        ny[j] = Elem.ny[j];
                        nid[j] = Elem.nid[j];
                        angle[j] = Elem.angle[j];
                }
        }

        void CopyState(const Mesh2D &Elem) {
                Qavg = Elem.Qavg;
                for (int i = 0; i < NP; i++) {
                        Q[i] = Elem.Q[i];
                }
        }

        void CopyGeometry(const Mesh2D &Elem) {
                for (int i = 0; i < NQ; i++) {
                        nx[i] = Elem.nx[i];
                        ny[i] = Elem.ny[i];
                        nid[i] = Elem.nid[i];
                        angle[i] = Elem.angle[i];
                }
        }
        
        void InitState (const Euler2D &Qi, const int &index) {
                Q[index] = Qi;
        }

        void InitState (const Euler2D &Qi) {
                for (int i = 0; i < NP-1; i++) {
                        Q[i] = Qi;
                }
                Q[NP-1] = Euler2D(ZERO,ZERO,ZERO,ZERO);
        }
  
        void InitState (Euler2D *Qi) {
                for (int i = 0; i < NP; i++) {
                        Q[i] = Qi[i];
                }
        }
  
        void InitNodes (double *nodex, double *nodey,
                        int *nodeid) {
                for (int i = 0; i < NQ; i++) {
                        nx[i] = nodex[i];
                        ny[i] = nodey[i];
                        nid[i] = nodeid[i];
                }
                // Determine the angle corresponding with each vertex
                angle[0] = acos( (l2sq() + l3sq() - l1sq())/(2.0*l2()*l3()) );
                angle[1] = acos( (l1sq() + l3sq() - l2sq())/(2.0*l1()*l3()) );
                angle[2] = acos( (l1sq() + l2sq() - l3sq())/(2.0*l1()*l2()) );
        }

        void InitEdgeQ (void) {
                // Assume nodes have Q values
                Q[1] = HALF*(Q[2] + Q[4]);
                Q[3] = HALF*(Q[0] + Q[4]);
                Q[5] = HALF*(Q[0] + Q[2]);
                Q[6] = Euler2D(ZERO, ZERO, ZERO, ZERO);   // bubble function
        }
  
        void InitAngle(void) {
                // Assume nodes have values
                angle[0] = acos( (l2sq() + l3sq() - l1sq())/(2.0*l2()*l3()) );
                angle[1] = acos( (l1sq() + l3sq() - l2sq())/(2.0*l1()*l3()) );
                angle[2] = acos( (l1sq() + l2sq() - l3sq())/(2.0*l1()*l2()) );
        }
     
        void InitAverage(void) {
                Q[6] = Euler2D(ZERO, ZERO, ZERO, ZERO);   // bubble function
                Q[6].setgas();
                Qavg = THIRD*(Q[1] + Q[3] + Q[5]);
        }

        void SetQuadAverage(void) {
                // quadratic 
                Qavg = THIRD*(Q[1] + Q[3] + Q[5]);
                Qavg.setgas();
                Q[6] = Euler2D(ZERO, ZERO, ZERO, ZERO); 
                Q[6].setgas();
        }

        void SetAverage(void) {
                Qavg = 9.0/20.0*Q[6] + THIRD*(Q[1] + Q[3] + Q[5]);
        }
        
        void SetAverage(const Euler2D &Qaverage) {
                Qavg = Qaverage;
        }
  
        void SetBubbleFunction (void) {
                Q[6] = 20.0/9.0*(Qavg - 1.0/3.0*(Q[1]+Q[3]+Q[5]));
        }
  
        void SetBubbleFunction(const Euler2D &Qaverage) {
                Qavg = Qaverage;
                Q[6] = 20.0/9.0*(Qaverage - 1.0/3.0*(Q[1]+Q[3]+Q[5]));
        }

        /********************************************************
         * Routine: CheckAvg                                    *
         *                                                      *
         * This function returns the value of the coefficient   *
         * associated with the 7th basis function, which is     *
         * directly related to the average value of the element.*
         *                                                      *
         ********************************************************/
        Euler2D CheckAvg(void) {
                return ( 20.0/9.0*(Qavg - (Q[1] + Q[3] + Q[5])/3.0) );
        }

        
        //@{ @name State functions.
        //! Straight edge lengths.
        double l1(void);
        double l1(void) const;
        double l2(void);
        double l2(void) const;
        double l3(void);
        double l3(void) const;
        //! Straight edge lengths squared.
        double l1sq(void);
        double l1sq(void) const;
        double l2sq(void);
        double l2sq(void) const;
        double l3sq(void);
        double l3sq(void) const;

        //! Minimum length to determine time step
        double lmin(void) {
                double length, value;
                length = max(l1sq(), max(l2sq(),l3sq()));
                value = A()/sqrt(length);
                return (value);
        }

        double lmin(void) const {
                double length, value;
                length = max(l1sq(), max(l2sq(),l3sq()));
                value = A()/sqrt(length);
                return (value);
        }

        //! Edge normal vectors.
        // Edge 1 normal vectors
        double norm1x(void);
        double norm1x(void) const;
        double norm1y(void);
        double norm1y(void) const;
        double tan1(void);
        double tan1(void) const;
        double norm1xl1(void);
        double norm1xl1(void) const;
        double norm1yl1(void);
        double norm1yl1(void) const;
        
        // Edge 2 normal vectors
        double norm2x(void);
        double norm2x(void) const;
        double norm2y(void);
        double norm2y(void) const;
        double tan2(void);
        double tan2(void) const;
        double norm2xl2(void);
        double norm2xl2(void) const;
        double norm2yl2(void);
        double norm2yl2(void) const;
        
        // Edge 3 normal vectors
        double norm3x(void);
        double norm3x(void) const;
        double norm3y(void);
        double norm3y(void) const;
        double tan3(void);
        double tan3(void) const;
        double norm3xl3(void);
        double norm3xl3(void) const;
        double norm3yl3(void);
        double norm3yl3(void) const;

        //! Area.
        double A(void);
        double A(void) const;

        //! Jacobian of coordinate transformation matrix.
        // Determinant
        double J(void);
        double J(void) const;

        // Matrix entries
        double J11(void);
        double J11(void) const;
        double J12(void);
        double J12(void) const;
        double J21(void);
        double J21(void) const;
        double J22(void);
        double J22(void) const;

        //! Arbitrary x- & y-coordinates based on xi and eta in reference element
        double x(const double &xi, const double &eta) {
                double sum;
                sum = nx[0]*(1.0 - xi - eta) + nx[1]*xi + nx[2]*eta;
                return (sum);
        }

        double x(const double &xi, const double &eta) const {
                double sum;
                sum = nx[0]*(1.0 - xi - eta) + nx[1]*xi + nx[2]*eta;
                return (sum);
        }
  
        double y(const double &xi, const double &eta) {
                double sum;
                sum = ny[0]*(1.0 - xi - eta) + ny[1]*xi + ny[2]*eta;
                return (sum);
        }
        
        double y(const double &xi, const double &eta) const {
                double sum;
                sum = ny[0]*(1.0 - xi - eta) + ny[1]*xi + ny[2]*eta;
                return (sum);
        }

  
        //! Vertices xi and eta in reference element
        double xip(const int &inode) {
                double xi;
                if (inode == 5 || inode == 1)
                        xi = HALF;
                else if (inode == 2)
                        xi = ONE;
                else if (inode == NP-1)
                        xi = THIRD;
                else
                        xi = ZERO;
                return (xi);
        }
        double xip(const int &inode) const {
                double xi;
                if (inode == 5 || inode == 1)
                        xi = HALF;
                else if (inode == 2)
                        xi = ONE;
                else if (inode == NP-1)
                        xi = THIRD;
                else
                        xi = ZERO;
                return (xi);
        }

        double etap(const int &inode) {
                double eta;
                if (inode == 1 || inode == 3)
                        eta = HALF;
                else if (inode == 4)
                        eta = ONE;
                else if (inode == NP-1)
                        eta = THIRD;
                else
                        eta = ZERO;
                return (eta);
        }
        double etap(const int &inode) const {
                double eta;
                if (inode == 1 || inode == 3)
                        eta = HALF;
                else if (inode == 4)
                        eta = ONE;
                else if (inode == NP-1)
                        eta = THIRD;
                else
                        eta = ZERO;
                return (eta);
        }

        double xietap(const int &inode) {
                double xieta;
                if (inode == 1)
                        xieta = HALF*HALF;
                else if (inode == NP-1)
                        xieta = THIRD*THIRD;
                else
                        xieta = ZERO;
                return (xieta);
        }
        double xietap(const int &inode) const {
                double xieta;
                if (inode == 1)
                        xieta = HALF*HALF;
                else if (inode == NP-1)
                        xieta = THIRD*THIRD;
                else
                        xieta = ZERO;
                return (xieta);
        }
        
        
        //! State Q inside triangle and along the edges
        // Q at arbitrary (xi,eta) in triangle
        Euler2D Qtri (const double &xi, const double &eta);
        Euler2D Qedge1 (const double &sig);
        Euler2D Qedge2 (const double &sig);
        Euler2D Qedge3 (const double &sig);
        // States at each node
        Euler2D dQdxi (const int &inode);
        Euler2D dQdeta (const int &inode);
        Euler2D dQdxi2 (const int &inode);
        Euler2D dQdeta2 (const int &inode);
        Euler2D dQdxieta (const int &inode);
        Euler2D dQdx (const int &inode);
        Euler2D dQdy (const int &inode);
        Euler2D dQdxx (const int &inode);
        Euler2D dQdyy (const int &inode);
        Euler2D dQdxy (const int &inode);


        double IntegralFunction(const double &d0) {
                double r12, r23, r31, r7;
                r12 = HALF*sqrt((nx[0]+nx[1])*(nx[0]+nx[1]) + (ny[0]+ny[1])*(ny[0]+ny[1]));
                r23 = HALF*sqrt((nx[2]+nx[1])*(nx[2]+nx[1]) + (ny[2]+ny[1])*(ny[2]+ny[1]));
                r31 = HALF*sqrt((nx[2]+nx[0])*(nx[2]+nx[0]) + (ny[2]+ny[0])*(ny[2]+ny[0]));
                r7 = THIRD*sqrt((nx[0]+nx[1]+nx[2])*(nx[0]+nx[1]+nx[2])
                                + (ny[0]+ny[1]+ny[2])*(ny[0]+ny[1]+ny[2]));

                double f1 = ZERO, f3 = ZERO, f5 = ZERO, fbubb = ZERO;
                f1 = (Q[5].d - d0)*46.0*r12*r12*r12*r12*exp(-4.0*r12);
                f3 = (Q[1].d - d0)*46.0*r23*r23*r23*r23*exp(-4.0*r23);
                f5 = (Q[3].d - d0)*46.0*r31*r31*r31*r31*exp(-4.0*r31);
                fbubb = (Q[6].d - ZERO)*46.0*r7*r7*r7*r7*exp(-4.0*r7);

                return ( A()*(THIRD*(f1 + f3 + f5) + 9.0/20.0*fbubb) );
        }

        
        //!> Spherical means functions
        double constflux (const int &inode);
        double constflux (const int &inode) const;  
        double xiflux (const double &flux_x, const double &flux_y, const int &inode);
        double xiflux (const double &flux_x, const double &flux_y, const int &inode) const;
        double etaflux (const double &flux_x, const double &flux_y, const int &inode);
        double etaflux (const double &flux_x, const double &flux_y, const int &inode) const;
        double xi2flux (const double &R, const double &flux_x, const double &flux_y,
                        const double &flux_x2, const double &flux_y2, const double &flux_xy,
                        const int &inode);
        double xi2flux (const double &R, const double &flux_x, const double &flux_y,
                        const double &flux_x2, const double &flux_y2, const double &flux_xy,
                        const int &inode) const;
        double eta2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode);
        double eta2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode) const;
        double xietaflux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode);
        double xietaflux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode) const;
        double xi3flux (const double &R, const double &flux_x, const double &flux_y,
                        const double &flux_x2, const double &flux_y2, const double &flux_xy,
                        const double &flux_x3, const double &flux_y3,
                        const double &flux_x2y, const double &flux_xy2, const int &inode);
        double xi3flux (const double &R, const double &flux_x, const double &flux_y,
                        const double &flux_x2, const double &flux_y2, const double &flux_xy,
                        const double &flux_x3, const double &flux_y3,
                        const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double eta3flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const double &flux_x3, const double &flux_y3,
                         const double &flux_x2y, const double &flux_xy2, const int &inode);
        double eta3flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const double &flux_x3, const double &flux_y3,
                         const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double xi2etaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const double &flux_x3, const double &flux_y3,
                           const double &flux_x2y, const double &flux_xy2, const int &inode);
        double xi2etaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const double &flux_x3, const double &flux_y3,
                           const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double xieta2flux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const double &flux_x3, const double &flux_y3,
                           const double &flux_x2y, const double &flux_xy2, const int &inode);
        double xieta2flux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const double &flux_x3, const double &flux_y3,
                           const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        
        double dxiflux (const double &R, const double &flux_x, const double &flux_y,
                        const int &inode);
        double dxiflux (const double &R, const double &flux_x, const double &flux_y,
                        const int &inode) const;
        double detaflux (const double &R, const double &flux_x, const double &flux_y,
                         const int &inode);
        double detaflux (const double &R, const double &flux_x, const double &flux_y,
                         const int &inode) const;
        double dxi2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode);
        double dxi2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode) const;
        double deta2flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode);
        double deta2flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode) const;
        double dxietaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const int &inode);
        double dxietaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const int &inode) const;
        double dxi2etaflux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode);
        double dxi2etaflux (const double &R, const double &flux_dx, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double dxieta2flux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode);
        double dxieta2flux (const double &R, const double &flux_dx, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode) const;

        double rconstflux (const double &R, const int &inode);
        double rconstflux (const double &R, const int &inode) const;
        double rxiflux (const double &R, const double &flux_x, const double &flux_y,
                        const int &inode);
        double rxiflux (const double &R, const double &flux_x, const double &flux_y,
                        const int &inode) const;
        double retaflux (const double &R, const double &flux_x, const double &flux_y,
                         const int &inode);
        double retaflux (const double &R, const double &flux_x, const double &flux_y,
                         const int &inode) const;
        double rxi2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode);
        double rxi2flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const int &inode) const;
        double reta2flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode);
        double reta2flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const int &inode) const;
        double rxietaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const int &inode);
        double rxietaflux (const double &R, const double &flux_x, const double &flux_y,
                           const double &flux_x2, const double &flux_y2, const double &flux_xy,
                           const int &inode) const;
        double rxi3flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const double &flux_x3, const double &flux_y3,
                         const double &flux_x2y, const double &flux_xy2, const int &inode);
        double rxi3flux (const double &R, const double &flux_x, const double &flux_y,
                         const double &flux_x2, const double &flux_y2, const double &flux_xy,
                         const double &flux_x3, const double &flux_y3,
                         const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double reta3flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const double &flux_x3, const double &flux_y3,
                          const double &flux_x2y, const double &flux_xy2, const int &inode);
        double reta3flux (const double &R, const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const double &flux_x3, const double &flux_y3,
                          const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double rxi2etaflux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode);
        double rxi2etaflux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        double rxieta2flux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode);
        double rxieta2flux (const double &R, const double &flux_x, const double &flux_y,
                            const double &flux_x2, const double &flux_y2, const double &flux_xy,
                            const double &flux_x3, const double &flux_y3,
                            const double &flux_x2y, const double &flux_xy2, const int &inode) const;
        
  
        /* ~~~~~~~~~~  MR(f) - Spherical mean of basis functions ~~~~~~~~~~~~~ */
        Euler2D MRf(const int &inode,
                    const double &xi, const double &eta,
                    const double &xi2, const double &eta2, const double &xieta,
                    const double &xi2eta, const double &xieta2);

        /* ~~~ MR(df/dx) - Spherical mean of Derivative of basis functions ~~~ */
        Euler2D MRdfdx(const int &inode,
                       const double &xi, const double &eta,
                       const double &xi2, const double &eta2, const double &xieta);
        Euler2D MRdfdy(const int &inode, 
                       const double &xi, const double &eta,
                       const double &xi2, const double &eta2, const double &xieta);
        
        /* ~~~~~~~~~~  dMR(f)/dR - Derivative of Spherical mean wrt R ~~~~~~~~~~~~~ */
        Euler2D dMRdR (const int &inode, 
                       const double &dxi, const double &deta,
                       const double &dxi2, const double &deta2, const double &dxieta,
                       const double &dxi2eta, const double &dxieta2);

        /* ~~~ MR(df/dxx) - Spherical mean of 2nd Derivative of basis functions ~~~ */
        Euler2D rMRdfdxx(const int &inode, const double &R,
                         const double &xi, const double &eta);
        Euler2D rMRdfdyy(const int &inode, const double &R,
                         const double &xi, const double &eta);
        Euler2D rMRdfdxy(const int &inode, const double &R,
                         const double &xi, const double &eta);

        /* ~~~ rMR{(df/dx)(df/dx)} - Spherical mean of 2 Derivatives of basis functions ~~~ */
        Euler2D rMRdfdxdy(const int &inode, const double &R,
                          const double &rxi, const double &reta,
                          const double &rxi2, const double &reta2, const double &rxieta,
                          const double &rxi2eta, const double &rxieta2,
                          const double &rxi3, const double &reta3);
        Euler2D rMRdfdx2(const int &inode, const double &R,
                         const double &rxi, const double &reta,
                         const double &rxi2, const double &reta2, const double &rxieta,
                         const double &rxi2eta, const double &rxieta2,
                         const double &rxi3, const double &reta3);
        Euler2D rMRdfdy2(const int &inode, const double &R,
                         const double &rxi, const double &reta,
                         const double &rxi2, const double &reta2, const double &rxieta,
                         const double &rxi2eta, const double &rxieta2,
                         const double &rxi3, const double &reta3);
        
        // Vorticity & Nonlinear Terms
        void VorticityMR(const int &inode, const double &R,
                         const int &xi, const int &eta,
                         Euler2D &Flux);        
        Euler2D NonlinearMR(const int &inode, const double &R,
                            const double &xi, const double &eta, 
                            const double &rxi, const double &reta,
                            const double &rxi2, const double &reta2, const double &rxieta,
                            const double &rxi2eta, const double &rxieta2,
                            const double &rxi3, const double &reta3);
        Euler2D PoissonMR(const int &inode, const double &R,
                          const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy,
                          const double &flux_x3, const double &flux_y3,
                          const double &flux_x2y, const double &flux_xy2);
        Euler2D PoissonMR2(const int &inode, const double &R,
                          const double &flux_x, const double &flux_y,
                          const double &flux_x2, const double &flux_y2, const double &flux_xy);
        
        //!> Updates using Spherical means
        Euler2D NodeFluxResidual(const int &inode, const double &dt,
                                 Euler2D &gradx, Euler2D &grady);

        Euler2D EdgeFluxResidual(const int &inode, const double &dt,
                                 Euler2D &gradx, Euler2D &grady);

        Euler2D NodeFluxResidualIsentEuler(const int &inode, const double &dt,
                                 Euler2D &gradx, Euler2D &grady);

        Euler2D EdgeFluxResidualIsentEuler(const int &inode, const double &dt,
                                 Euler2D &gradx, Euler2D &grady);

        //!> Updates using Spherical means for linear acoustics
        Euler2D linAcoNodeFluxResidual(const int &inode, const double &dt);
        
        Euler2D linAcoEdgeFluxResidual(const int &inode, const double &dt);

   

        //!> Edge update functions using Spherical means
        void EdgeMRflux(const int &inode, const double &R,
                        double &flux_x, double &flux_y,
                        double &flux_x3, double &flux_y3,
                        double &flux_x2y, double &flux_xy2);

        void EdgeMR(const double &R, const int &inode,
                    double flux_x, double flux_y,
                    double flux_x3, double flux_y3,
                    double flux_x2y, double flux_xy2,
                    Euler2D &MR,
                    Euler2D &MRdx,
                    Euler2D &MRdy,
                    Euler2D &drMR,
                    Euler2D &nonlinMR,
                    const int &iNonlin,
                    const int &iVort);

        void EdgeMR(const double &R, const int &inode,
                    double flux_x, double flux_y,
                    double flux_x3, double flux_y3,
                    double flux_x2y, double flux_xy2,
                    Euler2D &MRdx,
                    Euler2D &MRdy,
                    Euler2D &MRd2,
                    Euler2D &nonlinMR,
                    const int &iNonlin,
                    const int &iVort);
        
};


/********************************************************
 * Mesh2D -- External subroutines.                   *
 ********************************************************/
extern Euler2D AverageFluxResidual(const Mesh2D &En,
                                   const Mesh2D &Enhalf,
                                   const Mesh2D &En1,
                                   const double &dt);

extern Euler2D FluxResidual(const Mesh2D &En,
                            const Mesh2D &Enhalf,
                            const Mesh2D &En1,
                            const int &iedge);

extern void UpdateEdgeFlux(Mesh2D E1,
                           const int &iedge1,
                           Mesh2D E2,
                           const int &iedge2,
                           const double &R,
                           Mesh2D &E1temp,
                           Mesh2D &E2temp,
                           const int &iNonlin,
                           const int &iVort,
                           double &gradx,
                           double &grady);

//extern void UpdateEdgeFlux(Mesh2D E1,
//                           const int &inode1,
//                           const double &dt,
//                           Mesh2D &E1temp,
//                           const int &iNonlin,
//                           const int &iVort);

extern void UpdateNodeGlobal(int *INode,
                             Mesh2D *Ei,
                             const double &dt, 
                             int *nINode,
                             const int &ninode, 
                             const int &maxelem,
                             Euler2D *QNtemp,
                             const int &iNonlin,
                             const int &iVort);

#endif /* _MESH2D_INCLUDED  */
