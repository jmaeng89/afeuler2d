// Wrapper test file to check passing arrays into and out of c
// 
// 8 May 2015 - Initial creation (Maeng)
//
/* Include required C++ libraries. */

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cmath>

//#ifndef _TEST_INCLUDED
//#include "test.h"
//#endif

using namespace std;

// prototypes
extern "C"
{
    void testfunc_(int *scalarval ); 
    void test1darray_(int *array, int *n);
    void test2darray_(double *array, int *n);
    void test3darray_(double *array, int *n);
    void test4darray_(double *array, int *n);
}

void testfunc_(int *scalarval ) 
{
    int i = *scalarval;

    cout << "hello from c++" << endl;  
    *scalarval = i+1;
    cout << "scalar value is :" << *scalarval << endl;

}

void test1darray_(int *array, int *n) {
    int len = *n;
    for ( int i = 0; i < len; i++ ) {
        array[i] = array[i]+1.0;
    }
}

// test casting fortran continuous arrays into C++ multidimensional arrays
void test2darray_(double *array, int *n) {
    typedef double (*A2d_t)[n[0]];
    A2d_t A2d = (A2d_t) array; // cast to 2d array

    for ( int j = 0; j < n[1]; j++ ) {
        for ( int i = 0; i < n[0]; i++ ) {
            A2d[j][i] = A2d[j][i]+1.0;
        }
    }
}

void test3darray_(double *array, int *n) {
    typedef double (*A3d_t)[n[1]][n[0]];
    A3d_t A3d = (A3d_t) array; // cast to 3d array

    for ( int k = 0; k < n[2]; k++ ) {
        for ( int j = 0; j < n[1]; j++ ) {
            for ( int i = 0; i < n[0]; i++ ) {
                A3d[k][j][i] = A3d[k][j][i]+1.0;
            }
        }
    }
}

void test4darray_(double *array, int *n) {
    typedef double (*A4d_t)[n[2]][n[1]][n[0]];
    A4d_t A4d = (A4d_t) array; // cast to 4d array

    for ( int l = 0; l < n[3]; l++ ) {
        for ( int k = 0; k < n[2]; k++ ) {
            for ( int j = 0; j < n[1]; j++ ) {
                for ( int i = 0; i < n[0]; i++ ) {
                    A4d[l][k][j][i] = A4d[l][k][j][i]+3.0;
                }
            }
        }
    }
}
