/* FunctionSM.cc:  Spherical means of basis functions subroutines
   for 2D Euler Mesh Class. */

/* Include 2D mesh elements header file. */
#ifndef _MESH2D_INCLUDED
#include "Mesh2D.h"
#endif // _MESH2D_INCLUDED

/* ~~~~~~~~~~  MR(f) - Spherical mean of basis functions ~~~~~~~~~~~~~ */
Euler2D Mesh2D::MRf(const int &inode,
                    const double &xi, const double &eta,
                    const double &xi2, const double &eta2, const double &xieta,
                    const double &xi2eta, const double &xieta2) {
        Euler2D MR;
        double *Cj;
        Cj = new double [NP];
        
        Cj[0] = constflux(inode) - 3.0*xi + 2.0*xi2 - 3.0*eta + 4.0*xieta + 2.0*eta2;
        Cj[5] = 4.0*(xi - xi2 - xieta);
        Cj[2] = 2.0*xi2 - xi; 
        Cj[1] = 4.0*xieta; 
        Cj[4] = 2.0*eta2 - eta; 
        Cj[3] = 4.0*(eta - xieta - eta2);
        Cj[6] = 27.0*(xieta - xi2eta - xieta2); 
        
        MR = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                MR += Cj[i]*Q[i];
        delete [] Cj;
        
        return (MR);
};

/* ~~~ MR(df/dx) - Spherical mean of Derivative of basis functions wrt x ~~~ */
Euler2D Mesh2D::MRdfdx(const int &inode,
                       const double &xi, const double &eta,
                       const double &xi2, const double &eta2, const double &xieta) {
        Euler2D MRdx;
        double *Cj;
        Cj = new double [NP];
        double constL = constflux(inode);
        
        Cj[0] = (J11() + J21())*(4.0*xi + 4.0*eta - 3.0*constL);
        Cj[5] = 4.0*J11()*(constL - 2.0*xi - eta) - 4.0*J21()*xi;
        Cj[2] = J11()*(4.0*xi - constL); 
        Cj[1] = 4.0*(J11()*eta + J21()*xi); 
        Cj[4] = J21()*(4.0*eta - constL); 
        Cj[3] = -4.0*J11()*eta + 4.0*J21()*(constL - xi - 2.0*eta);
        Cj[6] = 27.0*J11()*(eta - 2.0*xieta - eta2) + 27.0*J21()*(xi - 2.0*xieta - xi2); 
        
        MRdx = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                MRdx += Cj[i]*Q[i];
        delete [] Cj;
        
        return (MRdx);
};

/* ~~~ MR(df/dy) - Spherical mean of Derivative of basis functions wrt y ~~~ */
Euler2D Mesh2D::MRdfdy(const int &inode, 
                       const double &xi, const double &eta,
                       const double &xi2, const double &eta2, const double &xieta) {
        Euler2D MRdy;
        double *Cj;
        Cj = new double [NP];
        double constL = constflux(inode);
        
        Cj[0] = (J12()+J22())*(4.0*xi + 4.0*eta - 3.0*constL);
        Cj[5] = 4.0*J12()*(constL - 2.0*xi - eta) - 4.0*J22()*xi;
        Cj[2] = J12()*(4.0*xi - constL); 
        Cj[1] = 4.0*(J12()*eta + J22()*xi); 
        Cj[4] = J22()*(4.0*eta - constL); 
        Cj[3] = -4.0*J12()*eta + 4.0*J22()*(constL - xi - 2.0*eta);
        Cj[6] = 27.0*J12()*(eta - 2.0*xieta - eta2) + 27.0*J22()*(xi - 2.0*xieta - xi2); 
        
        MRdy = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                MRdy += Cj[i]*Q[i];
        delete [] Cj;

        return (MRdy);
};

/* ~~~~~~~~~~  dMR(f)/dR - Derivative of Spherical mean wrt R ~~~~~~~~~~~~~ */
Euler2D Mesh2D::dMRdR (const int &inode, 
                       const double &dxi, const double &deta,
                       const double &dxi2, const double &deta2, const double &dxieta,
                       const double &dxi2eta, const double &dxieta2) {
        Euler2D drMR;
        double *Cj;
        Cj = new double [NP];
        
        Cj[0] = ZERO - 3.0*dxi + 2.0*dxi2 - 3.0*deta + 4.0*dxieta + 2.0*deta2;
        Cj[5] = 4.0*(dxi - dxi2 - dxieta);
        Cj[2] = 2.0*dxi2 - dxi; 
        Cj[1] = 4.0*dxieta; 
        Cj[4] = 2.0*deta2 - deta; 
        Cj[3] = 4.0*(deta - dxieta - deta2);
        Cj[6] = 27.0*(dxieta - dxi2eta - dxieta2); 
        
        drMR = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                drMR += Cj[i]*Q[i];
        delete[] Cj;

        return (drMR);
};
        

/* ~~~ MR(df/dxx) - Spherical mean of 2nd Derivative of basis functions wrt (x,x) ~~~ */
Euler2D Mesh2D::rMRdfdxx(const int &inode, const double &R,
                         const double &xi, const double &eta) {
        Euler2D int_rMRdxx;
        double *Cj;
        Cj = new double [NP];
        double constL = constflux(inode);
        double A1 = J11();
        double A3 = J21();
    
        // why HALF*R*R?
        Cj[0] = 4.0*(A1+A3)*(A1+A3)*constL*(HALF*R*R);
        Cj[5] = -8.0*A1*(A1+A3)*constL*(HALF*R*R);
        Cj[2] = 4.0*A1*A1*constL*(HALF*R*R); 
        Cj[1] = 8.0*A1*A3*constL*(HALF*R*R);  
        Cj[4] = 4.0*A3*A3*constL*(HALF*R*R); 
        Cj[3] = -8.0*A3*(A1+A3)*constL*(HALF*R*R); // check -4.0*A1*A3*constL
        Cj[6] = R*R*(27.0*A1*A3*constL - 18.0*A1*(A1+2.0*A3)*eta - 18.0*A3*(2.0*A1+A3)*xi
                      - 9.0*A1*(A1+2.0*A3)*etap(inode)*constL
                      - 9.0*A3*(2.0*A1+A3)*xip(inode)*constL);
        
        int_rMRdxx = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                int_rMRdxx += Cj[i]*Q[i];
        delete [] Cj;
    
        return (int_rMRdxx);
};

/* ~~~ MR(df/dyy) - Spherical mean of 2nd Derivative of basis functions wrt (y,y) ~~~ */
Euler2D Mesh2D::rMRdfdyy(const int &inode, const double &R,
                         const double &xi, const double &eta) {
        Euler2D int_rMRdyy;
        double *Cj;
        Cj = new double [NP];
        double constL = constflux(inode);
        double A1 = J12();
        double A3 = J22();
        
        Cj[0] = 4.0*(A1+A3)*(A1+A3)*constL*(HALF*R*R);
        Cj[5] = -8.0*A1*(A1+A3)*constL*(HALF*R*R);
        Cj[2] = 4.0*A1*A1*constL*(HALF*R*R); 
        Cj[1] = 8.0*A1*A3*constL*(HALF*R*R); 
        Cj[4] = 4.0*A3*A3*constL*(HALF*R*R); 
        Cj[3] = -8.0*A3*(A1+A3)*constL*(HALF*R*R);
        Cj[6] = R*R*(27.0*A1*A3*constL - 18.0*A1*(A1+2.0*A3)*eta - 18.0*A3*(2.0*A1+A3)*xi
                      - 9.0*A1*(A1+2.0*A3)*etap(inode)*constL
                      - 9.0*A3*(2.0*A1+A3)*xip(inode)*constL); 
        
        int_rMRdyy = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                int_rMRdyy += Cj[i]*Q[i];
        delete [] Cj;
        
        return (int_rMRdyy);
};

/* ~~~ MR(df/dxy) - Spherical mean of 2nd Derivative of basis functions wrt (x,y) ~~~ */
Euler2D Mesh2D::rMRdfdxy(const int &inode, const double &R,
                         const double &xi, const double &eta) {
        Euler2D int_rMRdxy;
        double *Cj;
        Cj = new double [NP];
        double constL = constflux(inode);
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        
        Cj[0] = 4.0*(A1+A3)*(A2+A4)*constL*(HALF*R*R);
        Cj[5] = (-8.0*A1*A2 - 4.0*(A1*A4+A2*A3))*constL*(HALF*R*R);
        Cj[2] = 4.0*A1*A2*constL*(HALF*R*R); 
        Cj[1] = 4.0*(A1*A4+A2*A3)*constL*(HALF*R*R); 
        Cj[4] = 4.0*A3*A4*constL*(HALF*R*R); 
        Cj[3] = (-8.0*A3*A4 - 4.0*(A1*A4+A2*A3))*constL*(HALF*R*R);
        Cj[6] = R*R*(13.5*(A1*A4+A2*A3)*constL
                      - 18.0*(A1*A2+A1*A4+A2*A3)*eta - 18.0*(A1*A4+A2*A3+A3*A4)*xi
                      - 9.0*(A1*A2+A1*A4+A2*A3)*etap(inode)*constL
                      - 9.0*(A1*A4+A2*A3+A3*A4)*xip(inode)*constL); 
        
        int_rMRdxy = Cj[0]*Q[0];
        for (int i = 1; i < NP; i++)
                int_rMRdxy += Cj[i]*Q[i];
        delete [] Cj;
        
        return (int_rMRdxy);
};


/* ~~~ rMR{(df/dx)(df/dy)} - Spherical mean of product of Derivatives of 2 basis functions ~~~ */
Euler2D Mesh2D::rMRdfdxdy(const int &inode, const double &R,
                          const double &rxi, const double &reta,
                          const double &rxi2, const double &reta2, const double &rxieta,
                          const double &rxi2eta, const double &rxieta2,
                          const double &rxi3, const double &reta3) {
        double *Cij;
        Cij = new double [NP*NP];
        double rconst = HALF*R*R*constflux(inode);
        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        
        // ij = i*7 + j  (row-ranked)
        // row 0
        Cij[0] = (A1+A3)*(A2+A4)*(16.0*rxi2 + 16.0*reta2 + 32.0*rxieta - 24.0*rxi - 24*reta + 9*rconst);
        Cij[5] = 4.0*(A1+A3)*(-(8*A2+4*A4)*rxi2 - 4*A2*reta2 - (12*A2+4*A4)*rxieta
                              + (10*A2+3*A4)*rxi + 7*A2*reta - 3*A2*rconst);
        Cij[2] = (A1+A3)*A2*(16.0*rxi2 + 16.0*rxieta - 16.0*rxi - 4.0*reta + 3.0*rconst); 
        Cij[1] = 4.0*(A1+A3)*(4*A4*rxi2 + 4*A2*reta2 + 4*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta); 
        Cij[4] = (A1+A3)*A4*(16.0*reta2 + 16.0*rxieta - 16.0*reta - 4.0*rxi + 3.0*rconst); 
        Cij[3] = 4.0*(A1+A3)*(-4*(2*A4+A2)*reta2 - 4*A4*rxi2 - 4*(3*A4+A2)*rxieta
                              + (10*A4+3*A2)*reta + 7*A4*rxi - 3*A4*rconst);
        Cij[6] = 27.0*(A1+A3)*(-4*A4*rxi3 - 4*A2*reta3 - (8*A2+12*A4)*rxi2eta - (12*A2+8*A4)*rxieta2
                               + 7*A4*rxi2 + 7*A2*reta2 + 10*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta);
        // row 1
        Cij[35] = 4.0*(A2+A4)*(-(8*A1+4*A3)*rxi2 - 4*A1*reta2 - 4*(3*A1+A3)*rxieta
                              + (10*A1+3*A3)*rxi + 7*A1*reta - 3*A1*rconst);
        Cij[40] = 16.0*((2*A1+A3)*(2*A2+A4)*rxi2 + A1*A2*reta2 +(4*A1*A2+A1*A4+A3*A2)*(rxieta-rxi)
                       - 2*A1*A2*reta + A1*A2*rconst);
        Cij[37] = 4.0*A2*(-4*(2*A1+A3)*rxi2 - 4*A1*rxieta + (6*A1+A3)*rxi + A1*reta - A1*rconst);
        Cij[36] = 16.0*(-A4*(2*A1+A3)*rxi2 - A1*A2*reta2 - (2*A1*A2+A1*A4+A3*A2)*rxieta + A1*A4*rxi + A1*A2*reta);
        Cij[39] = 4.0*A4*(-4*A1*reta2 - 4*(2*A1+A3)*rxieta + (2*A1+A3)*rxi + 5*A1*reta - A1*rconst);
        Cij[38] = 16.0*(A4*(2*A1+A3)*rxi2 + A1*(A2+2*A4)*reta2 + (2*A1*A2+5*A1*A4+A2*A3+2*A3*A4)*rxieta
                        - A4*(3*A1+A3)*rxi - A1*(A2+3*A4)*reta + A1*A4*rconst);
        Cij[41] = 108.0*(A4*(2*A1+A3)*rxi3 + A1*A2*reta3 + (4*A1*A2+5*A1*A4+2*A2*A3+2*A3*A4)*rxi2eta
                         + (4*A1*A2+2*A1*A4+A2*A3)*rxieta2 - A4*(3*A1+A3)*rxi2 - 2*A1*A2*reta2
                         - (4*A1*A2+3*A1*A4+A2*A3)*rxieta + A1*A4*rxi + A1*A2*reta);
        // row 2
        Cij[14] = A1*(A2+A4)*(16.0*rxi2 + 16.0*rxieta - 16.0*rxi - 4.0*reta + 3.0*rconst);
        Cij[19] = 4.0*A1*(-4*(2*A2+A4)*rxi2 - 4*A2*rxieta + (6*A2+A4)*rxi + A2*reta - A2*rconst);
        Cij[16] = A1*A2*(16.0*rxi2 - 8.0*rxi + 1.0*rconst);
        Cij[15] = 4.0*A1*(4*A4*rxi2 + 4*A2*rxieta - A4*rxi - A2*reta);
        Cij[18] = A1*A4*(16.0*rxieta - 4*rxi - 4*reta + 1*rconst);
        Cij[17] = 4.0*A1*(-4*A4*rxi2 - 4*(A2+2*A4)*rxieta + 5*A4*rxi + (A2+2*A4)*reta - A4*rconst);
        Cij[20] = 27.0*A1*(-4*A4*rxi3 - 8*(A2+A4)*rxi2eta - 4*A2*rxieta2
                           + 5*A4*rxi2 + A2*reta2 + 2*(3*A2+A4)*rxieta - A4*rxi - A2*reta);
        // row 3
        Cij[7] = 4.0*(A2+A4)*(4*A3*rxi2 + 4*A1*reta2 + 4*(A1+A3)*rxieta - 3*A3*rxi - 3*A1*reta);
        Cij[12] = 16.0*(-A3*(2*A2+A4)*rxi2 - A1*A2*reta2 - (2*A1*A2+A1*A4+A3*A2)*rxieta + A2*A3*rxi + A1*A2*reta);
        Cij[9] = 4.0*A2*(4*A3*rxi2 + 4*A1*rxieta - A3*rxi - A1*reta);
        Cij[8] = 16.0*(A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A3*A2)*rxieta);
        Cij[11] = 4.0*A4*(4*A1*reta2 + 4*A3*rxieta - A3*rxi - A1*reta);
        Cij[10] = 16.0*(-A3*A4*rxi2 - A1*(A2+2*A4)*reta2 - (A1*A4+A2*A3+2*A3*A4)*rxieta + A3*A4*rxi + A1*A4*reta);
        Cij[13] = 108*(-A3*A4*rxi3 - A1*A2*reta3 - (A1*A4+2*A2*A3+2*A3*A4)*rxi2eta - (2*A1*A2+2*A1*A4+A2*A3)*rxieta2
                       + A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A2*A3)*rxieta);
        // row 4
        Cij[28] = A3*(A2+A4)*(16.0*reta2 + 16.0*rxieta - 16.0*reta - 4.0*rxi + 3.0*rconst);
        Cij[33] = 4.0*A3*(-4*A2*reta2 - 4*(2*A2+A4)*rxieta + (2*A2+A4)*rxi + 5*A2*reta - A2*rconst);
        Cij[30] = A2*A3*(16*rxieta - 4*rxi - 4*reta + 1*rconst);
        Cij[29] = 4.0*A3*(4*A2*reta2 + 4*A4*rxieta - A4*rxi - A2*reta);
        Cij[32] = A3*A4*(16*reta2 - 8*reta + 1*rconst);
        Cij[31] = 4.0*A3*(-4*(A2+2*A4)*reta2 - 4*A4*rxieta + A4*rxi + (A2+6*A4)*reta - A4*rconst);
        Cij[34] = 27.0*A3*(-4*A2*reta3 - 4*A4*rxi2eta - 8*(A2+A4)*rxieta2
                           + A4*rxi2 + 5*A2*reta2 + 2*(A2+3*A4)*rxieta - A4*rxi - A2*reta);
        // row 5
        Cij[21] = 4.0*(A2+A4)*(-4*A3*rxi2 - 4*(A1+2*A3)*reta2 - 4*(A1+3*A3)*rxieta
                               + 7*A3*rxi + (3*A1+10*A3)*reta - 3*A3*rconst);
        Cij[26] = 16.0*(A3*(2*A2+A4)*rxi2 + A2*(A1+2*A3)*reta2 + (2*A1*A2+A1*A4+5*A2*A3+2*A3*A4)*rxieta
                        - A3*(3*A2+A4)*rxi - A2*(A1+3*A3)*reta + A2*A3*rconst);
        Cij[23] = 4.0*A2*(-4*A3*rxi2 - 4*(A1+2*A3)*rxieta + 5*A3*rxi + (A1+2*A3)*reta - A3*rconst);
        Cij[22] = 16.0*(-A3*A4*rxi2 - A2*(A1+2*A3)*reta2 - (A1*A4 + A2*A3 + 2*A3*A4)*rxieta + A3*A4*rxi + A2*A3*reta);
        Cij[25] = 4*A4*(-4*(A1+2*A3)*reta2 - 4*A3*rxieta + A3*rxi + (A1+6*A3)*reta - A3*rconst);
        Cij[24] = 16.0*(A3*A4*rxi2 + (A1+2*A3)*(A2+2*A4)*reta2 + (A1*A4+A2*A3+4*A3*A4)*rxieta
                        - 2*A3*A4*rxi - (A1*A4+A2*A3+4*A3*A4)*reta + A3*A4*rconst);
        Cij[27] = 108.0*(A3*A4*rxi3 + A2*(A1+2*A3)*reta3 + (A1*A4+2*A2*A3+4*A3*A4)*rxi2eta
                         + (2*A1*A2+2*A1*A4+5*A2*A3+4*A3*A4)*rxieta2 - 2*A3*A4*rxi2 - A2*(A1+3*A3)*reta2
                         - (A1*A4+3*A2*A3+4*A3*A4)*rxieta + A3*A4*rxi + A2*A3*reta);
        // row 6
        Cij[42] = 27.0*(A2+A4)*(-4*A3*rxi3 - 4*A1*reta3 - (8*A1+12*A3)*rxi2eta - (12*A1+8*A3)*rxieta2
                                + 7*A3*rxi2 + 7*A1*reta2 + 10*(A1+A3)*rxieta - 3*A3*rxi - 3*A1*reta);
        Cij[47] = 108.0*(A3*(2*A2+A4)*rxi3 + A1*A2*reta3 + (4*A1*A2+2*A1*A4+5*A3*A2+2*A3*A4)*rxi2eta
                         + (4*A1*A2+2*A3*A2+A1*A4)*rxieta2 - A3*(3*A2+A4)*rxi2 - 2*A1*A2*reta2
                         - (4*A1*A2+A1*A4+3*A3*A2)*rxieta + A3*A2*rxi + A1*A2*reta);
        Cij[44] = 27.0*A2*(-4*A3*rxi3 - 8*(A1+A3)*rxi2eta - 4*A1*rxieta2 + 5*A3*rxi2 + A1*reta2
                           + 2*(3*A1+A3)*rxieta - A3*rxi - A1*reta);
        Cij[43] = 108.0*(-A3*A4*rxi3 - A1*A2*reta3 - (2*A1*A4+2*A3*A4+A3*A2)*rxi2eta
                         - (2*A1*A2+2*A3*A2+A1*A4)*rxieta2 + A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A3*A2)*rxieta);
        Cij[46] = 27.0*A4*(-4*A1*reta3 - 4*A3*rxi2eta - 8*(A1+A3)*rxieta2 + A3*rxi2 + 5*A1*reta2
                           + 2*(A1+3*A3)*rxieta - A3*rxi - A1*reta);
        Cij[45] = 108.0*(A3*A4*rxi3 + A1*(A2+2*A4)*reta3 + (2*A1*A4+A2*A3+4*A3*A4)*rxi2eta
                         + (2*A1*A2+5*A1*A4+2*A2*A3+4*A3*A4)*rxieta2 - 2*A3*A4*rxi2 - A1*(A2+3*A4)*reta2 
                         - (3*A1*A4+A2*A3+4*A3*A4)*rxieta + A3*A4*rxi + A1*A4*reta);
        Cij[48] = ZERO;
        
        Euler2D int_rMRdfdxdy = Euler2D(ZERO,ZERO,ZERO,ZERO);
	    const double C1 = Q[inode].d/Q[inode].c2();
	    const double C2 = ONE/Q[inode].d/Q[inode].c2();
	    const double C3 = (GAMMA-1.0)/Q[inode].d/Q[inode].c2();
	    const double C4 = GAMMA*GAMMA*Q[inode].p/Q[inode].c2();
	    const double C5 = ONE/Q[inode].d;
        for (int i = 0; i < NP; i++) {
                for (int j = 0; j < NP; j++) {

                        int_rMRdfdxdy.d += (2.0*C1*Q[i].u*Q[j].v)*Cij[i*NP+j];
                        int_rMRdfdxdy.u += (C3*Q[i].p*Q[j].v)*Cij[i*NP+j];
                        int_rMRdfdxdy.v += (C3*Q[i].u*Q[j].p)*Cij[i*NP+j];
                        int_rMRdfdxdy.p += (2.0*C4*Q[i].u*Q[j].v)*Cij[i*NP+j];

                }
        }

        delete [] Cij;
        return (int_rMRdfdxdy);
}

/* ~~~ rMR{(df/dx)(df/dx)} - Spherical mean of product of 2 Derivatives of basis functions ~~~ */
Euler2D Mesh2D::rMRdfdx2(const int &inode, const double &R,
                         const double &rxi, const double &reta,
                         const double &rxi2, const double &reta2, const double &rxieta,
                         const double &rxi2eta, const double &rxieta2,
                         const double &rxi3, const double &reta3) {
        double *Cij;
        Cij = new double [NP*NP];
        double rconst = HALF*R*R*constflux(inode);
        double A1 = J11();    double A2 = A1;
        double A3 = J21();    double A4 = A3;

        // ij = i*7 + j  (row-ranked)
        // row 0
        Cij[0] = (A1+A3)*(A2+A4)*(16.0*rxi2 + 16.0*reta2 + 32.0*rxieta - 24.0*rxi - 24*reta + 9*rconst);
        Cij[5] = 4.0*(A1+A3)*(-(8*A2+4*A4)*rxi2 - 4*A2*reta2 - (12*A2+4*A4)*rxieta
                              + (10*A2+3*A4)*rxi + 7*A2*reta - 3*A2*rconst);
        Cij[2] = (A1+A3)*A2*(16.0*rxi2 + 16.0*rxieta - 16.0*rxi - 4.0*reta + 3.0*rconst); 
        Cij[1] = 4.0*(A1+A3)*(4*A4*rxi2 + 4*A2*reta2 + 4*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta); 
        Cij[4] = (A1+A3)*A4*(16.0*reta2 + 16.0*rxieta - 16.0*reta - 4.0*rxi + 3.0*rconst); 
        Cij[3] = 4.0*(A1+A3)*(-4*(2*A4+A2)*reta2 - 4*A4*rxi2 - 4*(3*A4+A2)*rxieta
                              + (10*A4+3*A2)*reta + 7*A4*rxi - 3*A4*rconst);
        Cij[6] = 27.0*(A1+A3)*(-4*A4*rxi3 - 4*A2*reta3 - (8*A2+12*A4)*rxi2eta - (12*A2+8*A4)*rxieta2
                               + 7*A4*rxi2 + 7*A2*reta2 + 10*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta);
        // row 1
        Cij[35] = Cij[5];
        Cij[40] = 16.0*((2*A1+A3)*(2*A2+A4)*rxi2 + A1*A2*reta2 +(4*A1*A2+A1*A4+A3*A2)*(rxieta-rxi)
                       - 2*A1*A2*reta + A1*A2*rconst);
        Cij[37] = 4.0*A2*(-4*(2*A1+A3)*rxi2 - 4*A1*rxieta + (6*A1+A3)*rxi + A1*reta - A1*rconst);
        Cij[36] = 16.0*(-A4*(2*A1+A3)*rxi2 - A1*A2*reta2 - (2*A1*A2+A1*A4+A3*A2)*rxieta + A1*A4*rxi + A1*A2*reta);
        Cij[39] = 4.0*A4*(-4*A1*reta2 - 4*(2*A1+A3)*rxieta + (2*A1+A3)*rxi + 5*A1*reta - A1*rconst);
        Cij[38] = 16.0*(A4*(2*A1+A3)*rxi2 + A1*(A2+2*A4)*reta2 + (2*A1*A2+5*A1*A4+A2*A3+2*A3*A4)*rxieta
                        - A4*(3*A1+A3)*rxi - A1*(A2+3*A4)*reta + A1*A4*rconst);
        Cij[41] = 108.0*(A4*(2*A1+A3)*rxi3 + A1*A2*reta3 + (4*A1*A2+5*A1*A4+2*A2*A3+2*A3*A4)*rxi2eta
                         + (4*A1*A2+2*A1*A4+A2*A3)*rxieta2 - A4*(3*A1+A3)*rxi2 - 2*A1*A2*reta2
                         - (4*A1*A2+3*A1*A4+A2*A3)*rxieta + A1*A4*rxi + A1*A2*reta);
        // row 2
        Cij[14] = Cij[2];
        Cij[19] = Cij[37];
        Cij[16] = A1*A2*(16.0*rxi2 - 8.0*rxi + 1.0*rconst);
        Cij[15] = 4.0*A1*(4*A4*rxi2 + 4*A2*rxieta - A4*rxi - A2*reta);
        Cij[18] = A1*A4*(16.0*rxieta - 4*rxi - 4*reta + 1*rconst);
        Cij[17] = 4.0*A1*(-4*A4*rxi2 - 4*(A2+2*A4)*rxieta + 5*A4*rxi + (A2+2*A4)*reta - A4*rconst);
        Cij[20] = 27.0*A1*(-4*A4*rxi3 - 8*(A2+A4)*rxi2eta - 4*A2*rxieta2
                           + 5*A4*rxi2 + A2*reta2 + 2*(3*A2+A4)*rxieta - A4*rxi - A2*reta);
        // row 3
        Cij[7] = Cij[1];
        Cij[12] = Cij[36];
        Cij[9] = Cij[15];
        Cij[8] = 16.0*(A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A3*A2)*rxieta);
        Cij[11] = 4.0*A4*(4*A1*reta2 + 4*A3*rxieta - A3*rxi - A1*reta);
        Cij[10] = 16.0*(-A3*A4*rxi2 - A1*(A2+2*A4)*reta2 - (A1*A4+A2*A3+2*A3*A4)*rxieta + A3*A4*rxi + A1*A4*reta);
        Cij[13] = 108*(-A3*A4*rxi3 - A1*A2*reta3 - (A1*A4+2*A2*A3+2*A3*A4)*rxi2eta - (2*A1*A2+2*A1*A4+A2*A3)*rxieta2
                       + A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A2*A3)*rxieta);
        // row 4
        Cij[28] = Cij[4];
        Cij[33] = Cij[39];
        Cij[30] = Cij[18];
        Cij[29] = Cij[11];
        Cij[32] = A3*A4*(16*reta2 - 8*reta + 1*rconst);
        Cij[31] = 4.0*A3*(-4*(A2+2*A4)*reta2 - 4*A4*rxieta + A4*rxi + (A2+6*A4)*reta - A4*rconst);
        Cij[34] = 27.0*A3*(-4*A2*reta3 - 4*A4*rxi2eta - 8*(A2+A4)*rxieta2
                           + A4*rxi2 + 5*A2*reta2 + 2*(A2+3*A4)*rxieta - A4*rxi - A2*reta);
        // row 5
        Cij[21] = Cij[3];
        Cij[26] = Cij[38];
        Cij[23] = Cij[17];
        Cij[22] = Cij[10];
        Cij[25] = Cij[31];
        Cij[24] = 16.0*(A3*A4*rxi2 + (A1+2*A3)*(A2+2*A4)*reta2 + (A1*A4+A2*A3+4*A3*A4)*rxieta
                        - 2*A3*A4*rxi - (A1*A4+A2*A3+4*A3*A4)*reta + A3*A4*rconst);
        Cij[27] = 108.0*(A3*A4*rxi3 + A2*(A1+2*A3)*reta3 + (A1*A4+2*A2*A3+4*A3*A4)*rxi2eta
                         + (2*A1*A2+2*A1*A4+5*A2*A3+4*A3*A4)*rxieta2 - 2*A3*A4*rxi2 - A2*(A1+3*A3)*reta2
                         - (A1*A4+3*A2*A3+4*A3*A4)*rxieta + A3*A4*rxi + A2*A3*reta);
        // row 6
        Cij[42] = Cij[6];
        Cij[47] = Cij[41];
        Cij[44] = Cij[20];
        Cij[43] = Cij[13];
        Cij[46] = Cij[34];
        Cij[45] = Cij[27];
        Cij[48] = ZERO;
        
        Euler2D int_rMRdfdx2 = Euler2D(ZERO,ZERO,ZERO,ZERO);
	    const double C1 = Q[inode].d/Q[inode].c2();
	    const double C2 = ONE/Q[inode].d/Q[inode].c2();
	    const double C3 = (GAMMA-1.0)/Q[inode].d/Q[inode].c2();
	    const double C4 = GAMMA*GAMMA*Q[inode].p/Q[inode].c2();
	    const double C5 = ONE/Q[inode].d;
        for (int i = 0; i < NP; i++) {
            for (int j = 0; j < NP; j++) {
                    
	            int_rMRdfdx2.d += (C1*Q[i].u*Q[j].u - C2*Q[i].p*Q[j].d)*Cij[i*NP+j];  
                int_rMRdfdx2.u += (C3*Q[i].p*Q[j].u)*Cij[i*NP+j];
                int_rMRdfdx2.p += (C4*Q[i].u*Q[j].u - C5*Q[i].p*Q[j].d)*Cij[i*NP+j];

            }
        }
        assert (Q[inode].d != ZERO);
        assert (Q[inode].p != ZERO);

    delete [] Cij;
    return (int_rMRdfdx2);
};

/* ~~~ rMR{(df/dy)(df/dy)} - Spherical mean of 2 Derivatives of basis functions ~~~ */
Euler2D Mesh2D::rMRdfdy2(const int &inode, const double &R,
                         const double &rxi, const double &reta,
                         const double &rxi2, const double &reta2, const double &rxieta,
                         const double &rxi2eta, const double &rxieta2,
                         const double &rxi3, const double &reta3) {
        double *Cij;
        Cij = new double [NP*NP];
        double rconst = HALF*R*R*constflux(inode);
        double A2 = J12();    double A1 = A2;
        double A4 = J22();    double A3 = A4;
        
        // ij = i*7 + j  (row-ranked)
        // row 0
        Cij[0] = (A1+A3)*(A2+A4)*(16.0*rxi2 + 16.0*reta2 + 32.0*rxieta - 24.0*rxi - 24*reta + 9*rconst);
        Cij[5] = 4.0*(A1+A3)*(-(8*A2+4*A4)*rxi2 - 4*A2*reta2 - (12*A2+4*A4)*rxieta
                              + (10*A2+3*A4)*rxi + 7*A2*reta - 3*A2*rconst);
        Cij[2] = (A1+A3)*A2*(16.0*rxi2 + 16.0*rxieta - 16.0*rxi - 4.0*reta + 3.0*rconst); 
        Cij[1] = 4.0*(A1+A3)*(4*A4*rxi2 + 4*A2*reta2 + 4*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta); 
        Cij[4] = (A1+A3)*A4*(16.0*reta2 + 16.0*rxieta - 16.0*reta - 4.0*rxi + 3.0*rconst); 
        Cij[3] = 4.0*(A1+A3)*(-4*(2*A4+A2)*reta2 - 4*A4*rxi2 - 4*(3*A4+A2)*rxieta
                              + (10*A4+3*A2)*reta + 7*A4*rxi - 3*A4*rconst);
        Cij[6] = 27.0*(A1+A3)*(-4*A4*rxi3 - 4*A2*reta3 - (8*A2+12*A4)*rxi2eta - (12*A2+8*A4)*rxieta2
                               + 7*A4*rxi2 + 7*A2*reta2 + 10*(A2+A4)*rxieta - 3*A4*rxi - 3*A2*reta);
        // row 1
        Cij[35] = Cij[5];
        Cij[40] = 16.0*((2*A1+A3)*(2*A2+A4)*rxi2 + A1*A2*reta2 +(4*A1*A2+A1*A4+A3*A2)*(rxieta-rxi)
                       - 2*A1*A2*reta + A1*A2*rconst);
        Cij[37] = 4.0*A2*(-4*(2*A1+A3)*rxi2 - 4*A1*rxieta + (6*A1+A3)*rxi + A1*reta - A1*rconst);
        Cij[36] = 16.0*(-A4*(2*A1+A3)*rxi2 - A1*A2*reta2 - (2*A1*A2+A1*A4+A3*A2)*rxieta + A1*A4*rxi + A1*A2*reta);
        Cij[39] = 4.0*A4*(-4*A1*reta2 - 4*(2*A1+A3)*rxieta + (2*A1+A3)*rxi + 5*A1*reta - A1*rconst);
        Cij[38] = 16.0*(A4*(2*A1+A3)*rxi2 + A1*(A2+2*A4)*reta2 + (2*A1*A2+5*A1*A4+A2*A3+2*A3*A4)*rxieta
                        - A4*(3*A1+A3)*rxi - A1*(A2+3*A4)*reta + A1*A4*rconst);
        Cij[41] = 108.0*(A4*(2*A1+A3)*rxi3 + A1*A2*reta3 + (4*A1*A2+5*A1*A4+2*A2*A3+2*A3*A4)*rxi2eta
                         + (4*A1*A2+2*A1*A4+A2*A3)*rxieta2 - A4*(3*A1+A3)*rxi2 - 2*A1*A2*reta2
                         - (4*A1*A2+3*A1*A4+A2*A3)*rxieta + A1*A4*rxi + A1*A2*reta);
        // row 2
        Cij[14] = Cij[2];
        Cij[19] = Cij[37];
        Cij[16] = A1*A2*(16.0*rxi2 - 8.0*rxi + 1.0*rconst);
        Cij[15] = 4.0*A1*(4*A4*rxi2 + 4*A2*rxieta - A4*rxi - A2*reta);
        Cij[18] = A1*A4*(16.0*rxieta - 4*rxi - 4*reta + 1*rconst);
        Cij[17] = 4.0*A1*(-4*A4*rxi2 - 4*(A2+2*A4)*rxieta + 5*A4*rxi + (A2+2*A4)*reta - A4*rconst);
        Cij[20] = 27.0*A1*(-4*A4*rxi3 - 8*(A2+A4)*rxi2eta - 4*A2*rxieta2
                           + 5*A4*rxi2 + A2*reta2 + 2*(3*A2+A4)*rxieta - A4*rxi - A2*reta);
        // row 3
        Cij[7] = Cij[1];
        Cij[12] = Cij[36];
        Cij[9] = Cij[15];
        Cij[8] = 16.0*(A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A3*A2)*rxieta);
        Cij[11] = 4.0*A4*(4*A1*reta2 + 4*A3*rxieta - A3*rxi - A1*reta);
        Cij[10] = 16.0*(-A3*A4*rxi2 - A1*(A2+2*A4)*reta2 - (A1*A4+A2*A3+2*A3*A4)*rxieta + A3*A4*rxi + A1*A4*reta);
        Cij[13] = 108*(-A3*A4*rxi3 - A1*A2*reta3 - (A1*A4+2*A2*A3+2*A3*A4)*rxi2eta - (2*A1*A2+2*A1*A4+A2*A3)*rxieta2
                       + A3*A4*rxi2 + A1*A2*reta2 + (A1*A4+A2*A3)*rxieta);
        // row 4
        Cij[28] = Cij[4];
        Cij[33] = Cij[39];
        Cij[30] = Cij[18];
        Cij[29] = Cij[11];
        Cij[32] = A3*A4*(16*reta2 - 8*reta + 1*rconst);
        Cij[31] = 4.0*A3*(-4*(A2+2*A4)*reta2 - 4*A4*rxieta + A4*rxi + (A2+6*A4)*reta - A4*rconst);
        Cij[34] = 27.0*A3*(-4*A2*reta3 - 4*A4*rxi2eta - 8*(A2+A4)*rxieta2
                           + A4*rxi2 + 5*A2*reta2 + 2*(A2+3*A4)*rxieta - A4*rxi - A2*reta);
        // row 5
        Cij[21] = Cij[3];
        Cij[26] = Cij[38];
        Cij[23] = Cij[17];
        Cij[22] = Cij[10];
        Cij[25] = Cij[31];
        Cij[24] = 16.0*(A3*A4*rxi2 + (A1+2*A3)*(A2+2*A4)*reta2 + (A1*A4+A2*A3+4*A3*A4)*rxieta
                        - 2*A3*A4*rxi - (A1*A4+A2*A3+4*A3*A4)*reta + A3*A4*rconst);
        Cij[27] = 108.0*(A3*A4*rxi3 + A2*(A1+2*A3)*reta3 + (A1*A4+2*A2*A3+4*A3*A4)*rxi2eta
                         + (2*A1*A2+2*A1*A4+5*A2*A3+4*A3*A4)*rxieta2 - 2*A3*A4*rxi2 - A2*(A1+3*A3)*reta2
                         - (A1*A4+3*A2*A3+4*A3*A4)*rxieta + A3*A4*rxi + A2*A3*reta);
        // row 6
        Cij[42] = Cij[6];
        Cij[47] = Cij[41];
        Cij[44] = Cij[20];
        Cij[43] = Cij[13];
        Cij[46] = Cij[34];
        Cij[45] = Cij[27];
        Cij[48] = ZERO;

        Euler2D int_rMRdfdy2 = Euler2D(ZERO,ZERO,ZERO,ZERO);
	    const double C1 = Q[inode].d/Q[inode].c2();
	    const double C2 = ONE/Q[inode].d/Q[inode].c2();
	    const double C3 = (GAMMA-1.0)/Q[inode].d/Q[inode].c2();
	    const double C4 = GAMMA*GAMMA*Q[inode].p/Q[inode].c2();
	    const double C5 = ONE/Q[inode].d;
        for (int i = 0; i < NP; i++) {
                for (int j = 0; j < NP; j++) {
                        
		        int_rMRdfdy2.d += (C1*Q[i].v*Q[j].v - C2*Q[i].p*Q[j].d)*Cij[i*NP+j]; 
                int_rMRdfdy2.v += (C3*Q[i].p*Q[j].v)*Cij[i*NP+j]; 
                int_rMRdfdy2.p += (C4*Q[i].v*Q[j].v - C5*Q[i].p*Q[j].d)*Cij[i*NP+j];

                }
        }
        assert (Q[inode].d != ZERO);
        assert (Q[inode].p != ZERO);
        
        delete [] Cij;
        return (int_rMRdfdy2);
};


/* ~~~ VORTICITY TERMS ~~~ */
void Mesh2D::VorticityMR(const int &inode,
                         const double &R,
                         const int &xi,
                         const int &eta,
                         Euler2D &Flux) {
        Euler2D int_rMRdxx, int_rMRdxy, int_rMRdyy;
        int_rMRdxx = rMRdfdxx(inode, R, xi, eta);
        int_rMRdxy = rMRdfdxy(inode, R, xi, eta);
        int_rMRdyy = rMRdfdyy(inode, R, xi, eta);
        
        Flux.u += (int_rMRdxy.v - int_rMRdyy.u);
        Flux.v += (int_rMRdxy.u - int_rMRdxx.v);
        
};

// Nonlinearity terms
Euler2D Mesh2D::NonlinearMR(const int &inode, const double &R,
                            const double &xi, const double &eta, 
                            const double &rxi, const double &reta,
                            const double &rxi2, const double &reta2, const double &rxieta,
                            const double &rxi2eta, const double &rxieta2,
                            const double &rxi3, const double &reta3) {
        
        Euler2D int_rMRdfdxx, int_rMRdfdyy, int_rMRdfdxy;
        int_rMRdfdxx = rMRdfdx2(inode, R, rxi, reta, rxi2, reta2, rxieta,
                                rxi2eta, rxieta2, rxi3, reta3);
        int_rMRdfdyy = rMRdfdy2(inode, R, rxi, reta, rxi2, reta2, rxieta,
                                rxi2eta, rxieta2, rxi3, reta3);
        int_rMRdfdxy = rMRdfdxdy(inode, R, rxi, reta, rxi2, reta2, rxieta,
                                rxi2eta, rxieta2, rxi3, reta3);
        
        Euler2D nonlinMR = int_rMRdfdxx + int_rMRdfdyy + int_rMRdfdxy; 
        // assert (Q[inode].p != ZERO);
        
        return ( nonlinMR );
};

// Nonlinearity terms
Euler2D Mesh2D::PoissonMR2(const int &inode, 
                           const double &R,
                           const double &flux_x,
                           const double &flux_y,
                           const double &flux_x2,
                           const double &flux_y2,
                           const double &flux_xy) {

        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;

        Euler2D int_rMRddf;
        int_rMRddf = 2.0*(dQdx(inode)*flux_x + dQdy(inode)*flux_y)
                + 1.5*(dQdxx(inode)*x2 + dQdyy(inode)*y2 + 2.0*dQdxy(inode)*flux_xy);
        
        return ( int_rMRddf );
};
Euler2D Mesh2D::PoissonMR(const int &inode, const double &R,
                          const double &flux_x,
                          const double &flux_y,
                          const double &flux_x2,
                          const double &flux_y2,
                          const double &flux_xy,
                          const double &flux_x3,
                          const double &flux_y3,
                          const double &flux_x2y,
                          const double &flux_xy2) {

        double A1 = J11();    double A2 = J12();
        double A3 = J21();    double A4 = J22();
        double x2, y2;
        // Add Lagrangian approximation
        double constL = constflux(inode);
        x2 = R*R/3.0*constL + flux_x2;
        y2 = R*R/3.0*constL + flux_y2;

        double xi2eta = (A1*A1*A3*flux_x3 + A2*A2*A4*flux_y3 + (A1*A1*A4 + 2.0*A1*A2*A3)*flux_x2y
                         + (A2*A2*A3 + 2.0*A1*A2*A4)*flux_xy2);
        double xieta2 = (A1*A3*A3*flux_x3 + A2*A4*A4*flux_y3 + (A3*A3*A2 + 2.0*A1*A3*A4)*flux_x2y
                         + (A1*A4*A4 + 2.0*A2*A3*A4)*flux_xy2);

        Euler2D int_rMRddf;
        int_rMRddf = 2.0*(dQdx(inode)*flux_x + dQdy(inode)*flux_y)
                + 1.5*(dQdxx(inode)*x2 + dQdyy(inode)*y2 + 2.0*dQdxy(inode)*flux_xy)
                - 108.0*Q[6]*(xi2eta + xieta2);
       
        
        return ( int_rMRddf );
};
