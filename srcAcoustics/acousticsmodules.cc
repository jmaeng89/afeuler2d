// Wrapper file for acoustics modules for AF
// 
// 5 May 2016 - Initial creation (Maeng)
// 13 June 2016 - Implement linear acoustic operators   
//                to be used for two step Lax-Wendroff scheme (Maeng)
//
/* Include required C++ libraries. */
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <fstream>
#include <sstream>

using namespace std;

// Include header files.
#include "Mesh2D.h"

// prototypes
extern "C" 
{

    // SECOND-ORDER ACOUSTICS UPDATE WITH NONLINEAR INTERACTION CORRECTION TERMS INCLUDED
    void acsignalupdate_( int *nEqns_, 
                          int *nDim_,
                          int *iCell_, 
                          double *dtIn_,   
                          double *reconData_, // local reconstruction data
                          double *nodeSignal_,// local cell node data signal
                          double *edgeSignal_,// local cell edge data signal
                          double *nodeCoord_  // local cell mesh node coordinate
                        );  

    void isentropicacsignalupdate_( int *nEqns_, 
                                    int *nDim_,
                                    int *iCell_, 
                                    double *dtIn_,   
                                    double *reconData_, // local reconstruction data
                                    double *nodeSignal_,// local cell node data signal
                                    double *edgeSignal_,// local cell edge data signal
                                    double *nodeCoord_  // local cell mesh node coordinate
                                  );

    // SECOND-ORDER ACOUSTICS OPERATOR RETURNS SIGNAL VALUES without interaction terms
    void acsignalvalue_( int *nEqns_, 
                         int *nDim_,
                         int *iCell_, 
                         double *dtIn_,   
                         double *reconData_, // local reconstruction data
                         double *nodeSignal_,// local cell node data signal
                         double *edgeSignal_,// local cell edge data signal
                         double *nodeCoord_  // local cell mesh node coordinate
                       );  

    void isentropicacsignalvalue_( int *nEqns_, 
                                   int *nDim_,
                                   int *iCell_, 
                                   double *dtIn_,   
                                   double *reconData_, // local reconstruction data
                                   double *nodeSignal_,// local cell node data signal
                                   double *edgeSignal_,// local cell edge data signal
                                   double *nodeCoord_  // local cell mesh node coordinate
                                 );
   
}

// second-order acoustic operator for Euler equations with corrections
// NOTE: NodeFluxResidual and EdgeFluxResidual in ResSM.cc are constructed based
// only on the acoustic terms of Euler Equations with pressure corrected characteristic
// origins.
void acsignalupdate_( int *nEqns_, 
                      int *nDim_,
                      int *iCell_, 
                      double *dtIn_,   
                      double *reconData_, // local reconstruction data
                      double *nodeSignal_,// local cell node data signal
                      double *edgeSignal_,// local cell edge data signal
                      double *nodeCoord_  // local cell mesh node coordinate
                    )       
{

    // set variables consistent with C++ code 
    int nDim = *nDim_;           // number of dimensions
    int nEqns = *nEqns_;         // number of equations 
    int iCell = *iCell_;         // current cell index
    double dt = *dtIn_;          // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[nEqns];
    dA2d_t1 reconData = (dA2d_t1) reconData_;    
    dA2d_t1 nodeSignal = (dA2d_t1) nodeSignal_;   
    dA2d_t1 edgeSignal = (dA2d_t1) edgeSignal_;  
    typedef double (*dA2d_t2)[nDim];
    dA2d_t2 nodeCoord = (dA2d_t2) nodeCoord_;
    
    int iNode, iEdge, lNode, lEdge;
    // Initialize mesh elements
    Mesh2D Elem;
    for ( iNode = 0; iNode < 3; iNode++ ) {
        Elem.nx[iNode] = nodeCoord[iNode][0]; 
        Elem.ny[iNode] = nodeCoord[iNode][1]; 
        Elem.nid[iNode] = 0; // global node number
    }
    Elem.InitAngle();

    // copy reconstruction data
    Euler2D *Qrecon;
    Qrecon = new Euler2D [NP];
    double dprime, uprime, vprime, pprime;
    for ( iNode = 0; iNode < 3; iNode++){
        lNode = 2*iNode; // local node index
        dprime = reconData[lNode][0];
        uprime = reconData[lNode][1];
        vprime = reconData[lNode][2];
        pprime = reconData[lNode][3];
        Qrecon[lNode] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lNode].setgas();
    }
    for ( iEdge = 0; iEdge < 3; iEdge++){
        lEdge = 2*iEdge+1; // local edge index
        dprime = reconData[lEdge][0];
        uprime = reconData[lEdge][1];
        vprime = reconData[lEdge][2];
        pprime = reconData[lEdge][3];
        Qrecon[lEdge] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lEdge].setgas();
    }
    // bubble function
    dprime = reconData[6][0];
    uprime = reconData[6][1];
    vprime = reconData[6][2];
    pprime = reconData[6][3];
    Qrecon[6] = Euler2D(dprime,uprime,vprime,pprime);
    Qrecon[6].setgas();
    for ( int i = 0; i < NP; i++ ) {
        Elem.Q[i] = Qrecon[i]; 
        Elem.Q[i].setgas();
    }
    // set reconstruction average
    Elem.SetAverage();
    
    // Time related solution variables
    Euler2D *Qsignal;             // node and edge signals 
    Qsignal = new Euler2D [NP];
    for ( int i = 0; i < NP; i++ ) {
        Qsignal[i].Zero();
    }

    // Cell based acoustics updates
    Euler2D gradx, grady;
    // update 3 nodes in one cell
    for ( iNode = 0; iNode < 3; iNode++ ){
        lNode = 2*iNode; // local node index

        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lNode] += Elem.NodeFluxResidual(lNode, dt, gradx, grady);

        // remaining second-order terms 
        // In case of advection operator --> acoutics operator 
        Qsignal[lNode].d -= HALF*dt*dt*(Qrecon[lNode].d*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 
        Qsignal[lNode].u -= HALF*dt*dt*((gradx.p*gradx.u+grady.p*gradx.v)/Qrecon[lNode].d); 
        Qsignal[lNode].v -= HALF*dt*dt*((gradx.p*grady.u+grady.p*grady.v)/Qrecon[lNode].d); 
        Qsignal[lNode].p -= HALF*dt*dt*(GAMMA*Qrecon[lNode].p*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 

        /* In case of acoustics operator --> advection operator. Moved to advection
        Qsignal[lNode].d += HALF*dt*dt*(Qrecon[lNode].d*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 
        Qsignal[lNode].u += HALF*dt*dt*((gradx.p*gradx.u+grady.p*gradx.v)/Qrecon[lNode].d); 
        Qsignal[lNode].v += HALF*dt*dt*((gradx.p*grady.u+grady.p*grady.v)/Qrecon[lNode].d); 
        Qsignal[lNode].p += HALF*dt*dt*(GAMMA*Qrecon[lNode].p*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 
        */

    }
    // update 3 edges in one cell
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        
        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lEdge] += Elem.EdgeFluxResidual(lEdge, dt, gradx, grady);

        // remaining second-order terms 
        // In case of advection operator --> acoutics operator 
        Qsignal[lEdge].d -= HALF*dt*dt*(Qrecon[lEdge].d*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 
        Qsignal[lEdge].u -= HALF*dt*dt*((gradx.p*gradx.u+grady.p*gradx.v)/Qrecon[lEdge].d); 
        Qsignal[lEdge].v -= HALF*dt*dt*((gradx.p*grady.u+grady.p*grady.v)/Qrecon[lEdge].d); 
        Qsignal[lEdge].p -= HALF*dt*dt*(GAMMA*Qrecon[lEdge].p*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 

        /* In case of acoustics operator --> advection operator. Moved to advection
        Qsignal[lEdge].d += HALF*dt*dt*(Qrecon[lEdge].d*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v)); 
        Qsignal[lEdge].u += HALF*dt*dt*((gradx.p*gradx.u+grady.p*gradx.v)/Qrecon[lEdge].d); 
        Qsignal[lEdge].v += HALF*dt*dt*((gradx.p*grady.u+grady.p*grady.v)/Qrecon[lEdge].d); 
        Qsignal[lEdge].p += HALF*dt*dt*(GAMMA*Qrecon[lEdge].p*(gradx.u*gradx.u+2.0*grady.u*gradx.v+grady.v*grady.v));  
        */

    }
    
    // return signals 
    for ( iNode = 0; iNode < 3; iNode++ ) {
        lNode = 2*iNode; // local node index
        nodeSignal[iNode][0] = Qsignal[lNode].d; 
        nodeSignal[iNode][1] = Qsignal[lNode].u; 
        nodeSignal[iNode][2] = Qsignal[lNode].v; 
        nodeSignal[iNode][3] = Qsignal[lNode].p; 
    }
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        edgeSignal[iEdge][0] = Qsignal[lEdge].d; 
        edgeSignal[iEdge][1] = Qsignal[lEdge].u; 
        edgeSignal[iEdge][2] = Qsignal[lEdge].v; 
        edgeSignal[iEdge][3] = Qsignal[lEdge].p; 
    }

    // clean up the memory
    delete[] Qrecon;
    delete[] Qsignal;

}



// second-order nonlinear acoustic operator for Euler equations!
void acsignalvalue_( int *nEqns_, 
                     int *nDim_,
                     int *iCell_, 
                     double *dtIn_,   
                     double *reconData_, // local reconstruction data
                     double *nodeSignal_,// local cell node data signal
                     double *edgeSignal_,// local cell edge data signal
                     double *nodeCoord_  // local cell mesh node coordinate
                   )       
{

    // set variables consistent with C++ code 
    int nDim = *nDim_;           // number of dimensions
    int nEqns = *nEqns_;         // number of equations 
    int iCell = *iCell_;         // current cell index
    double dt = *dtIn_;          // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[nEqns];
    dA2d_t1 reconData = (dA2d_t1) reconData_;    
    dA2d_t1 nodeSignal = (dA2d_t1) nodeSignal_;   
    dA2d_t1 edgeSignal = (dA2d_t1) edgeSignal_;  
    typedef double (*dA2d_t2)[nDim];
    dA2d_t2 nodeCoord = (dA2d_t2) nodeCoord_;
    
    int iNode, iEdge, lNode, lEdge;
    // Initialize mesh elements
    Mesh2D Elem;
    for ( iNode = 0; iNode < 3; iNode++ ) {
        Elem.nx[iNode] = nodeCoord[iNode][0]; 
        Elem.ny[iNode] = nodeCoord[iNode][1]; 
        Elem.nid[iNode] = 0; // global node number
    }
    Elem.InitAngle();

    // copy reconstruction data
    Euler2D *Qrecon;
    Qrecon = new Euler2D [NP];
    double dprime, uprime, vprime, pprime;
    for ( iNode = 0; iNode < 3; iNode++){
        lNode = 2*iNode; // local node index
        dprime = reconData[lNode][0];
        uprime = reconData[lNode][1];
        vprime = reconData[lNode][2];
        pprime = reconData[lNode][3];
        Qrecon[lNode] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lNode].setgas();
    }
    for ( iEdge = 0; iEdge < 3; iEdge++){
        lEdge = 2*iEdge+1; // local edge index
        dprime = reconData[lEdge][0];
        uprime = reconData[lEdge][1];
        vprime = reconData[lEdge][2];
        pprime = reconData[lEdge][3];
        Qrecon[lEdge] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lEdge].setgas();
    }
    // bubble function
    dprime = reconData[6][0];
    uprime = reconData[6][1];
    vprime = reconData[6][2];
    pprime = reconData[6][3];
    Qrecon[6] = Euler2D(dprime,uprime,vprime,pprime);
    Qrecon[6].setgas();
    for ( int i = 0; i < NP; i++ ) {
        Elem.Q[i] = Qrecon[i]; 
        Elem.Q[i].setgas();
    }
    // set reconstruction average
    Elem.SetAverage();
    
    // Time related solution variables
    Euler2D *Qsignal;             // node and edge signals 
    Qsignal = new Euler2D [NP];
    for ( int i = 0; i < NP; i++ ) {
        Qsignal[i].Zero();
    }

    // Cell based acoustics updates
    Euler2D gradx, grady;
    // update 3 nodes in one cell
    for ( iNode = 0; iNode < 3; iNode++ ){
        lNode = 2*iNode; // local node index

        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lNode] += Elem.NodeFluxResidual(lNode, dt,
                                             gradx, grady);

    }
    // update 3 edges in one cell
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        
        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lEdge] += Elem.EdgeFluxResidual(lEdge, dt, 
                                             gradx, grady);

    }
    
    // return signals 
    for ( iNode = 0; iNode < 3; iNode++ ) {
        lNode = 2*iNode; // local node index
        nodeSignal[iNode][0] = Qsignal[lNode].d; 
        nodeSignal[iNode][1] = Qsignal[lNode].u; 
        nodeSignal[iNode][2] = Qsignal[lNode].v; 
        nodeSignal[iNode][3] = Qsignal[lNode].p; 
    }
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        edgeSignal[iEdge][0] = Qsignal[lEdge].d; 
        edgeSignal[iEdge][1] = Qsignal[lEdge].u; 
        edgeSignal[iEdge][2] = Qsignal[lEdge].v; 
        edgeSignal[iEdge][3] = Qsignal[lEdge].p; 
    }

    // clean up the memory
    delete[] Qrecon;
    delete[] Qsignal;

}

// second-order acoustic operator for isentropic Euler equations with corrections
void isentropicacsignalupdate_( int *nEqns_, 
                                int *nDim_,
                                int *iCell_, 
                                double *dtIn_,   
                                double *reconData_, // local reconstruction data
                                double *nodeSignal_,// local cell node data signal
                                double *edgeSignal_,// local cell edge data signal
                                double *nodeCoord_  // local cell mesh node coordinate
                              )       
{

    // set variables consistent with C++ code 
    int nDim = *nDim_;           // number of dimensions
    int nEqns = *nEqns_;         // number of equations 
    int iCell = *iCell_;         // current cell index
    double dt = *dtIn_;          // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[nEqns];
    dA2d_t1 reconData = (dA2d_t1) reconData_;    
    dA2d_t1 nodeSignal = (dA2d_t1) nodeSignal_;   
    dA2d_t1 edgeSignal = (dA2d_t1) edgeSignal_;  
    typedef double (*dA2d_t2)[nDim];
    dA2d_t2 nodeCoord = (dA2d_t2) nodeCoord_;
    
    int iNode, iEdge, lNode, lEdge;
    // Initialize mesh elements
    Mesh2D Elem;
    for ( iNode = 0; iNode < 3; iNode++ ) {
        Elem.nx[iNode] = nodeCoord[iNode][0]; 
        Elem.ny[iNode] = nodeCoord[iNode][1]; 
        Elem.nid[iNode] = 0; // global node number
    }
    Elem.InitAngle();

    // copy reconstruction data
    Euler2D *Qrecon;
    Qrecon = new Euler2D [NP];
    double dprime, uprime, vprime, pprime;
    for ( iNode = 0; iNode < 3; iNode++){
        lNode = 2*iNode; // local node index
        dprime = reconData[lNode][0];
        uprime = reconData[lNode][1];
        vprime = reconData[lNode][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);         
        Qrecon[lNode] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lNode].setgas();
    }
    for ( iEdge = 0; iEdge < 3; iEdge++){
        lEdge = 2*iEdge+1; // local edge index
        dprime = reconData[lEdge][0];
        uprime = reconData[lEdge][1];
        vprime = reconData[lEdge][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);         
        Qrecon[lEdge] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lEdge].setgas();
    }
    // bubble function
    dprime = reconData[6][0];
    uprime = reconData[6][1];
    vprime = reconData[6][2];
    pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);
    Qrecon[6] = Euler2D(dprime,uprime,vprime,pprime);
    Qrecon[6].setgas();
    for ( int i = 0; i < NP; i++ ) {
        Elem.Q[i] = Qrecon[i]; 
        Elem.Q[i].setgas();
    }
    // set reconstruction average
    Elem.SetAverage();
    
    // Time related solution variables
    Euler2D *Qsignal;             // node and edge signals 
    Qsignal = new Euler2D [NP];
    for ( int i = 0; i < NP; i++ ) {
        Qsignal[i].Zero();
    }

    // Cell based acoustics updates
    Euler2D gradx, grady;
    double isentGradPx, isentGradPy;
    // update 3 nodes in one cell
    for ( iNode = 0; iNode < 3; iNode++ ){
        lNode = 2*iNode; // local node index

        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lNode] += Elem.NodeFluxResidualIsentEuler(lNode, dt,
                                             gradx, grady);

        isentGradPx = ISENTROPIC_CONST*GAMMA*pow(Qrecon[lNode].d,GAMMA-1.0)*gradx.d;
        isentGradPy = ISENTROPIC_CONST*GAMMA*pow(Qrecon[lNode].d,GAMMA-1.0)*grady.d;
        //// corrections 
        //Qsignal[lNode].u += HALF*(dt)*(dt)*(GAMMA-1.0)/Qrecon[lNode].d*isentGradPx*(gradx.u+grady.v);
        //Qsignal[lNode].v += HALF*(dt)*(dt)*(GAMMA-1.0)/Qrecon[lNode].d*isentGradPy*(gradx.u+grady.v);

        // interaction terms
        Qsignal[lNode].d -= HALF*(dt)*(dt)*Qrecon[lNode].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        Qsignal[lNode].u -= HALF*(dt)*(dt)/Qrecon[lNode].d*(isentGradPy*(gradx.v-grady.u));
        Qsignal[lNode].v -= HALF*(dt)*(dt)/Qrecon[lNode].d*(isentGradPx*(grady.u-gradx.v));

    }
    // update 3 edges in one cell
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        
        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lEdge] += Elem.EdgeFluxResidualIsentEuler(lEdge, dt, 
                                             gradx, grady);

        isentGradPx = ISENTROPIC_CONST*GAMMA*pow(Qrecon[lEdge].d,GAMMA-1.0)*gradx.d;
        isentGradPy = ISENTROPIC_CONST*GAMMA*pow(Qrecon[lEdge].d,GAMMA-1.0)*grady.d;
        //// corrections 
        //Qsignal[lEdge].u += HALF*(dt)*(dt)*(GAMMA-1.0)/Qrecon[lEdge].d*isentGradPx*(gradx.u+grady.v);
        //Qsignal[lEdge].v += HALF*(dt)*(dt)*(GAMMA-1.0)/Qrecon[lEdge].d*isentGradPy*(gradx.u+grady.v);

        // interaction terms
        Qsignal[lEdge].d -= HALF*(dt)*(dt)*Qrecon[lEdge].d*2.0*(grady.u*gradx.v-gradx.u*grady.v);
        Qsignal[lEdge].u -= HALF*(dt)*(dt)/Qrecon[lEdge].d*(isentGradPy*(gradx.v-grady.u));
        Qsignal[lEdge].v -= HALF*(dt)*(dt)/Qrecon[lEdge].d*(isentGradPx*(grady.u-gradx.v));

    }
    
    // return signals 
    for ( iNode = 0; iNode < 3; iNode++ ) {
        lNode = 2*iNode; // local node index
        nodeSignal[iNode][0] = Qsignal[lNode].d; 
        nodeSignal[iNode][1] = Qsignal[lNode].u; 
        nodeSignal[iNode][2] = Qsignal[lNode].v; 
        //nodeSignal[iNode][3] = Qsignal[lNode].p; 
    }
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        edgeSignal[iEdge][0] = Qsignal[lEdge].d; 
        edgeSignal[iEdge][1] = Qsignal[lEdge].u; 
        edgeSignal[iEdge][2] = Qsignal[lEdge].v; 
        //edgeSignal[iEdge][3] = Qsignal[lEdge].p; 
    }

    // clean up the memory
    delete[] Qrecon;
    delete[] Qsignal;

}
// second-order acoustic operator for isentropic Euler equations 
void isentropicacsignalvalue_( int *nEqns_, 
                               int *nDim_,
                               int *iCell_, 
                               double *dtIn_,   
                               double *reconData_, // local reconstruction data
                               double *nodeSignal_,// local cell node data signal
                               double *edgeSignal_,// local cell edge data signal
                               double *nodeCoord_  // local cell mesh node coordinate
                             )       
{

    // set variables consistent with C++ code 
    int nDim = *nDim_;           // number of dimensions
    int nEqns = *nEqns_;         // number of equations 
    int iCell = *iCell_;         // current cell index
    double dt = *dtIn_;          // time step

    // cast multidimensional Fortran arrays into C++ multidimensional arrays
    // Fortran array - column major ordered arrays
    // C++ array - row major ordered arrays
    // means that A(i,j) = A[j][i], from Fortran to C++
    // This routine simplifies indexing of arrays
    // prefix c in front of variable and array names to distinguish  
    typedef double (*dA2d_t1)[nEqns];
    dA2d_t1 reconData = (dA2d_t1) reconData_;    
    dA2d_t1 nodeSignal = (dA2d_t1) nodeSignal_;   
    dA2d_t1 edgeSignal = (dA2d_t1) edgeSignal_;  
    typedef double (*dA2d_t2)[nDim];
    dA2d_t2 nodeCoord = (dA2d_t2) nodeCoord_;
    
    int iNode, iEdge, lNode, lEdge;
    // Initialize mesh elements
    Mesh2D Elem;
    for ( iNode = 0; iNode < 3; iNode++ ) {
        Elem.nx[iNode] = nodeCoord[iNode][0]; 
        Elem.ny[iNode] = nodeCoord[iNode][1]; 
        Elem.nid[iNode] = 0; // global node number
    }
    Elem.InitAngle();

    // copy reconstruction data
    Euler2D *Qrecon;
    Qrecon = new Euler2D [NP];
    double dprime, uprime, vprime, pprime;
    for ( iNode = 0; iNode < 3; iNode++){
        lNode = 2*iNode; // local node index
        dprime = reconData[lNode][0];
        uprime = reconData[lNode][1];
        vprime = reconData[lNode][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);         
        Qrecon[lNode] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lNode].setgas();
    }
    for ( iEdge = 0; iEdge < 3; iEdge++){
        lEdge = 2*iEdge+1; // local edge index
        dprime = reconData[lEdge][0];
        uprime = reconData[lEdge][1];
        vprime = reconData[lEdge][2];
        pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);         
        Qrecon[lEdge] = Euler2D(dprime,uprime,vprime,pprime);
        Qrecon[lEdge].setgas();
    }
     // bubble function
    dprime = reconData[6][0];
    uprime = reconData[6][1];
    vprime = reconData[6][2];
    pprime = ISENTROPIC_CONST*pow(dprime,GAMMA);
    Qrecon[6] = Euler2D(dprime,uprime,vprime,pprime); 
    Qrecon[6].setgas();

    for ( int i = 0; i < NP; i++ ) {
        Elem.Q[i] = Qrecon[i]; 
        Elem.Q[i].setgas();
    }
    // initialize average
    //Elem.InitAverage();
    Elem.SetAverage();
    
    // Time related solution variables
    Euler2D *Qsignal;             // node and edge signals 
    Qsignal = new Euler2D [NP];
    for ( int i = 0; i < NP; i++ ) {
        Qsignal[i].Zero();
    }

    // Cell based acoustics updates
    Euler2D gradx, grady;
    // update 3 nodes in one cell
    for ( iNode = 0; iNode < 3; iNode++ ){
        lNode = 2*iNode; // local node index

        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lNode] += Elem.NodeFluxResidualIsentEuler(lNode, dt,
                                             gradx, grady);

    }
    // update 3 edges in one cell
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        
        gradx.Zero();
        grady.Zero();

        // main update
        Qsignal[lEdge] += Elem.EdgeFluxResidualIsentEuler(lEdge, dt, 
                                             gradx, grady);

    }
    
    // return signals 
    for ( iNode = 0; iNode < 3; iNode++ ) {
        lNode = 2*iNode; // local node index
        nodeSignal[iNode][0] = Qsignal[lNode].d; 
        nodeSignal[iNode][1] = Qsignal[lNode].u; 
        nodeSignal[iNode][2] = Qsignal[lNode].v; 
        nodeSignal[iNode][3] = 0.0; 
    }
    for ( iEdge = 0; iEdge < 3; iEdge++ ) {
        lEdge = 2*iEdge+1; // c style index for local edge number, 1,3,5
        edgeSignal[iEdge][0] = Qsignal[lEdge].d; 
        edgeSignal[iEdge][1] = Qsignal[lEdge].u; 
        edgeSignal[iEdge][2] = Qsignal[lEdge].v; 
        edgeSignal[iEdge][3] = 0.0; 
    }

    // clean up the memory
    delete[] Qrecon;
    delete[] Qsignal;

}
