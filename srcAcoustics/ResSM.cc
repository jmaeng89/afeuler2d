/* ResSM.cc:  Residual subroutines for 2D Euler Mesh Class. */

/* Include 2D mesh elements header file. */
#ifndef _MESH2D_INCLUDED
#include "Mesh2D.h"
#endif // _MESH2D_INCLUDED

//!> Second-order node update for Euler equations
Euler2D Mesh2D::NodeFluxResidual(const int &inode,
                                 const double &dt,
                                 Euler2D &gradx, 
                                 Euler2D &grady) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        double R = Q[inode].c()*dt;
                
        double xCart, yCart;
        assert (inode%2 == 0);    // Nodes only (no edges)
        switch (inode) {
        case 0:
                sinP = (ny[1]-ny[0])/len3;
                cosP = (nx[1]-nx[0])/len3;
                sinQ = (ny[2]-ny[0])/len2;
                cosQ = (nx[2]-nx[0])/len2;
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ONE - 2.0*(ny[1]-ny[0])*(ny[1]-ny[0])/l3sq();
                sin2Q = 2.0*(nx[2]-nx[0])*(ny[2]-ny[0])/l2sq();
                cos2Q = ONE - 2.0*(ny[2]-ny[0])*(ny[2]-ny[0])/l2sq();

                xCart = nx[0];
                yCart = ny[0];
                break;
        case 2:
                sinP = (ny[2]-ny[1])/len1;
                cosP = (nx[2]-nx[1])/len1;
                sinQ = (ny[0]-ny[1])/len3;
                cosQ = (nx[0]-nx[1])/len3;
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ONE - 2.0*(ny[2]-ny[1])*(ny[2]-ny[1])/l1sq();
                sin2Q = 2.0*(nx[0]-nx[1])*(ny[0]-ny[1])/l3sq();
                cos2Q = ONE - 2.0*(ny[0]-ny[1])*(ny[0]-ny[1])/l3sq();;

                xCart = nx[1];
                yCart = ny[1];
                break;
        case 4:
                sinP = (ny[0]-ny[2])/len2;
                cosP = (nx[0]-nx[2])/len2;
                sinQ = (ny[1]-ny[2])/len1;
                cosQ = (nx[1]-nx[2])/len1;
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ONE - 2.0*(ny[0]-ny[2])*(ny[0]-ny[2])/l2sq();;
                sin2Q = 2.0*(nx[1]-nx[2])*(ny[1]-ny[2])/l1sq();
                cos2Q = ONE - 2.0*(ny[1]-ny[2])*(ny[1]-ny[2])/l1sq();;

                xCart = nx[2];
                yCart = ny[2];
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sinQ = ZERO;     cosQ = ONE;
                sin2P = ZERO;    cos2P = ONE;
                sin2Q = ZERO;    cos2Q = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
        sin3Q = sinQ*(2.0*cos2Q + 1.0);
        cos3Q = cosQ*(2.0*cos2Q - 1.0);

        double flux_x = R/8.0*(sinQ-sinP);
        double flux_y = R/8.0*(cosP-cosQ);
        double flux_x2 = R*R/12.0/PI*(sin2Q-sin2P);
        double flux_y2 = R*R/12.0/PI*(sin2P-sin2Q);
        double flux_xy = R*R/12.0/PI*(cos2P-cos2Q);
        double flux_x3 = R*R*R/128.0*(9.0*sinQ+sin3Q-9.0*sinP-sin3P);
        double flux_y3 = R*R*R/128.0*(cos3Q-9.0*cosQ-cos3P+9.0*cosP);
        double flux_x2y = R*R*R/32.0*(pow(cosP,3)-pow(cosQ,3));
        double flux_xy2 = R*R*R/32.0*(pow(sinQ,3)-pow(sinP,3));
        double xi, eta, xi2, eta2, xieta, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        eta2 = eta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        xieta = xietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        xi2eta = xi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        xieta2 = xieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        // spherical mean of gradient
        Euler2D MRdx, MRdy, MRddf;
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);
        //MRdx = MRdfdx(inode, xi, eta, ZERO, ZERO, ZERO);
        //MRdy = MRdfdy(inode, xi, eta, ZERO, ZERO, ZERO);

        // First order terms
        double divU = (MRdx.u + MRdy.v);
        Qflux.d = -1.0*dt*Q[inode].d*(divU); 
        Qflux.u = -1.0*dt*MRdx.p/Q[inode].d;
        Qflux.v = -1.0*dt*MRdy.p/Q[inode].d;
        Qflux.p = -1.0*dt*GAMMA*Q[inode].p*(divU); 

        // Laplacian by integrating from 0 to R -> better one of two
        MRddf = PoissonMR(inode, R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                          flux_x3, flux_y3, flux_x2y, flux_xy2);
        //MRddf = PoissonMR2(inode, R, flux_x, flux_y, flux_x2, flux_y2, flux_xy);
        //assert (Q[inode].c2() != ZERO);
        //Qflux.d += MRddf.p/Q[inode].c2();
        //// Assuming isentropic relation for pressure, to avoid dividing by c2
        Qflux.d += MRddf.d + dt*dt*Q[inode].c2()* 
            (((MRdx.p*MRdx.d+MRdy.p*MRdy.d)/Q[inode].p) - ((MRdx.d*MRdx.d+MRdy.d*MRdy.d)/Q[inode].d));
        Qflux.u += MRddf.u;
        Qflux.v += MRddf.v;
        Qflux.p += MRddf.p;
        // include VorticityMR to get correct velocity terms
        VorticityMR(inode, R, xi, eta, Qflux);

        // Second-order terms
        Qflux.d += HALF*dt*dt*(Q[inode].d*(divU*divU) - (MRdx.p*MRdx.d+MRdy.p*MRdy.d)/Q[inode].d); 
        Qflux.u += HALF*dt*dt*((GAMMA-1.0)/Q[inode].d*MRdx.p*(divU)); 
        Qflux.v += HALF*dt*dt*((GAMMA-1.0)/Q[inode].d*MRdy.p*(divU)); 
        Qflux.p += HALF*dt*dt*(GAMMA*GAMMA*Q[inode].p*(divU*divU) - 
                    GAMMA*Q[inode].p*(MRdx.p*MRdx.d+MRdy.p*MRdy.d)/(Q[inode].d*Q[inode].d)); 
        
        // Second-order nonlinear terms - product of two 7 basis functions
        //if (iNonlin == NONLINEAR_ON) {
        //double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
        //rxi = rxiflux(R, flux_x, flux_y, inode);
        //reta = retaflux(R, flux_x, flux_y, inode);
        //rxi2 = rxi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        //reta2 = reta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        //rxieta = rxietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        //rxi2eta = rxi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //rxieta2 = rxieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //rxi3 = rxi3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //reta3 = reta3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        //Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
        //                     rxi2eta, rxieta2, rxi3, reta3);
        //}

        //// Vorticity terms
        //if (iVort == VORTICITY_ON) {
        //    VorticityMR(inode, R, xi, eta, Qflux);
        //}
       

        gradx = MRdx;
        grady = MRdy;

        return (Qflux);
}

// Second-order edge update
Euler2D Mesh2D::EdgeFluxResidual(const int &inode,
                                 const double &dt,
                                 Euler2D &gradx,
                                 Euler2D &grady) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        double R = Q[inode].c()*dt;
                
        double xCart, yCart;
        assert (inode%2 == 1);    // Edges only (no vertices)
        switch (inode) {
        case 5:
                sinP = (ny[1]-ny[0])/l3();
                cosP = (nx[1]-nx[0])/l3();
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ((nx[1]-nx[0])*(nx[1]-nx[0]) - (ny[1]-ny[0])*(ny[1]-ny[0]))/l3sq();

                xCart = 0.5*(nx[1] + nx[0]);
                yCart = 0.5*(ny[1] + ny[0]);
                break;
        case 1:
                sinP = (ny[2]-ny[1])/l1();
                cosP = (nx[2]-nx[1])/l1();
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ((nx[2]-nx[1])*(nx[2]-nx[1]) - (ny[2]-ny[1])*(ny[2]-ny[1]))/l1sq();

                xCart = 0.5*(nx[2] + nx[1]);
                yCart = 0.5*(ny[2] + ny[1]);
                break;
        case 3:
                sinP = (ny[0]-ny[2])/l2();
                cosP = (nx[0]-nx[2])/l2();
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ((nx[0]-nx[2])*(nx[0]-nx[2]) - (ny[0]-ny[2])*(ny[0]-ny[2]))/l2sq();

                xCart = 0.5*(nx[2] + nx[0]);
                yCart = 0.5*(ny[2] + ny[0]);
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sin2P = ZERO;    cos2P = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
                
        double flux_x = -R/4.0*sinP;
        double flux_y = R/4.0*cosP;
        double flux_x3 = -R*R*R/64.0*(9.0*sinP+sin3P);
        double flux_y3 = R*R*R/64.0*(-cos3P+9.0*cosP);
        double flux_x2y = R*R*R/16.0*pow(cosP,3);
        double flux_xy2 = -R*R*R/16.0*pow(sinP,3);
        double xi, eta, xi2, eta2, xieta, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        eta2 = eta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xieta = xietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xi2eta = xi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        xieta2 = xieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        // calculate spherical mean of gradients 
        Euler2D MRdx, MRdy, MRddf;
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);
        //MRdx = MRdfdx(inode, xi, eta, ZERO, ZERO, ZERO);
        //MRdy = MRdfdy(inode, xi, eta, ZERO, ZERO, ZERO);
        // First order corrections
        double divU = (MRdx.u + MRdy.v);
        Qflux.d = -1.0*dt*Q[inode].d*(divU); 
        Qflux.u = -1.0*dt*MRdx.p/Q[inode].d;
        Qflux.v = -1.0*dt*MRdy.p/Q[inode].d;
        Qflux.p = -1.0*dt*GAMMA*Q[inode].p*(divU); 

        // Laplacian by integrating from 0 to R  -> better one of two
        MRddf = PoissonMR(inode, R, flux_x, flux_y, ZERO, ZERO, ZERO,
                          flux_x3, flux_y3, flux_x2y, flux_xy2);
        //MRddf = PoissonMR2(inode, R, flux_x, flux_y, ZERO, ZERO, ZERO);
        //assert (Q[inode].c2() != ZERO);
        //Qflux.d += MRddf.p/Q[inode].c2();
        //// Assuming isentropic relation for pressure, to avoid dividing by c2
        Qflux.d += MRddf.d + dt*dt*Q[inode].c2()* 
            (((MRdx.p*MRdx.d+MRdy.p*MRdy.d)/Q[inode].p) - ((MRdx.d*MRdx.d+MRdy.d*MRdy.d)/Q[inode].d));
        Qflux.u += MRddf.u;
        Qflux.v += MRddf.v;
        Qflux.p += MRddf.p;
        // include VorticityMR to get correct velocity terms
        VorticityMR(inode, R, xi, eta, Qflux);

        // Second-order nonlinear terms
        Qflux.d += HALF*dt*dt*(Q[inode].d*(divU*divU) - (MRdx.p*MRdx.d+MRdy.p*MRdy.d)/Q[inode].d); 
        Qflux.u += HALF*dt*dt*((GAMMA-1.0)/Q[inode].d*MRdx.p*(divU)); 
        Qflux.v += HALF*dt*dt*((GAMMA-1.0)/Q[inode].d*MRdy.p*(divU)); 
        Qflux.p += HALF*dt*dt*(GAMMA*GAMMA*Q[inode].p*(divU*divU) - 
                    GAMMA*Q[inode].p*(MRdx.p*MRdx.d+MRdy.p*MRdy.d)/(Q[inode].d*Q[inode].d)); 

        // Second-order nonlinear terms - product of two 7 basis functions
        //if (iNonlin == NONLINEAR_ON) {
        //double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
        //rxi = rxiflux(R, flux_x, flux_y, inode);
        //reta = retaflux(R, flux_x, flux_y, inode);
        //rxi2 = rxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        //reta2 = reta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        //rxieta = rxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        //rxi2eta = rxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //rxieta2 = rxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //rxi3 = rxi3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //reta3 = reta3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //
        //Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
        //                     rxi2eta, rxieta2, rxi3, reta3);
        //}
        //// Vorticity terms
        //if (iVort == VORTICITY_ON) {
        //    VorticityMR(inode, R, xi, eta, Qflux);
        //}
        

        gradx = MRdx;
        grady = MRdy;

        return (Qflux);
}

//!> Second-order nodal update using Spherical means
Euler2D Mesh2D::NodeFluxResidualIsentEuler(const int &inode,
                                           const double &dt,
                                           Euler2D &gradx, 
                                           Euler2D &grady) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        //double R = Q[inode].c()*dt;
        double R = sqrt(GAMMA*ISENTROPIC_CONST*pow(Q[inode].d,GAMMA-1.0))*dt;
                
        double xCart, yCart;
        assert (inode%2 == 0);    // Nodes only (no edges)
        switch (inode) {
        case 0:
                sinP = (ny[1]-ny[0])/len3;
                cosP = (nx[1]-nx[0])/len3;
                sinQ = (ny[2]-ny[0])/len2;
                cosQ = (nx[2]-nx[0])/len2;
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ONE - 2.0*(ny[1]-ny[0])*(ny[1]-ny[0])/l3sq();
                sin2Q = 2.0*(nx[2]-nx[0])*(ny[2]-ny[0])/l2sq();
                cos2Q = ONE - 2.0*(ny[2]-ny[0])*(ny[2]-ny[0])/l2sq();

                xCart = nx[0];
                yCart = ny[0];
                break;
        case 2:
                sinP = (ny[2]-ny[1])/len1;
                cosP = (nx[2]-nx[1])/len1;
                sinQ = (ny[0]-ny[1])/len3;
                cosQ = (nx[0]-nx[1])/len3;
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ONE - 2.0*(ny[2]-ny[1])*(ny[2]-ny[1])/l1sq();
                sin2Q = 2.0*(nx[0]-nx[1])*(ny[0]-ny[1])/l3sq();
                cos2Q = ONE - 2.0*(ny[0]-ny[1])*(ny[0]-ny[1])/l3sq();;

                xCart = nx[1];
                yCart = ny[1];
                break;
        case 4:
                sinP = (ny[0]-ny[2])/len2;
                cosP = (nx[0]-nx[2])/len2;
                sinQ = (ny[1]-ny[2])/len1;
                cosQ = (nx[1]-nx[2])/len1;
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ONE - 2.0*(ny[0]-ny[2])*(ny[0]-ny[2])/l2sq();;
                sin2Q = 2.0*(nx[1]-nx[2])*(ny[1]-ny[2])/l1sq();
                cos2Q = ONE - 2.0*(ny[1]-ny[2])*(ny[1]-ny[2])/l1sq();;

                xCart = nx[2];
                yCart = ny[2];
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sinQ = ZERO;     cosQ = ONE;
                sin2P = ZERO;    cos2P = ONE;
                sin2Q = ZERO;    cos2Q = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
        sin3Q = sinQ*(2.0*cos2Q + 1.0);
        cos3Q = cosQ*(2.0*cos2Q - 1.0);

        double flux_x = R/8.0*(sinQ-sinP);
        double flux_y = R/8.0*(cosP-cosQ);
        double flux_x2 = R*R/12.0/PI*(sin2Q-sin2P);
        double flux_y2 = R*R/12.0/PI*(sin2P-sin2Q);
        double flux_xy = R*R/12.0/PI*(cos2P-cos2Q);
        //double flux_x3 = R*R*R/128.0*(9.0*sinQ+sin3Q-9.0*sinP-sin3P);
        //double flux_y3 = R*R*R/128.0*(cos3Q-9.0*cosQ-cos3P+9.0*cosP);
        //double flux_x2y = R*R*R/32.0*(pow(cosP,3)-pow(cosQ,3));
        //double flux_xy2 = R*R*R/32.0*(pow(sinQ,3)-pow(sinP,3));
        double xi, eta, xi2, eta2, xieta; //, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        eta2 = eta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        xieta = xietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        //xi2eta = xi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                    flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //xieta2 = xieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                    flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        // spherical mean of gradient
        Euler2D MRdx, MRdy, MRddf;
        MRdx = MRdfdx(inode, xi, eta, ZERO, ZERO, ZERO);
        MRdy = MRdfdy(inode, xi, eta, ZERO, ZERO, ZERO);

        // First order terms
        double divU = (MRdx.u + MRdy.v);
        Qflux.d = -1.0*dt*Q[inode].d*(divU); 
        Qflux.u = -1.0*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*MRdx.d;
        Qflux.v = -1.0*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*MRdy.d;
        Qflux.p = 0.0;

        // Laplacian by integrating from 0 to R -> better one of two
        //MRddf = PoissonMR(inode, R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
        //                  flux_x3, flux_y3, flux_x2y, flux_xy2);
        MRddf = PoissonMR2(inode, R, flux_x, flux_y, flux_x2, flux_y2, flux_xy);
        Qflux.d += MRddf.d;
        Qflux.u += MRddf.u;
        Qflux.v += MRddf.v;
        // include VorticityMR to get correct velocity terms
        VorticityMR(inode, R, xi, eta, Qflux);

        // Second-order terms
        Qflux.d += HALF*dt*dt*(Q[inode].d*(divU*divU) - 
                        ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-2.0)*(MRdx.d*MRdx.d+MRdy.d*MRdy.d)); 
        Qflux.u += HALF*dt*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-1.0)*MRdx.d*(divU); 
        Qflux.v += HALF*dt*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-1.0)*MRdy.d*(divU); 

        /*
        // Non-linear terms
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                reta2 = reta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

                Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                     rxi2eta, rxieta2, rxi3, reta3);
        }

        // Vorticity terms
        if (iVort == VORTICITY_ON) {
            VorticityMR(inode, R, xi, eta, Qflux);
        }
        */
       
        gradx = MRdx;
        grady = MRdy;

        return (Qflux);
}

//!> Second-order edge update using Spherical means
Euler2D Mesh2D::EdgeFluxResidualIsentEuler(const int &inode,
                                           const double &dt,
                                           Euler2D &gradx,
                                           Euler2D &grady) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        //double R = Q[inode].c()*dt;
        double R = sqrt(GAMMA*ISENTROPIC_CONST*pow(Q[inode].d,GAMMA-1.0))*dt;
                
        double xCart, yCart;
        assert (inode%2 == 1);    // Edges only (no vertices)
        switch (inode) {
        case 5:
                sinP = (ny[1]-ny[0])/l3();
                cosP = (nx[1]-nx[0])/l3();
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ((nx[1]-nx[0])*(nx[1]-nx[0]) - (ny[1]-ny[0])*(ny[1]-ny[0]))/l3sq();

                xCart = 0.5*(nx[1] + nx[0]);
                yCart = 0.5*(ny[1] + ny[0]);
                break;
        case 1:
                sinP = (ny[2]-ny[1])/l1();
                cosP = (nx[2]-nx[1])/l1();
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ((nx[2]-nx[1])*(nx[2]-nx[1]) - (ny[2]-ny[1])*(ny[2]-ny[1]))/l1sq();

                xCart = 0.5*(nx[2] + nx[1]);
                yCart = 0.5*(ny[2] + ny[1]);
                break;
        case 3:
                sinP = (ny[0]-ny[2])/l2();
                cosP = (nx[0]-nx[2])/l2();
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ((nx[0]-nx[2])*(nx[0]-nx[2]) - (ny[0]-ny[2])*(ny[0]-ny[2]))/l2sq();

                xCart = 0.5*(nx[2] + nx[0]);
                yCart = 0.5*(ny[2] + ny[0]);
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sin2P = ZERO;    cos2P = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
                
        double flux_x = -R/4.0*sinP;
        double flux_y = R/4.0*cosP;
        //double flux_x3 = -R*R*R/64.0*(9.0*sinP+sin3P);
        //double flux_y3 = R*R*R/64.0*(-cos3P+9.0*cosP);
        //double flux_x2y = R*R*R/16.0*pow(cosP,3);
        //double flux_xy2 = -R*R*R/16.0*pow(sinP,3);
        double xi, eta, xi2, eta2, xieta; //, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        eta2 = eta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xieta = xietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        //xi2eta = xi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                    flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        //xieta2 = xieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                    flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        // calculate spherical mean of gradients 
        Euler2D MRdx, MRdy, MRddf;
        MRdx = MRdfdx(inode, xi, eta, ZERO, ZERO, ZERO);
        MRdy = MRdfdy(inode, xi, eta, ZERO, ZERO, ZERO);

        // First order terms
        double divU = (MRdx.u + MRdy.v);
        Qflux.d = -1.0*dt*Q[inode].d*(divU); 
        Qflux.u = -1.0*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*MRdx.d;
        Qflux.v = -1.0*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*MRdy.d;
        Qflux.p = 0.0;

        // Laplacian by integrating from 0 to R  -> better one of two
        //MRddf = PoissonMR(inode, R, flux_x, flux_y, ZERO, ZERO, ZERO,
        //                  flux_x3, flux_y3, flux_x2y, flux_xy2);
        //Qflux.d += MRddf.p/Q[inode].c2();
        MRddf = PoissonMR2(inode, R, flux_x, flux_y, ZERO, ZERO, ZERO);
        Qflux.d += MRddf.d;
        Qflux.u += MRddf.u;
        Qflux.v += MRddf.v;
        // include VorticityMR to get correct velocity terms
        VorticityMR(inode, R, xi, eta, Qflux);

        // Second-order terms
        Qflux.d += HALF*dt*dt*(Q[inode].d*(divU*divU) - 
                        ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-2.0)*(MRdx.d*MRdx.d+MRdy.d*MRdy.d)); 
        Qflux.u += HALF*dt*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-1.0)*MRdx.d*(divU); 
        Qflux.v += HALF*dt*dt*ISENTROPIC_CONST*GAMMA*pow(Q[inode].d,GAMMA-2.0)*(GAMMA-1.0)*MRdy.d*(divU); 

        /*
        // Non-linear terms
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                reta2 = reta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                
                Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                     rxi2eta, rxieta2, rxi3, reta3);
        }

        // Vorticity terms
        if (iVort == VORTICITY_ON) {
            VorticityMR(inode, R, xi, eta, Qflux);
        }
        */
        
        gradx = MRdx;
        grady = MRdy;

        return (Qflux);
}

// linear acoustics update using Spherical means
Euler2D Mesh2D::linAcoNodeFluxResidual(const int &inode,
                                       const double &dt) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        double R = Q[inode].c()*dt;
        //double R = dt;
                
        assert (inode%2 == 0);    // Nodes only (no edges)
        switch (inode) {
        case 0:
                sinP = (ny[1]-ny[0])/len3;
                cosP = (nx[1]-nx[0])/len3;
                sinQ = (ny[2]-ny[0])/len2;
                cosQ = (nx[2]-nx[0])/len2;
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ONE - 2.0*(ny[1]-ny[0])*(ny[1]-ny[0])/l3sq();
                sin2Q = 2.0*(nx[2]-nx[0])*(ny[2]-ny[0])/l2sq();
                cos2Q = ONE - 2.0*(ny[2]-ny[0])*(ny[2]-ny[0])/l2sq();
                break;
        case 2:
                sinP = (ny[2]-ny[1])/len1;
                cosP = (nx[2]-nx[1])/len1;
                sinQ = (ny[0]-ny[1])/len3;
                cosQ = (nx[0]-nx[1])/len3;
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ONE - 2.0*(ny[2]-ny[1])*(ny[2]-ny[1])/l1sq();
                sin2Q = 2.0*(nx[0]-nx[1])*(ny[0]-ny[1])/l3sq();
                cos2Q = ONE - 2.0*(ny[0]-ny[1])*(ny[0]-ny[1])/l3sq();;
                break;
        case 4:
                sinP = (ny[0]-ny[2])/len2;
                cosP = (nx[0]-nx[2])/len2;
                sinQ = (ny[1]-ny[2])/len1;
                cosQ = (nx[1]-nx[2])/len1;
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ONE - 2.0*(ny[0]-ny[2])*(ny[0]-ny[2])/l2sq();;
                sin2Q = 2.0*(nx[1]-nx[2])*(ny[1]-ny[2])/l1sq();
                cos2Q = ONE - 2.0*(ny[1]-ny[2])*(ny[1]-ny[2])/l1sq();;
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sinQ = ZERO;     cosQ = ONE;
                sin2P = ZERO;    cos2P = ONE;
                sin2Q = ZERO;    cos2Q = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
        sin3Q = sinQ*(2.0*cos2Q + 1.0);
        cos3Q = cosQ*(2.0*cos2Q - 1.0);

        double flux_x = R/8.0*(sinQ - sinP);
        double flux_y = R/8.0*(cosP - cosQ);
        double flux_x2 = R*R/12.0/PI*(sin2Q-sin2P);
        double flux_y2 = R*R/12.0/PI*(sin2P-sin2Q);
        double flux_xy = R*R/12.0/PI*(cos2P-cos2Q);
        double flux_x3 = R*R*R/128.0*(9.0*sinQ+sin3Q-9.0*sinP-sin3P);
        double flux_y3 = R*R*R/128.0*(cos3Q-9.0*cosQ-cos3P+9.0*cosP);
        double flux_x2y = R*R*R/32.0*(pow(cosP,3)-pow(cosQ,3));
        double flux_xy2 = R*R*R/32.0*(pow(sinQ,3)-pow(sinP,3));
        double xi, eta, xi2, eta2, xieta, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        eta2 = eta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        xieta = xietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
        xi2eta = xi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        xieta2 = xieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        double dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2;
        dxi = dxiflux(R, flux_x, flux_y, inode);
        deta = detaflux(R, flux_x, flux_y, inode);
        dxi2 = dxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        deta2 = deta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxieta = dxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxi2eta = dxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        dxieta2 = dxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D MR, drdMR, MRdx, MRdy; //, MRddf;
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        MR = MRf(inode, xi, eta, xi2, eta2, xieta, xi2eta, xieta2);
        drdMR = dMRdR(inode, dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2);
        // calculate gradients by spherical means approximation
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);

        //double divU = (MRdx.u + MRdy.v);
        // linear acoustics residuals
        Qflux.d = MR.d + R*(drdMR.d - (MRdx.u + MRdy.v)); 
        Qflux.u = MR.u + R*(drdMR.u - (MRdx.d));
        Qflux.v = MR.v + R*(drdMR.v - (MRdy.d));
        Qflux.p = MR.d + R*(drdMR.d - (MRdx.u + MRdy.v));

        /*
        // Non-linear terms
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                reta2 = reta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, flux_x2, flux_y2, flux_xy,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

                Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                     rxi2eta, rxieta2, rxi3, reta3);
        }

        // Vorticity terms
        if (iVort == VORTICITY_ON) {
                VorticityMR(inode, R, xi, eta, Qflux);
        }
        */ 
        return (Qflux);
}

Euler2D Mesh2D::linAcoEdgeFluxResidual(const int &inode,
                                       const double &dt) {
        double len1 = l1();    double len2 = l2();    double len3 = l3();
        double A1 = J11();     double A2 = J12();     double A3 = J21();     double A4 = J22();
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
        double sinQ, cosQ, sin2Q, cos2Q, sin3Q, cos3Q;
        double R = Q[inode].c()*dt;
        //double R = dt;
                
        assert (inode%2 == 1);    // Edges only (no vertices)
        switch (inode) {
        case 5:
                sinP = (ny[1]-ny[0])/l3();
                cosP = (nx[1]-nx[0])/l3();
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ((nx[1]-nx[0])*(nx[1]-nx[0]) - (ny[1]-ny[0])*(ny[1]-ny[0]))/l3sq();
                break;
        case 1:
                sinP = (ny[2]-ny[1])/l1();
                cosP = (nx[2]-nx[1])/l1();
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ((nx[2]-nx[1])*(nx[2]-nx[1]) - (ny[2]-ny[1])*(ny[2]-ny[1]))/l1sq();
                break;
        case 3:
                sinP = (ny[0]-ny[2])/l2();
                cosP = (nx[0]-nx[2])/l2();
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ((nx[0]-nx[2])*(nx[0]-nx[2]) - (ny[0]-ny[2])*(ny[0]-ny[2]))/l2sq();
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sin2P = ZERO;    cos2P = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);
                
        double flux_x = -R/4.0*sinP;
        double flux_y = R/4.0*cosP;
        double flux_x3 = -R*R*R/64.0*(9.0*sinP+sin3P);
        double flux_y3 = R*R*R/64.0*(-cos3P+9.0*cosP);
        double flux_x2y = R*R*R/16.0*pow(cosP,3);
        double flux_xy2 = -R*R*R/16.0*pow(sinP,3);
        double xi, eta, xi2, eta2, xieta, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        eta2 = eta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xieta = xietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xi2eta = xi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        xieta2 = xieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        double dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2;
        dxi = dxiflux(R, flux_x, flux_y, inode);
        deta = detaflux(R, flux_x, flux_y, inode);
        dxi2 = dxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        deta2 = deta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxieta = dxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxi2eta = dxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        dxieta2 = dxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

        // Calculating residual
        Euler2D MR, drdMR, MRdx, MRdy; //, MRddf;
        Euler2D Qflux = Euler2D(ZERO,ZERO,ZERO,ZERO);
        MR = MRf(inode, xi, eta, xi2, eta2, xieta, xi2eta, xieta2);
        drdMR = dMRdR(inode, dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2);
        // calculate gradients by spherical means approximation
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);

        //double divU = (MRdx.u + MRdy.v);
        // linear acoustics residuals
        Qflux.d = MR.d + R*(drdMR.d - (MRdx.u + MRdy.v)); 
        Qflux.u = MR.u + R*(drdMR.u - (MRdx.d));
        Qflux.v = MR.v + R*(drdMR.v - (MRdy.d));
        Qflux.p = MR.d + R*(drdMR.d - (MRdx.u + MRdy.v));

        /*
        // Non-linear terms
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                reta2 = reta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                
                Qflux += NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                     rxi2eta, rxieta2, rxi3, reta3);
        }

        // Vorticity terms
        if (iVort == VORTICITY_ON) {
                VorticityMR(inode, R, xi, eta, Qflux);
        }
        */

        return (Qflux);
}



//!> Spherical means of edge basis functions
void Mesh2D::EdgeMRflux(const int &inode, const double &R,
                        double &flux_x, double &flux_y,
                        double &flux_x3, double &flux_y3,
                        double &flux_x2y, double &flux_xy2) {
        double sinP, cosP, sin2P, cos2P, sin3P, cos3P;
                
        assert (inode%2 == 1);    // Edges only (no vertices)
        switch (inode) {
        case 5:
                sinP = (ny[1]-ny[0])/l3();
                cosP = (nx[1]-nx[0])/l3();
                sin2P = 2.0*(nx[1]-nx[0])*(ny[1]-ny[0])/l3sq();
                cos2P = ((nx[1]-nx[0])*(nx[1]-nx[0]) - (ny[1]-ny[0])*(ny[1]-ny[0]))/l3sq();
                break;
        case 1:
                sinP = (ny[2]-ny[1])/l1();
                cosP = (nx[2]-nx[1])/l1();
                sin2P = 2.0*(nx[2]-nx[1])*(ny[2]-ny[1])/l1sq();
                cos2P = ((nx[2]-nx[1])*(nx[2]-nx[1]) - (ny[2]-ny[1])*(ny[2]-ny[1]))/l1sq();
                break;
        case 3:
                sinP = (ny[0]-ny[2])/l2();
                cosP = (nx[0]-nx[2])/l2();
                sin2P = 2.0*(nx[0]-nx[2])*(ny[0]-ny[2])/l2sq();
                cos2P = ((nx[0]-nx[2])*(nx[0]-nx[2]) - (ny[0]-ny[2])*(ny[0]-ny[2]))/l2sq();
                break;
        default:
                sinP = ZERO;     cosP = ONE;
                sin2P = ZERO;    cos2P = ONE;
        }
        sin3P = sinP*(2.0*cos2P + 1.0);
        cos3P = cosP*(2.0*cos2P - 1.0);

        flux_x = -R/4.0*sinP;
        flux_y = R/4.0*cosP;
        flux_x3 = -R*R*R/64.0*(9.0*sinP+sin3P);
        flux_y3 = R*R*R/64.0*(-cos3P+9.0*cosP);
        flux_x2y = R*R*R/16.0*pow(cosP,3);
        flux_xy2 = -R*R*R/16.0*pow(sinP,3);
}

void Mesh2D::EdgeMR(const double &R, const int &inode,
                    double flux_x, double flux_y,
                    double flux_x3, double flux_y3,
                    double flux_x2y, double flux_xy2,
                    Euler2D &MR,
                    Euler2D &MRdx,
                    Euler2D &MRdy,
                    Euler2D &drMR,
                    Euler2D &nonlinMR,
                    const int &iNonlin,
                    const int &iVort) {
        double xi, eta, xi2, eta2, xieta, xi2eta, xieta2;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        eta2 = eta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xieta = xietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xi2eta = xi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        xieta2 = xieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                            flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        
        double dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2;
        dxi = dxiflux(R, flux_x, flux_y, inode);
        deta = detaflux(R, flux_x, flux_y, inode);
        dxi2 = dxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        deta2 = deta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxieta = dxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        dxi2eta = dxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
        dxieta2 = dxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                              flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                
        MR = MRf(inode, xi, eta, xi2, eta2, xieta, xi2eta, xieta2);
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);
        drMR = dMRdR(inode, dxi, deta, dxi2, deta2, dxieta, dxi2eta, dxieta2);

        // Consider non-linear terms
        nonlinMR.Zero();
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                reta2 = reta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

                nonlinMR = NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                       rxi2eta, rxieta2, rxi3, reta3);
        }

        if (iVort == VORTICITY_ON)
                VorticityMR(inode, R, xi, eta, nonlinMR);
}


void Mesh2D::EdgeMR(const double &R, const int &inode,
                    double flux_x, double flux_y,
                    double flux_x3, double flux_y3,
                    double flux_x2y, double flux_xy2,
                    Euler2D &MRdx,
                    Euler2D &MRdy,
                    Euler2D &MRddf,
                    Euler2D &nonlinMR,
                    const int &iNonlin,
                    const int &iVort) {
        double xi, eta, xi2, eta2, xieta;
        xi = xiflux(flux_x, flux_y, inode);
        eta = etaflux(flux_x, flux_y, inode);
        xi2 = xi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        eta2 = eta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        xieta = xietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
        
        MRdx = MRdfdx(inode, xi, eta, xi2, eta2, xieta);
        MRdy = MRdfdy(inode, xi, eta, xi2, eta2, xieta);
        MRddf = PoissonMR(inode, R, flux_x, flux_y, ZERO, ZERO, ZERO,
                          flux_x3, flux_y3, flux_x2y, flux_xy2);
        
        // Consider non-linear terms
        nonlinMR.Zero();
        if (iNonlin == NONLINEAR_ON) {
                double rxi, reta, rxi2, reta2, rxieta, rxi2eta, rxieta2, rxi3, reta3;
                rxi = rxiflux(R, flux_x, flux_y, inode);
                reta = retaflux(R, flux_x, flux_y, inode);
                rxi2 = rxi2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                reta2 = reta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxieta = rxietaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO, inode);
                rxi2eta = rxi2etaflux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxieta2 = rxieta2flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                      flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                rxi3 = rxi3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                flux_x3, flux_y3, flux_x2y, flux_xy2, inode);
                reta3 = reta3flux(R, flux_x, flux_y, ZERO, ZERO, ZERO,
                                  flux_x3, flux_y3, flux_x2y, flux_xy2, inode);

                nonlinMR = NonlinearMR(inode, R, xi, eta, rxi, reta, rxi2, reta2, rxieta,
                                       rxi2eta, rxieta2, rxi3, reta3);
        }

        if (iVort == VORTICITY_ON) {
                VorticityMR(inode, R, xi, eta, nonlinMR);
        }
}

        

