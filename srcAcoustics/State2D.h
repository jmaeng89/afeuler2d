/*!\file State2D.h
  \brief Header file defining 2D Nonlinear Barotropic Acoustics Solution State Classes. */

#ifndef _EULER2D_INCLUDED
#define _EULER2D_INCLUDED

/* Include required C++ libraries. */

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cmath>

/* Using std namespace functions */
using namespace std;

/* Define the classes. */

#define	NUM_VAR    4
#define	PRESSURE_STDATM     101325.0000000000
#define	DENSITY_STDATM      1.204100000000000
#define TEMPERATURE_STDATM  293.1500000000000
#define GAMMA            1.40000000000000000000
#define ISENTROPIC_CONST 1.00000000000000000000
#define ZERO       0.000000000
#define ONE        1.000000000
#define HALF       0.500000000
#define THIRD      0.33333333333333333333
#define PI         3.14159265358979323846
#define TOL        1.0e-9        // residual tolerance (precision)

/*!
 * Class: State2D
 *
 * Primitive variable solution state class definition for an inviscid
 * compressible gas-flow.
 *
 * Member functions
 *     d        -- Return density.
 *     u        -- Return x-dir flow velocity
 *     v        -- Return y-dir flow velocity.
 *     p        -- Return pressure.
 *     q        -- Return flow speed.
 *     c        -- Return speed of sound.
 *     Fx       -- Return x-direction solution flux.
 *     Fy       -- Return y-direction solution flux.
 *     Fn       -- Return n-direction solution flux.
 *
 * Member operators
 *      Q -- a primitive solution state
 *      a -- a scalar (double)
 *
 * Q = Q;
 * a = Q[i];
 * Q = Q + Q;
 * Q = Q - Q;
 * a = Q * Q; (inner product)
 * Q = a * Q;
 * Q = Q * a;
 * Q = Q / a;
 * Q = Q ^ Q; (a useful product)
 * Q = +Q;
 * Q = -Q;
 * Q += Q;
 * Q -= Q;
 * Q == Q;
 * Q != Q;
 * cout << Q; (output function)
 * cin  >> Q; (input function)
 * \endverbatim
 */
class Euler2D{
 private:
 public:
        //@{ @name Primitive variables and associated constants:
        double             d; //!< Density.
        double             u; //!< Flow velocity (x-dir).
        double             v; //!< Flow velocity (y-dir).
        double             p; //!< Pressure
        static double      R; //!< Gas constant.
        //@}

        //@{ @name Creation, copy, and assignment constructors.
        //! Creation constructor.
        Euler2D(void) {
                d = DENSITY_STDATM; 
                u = ZERO;
                v = ZERO;
                p = PRESSURE_STDATM;
        }

        //! Copy constructor.
        Euler2D(const Euler2D &Q) {
                d = Q.d;
                u = Q.u; 
                v = Q.v;
                p = Q.p;
        }

        //! Assignment constructor.
        Euler2D(const double &din) {
                d = din;
                u = ZERO;
                v = ZERO;
                p = ZERO;
        }

        //! Assignment constructor.
        Euler2D(const double &din,
                const double &uin) {
                d = din;
                u = uin;
                v = ZERO;
                p = ZERO;
        }

        //! Assignment constructor.
        Euler2D(const double &din,
                const double &uin,
                const double &vin) {
                d = din;
                u = uin;
                v = vin;
                p = ZERO;
        }

        //! Assignment constructor.
        Euler2D(const double &din,
                const double &uin,
                const double &vin,
                const double &pin) {
                d = din;
                u = uin;
                v = vin;
                p = pin;
        }


        /* Destructor. */
        ~Euler2D(void) {};
        // Use automatically generated destructor.
        //@}
 
        //@{ @name Useful operators.
        //! Return the number of variables.
        static int NumVar(void) { return NUM_VAR; }

        //! Copy operator.
        void Copy(const Euler2D &Q) {
                d = Q.d;
                u = Q.u; 
                v = Q.v;
                p = Q.p;
                R = Q.R;
        }

        void CopyState(const Euler2D &Q) {
                d = Q.d; 
                u = Q.u; 
                v = Q.v; 
                p = Q.p;
        }

        //! Zero operator
        void Zero(void) {
                d = ZERO; u = ZERO; v = ZERO; p = ZERO;
        }

        //! Check for unphysical Euler2D properties.
        int Unphysical_Properties(void) const {
                if (d < ZERO) return 1;
                else return 0;
        }
        //@}

        void setgas(void) {
                R = 287.15;
        }
        void setgas(const double &Rgas) {
                R = Rgas;
        }
        
        //@{ @name Euler2D functions.
        //! Temperature.
        double T(void);
        double T(void) const;
        
        //! Specific internal energy.
        double e(void);
        double e(void) const;
        
        //! Total energy.
        double E(void);
        double E(void) const;
        
        //! Specific enthalpy.
        double h(void);
        double h(void) const;
        
        //! Total enthalpy.
        double H(void);
        double H(void) const;
        
        //! Flow speed.
        double q(void);
        double q(void) const;

        //! Flow speed squared.
        double q2(void);
        double q2(void) const;

        //! Sound speed.
        double c(void);
        double c(void) const;
        
        //! Sound speed squared.
        double c2(void);
        double c2(void) const;
        
        //! Mach number.
        double M(void);
        double M(void) const;
        
        //! Specific entropy.
        double s(void);
        double s(void) const;


        //@{ @name Solution flux (x-direction).
        Euler2D Fx(void);
        Euler2D Fx(void) const;
        //@}

        //@{ @name Solution flux (y-direction).
        Euler2D Fy(void);
        Euler2D Fy(void) const;
        //@}

        //@{ @name Solution flux (n-direction).
        Euler2D Fn(void);
        Euler2D Fn(void) const;
        //! Calculate flux in the provided normal direction for a given solution state2D
        Euler2D Fn(const double &normal_x, const double &normal_y) const;
        //@}

  
        //@{ @name Index operator.
        double &operator[](int index) {
                assert( index >= 1 && index <= NUM_VAR );
                switch(index) {
                case 1 :
                        return (d);
                case 2 :
                        return (u);
                case 3 :
                        return (v);
                case 4 :
                        return (p);
                default:
                        return (d);
                };
        }
    
        const double &operator[](int index) const {
                assert( index >= 1 && index <= NUM_VAR );
                switch(index) {
                case 1 :
                        return (d);
                case 2 :
                        return (u);
                case 3 :
                        return (v);
                case 4 :
                        return (p);
                default:
                        return (d);
                };
        }
        //@}

        //@{ @name Binary arithmetic operators.
        friend Euler2D operator +(const Euler2D &Q1, const Euler2D &Q2);
        friend Euler2D operator -(const Euler2D &Q1, const Euler2D &Q2);
        friend double operator *(const Euler2D &Q1, const Euler2D &Q2);
        friend Euler2D operator *(const Euler2D &Q, const double &a);
        friend Euler2D operator *(const double &a, const Euler2D &Q);
        friend Euler2D operator /(const Euler2D &Q, const double &a);
        friend Euler2D operator /(const Euler2D &Q1, const Euler2D &Q2);
        friend Euler2D operator ^(const Euler2D &Q1, const Euler2D &Q2);
        friend Euler2D max(const Euler2D &Q1, const Euler2D &Q2);
        friend Euler2D min(const Euler2D &Q1, const Euler2D &Q2);
        //@}

        //@{ @name Unary arithmetic operators.
        friend Euler2D operator +(const Euler2D &Q);
        friend Euler2D operator -(const Euler2D &Q);
        friend Euler2D fabs(const Euler2D &Q);
        friend Euler2D sqr(const Euler2D &Q);
        //@}

        //@{ @name Shortcut arithmetic operators.
        Euler2D &operator +=(const Euler2D &Q);
        Euler2D &operator -=(const Euler2D &Q);
        Euler2D &operator /=(const Euler2D &Q);
        Euler2D &operator *=(const Euler2D &Q);
        Euler2D &operator *=(const double &a);
        Euler2D &operator /=(const double &a);
        //@}

        //@{ @name Relational operators.
        friend int operator ==(const Euler2D &Q1, const Euler2D &Q2);
        friend int operator !=(const Euler2D &Q1, const Euler2D &Q2);
        friend bool operator >=(const Euler2D &Q1, const Euler2D &Q2);
        friend bool operator <=(const Euler2D &Q1, const Euler2D &Q2);
        friend bool operator <(const Euler2D &Q1, const Euler2D &Q2);
        friend bool operator >(const Euler2D &Q1, const Euler2D &Q2);
        //@}

        //@{ @name Input-output operators.
        friend ostream &operator << (ostream &out_file, const Euler2D &Q);
        friend istream &operator >> (istream &in_file,  Euler2D &Q);
        //@}

        //@{ @name Output functions.
        void output_labels(ostream &out_file) {
                out_file << "\"d\" \\ \n"
                         << "\"u\" \\ \n"
                         << "\"v\" \\ \n"
                         << "\"p\" \\ \n";
        }

        void output_data(ostream &out_file) {  
                out_file << " " << d << " " << u << " " << v << " " << p;
        }
        //@}
};

/********************************************************
 * Euler2D::T -- Temperature.                    *
 ********************************************************/
inline double Euler2D::T(void) {
        return (p/(d*R));
}

inline double Euler2D::T(void) const {
        return (p/(d*R));
}

/********************************************************
 * Euler2D::e -- Specific internal energy.       *
 ********************************************************/
inline double Euler2D::e(void) {
        return (p/((GAMMA - 1.0)*d));
}

inline double Euler2D::e(void) const {
        return (p/((GAMMA - 1.0)*d));
}

/********************************************************
 * Euler2D::E -- Total energy.                   *
 ********************************************************/
inline double Euler2D::E(void) {
        return (p/(GAMMA-1.0) + HALF*d*q2());
}

inline double Euler2D::E(void) const {
        return (p/(GAMMA-1.0) + HALF*d*q2());
}

/********************************************************
 * Euler2D::h -- Specific enthalpy.              *
 ********************************************************/
inline double Euler2D::h(void) {
        return (GAMMA*p/((GAMMA-1.0)*d) + HALF*q2());
}

inline double Euler2D::h(void) const {
        return (GAMMA*p/((GAMMA-1.0)*d) + HALF*q2());
}

/********************************************************
 * Euler2D::H -- Total enthalpy.                 *
 ********************************************************/
inline double Euler2D::H(void) {
        return (GAMMA*p/(GAMMA-1.0) + HALF*d*q2());
}

inline double Euler2D::H(void) const {
        return (GAMMA*p/(GAMMA-1.0) + HALF*d*q2());
}

/********************************************************
 * Euler2D::q -- Flow speed.                    *
 ********************************************************/
inline double Euler2D::q(void) {
        return (sqrt(u*u+v*v));
}

inline double Euler2D::q(void) const {
        return (sqrt(u*u+v*v));
}

/********************************************************
 * Euler2D::q2 -- Flow speed squared.           *
 ********************************************************/
inline double Euler2D::q2(void) {
        return ( u*u+v*v );
}

inline double Euler2D::q2(void) const {
        return ( u*u+v*v );
}

/********************************************************
 * Euler2D::c -- Sound speed.                    *
 ********************************************************/
inline double Euler2D::c(void) {
        if ( fabs(d) < TOL || (p/d) < ZERO )
                return ( ZERO );
        else
                return (sqrt(GAMMA*p/d));
}

inline double Euler2D::c(void) const {
        if ( fabs(d) < TOL || (p/d) < ZERO )
                return ( ZERO );
        else
                return (sqrt(GAMMA*p/d));
}

/********************************************************
 * Euler2D::c2 -- Sound speed squared.           *
 ********************************************************/
inline double Euler2D::c2(void) {
        if ( fabs(d) < TOL || (p/d) < ZERO ) 
                return ( ZERO );
                //return( fabs(GAMMA*p/d) );
        else
                return (GAMMA*p/d);
}

inline double Euler2D::c2(void) const {
        if ( fabs(d) < TOL || (p/d) < ZERO )
                return ( ZERO );
                //return( fabs(GAMMA*p/d) );
        else
                return (GAMMA*p/d);
}

/********************************************************
 * Euler2D::M -- Mach number.                    *
 ********************************************************/
inline double Euler2D::M(void) {
        assert (c() != ZERO);
        return (q()/c());
}

inline double Euler2D::M(void) const {
        assert (c() != ZERO);
        return (q()/c());
}

/********************************************************
 * Euler2D::s -- Specific entropy.               *
 ********************************************************/
inline double Euler2D::s(void) {
        return (R/(GAMMA-1.0)*log(p/pow(d, GAMMA)));
}

inline double Euler2D::s(void) const {
        return (R/(GAMMA-1.0)*log(p/pow(d, GAMMA)));
}


/********************************************************
 * Euler2D -- Binary arithmetic operators.       *
 ********************************************************/
inline Euler2D operator +(const Euler2D &Q1, const Euler2D &Q2) {
        return (Euler2D(Q1.d+Q2.d,Q1.u+Q2.u,Q1.v+Q2.v,Q1.p+Q2.p));
}

inline Euler2D operator -(const Euler2D &Q1, const Euler2D &Q2) {
        return (Euler2D(Q1.d-Q2.d,Q1.u-Q2.u,Q1.v-Q2.v,Q1.p-Q2.p));
}

// Inner product operator.
inline double operator *(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d*Q2.d+Q1.u*Q2.u+Q1.v*Q2.v+Q1.p*Q2.p);
}

inline Euler2D operator *(const Euler2D &Q, const double &a) {
        return (Euler2D(a*Q.d,a*Q.u,a*Q.v,a*Q.p));
}

inline Euler2D operator *(const double &a, const Euler2D &Q) {
        return (Euler2D(a*Q.d,a*Q.u,a*Q.v,a*Q.p));
}

inline Euler2D operator /(const Euler2D &Q, const double &a) {
        return (Euler2D(Q.d/a,Q.u/a,Q.v/a,Q.p/a));
}

inline Euler2D operator /(const Euler2D &Q1, const Euler2D &Q2) {
        return (Euler2D(Q1.d/Q2.d, Q1.u/Q2.u, Q1.v/Q2.v, Q1.p/Q2.p));
        // return (Euler2D(Q1[1]/Q2[1], Q1[2]/Q2[2], Q1[3]/Q2[3], Q1[4]/Q2[4]));
}

// Useful solution Euler2D product operator.
inline Euler2D operator ^(const Euler2D &Q1, const Euler2D &Q2) {
        return (Euler2D(Q1.d*Q2.d,Q1.u*Q2.u,Q1.v*Q2.v,Q1.p*Q2.p));
}

/*!
 * Compute maximum between 2 state2Ds. 
 * Return the state2D of maximum values.
 */
inline Euler2D max(const Euler2D &Q1, const Euler2D &Q2 ){
        return (Euler2D(max(Q1.d,Q2.d),max(Q1.u,Q2.u),max(Q1.v,Q2.v),max(Q1.p,Q2.p)));
}

/*!
 * Compute minimum between 2 Euler2Ds. 
 * Return the Euler2D of minimum values.
 */
inline Euler2D min(const Euler2D &Q1, const Euler2D &Q2 ){
        return (Euler2D(min(Q1.d,Q2.d),min(Q1.u,Q2.u),min(Q1.v,Q2.v),min(Q1.p,Q2.p)));
}

/********************************************************
 * Euler2D -- Unary arithmetic operators.        *
 ********************************************************/
inline Euler2D operator +(const Euler2D &Q) {
        return (Euler2D(Q.d,Q.u,Q.v,Q.p));
}

inline Euler2D operator -(const Euler2D &Q) {
        return (Euler2D(-Q.d,-Q.u,-Q.v,-Q.p));
}

inline Euler2D fabs(const Euler2D &Q){
        return Euler2D(fabs(Q.d),fabs(Q.u),fabs(Q.v),fabs(Q.p));
}

inline Euler2D sqr(const Euler2D &Q){
        return Euler2D(Q.d*Q.d,Q.u*Q.u,Q.v*Q.v,Q.p*Q.p);
}


/********************************************************
 * Euler2D -- Shortcut arithmetic operators.     *
 ********************************************************/
inline Euler2D& Euler2D::operator +=(const Euler2D &Q) {
        d += Q.d; u += Q.u; v += Q.v; p += Q.p;
        return *this;
}

inline Euler2D& Euler2D::operator -=(const Euler2D &Q) {
        d -= Q.d; u -= Q.u; v -= Q.v; p -= Q.p;
        return *this;
}

inline Euler2D& Euler2D::operator /=(const Euler2D &Q){
        d /= Q.d;
        u /= Q.u;
        v /= Q.v;
        p /= Q.p;
        return *this;
}

inline Euler2D& Euler2D::operator *=(const Euler2D &Q) {
        d *= Q.d; u *= Q.u; v *= Q.v; p *= Q.p;
        return *this;
}

inline Euler2D& Euler2D::operator *=(const double &a) {
        d *= a; u *= a; v *= a; p *= a; 
        return *this;
}

inline Euler2D& Euler2D::operator /=(const double &a) {
        d /= a; u /= a; v /= a; p /= a; 
        return *this;
}

/********************************************************
 * Euler2D -- Relational operators.              *
 ********************************************************/
inline int operator ==(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d == Q2.d && Q1.u == Q2.u && Q1.v == Q2.v && Q1.p == Q2.p);
}

inline int operator !=(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d != Q2.d || Q1.u != Q2.u || Q1.v != Q2.v || Q1.p != Q2.p);
}

inline bool operator <=(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d <= Q2.d && Q1.u <= Q2.u && Q1.v <= Q2.v && Q1.p <= Q2.p);
}

inline bool operator >=(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d >= Q2.d && Q1.u >= Q2.u && Q1.v >= Q2.v && Q1.p >= Q2.p);
}

inline bool operator <(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d < Q2.d && Q1.u < Q2.u && Q1.v < Q2.v && Q1.p < Q2.p);
}

inline bool operator >(const Euler2D &Q1, const Euler2D &Q2) {
        return (Q1.d > Q2.d && Q1.u > Q2.u && Q1.v > Q2.v && Q1.p > Q2.p);
}

/********************************************************
 * Euler2D -- Input-output operators.            *
 ********************************************************/
inline ostream &operator << (ostream &out_file, const Euler2D &Q) {
        out_file.setf(ios::scientific);
        out_file << " " << Q.d
                 << " " << Q.u
                 << " " << Q.v
                 << " " << Q.p;
        out_file.unsetf(ios::scientific);
        return (out_file);
}

inline istream &operator >> (istream &in_file, Euler2D &Q) {
        in_file.setf(ios::skipws);
        in_file >> Q.d >> Q.u >> Q.v >> Q.p;
        in_file.unsetf(ios::skipws);
        return (in_file);
}


/********************************************************
 * Euler2D::Fx -- Solution flux (x-direction).   *
 ********************************************************/
inline Euler2D Euler2D::Fx(void) {
        //return (Euler2D(d*u, c2()/(GAMMA-ONE), ZERO, p*u));
        // for linear acoustics
        return (Euler2D(c()*u, c()*d, ZERO, ZERO));
        //return (Euler2D(ZERO, ZERO, ZERO, ZERO));
}

inline Euler2D Euler2D::Fx(void) const {
        //return (Euler2D(d*u, c2()/(GAMMA-ONE), ZERO, p*u));
        // for linear acoustics
        return (Euler2D(c()*u, c()*d, ZERO, ZERO));
        //return (Euler2D(ZERO, ZERO, ZERO, ZERO));
}

/********************************************************
 * Euler2D::Fy -- Solution flux (y-direction).   *
 ********************************************************/
inline Euler2D Euler2D::Fy(void) {
        //return (Euler2D(d*v, ZERO, c2()/(GAMMA-ONE), p*v));
        // for linear acoustics
        return (Euler2D(c()*v, ZERO, c()*d, ZERO));
        //return (Euler2D(ZERO, ZERO, ZERO, ZERO));
}

inline Euler2D Euler2D::Fy(void) const {
        //return (Euler2D(d*v, ZERO, c2()/(GAMMA-ONE), p*v));
        // for linear acoustics
        return (Euler2D(c()*v, ZERO, c()*d, ZERO));
        //return (Euler2D(ZERO, ZERO, ZERO, ZERO));
}

/********************************************************
 * Euler2D::Fn -- Solution flux (n-direction).   *
 ********************************************************/
inline Euler2D Euler2D::Fn(void) {
        return (Euler2D(d*u, c2()/(GAMMA-ONE), ZERO, p*u));
}

inline Euler2D Euler2D::Fn(void) const {
        return (Euler2D(d*u, c2()/(GAMMA-ONE), ZERO, p*u));
}

/*
 * Calculate the flux in the normal direction 
 * using the given solution state2D.
 *
 * \param normal vector defining the normal direction
 */ 
inline Euler2D Euler2D::Fn(const double &normal_x, const double &normal_y) const {
        return (Euler2D(d*(u*normal_x + v*normal_y),
                        p/d*normal_x,
                        p/d*normal_y,
                        d*(u*normal_x + v*normal_y)*c2()));
}

/*
 * Calculate the flux in the normal direction 
 * using the given solution Euler2D.
 */ 
inline Euler2D Fn(const Euler2D &Q, const double &normal_x, const double &normal_y) {
        return Q.Fn(normal_x, normal_y);
}


/********************************************************
 * Euler2D -- External subroutines.                       *
 ********************************************************/

extern Euler2D FluxAverage_x(const Euler2D &QL_n,
                             const Euler2D &QM_n,
                             const Euler2D &QR_n,
                             const Euler2D &QL_nhalf,
                             const Euler2D &QM_nhalf,
                             const Euler2D &QR_nhalf,
                             const Euler2D &QL_n1,
                             const Euler2D &QM_n1,
                             const Euler2D &QR_n1);

extern Euler2D FluxAverage_y(const Euler2D &QL_n,
                             const Euler2D &QM_n,
                             const Euler2D &QR_n,
                             const Euler2D &QL_nhalf,
                             const Euler2D &QM_nhalf,
                             const Euler2D &QR_nhalf,
                             const Euler2D &QL_n1,
                             const Euler2D &QM_n1,
                             const Euler2D &QR_n1);

extern Euler2D FluxAverage_n(const Euler2D &QL_n,
                             const Euler2D &QM_n,
                             const Euler2D &QR_n,
                             const Euler2D &QL_nhalf,
                             const Euler2D &QM_nhalf,
                             const Euler2D &QR_nhalf,
                             const Euler2D &QL_n1,
                             const Euler2D &QM_n1,
                             const Euler2D &QR_n1,
                             const double &normal_x, const double &normal_y);

extern Euler2D Rotate(const Euler2D &Q,
                      const double &norm_x,
                      const double &norm_y);

extern Euler2D BC_Wall(const Euler2D &Qi,
                       const double &norm_x,
                       const double &norm_y);

#endif /* _STATE2D_INCLUDED  */
