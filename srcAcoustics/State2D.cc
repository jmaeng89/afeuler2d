/* State2D.cc:  Subroutines for 2D Nonlinear Acoustics Solution State Classes. */

/* Include 2D nonlinear acoustics solution state header file. */
#ifndef _EULER2D_INCLUDED
#include "State2D.h"
#endif // _EULER2D_INCLUDED

double Euler2D::R = 287.15;

/********************************************************
 * Routine: FluxAverage_x                               *
 *                                                      *
 * This function returns the average flux in the        *
 * x-direction using nine solution points and Simpsons  *
 * integration approximation.                           *
 *                                                      *
 ********************************************************/
Euler2D FluxAverage_x(const Euler2D &QL_n,
                      const Euler2D &QM_n,
                      const Euler2D &QR_n,
                      const Euler2D &QL_nhalf,
                      const Euler2D &QM_nhalf,
                      const Euler2D &QR_nhalf,
                      const Euler2D &QL_n1,
                      const Euler2D &QM_n1,
                      const Euler2D &QR_n1){
        Euler2D favgx;
        favgx = 1.0/9.0*( 0.25*(QL_n.Fx() + QR_n.Fx() + QL_n1.Fx() + QR_n1.Fx()) +
                          (QM_n.Fx() + QL_nhalf.Fx() + QR_nhalf.Fx() + QM_n1.Fx()) +
                          4.0*QM_nhalf.Fx() );
        return (favgx);
}


/********************************************************
 * Routine: FluxAverage_y                               *
 *                                                      *
 * This function returns the average flux in the        *
 * y-direction using nine solution points and Simpsons  *
 * integration approximation.                           *
 *                                                      *
 ********************************************************/
Euler2D FluxAverage_y(const Euler2D &QL_n,
                      const Euler2D &QM_n,
                      const Euler2D &QR_n,
                      const Euler2D &QL_nhalf,
                      const Euler2D &QM_nhalf,
                      const Euler2D &QR_nhalf,
                      const Euler2D &QL_n1,
                      const Euler2D &QM_n1,
                      const Euler2D &QR_n1){
        Euler2D favgy;
        favgy = 1.0/9.0*( 0.25*(QL_n.Fy() + QR_n.Fy() + QL_n1.Fy() + QR_n1.Fy()) +
                          (QM_n.Fy() + QL_nhalf.Fy() + QR_nhalf.Fy() + QM_n1.Fy()) +
                          4.0*QM_nhalf.Fy() );
        return (favgy);
}


/********************************************************
 * Routine: FluxAverage_n                               *
 *                                                      *
 * This function returns the average flux in the        *
 * n-direction using nine solution points and Simpsons  *
 * integration approximation.                           *
 *                                                      *
 ********************************************************/
Euler2D FluxAverage_n(const Euler2D &QL_n,
                      const Euler2D &QM_n,
                      const Euler2D &QR_n,
                      const Euler2D &QL_nhalf,
                      const Euler2D &QM_nhalf,
                      const Euler2D &QR_nhalf,
                      const Euler2D &QL_n1,
                      const Euler2D &QM_n1,
                      const Euler2D &QR_n1,
                      const double &nx,
                      const double &ny){
        Euler2D favgn;
        favgn = 1.0/9.0*( 0.25*(QL_n.Fn(nx,ny) + QR_n.Fn(nx,ny) + QL_n1.Fn(nx,ny) + QR_n1.Fn(nx,ny)) +
                          (QM_n.Fn(nx,ny) + QL_nhalf.Fn(nx,ny) + QR_nhalf.Fn(nx,ny) + QM_n1.Fn(nx,ny)) +
                          4.0*QM_nhalf.Fn(nx,ny) );
        return(favgn);
}


/*********************************************************
 * Routine: Rotate                                       *
 *                                                       *
 * This function returns the solution in the local       *
 * rotated frame.                                        *
 *                                                       *
 *********************************************************/
Euler2D Rotate(const Euler2D &Q,
               const double &norm_x,
               const double &norm_y) {
        return Euler2D(Q.d,
                       Q.u*norm_x+Q.v*norm_y,
                       -Q.u*norm_y+Q.v*norm_x,
                       Q.p);

}

/********************************************************
 * Routine: BC_Wall                                     *
 *   (Boundary Condition with Inviscid Wall)            *
 *                                                      *
 * This function returns the boundary flux state for a  *
 * given direction given the primitive solution state   *
 * on the interior of the boundary, and                 *
 * the unit normal vector in the direction of interest. *
 * The pressure outside the boundary is calculated      *
 * and used in determining the boundary flux.           *
 ********************************************************/
Euler2D BC_Wall(const Euler2D &Qi,
                const double &norm_x,
                const double &norm_y) {
        Euler2D Qi_rotated;
        double pb_rotated, pb_x, pb_y, uw;
        double cos_angle, sin_angle;
        
        /* Determine the direction cosine's for the frame
           rotation. */
        cos_angle = norm_x; 
        sin_angle = norm_y;
        uw = Qi.u*sin_angle - Qi.v*cos_angle;
        
        //pb_x = Wi.gm1*(Wi.d*Wi.E() - HALF*Wi.d*uw*uw)*cos_angle;
        //pb_y = Wi.gm1*(Wi.d*Wi.E() - HALF*Wi.d*uw*uw)*sin_angle;
        pb_x = Qi.p*cos_angle;
        pb_y = Qi.p*sin_angle;
        
        /* Return boundary solution flux state. */
        return (Euler2D(ZERO, pb_x, pb_y, ZERO));
}

