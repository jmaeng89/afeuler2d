/* MeshStates.cc:  State functions subroutines for 2D Euler Mesh Class. */

/* Include 2D mesh elements header file. */

#ifndef _MESH_INCLUDED
#include "Mesh2D.h"
#endif // _MESH_INCLUDED

// Linearized states at each node
// First derivatives in reference frame
Euler2D Mesh2D::dQdxi(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        value = Q[0]*(4.0*xi + 4.0*eta - 3.0)
                + Q[5]*(4.0 - 8.0*xi - 4.0*eta)
                + Q[2]*(4.0*xi - 1.0)
                + Q[1]*(4.0*eta)
                + Q[3]*(-4.0*eta)
                + Q[6]*27.0*eta*(ONE - 2.0*xi - eta);
        return (value);
};

Euler2D Mesh2D::dQdeta(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        value = Q[0]*(4.0*xi + 4.0*eta - 3.0) 
                + Q[5]*(-4.0*xi) 
                + Q[1]*(4.0*xi) 
                + Q[4]*(4.0*eta - 1.0) 
                + Q[3]*(4.0 - 4.0*xi - 8.0*eta)
                + Q[6]*27.0*xi*(ONE - xi - 2.0*eta);
        return (value);
};

// Second derivatives in reference frame
Euler2D Mesh2D::dQdxi2(const int &inode) {
        Euler2D value = 4.0*Q[0] - 8.0*Q[5] + 4.0*Q[2] - 54.0*Q[6]*etap(inode);
        return (value);
};

Euler2D Mesh2D::dQdeta2(const int &inode) {
        Euler2D value = 4.0*Q[0] + 4.0*Q[4] - 8.0*Q[3] - 54.0*Q[6]*xip(inode);
        return (value);
};

Euler2D Mesh2D::dQdxieta(const int &inode) {
        Euler2D value = 4.0*(Q[0] - Q[5] + Q[1] - Q[3])
                        + 54.0*Q[6]*(HALF - xip(inode) - etap(inode));
        return (value);
};

// First derivatives in physical frame
Euler2D Mesh2D::dQdx(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        double A1 = J11();     double A2 = J21();
        value = Q[0]*(A1 + A2)*(4.0*xi + 4.0*eta - 3.0)
                + Q[5]*(A1*(4.0 - 8.0*xi - 4.0*eta) + A2*(-4.0*xi))
                + Q[2]*A1*(4.0*xi - 1.0)
                + Q[1]*4.0*(A1*eta + A2*xi)
                + Q[4]*A2*(4.0*eta - 1.0)
                + Q[3]*(A1*(-4.0*eta) + A2*(4.0 - 4.0*xi - 8.0*eta))
                + Q[6]*27.0*(A1*eta*(ONE - 2.0*xi - eta) + A2*xi*(ONE - xi - 2.0*eta));
        return (value);
}

Euler2D Mesh2D::dQdy(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        double A1 = J12();     double A2 = J22();
        value = Q[0]*(A1 + A2)*(4.0*xi + 4.0*eta - 3.0)
                + Q[5]*(A1*(4.0 - 8.0*xi - 4.0*eta) + A2*(-4.0*xi))
                + Q[2]*A1*(4.0*xi - 1.0)
                + Q[1]*4.0*(A1*eta + A2*xi)
                + Q[4]*A2*(4.0*eta - 1.0)
                + Q[3]*(A1*(-4.0*eta) + A2*(4.0 - 4.0*xi - 8.0*eta))
                + Q[6]*27.0*(A1*eta*(ONE - 2.0*xi - eta) + A2*xi*(ONE - xi - 2.0*eta));
        return (value);
}

// Second derivatives in physical frame
Euler2D Mesh2D::dQdxx(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        double A1 = J11();     double A2 = J21();
        value = Q[0]*(A1+A2)*(A1+A2)*4.0
                + Q[5]*(A1*A1*(-8.0) + A1*A2*(-8.0))
                + Q[2]*A1*A1*4.0
                + Q[1]*A1*A2*8.0
                + Q[4]*A2*A2*4.0
                + Q[3]*(A1*A2*(-8.0) + A2*A2*(-8.0))
                + Q[6]*54.0*(A1*A1*(-eta) + A2*A2*(-xi) + A1*A2*(ONE - 2.0*xi - 2.0*eta));
        return (value);
}

Euler2D Mesh2D::dQdyy(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        double A1 = J12();     double A2 = J22();
        value = Q[0]*(A1+A2)*(A1+A2)*4.0
                + Q[5]*(A1*A1*(-8.0) + A1*A2*(-8.0))
                + Q[2]*A1*A1*4.0
                + Q[1]*A1*A2*8.0
                + Q[4]*A2*A2*4.0
                + Q[3]*(A1*A2*(-8.0) + A2*A2*(-8.0))
                + Q[6]*54.0*(A1*A1*(-eta) + A2*A2*(-xi) + A1*A2*(ONE - 2.0*xi - 2.0*eta));
        return (value);
}

Euler2D Mesh2D::dQdxy(const int &inode) {
        Euler2D value(ZERO,ZERO,ZERO,ZERO);
        double xi = xip(inode);
        double eta = etap(inode);
        double A1 = J11();     double A2 = J21();
        double A3 = J12();     double A4 = J22();
        value = Q[0]*(A1+A2)*(A3+A4)*4.0
                + Q[5]*(A1*A3*(-8.0) + (A1*A4+A2*A3)*(-4.0))
                + Q[2]*A1*A3*4.0
                + Q[1]*(A1*A4+A2*A3)*4.0
                + Q[4]*A2*A4*4.0
                + Q[3]*(A2*A4*(-8.0) + (A1*A4+A2*A3)*(-4.0))
                + Q[6]*27.0*(A1*A3*(-2.0*eta) + A2*A4*(-2.0*xi) + (A1*A4+A2*A3)*(ONE - 2.0*xi - 2.0*eta));
        return (value);
}


// Q at arbitrary (xi,eta) in triangle
Euler2D Mesh2D::Qtri(const double &xi, const double &eta) {
        Euler2D Qin;
        Qin = Q[0]*(1.0 - 3.0*xi + 2.0*xi*xi - 3.0*eta + 4.0*xi*eta + 2.0*eta*eta) + 
                Q[5]*(4.0*xi - 4.0*xi*xi - 4.0*xi*eta) +
                Q[2]*(2.0*xi*xi - xi) +
                Q[1]*(4.0*xi*eta) +
                Q[4]*(2.0*eta*eta - eta) +
                Q[3]*(4.0*eta - 4.0*xi*eta - 4.0*eta*eta) +
                Q[6]*27.0*xi*eta*(1.0 - xi - eta);
    
        return (Qin);
};

// Q along Edge 1
Euler2D Mesh2D::Qedge1(const double &sig) {
        Euler2D Qedge;
        Qedge = Qtri(1.0-sig, sig);
        return (Qedge);
};

// Q along Edge 2
Euler2D Mesh2D::Qedge2(const double &sig) {
        Euler2D Qedge;
        Qedge = Qtri(ZERO, 1.0-sig);
        return (Qedge);
};

// Q along Edge 3
Euler2D Mesh2D::Qedge3(const double &sig) {
        Euler2D Qedge;
        Qedge = Qtri(sig, ZERO);
        return (Qedge);
};

