EXE = ./afEuler
SDIR = ./src
SACDIR = ./srcAcoustics
ODIR = ./obj

# tecplot libraries
#TECHOME = "/Applications/Tecplot 360 EX 2017 R1/Tecplot 360 EX 2017 R1.app/Contents"
TECHOME = tecio
TECINCL = $(TECHOME)/include
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	#TECIOLIB = $(TECHOME)/MacOS/libtecio.dylib
	TECIOLIB = $(TECHOME)/mac/lib/libtecio.dylib
	TECIOEXTRALIB = -lstdc++
else
	TECIOLIB = $(TECHOME)/linux/lib/libtecio.so
	FOUND_INSTALLED_LIBSTDCXX_S:=$(shell test -f $(TECHOME)/bin/libstdc++.so.6 && echo found || echo missing)
	ifeq ($(FOUND_INSTALLED_LIBSTDCXX_S),found)
		TECIOEXTRALIB = $(TECHOME)/bin/libstdc++.so.6
	else
		TECIOEXTRALIB = -lstdc++
	endif
endif
LINKLIBS = -lpthread

# set default variables
ifndef DBG_LEVEL
	DBG_LEVEL = run
endif
ifndef VERBOSE
	VERBOSE = clean
endif
ifndef MACHINE
	MACHINE = glg20
endif

FC = gfortran
BASEFLAGS = -fdefault-real-8 -cpp -falign-commons  \
			-ffpe-trap=invalid,zero,overflow,underflow -fcray-pointer -lstdc++ 
CC = g++
CBASEFLAGS = -lgfortran

# add optimization/debug flags
ifeq ($(DBG_LEVEL),debug)
	DBGFLAGS = -O0 $(BASEFLAGS) -g3 -pedantic -Wall -Wextra\
		   -fbacktrace -fbounds-check -fcheck=all
	CDBGFLAGS = -O0 $(CBASEFLAGS) -g3 -pedantic -Wall -Wextra\
		   -fbounds-check
else
	DBGFLAGS = -O3 $(BASEFLAGS) 
	CDBGFLAGS = -O3 $(CBASEFLAGS) 
endif

# add precompiler directives
ifeq ($(VERBOSE),verb)
	VFLAGS = $(DBGFLAGS) -DVERBOSE
	CVFLAGS = $(CDBGFLAGS) -DVERBOSE
else
	VFLAGS = $(DBGFLAGS)
	CVFLAGS = $(CDBGFLAGS)
endif

# fake Fortran 2008 support
ifeq ($(MACHINE),nyx)
	FFLAGS = $(VFLAGS) -DFORT2K8
	CFFLAGS = $(CVFLAGS) -DFORT2K8
else
	FFLAGS = $(VFLAGS)
	CFFLAGS = $(CVFLAGS)
endif

# Acoustics source code ( order matters )
FACMODS = $(ODIR)/State2D.o $(ODIR)/Mesh2D.o \
		  $(ODIR)/acousticsmodules.o

# Advection Modules ( order matters )
FMODS = $(ODIR)/solverVars.o $(ODIR)/physics.o $(ODIR)/mathUtil.o \
		$(ODIR)/meshUtil.o $(ODIR)/analyticFunctions.o \
		$(ODIR)/iterativeFunctions.o $(ODIR)/reconstruction.o \
		$(ODIR)/boundaries.o $(ODIR)/update.o $(ODIR)/timeControl.o \
		$(ODIR)/inputOutput.o $(ODIR)/tecplotVisualUtil.o \
		$(ODIR)/startStop.o $(ODIR)/postProc.o

# Object files for main program
FOBJS = $(FMODS) $(FACMODS) \
	$(ODIR)/afEuler.o

# Object files for error calculation
EOBJS = $(FMODS) $(FACMODS) \
	$(ODIR)/postProc.o

# Object files for test program
TOBJS = $(FMODS) $(FACMODS) \
	$(ODIR)/afTest.o

# Make types
all : $(EXE) 

afErr: $(EOBJS) $(ODIR)/afError.o
	$(FC) $(FFLAGS) -o afError $^ $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS)

afTest: $(TOBJS) $(ODIR)/afTest.o
	$(FC) $(FFLAGS) -o afTest $^ $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS)

$(EXE) : $(FOBJS)
	$(FC) $(FFLAGS) -o $@ $^ -I$(TECINCL) $(TECIOLIB) $(TECIOEXTRALIB) $(LINKLIBS) 

$(ODIR)/%.o : $(SDIR)/%.f90
	$(FC) $(FFLAGS) -c -o $@ -I$(TECINCL) -J$(ODIR) $^

# acoustics source code compile instruction
$(ODIR)/%.o : $(SACDIR)/%.cc
	$(CC) $(CFFLAGS) -c -o $@ $^

# phony targets 
.PHONY : realclean clean debug verbose nyx distro

realclean :
	@-rm -vf afError
	@-rm -vf afTest
	@make clean 
clean :
	@-rm -vf $(EXE) $(ODIR)/*.o $(ODIR)/*.mod $(ODIR)/*genmod.f90 
debug :
	@make all DBG_LEVEL=debug
verbose :
	@make all DBG_LEVEL=debug VERBOSE=verb
