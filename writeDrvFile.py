#-------------------------------------------------------------------------------
# @ purpose
#  Write driver files to run Active Flux Euler code
#
# @ history
#  29 March 2016 - Initial creation (Maeng)

# required libraries
import sys, os, numpy, subprocess

# base path for cases
basePath = 'cases/euler_mvortex_unst';

# driver file names
#drvFileList = ['diagL000', 'diagL001', 'diagL002', 'diagL003', 'diagL004']
#drvFileList = ['unstL000', 'unstL001', 'unstL002', 'unstL003', 'unstL004']
#drvFileList = ['diag002', 'diag008', 'diag032', 'diag128', 'diag512']
drvFileList = ['unst002', 'unst008', 'unst032', 'unst128', 'unst512']
# high order workshop vortex meshes
#drvFileList = ['vort000', 'vort001', 'vort002', 'vort003', 'vort004', 'vort005']
#drvFileList = ['dvort000', 'dvort001', 'dvort002', 'dvort003', 'dvort004', 'dvort005']

# mesh file list. Unless otherwise listed, the same name as driver file is assumed.
meshFileList = drvFileList;
#meshFileList = ['unstL000', 'unstL001', 'unstL002', 'unstL003', 'unstL004']
#meshFileList = ['unst032', 'unst032', 'unst032', 'unst032', 'unst032']
#meshFileList = ['diag032', 'diag032', 'diag032', 'diag032', 'diag032']

# boundary condition file
bcCond = 'periodic.bc'; # periodic in all directions
#bcCond = 'xPeryOut.bc'; # periodic in x-dir and outflow in y-dir
#bcCond = 'xOutyPer.bc'; # periodic in x-dir and outflow in y-dir
#bcCond = 'outflow.bc'; # outflow in all directions
#bcCond = 'wall.bc'; # outflow in all directions

# governing equation
gvrnEq = 'euler'; # full Euler equations
#gvrnEq = 'acoustic'; # full Euler equations
#gvrnEq = 'isenteuler'; # isentropic Euler equations
#gvrnEq = 'pleuler'; # pressureless Euler equations
#gvrnEq = 'advection'; # linear advection equation

# initial condition
#initSol = 'constVec';
#initSol = 'sinusoid';
#initSol = 'gaussian';
#initSol = 'solidRot';
#initSol = 'entWave';
#initSol = 'testWave';
#initSol = 'pless1';
#initSol = 'pless2';
initSol = 'mvortex';
#initSol = 'svortex';
#initSol = 'mvortv2';
#initSol = 'svortv2';
#initSol = 'fastvort';
#initSol = 'slowvort';
#initSol = 'sod1D';
#initSol = 'sod2D';

# time related parameters
tFinal = 10.0;
#tFinal = 5.759051207664e-4; # fast vortex in HO workshop
cfl = 0.4;
outputFreq0 = 100; # solution output frequency for coarsest mesh

# driver file printing parameter
# approximate element area refinement factor between consecutive files
refinmtFactor = 2;
#
# check if directory exists, and create if it does not
if ( not os.path.isdir(basePath) ):
    os.makedirs(basePath);

# write driver files
i = 0; # counter
for drvFile in drvFileList:

    # define variables
    meshFile = meshFileList[i];
    factor = refinmtFactor**(i);
    outputFreq = outputFreq0*factor;
    #nIter = nIter0*factor;
    #dtIn = tFinal/nIter;

    fileName = basePath + '/' + drvFile + '.drv'
    print 'writing ', fileName
    fid = open(fileName, 'w');
    fid.write( '# AF solver input file \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# FILE names (do NOT follow with tabs) \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( basePath + '    # base path \n' );
    fid.write( drvFile + '.plt' + '    # centroid average file \n' );
    fid.write( drvFile + '.dat' + '    # reconstruction file \n' );
    fid.write( 'grids/' + meshFile + '.grd' + '     # mesh file \n' );
    fid.write( 'grids/' + bcCond + '     # boundary condition file \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Solution parameters \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( gvrnEq + '    # governing equation \n' );
    fid.write( initSol + '    # initial solution \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Time-step parameters \n' );
    fid.write( '# ---------------------- \n' );
    #dtInTemp = '%.4e' % dtIn;
    #fid.write(  dtInTemp + '    # Time step \n' );
    #nIterTemp = '%d' % nIter;
    #fid.write( nIterTemp + '    # Number of iterations \n' );
    tFinalTemp = '%.12f' % tFinal;
    fid.write( tFinalTemp + '    # final simulation time \n' );
    cflTemp = '%.4e' % cfl;
    fid.write(  cflTemp + '    # Courant number \n' );
    fid.write( ' \n' );
    fid.write( '# ---------------------- \n' );
    fid.write( '# Output parameters \n' );
    fid.write( '# ---------------------- \n' );
    outputFreqTemp = '%d' % outputFreq;
    fid.write( outputFreqTemp + '  # Frequency to output solution \n' );
    fid.write( ' \n' );
    fid.write( '# ------------------------------------------------------------ \n' );
    fid.write( '# Solution input file name. Input path + recon. data file name\n' );
    fid.write( '# If following line is empty, no file will be read ----------- \n' );
    fid.close()

    i = i + 1

