function[] = plotResidConvergence(baseDir,fileNames)
% PLOTRESIDCONVERGENCE(BASEDIR,FILENAMES) plots the residual 
%   convergence history. 
%
%   J. Brad Maeng
%   3/31/2016 - created
%
    close all;
    
    baseDir = '../cases/linadv_test/sinusoid_BF_short';
    baseDir = '../cases/linadv_test/sinusoid_rec1_short';
    baseDir = '../cases/linadv_test/sinusoid_rec5_short';
    baseDir = '../cases/linadv_test/sinusoid_rec1_diag';
    baseDir = '../cases/linadv_test/sinusoid_rec5_diag';
    baseDir = '../cases/linadv_test/sinusoid_BF_diag';
    baseDir = '../cases/linadv_test/sinusoid_BF';
    baseDir = '../cases/linadv_test/sinusoid_rec1';
    baseDir = '../cases/linadv_test/sinusoid_rec5';

    baseDir = '../cases/euler_test/entWave_test';
    baseDir = '../cases/linadv_test/sinusoid_test';

    baseDir = '../cases/euler_test/mvortex_rec1_diag';
    baseDir = '../cases/euler_test/mvortex_rec5';
    baseDir = '../cases/euler_test/mvortex_rec1';
    baseDir = '../cases/euler_test/mvortex_rec6';

    baseDir = '../cases/linadv_test/sinusoid_norec';
    baseDir = '../cases/linadv_test/sinusoid_norec_trapez';
    baseDir = '../cases/linadv_test/sinusoid_rec2';
    baseDir = '../cases/linadv_test/sinusoid_rec1';

    baseDir = '../cases/linadv_test/sinusoid_rec5';
    baseDir = '../cases/linadv_test/sinusoid_rec6';
    baseDir = '../cases/linadv_test/sinusoid_rec7';

    baseDir = '../cases/euler_test/mvortex_rec6';
    baseDir = '../cases/euler_test/mvortex_rec6_diag';
    baseDir = '../cases/euler_test/mvortex_rec7_diag';
    baseDir = '../cases/euler_test/mvortex_rec7';
    
    baseDir = '../cases/linadv_test/sinusoid_rec5_1';
    baseDir = '../cases/euler_test/mvortex_rec5_1';

    % residual
    %fileNames = {'residunst002','residunst008','residunst032','residunst128','residunst512'};

    % error in time
    fileNames = {'auxdiag002','auxdiag008','auxdiag032','auxdiag128','auxdiag512'};
    fileNames = {'aux2diag002','aux2diag008','aux2diag032','aux2diag128','aux2diag512'};

    fileNames = {'auxunst002','auxunst008','auxunst032','auxunst128','auxunst512'};
    fileNames = {'aux2unst002','aux2unst008','aux2unst032','aux2unst128','aux2unst512'};

    % output flag
    outputOn = false;
    %outputOn = true;

    [~,fileNum] = size(fileNames);
        
    % legend names
    legendNames = {'1\Delta x', '2 \Delta x', '2^2 \Delta x', '2^3 \Delta x','2^4\Delta x'};   
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [baseDir,'/',fileNames{i},'.dat'];     % complete file location
        [fIter, fT, fResid, fResidMax] = readResidSol(FN);
        iT{i} = fIter(2:end);
        t{i} = fT(2:end); 
        resid{i} = fResid(:,2:end);
        residMax{i} = fResidMax(:,2:end);  
    end
    
    h = [0.376621788577355E-01,0.199720586631353E-01,0.101760418478777E-01,0.509839244988350E-02,0.253115893306108E-02];
    [nEqns,totalLevs] = size(resid{1});
    %nEqns = 1;

    style = ['bo-';'ro-';'ko-';'go-';'mo-'];
    style = ['b-';'r-';'k-';'g-';'m-'];
    lWidth = 2;
    fSize = 16;
    
    % u residual convergence history
    for i = 1:fileNum
        for iEq = 1:nEqns
            figure(iEq)
            %semilogy(t{i}(:),resid{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            loglog(t{i}(:),resid{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            %loglog(t{i}(:),resid{i}(iEq,:)/(h(i)^2),style(i,:),'linewidth',lWidth,'markersize',fSize)
            %loglog(t{i}(:),residMax{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            hold on
        end 
    end
    hx = xlabel('Time'); 
    %hy = ylabel('|Resid|_1'); 
    hy = ylabel('|\epsilon|^1_p');
    legend([legendNames],'location','best')
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    %for i = 1:fileNum
    %    loglog(t{i}(:),(t{i}(:)).^1/(t{1}(end)^1)*resid{i}(1,end),'k--','linewidth',lWidth) 
    %end
    hold off
    if outputOn
        %fname1 = [baseDir,'/','errorTimeHistoryL1.eps'];
        fname1 = [baseDir,'/','prim_err_timehist.eps'];
        print('-depsc2',fname1);    
    end

    % maximum residual convergence history
    for i = 1:fileNum
        for iEq = 1:nEqns
            figure(iEq+nEqns)
            loglog(t{i}(:),residMax{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            %loglog(t{i}(:),residMax{i}(iEq,:)/(h(i)^2),style(i,:),'linewidth',lWidth,'markersize',fSize)
            hold on
        end 
    end
    hx = xlabel('Time'); 
    %hy = ylabel('|Resid|max');  
    hy = ylabel('|\epsilon|^1_c'); 
    legend([legendNames],'location','best')
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    %for i = 1:fileNum
    %    loglog(t{i}(:),(t{i}(:)).^1/(t{1}(end)^1)*residMax{i}(1,end),'k--','linewidth',lWidth) 
    %end
    hold off
    if outputOn
        %fname1 = [baseDir,'/','discrepancyTimeHistoryL1.eps'];
        fname1 = [baseDir,'/','cons_err_timehist.eps'];
        print('-depsc2', fname1);    
    end

    %
    h = [2^-1, 2^-2, 2^-3, 2^-4, 2^-5];
    for iEq = 1:nEqns
        fprintf('Equation %5d \n', iEq);
        fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','err_1','Order','err_2','Order')
        fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
        for i = 1:fileNum
            if i == 1
                fprintf('%5d  %e   %e   %5s   %e    %7s \n',i,h(i),resid{i}(iEq,end),'',residMax{i}(iEq,end),'');
            else
                fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                    i,h(i),resid{i}(iEq,end), ...
                    log(resid{i-1}(iEq,end)/resid{i}(iEq,end))/log(h(i-1)/h(i)), ...
                    residMax{i}(iEq,end), ... 
                    log(residMax{i-1}(iEq,end)/residMax{i}(iEq,end))/log(h(i-1)/h(i)))
            end 

        end
    end 

end

function [fIter, fT, fResid, fResidMax] = readResidSol(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE
    
    A = load(file);

    [totalLevs, cols] = size(A);
    nEqns = (cols-2)/2;
    
    % initialize output variables
    fIter = zeros(totalLevs,1); 
    fT = zeros(totalLevs,1);
    fResid = zeros(nEqns,totalLevs,1);
    fResidMax = zeros(nEqns,totalLevs,1);

    for lev = 1:totalLevs
        fIter(lev) = A(lev,1); 
        fT(lev) = A(lev,2);
        for iEq = 1:nEqns
            fResid(iEq,lev) = A(lev,2+iEq);
            fResidMax(iEq,lev) = A(lev,2+nEqns+iEq);
        end
    end

end
