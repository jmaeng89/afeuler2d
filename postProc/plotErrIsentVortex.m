function[] = plotErrIsentVortex(baseDir,caseDir)
% PLOTERRISENTVORTEX(BASEDIR,CASEDIR) returns the vortex peak error convergence
%   result for isentropic vortex problem solved by Euler solver
%
%   J. Brad Maeng
%
%   4/22/2016 - created
%   1/18/2017 - modified to output extrema error as well as L1 norm error
%
    close all;

    baseDir = '../../euler2d_BF/cases/euler_test/mvortex_diagL';    
    baseDir = '../../euler2d_BF/cases/euler_test/mvortex_unstL';    

    baseDir = '../cases/euler_test/mvortex_diagL';    
    baseDir = '../cases/euler_test/mvortex_unstL';    

    baseDir = '../../dg_euler2d/cases/euler/mvortex_diagL_p1';    
    baseDir = '../../dg_euler2d/cases/euler/mvortex_diagL_p2';    

    baseDir = '../../dg_euler2d/cases/euler/mvortex_unstL_p1';    
    baseDir = '../../dg_euler2d/cases/euler/mvortex_unstL_p2';    


    caseDir = {''};

    % figure output flag
    figOutputOn = false;
    %figOutputOn = true;

    % table output flag
    tabOutputOn = false;
    %tabOutputOn = true;

    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            FN1 = [baseDir,'/','vortexExtremaData','.dat'];    
            FN2 = [baseDir,'/','vortexErrorData','.dat'];     
        else
            FN1 = [baseDir,caseDir{i},'/','vortexExtremaData','.dat'];    
            FN2 = [baseDir,caseDir{i},'/','vortexErrorData','.dat'];     
        end 
        [fh, fxMin0, fqPt0, fxMin, fqPt] = readVortexExtremaData(FN1);
        xMin0{i} = fxMin0;  % minimum pressure location, initial     
        xMin{i} = fxMin;    % minimum pressure location, final 
        qPt0{i} = fqPt0;    % state variables at minimum pressure location, initial
        qPt{i} = fqPt;      % state variables at minimum pressure location, final   

        % evaluate error quantities
        for lev = 1:size(fqPt,2)
            for iEq = 1:size(fqPt,1)
                AmpErr(iEq) = abs(fqPt(iEq,lev)-fqPt0(iEq,lev)); %/fqPt0(iEq,lev);
                %PhsErr(iEq) = abs(norm(fxMin(:,lev)));
            end
            AmpError{i}(:,lev) = AmpErr(:);
            %PhsError{i}(:,lev) = PhsErr(:);
        end 

        % extra relative error quantities for vortex problem 
        %[fh, fL1, fL2] = readErrData(FN2);
        [fh, fDOF, fL1, fL2] = readErrData(FN2);
        h{i} = fh;          % 1/dof^(-1/2)
        DOF{i} = fDOF;          % dof
        L1{i} = fL1; % L1 norm of error in density, pressure, velocity magnitude, entropy
        L2{i} = fL2; % L2 norm of error in density, pressure, velocity magnitude, entropy

    end

    [nEqns,totalLevs] = size(qPt{1});
    % refinement levels
    refLevel = 5;
    % hard code
    %nEqns = 4;
    % variables names
    pvNames = {'\rho', 'u', 'v', 'p'};
     
    fprintf('\n \n ISENTROPIC VORTEX EXTREMA CONVERGENCE RESULTS');
    if tabOutputOn
        % open a file to store the convergence study data
        for i = 1:fileNum
            [fid] = fopen([baseDir,'/','vortexExtremaErrConvergence',caseDir{i},'.dat'],'w+');
            for iEq = 1:nEqns
                for lev = 1:totalLevs
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s  %7s  %12s  %8s  %7s  %7s %s\n', ...
                                'Level','DOF','Amp Error_max','Order','Pxmin','Pymin', '\\');
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4s %s %4.2e %s %4.2e %s\n', ...
                                lev,'&',DOF{i}(lev),'&',AmpError{i}(iEq,lev),'&','','&', ...
                                xMin{i}(1,lev),'&',xMin{i}(2,lev),'\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf(fid, '%5d %s %5.3e %s %5.3e %s %7s %s %5.3e %s %7s %s\n',...
                                lev,'&',DOF{i}(lev),'&',AmpError{i}(iEq,lev),'&', ...
                                ' ','&', ...
                                xMin{i}(1,lev),'&', ...
                                xMin{i}(2,lev), '\\');
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %.2f %s %4.2e %s %4.2e %s\n',...
                                lev,'&',DOF{i}(lev),'&',AmpError{i}(iEq,lev),'&', ...
                                log(AmpError{i}(iEq,lev-1)/AmpError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
                                xMin{i}(1,lev),'&', xMin{i}(2,lev), '\\');
                    end
                end
            end
            fclose(fid);    
        end
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %7s  %7s  %7s  %4s  %7s  %7s\n', ...
                            'Level','DOF','h','Amp Error_max','Order','Pxmin','Pymin')
                    fprintf('%5s  %7s  %7s  %7s  %4s  %7s  %7s\n', ...
                            '-----','---------','---------','---------','----','---------','----')
                    fprintf('%5d  %4.2e  %4.2e  %4.2e  %4s  %4.2e  %4.2e \n', ...
                            lev,DOF{i}(lev),h{i}(lev),AmpError{i}(iEq,lev),'',xMin{i}(1,lev),xMin{i}(2,lev));
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %4.2e  %4.2e   %4.2e  %4s  %4.2e  %4.2e\n',...
                            lev,DOF{i}(lev),h{i}(lev),AmpError{i}(iEq,lev), ...
                            '', xMin{i}(1,lev), xMin{i}(2,lev))
                else
                    fprintf('%5d  %4.2e  %4.2e  %4.2e  %.2f  %4.2e  %4.2e\n',...
                            lev,DOF{i}(lev),h{i}(lev),AmpError{i}(iEq,lev), ...
                            log(AmpError{i}(iEq,lev-1)/AmpError{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            xMin{i}(1,lev), ... 
                            xMin{i}(2,lev))
                end
            end
        end
    end
    
    % legend names
    legendNames = {pvNames{1},pvNames{4}};

    hRef = h{1};
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    style2 = ['bo-.';'rs-.';'k*-.';'gd-.';'m<-.'];
    lWidth = 2;
    fSize = 18;
    % AmpError norm error
    for i = 1:fileNum
        figure(1)
        for iEq = [1,4]
            if ( i == 1 )
                loglog(h{i}(:),AmpError{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            else
                loglog(h{i}(:),AmpError{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
            end

            hold on
            if ( iEq == nEqns && i == fileNum )
                %loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*AmpError{i}(1,1),'k--','linewidth',lWidth)    % 1st order
                %loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*AmpError{i}(1,1),'k-','linewidth',lWidth)    % 3rd order
                loglog(hRef(:),0.05*(hRef(:)).^3/(hRef(1)^3)*AmpError{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.05*(hRef(end)).^3/(hRef(1)^3)*AmpError{i}(1,1),'3','FontSize',fSize)
                legend(legendNames,'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('Peak Amplitude |\epsilon|_\infty');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if figOutputOn
                    fname1 = [baseDir,'/','peakAmpError.eps'];
                    print('-depsc2', '-r300', fname1);
                end
            end
        end
    end

    
    %
    %
    %
    %
    %
    %
    %
    %
    %% plot vortexErrorData

    [nEqns,totalLevs] = size(L1{1});
    % refinement levels
    refLevel = 5;
    % hard code
    pvNames = {'\rho', 'p', '|v|', 's'};
    legendNames = pvNames;

    fprintf('\n \n ISENTROPIC VORTEX VARIABLE CONVERGENCE RESULTS ');
    if tabOutputOn
        % open a file to store the convergence study data
        for i = 1:fileNum
            [fid] = fopen([baseDir,'/','isentVortexErrConvergence',caseDir{i},'.dat'],'w+');
            for iEq = 1:nEqns
                for lev = 1:totalLevs
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s %9s  %9s   %9s  %4s  %9s  %4s\n', ...
                                     'Level','DOF','h','L1Error','Order','L2Error','Order');
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s %4.2e %s %4s %s\n', ...
                                     lev,'&',DOF{i}(lev), '&', h{i}(lev),'&',L1{i}(iEq,lev),'&','','&',L2{i}(iEq,lev),'&','','\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %4s %s %4.2e %s %4s %s\n',...
                            lev,'&', DOF{i}(lev), '&',h{i}(lev),'&',L1{i}(iEq,lev),'&', ...
                            '    ','&', ...
                            L2{i}(iEq,lev), '&', ...
                            '    ', '\\')
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %4.2e %s %4.2e %s %4.2e %s %.2f %s %4.2e %s %.2f %s\n',...
                                lev,'&',DOF{i}(lev),'&',h{i}(lev),'&',L1{i}(iEq,lev),'&', ...
                                log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
                                L2{i}(iEq,lev),'&', ...
                                log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
            fclose(fid);    
        end
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %7s  %7s  %7s  %4s  %7s  %4s\n', ... 
                            'Level','DOF','h','L1Error','Order','L2Error','Order')
                    fprintf('%5s  %7s  %7s  %7s  %4s  %7s  %4s\n', ...
                            '-----','-----','-----','---------','----','---------','----')
                    fprintf('%5d  %4.2e  %4.2e  %4.2e  %4s  %4.2e  %4s \n', ...
                            lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev),'',L2{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %4.2e  %4.2e  %4s  %4.2e  %4s\n',...
                        lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev), ...
                        '       ', ...
                        L2{i}(iEq,lev), ...
                        '       ')
                else
                    fprintf('%5d  %4.2e  %4.2e  %4.2e  %.2f  %4.2e  %.2f\n',...
                            lev,DOF{i}(lev),h{i}(lev),L1{i}(iEq,lev), ...
                            log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            L2{i}(iEq,lev), ... 
                            log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end

    % L1 norm error
    for i = 1:fileNum
        figure(3)
        %figure(i)
        for iEq = 1:nEqns
            if ( i == 1 )
                loglog(h{i}(:),L1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            else
                loglog(h{i}(:),L1{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
            end 
            hold on
            if ( iEq == nEqns && i == fileNum  )
                loglog(hRef(:),0.01*(hRef(:)).^3/(hRef(1)^3)*L2{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.01*(hRef(end)).^3/(hRef(1)^3)*L2{i}(1,1),'3','FontSize',fSize)
                legend([legendNames],'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_1');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if figOutputOn
                    fname1 = [baseDir,'/','isentvort_errorL1.eps'];
                    print('-depsc2', '-r300', fname1);
                end
            end
        end
    end
    
    % L2 norm error
    for i = 1:fileNum
        figure(4)
        %figure(i+fileNum)        
        for iEq = 1:nEqns
            %figure(iEq)
            if ( i == 1 ) 
                loglog(h{i}(:),L2{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            else
                loglog(h{i}(:),L2{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
            end 
            hold on
            if ( iEq == nEqns && i == fileNum )
                loglog(hRef(:),0.05*(hRef(:)).^3/(hRef(1)^3)*L2{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.05*(hRef(end)).^3/(hRef(1)^3)*L2{i}(1,1),'3','FontSize',fSize)
                legend([legendNames],'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if figOutputOn
                    fname2 = [baseDir,'/','isentvort_errorL2.eps'];
                    print('-depsc2', '-r300', fname2);
                end
            end
        end
    end
    
end

function [h, xmin0, qpt0, xmin, qpt] = readVortexExtremaData(file)
% solvortex(file) returns vortex peak data 

    a = load(file);

    [totallevs, cols] = size(a);
    
    % initialize output variables
    ncells = zeros(totallevs,1);
    dof = zeros(totallevs,1);
    h = zeros(totallevs,1); 
    xmin0 = zeros(2,totallevs);
    xmin = zeros(2,totallevs);
    qpt0 = zeros(4,totallevs);
    qpt = zeros(4,totallevs);
        
    for lev = 1:totallevs
        %h(lev) = a(lev,3);   % cell size, 1/sqrt(dof)
        ncells(lev) = a(lev,1);   % ncells
        dof(lev) = a(lev,2);   % ncells + nedges + nnodes
        %h(lev) = 1.0/sqrt(a(lev,2)-a(lev,1));   % cell size, 1/sqrt(dof)
        h(lev) = 1.0/sqrt(3.0*a(lev,1));   % cell size, 1/sqrt(dof)
        xmin0(:,lev) = a(lev,4:5);
        qpt0(:,lev) = a(lev,6:9);
        xmin(:,lev) = a(lev,10:11);
        qpt(:,lev) = a(lev,12:15);
    end
            
end

function [h, DOF, L1, L2] = readErrData(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;

    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
 
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
 
end

