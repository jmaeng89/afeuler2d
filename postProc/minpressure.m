function[] = minpressure() 

baseDir = '../cases/euler/inviscidmvort_diag';

% raw data
A = load([baseDir,'/residdiag008.dat']);
B = load([baseDir,'/residdiag032.dat']);
C = load([baseDir,'/residdiag128.dat']);
D = load([baseDir,'/residdiag512.dat']);

pInf = 4/1.4;
[a,b] = size(A(:,2));
At(1) = A(2,2);
Ap(1) = A(2,6)/pInf;
j = 2;
for i = 3:250:a
    At(j) = A(i,2);
    Ap(j) = A(i,6)/pInf;
    j = j + 1;
end

[a,b] = size(B(:,2));
Bt(1) = B(2,2);
Bp(1) = B(2,6);
j = 2;
for i = 3:500:a
    Bt(j) = B(i,2);
    Bp(j) = B(i,6);
    j = j + 1;
end

[a,b] = size(C(:,2));
Ct(1) = C(2,2);
Cp(1) = C(2,6);
j = 2;
for i = 3:1000:a
    Ct(j) = C(i,2);
    Cp(j) = C(i,6);
    j = j + 1;
end


lWidth = 2;
fSize = 18;

figure(123)
plot(At, Ap, 'r-', 'linewidth', lWidth)
%plot(A(:,2), A(:,6), 'r-', 'linewidth', lWidth)
hold on;
plot(Bt, Bp, 'g-', 'linewidth', lWidth)
plot(Ct, Cp, 'b-', 'linewidth', lWidth)
axis([0,50,0,1]);
hx = xlabel('t'); hy = ylabel('min(p)/p_\infty');
legend('Coarse','Medium','Fine')
set(gca,'FontSize',fSize)
set(hx,'FontSize',fSize)
set(hy,'FontSize',fSize)

%fname1 = [baseDir,'/','minPressure_timehist.eps'];
%print('-depsc2', '-r300', fname1);


