function[] = plotErrFastVortex(baseDir,caseDir)
% PLOTERRFASTVORTEX(BASEDIR,CASEDIR) returns the error convergence
%   for fast vortex problem solved using Euler solver. Compared to various results from different schemes.
%
%   J. Brad Maeng
%   28/4/2016 - Initial creation
%
    close all;

    %baseDir = '../cases/euler_test/fastvort_diag';    
    baseDir = '../cases/euler/fastvort_unst';    

    caseDir = {''};

    % output flag
    outputOn = false;
    outputOn = true;
 
    % variables names
    pvNames = {'\rho', 'u', 's'};
 
    % legend names
    %legendNames = {'AF', 'FVM2', 'DG1', 'DG2'};
    legendNames = {'AF', 'DG1', 'DG2'};
    
    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            FN = [baseDir,'/','vortexErrorData','.dat'];     % complete file location     
            %FN = [baseDir,'/','vortexErrorData_newdof','.dat'];     % complete file location     
        else
            FN = [baseDir,caseDir{i},'/','vortexErrorData','.dat'];     
        end 
        %[fh, fL1, fL2] = readVortexError(FN);
        [fh, fL1, fL2] = readErrData(FN);
        h{i} = fh;            % 1/dof^(-1/2)
        L1{i}(1,:) = fL1(1,:); % density
        L1{i}(2,:) = fL1(3,:); % velocity magnitude
        L1{i}(3,:) = 287.15/(1.4-1).*fL1(4,:); % entropy
        L2{i}(1,:) = fL2(1,:); % density
        L2{i}(2,:) = fL2(3,:); % velocity magnitude
        L2{i}(3,:) = 287.15/(1.4-1).*fL2(4,:); % entropy

    end

    WU{1} = h{1};
    WU{1}(1:4,1) = h{1}(:).^-2*2/(1); 

    [nEqns,totalLevs] = size(L1{1});
    % refinement levels
    nEqns = 3;
    refLevel = 5;

    % Reference L2 norm error results from FVM and DG 
    hFVM = [2.491214327699374e-2, 1.248095912401898e-2, 6.252948167622722e-3, 3.109274419318637e-3, 1.557743406554327e-3];
    L2FVM(1,:) = [2.659998390375310e-4, 1.358874822805313e-4, 3.489171718867833e-5, 7.543326410043008e-6, 1.828982401240867e-6];
    L2FVM(2,:) = [1.086204511318988e+0, 4.602390085034128e-1, 1.281547791768717e-1, 3.637252747971093e-2, 9.383734048371296e-3];
    L2FVM(3,:) = [4.157043129819266e-2, 2.531132714935500e-2, 5.713550592658714e-3, 9.686967558720993e-4, 1.437005940663911e-4];
    workunitFVM = (hFVM.^-2)*2/1; % DOF*(Stages per time step)/(max CFL) - MUSCL Hancock 

    hDG1 = [2.547997638796255e-2, 1.276544295057963e-2, 6.395474283150761e-3, 3.180145437791298e-3, 1.593249716661807e-3];    
    L2DG1(1,:) = [2.488148773428313e-5, 1.155262421627612e-5, 3.263753227819373e-6, 7.763394350849077e-7, 1.882340875862320e-7];
    L2DG1(2,:) = [2.214643710775965e+1, 8.368583112940467e+0, 1.925441759780314e+0, 3.523394937246477e-1, 6.958913472118217e-2];    
    L2DG1(3,:) = [5.095447372888168e-3, 2.874508094314469e-3, 9.873591111860395e-4, 1.772632814277520e-4, 2.390303452003264e-5];
    workunitDG1 = ((sqrt(2).*hDG1).^-2)*5/(1/(2*1+1)); 
    h{2} = zeros(5,1);
    h{2} = hDG1;
    WU{2} = zeros(5,1);
    WU{2}(1:5,1) = workunitDG1(:); 
    L2{2} = L2{1};
    L2{2}(1,1:5) = L2DG1(1,:);
    L2{2}(2,1:5) = L2DG1(2,:);
    L2{2}(3,1:5) = L2DG1(3,:);

    hDG2 = [1.790007342356765e-2, 9.035544372281070e-3, 4.526798787234193e-3, 2.250947759879006e-3, 1.127722599736998e-3];  
    L2DG2(1,:) = [8.360091212472374e-6, 1.187151196431640e-6, 1.561681604213002e-7, 1.939854475348903e-8, 2.456168180239727e-9];
    L2DG2(2,:) = [5.823624576718240e+0, 6.870946620651206e-1, 7.229629178962509e-2, 8.694137799283672e-3, 1.107130703557128e-3];
    L2DG2(3,:) = [2.375147623808672e-3, 4.602390085034123e-4, 3.122303659867320e-5, 2.467545635425734e-6, 2.967397255012593e-7];
    workunitDG2 = ((sqrt(2).*hDG2).^-2)*5/(1/(2*2+1)); 
    h{3} = zeros(5,1);
    h{3} = hDG2;
    WU{3} = zeros(5,1);
    WU{3}(1:5,1) = workunitDG2(:); 
    L2{3} = L2{1};
    L2{3}(1,1:5) = L2DG2(1,:);
    L2{3}(2,1:5) = L2DG2(2,:);
    L2{3}(3,1:5) = L2DG2(3,:);

    if outputOn
        % open a file to store the convergence study data
        %for i = 1:fileNum
        [fid] = fopen([baseDir,'/','fastVorterrConvergence_comp.dat'],'w+');
        for i = 1: 3
            %[fid] = fopen([baseDir,'/','fastVorterrConvergence',caseDir{i},'.dat'],'w+');
            for iEq = 1:nEqns
                %for lev = 1:totalLevs
                for lev = 1:length(L2{i}(iEq,:))
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', sprintf('%d', i));
                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                        fprintf(fid, '%5s  %12s   %12s  %12s  %7s\n', ...
                            'Level','h','WU','L2Error','Order');
                        fprintf(fid, '%5s  %12s   %12s  %12s  %7s\n', ...
                            '-----','-----','------------','------------','-------');
                        fprintf(fid, '%5d %s %e %s %e %s %7s %s %s %s\n', ...
                            lev,'&', h{i}(lev),'&',WU{i}(lev),'&',L2{i}(iEq,lev),'&','','\\');
                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                        fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
                            lev,h{i}(lev),WU{i}(lev), ...
                            L2{i}(iEq,lev), ...
                            '       ')
                    else
                        % latex tabular output
                        fprintf(fid, '%5d %s %e %s %e %s %e %s %7.4f %s\n',...
                                lev,'&',h{i}(lev),'&',WU{i}(lev),'&', ...
                                L2{i}(iEq,lev),'&', ...
                                log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
        end
        fclose(fid);    
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order')
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
                    fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h{i}(lev),L1{i}(iEq,lev),'',L2{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
                        lev,h{i}(lev),L1{i}(iEq,lev), ...
                        '       ', ...
                        L2{i}(iEq,lev), ...
                        '       ')
                else
                    fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                            lev,h{i}(lev),L1{i}(iEq,lev), ...
                            log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            L2{i}(iEq,lev), ... 
                            log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end
    
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    lWidth = 2;
    fSize = 18;
    
    hRef = h{1};
    % L2 norm error convergence
    for i = 1:fileNum
        for iEq = 1:nEqns
            figure(iEq)
            loglog(h{i}(:),L2{i}(iEq,:),style(1,:),'linewidth',lWidth,'markersize',fSize)
            hold on
            %loglog(hFVM(:),L2FVM(iEq,:),style(2,:),'linewidth',lWidth,'markersize',fSize)
            loglog(sqrt(2).*hDG1(:),L2DG1(iEq,:),style(3,:),'linewidth',lWidth,'markersize',fSize)
            loglog(sqrt(2).*hDG2(:),L2DG2(iEq,:),style(4,:),'linewidth',lWidth,'markersize',fSize)

            %loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*L2{i}(iEq,1), 'k--')    % 1st order
            %loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*L2{i}(iEq,1), 'k-')    % 3rd order
            if ( iEq == 1 )
                loglog(hRef(:),0.01*(hRef(:)).^3/(hRef(1)^3)*L2{i}(iEq,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.01*(hRef(end)).^3/(hRef(1)^3)*L2{i}(iEq,1),'3','FontSize',fSize)
            elseif ( iEq == 2 ) 
                loglog(hRef(:),0.05*(hRef(:)).^3/(hRef(1)^3)*L2{i}(iEq,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.05*(hRef(end)).^3/(hRef(1)^3)*L2{i}(iEq,1),'3','FontSize',fSize)
            else
                loglog(hRef(:),0.01*(hRef(:)).^3/(hRef(1)^3)*L2{i}(iEq,1),'k--','linewidth',lWidth)    % 3rd order
                text(0.8*hRef(end),0.01*(hRef(end)).^3/(hRef(1)^3)*L2{i}(iEq,1),'3','FontSize',fSize)
            end
            legend([legendNames],'location','best')
            hx = xlabel('DOF^-^1^/^2'); 
            hy = ylabel(['|\epsilon_{', pvNames{iEq}, '}|_2']);
            set(gca,'FontSize',fSize)
            set(hx,'FontSize',fSize)
            set(hy,'FontSize',fSize)

            if outputOn
                fname2 = [baseDir,'/','fastvortErrConvg_eq',num2str(iEq),'.eps'];
                %fname2 = sprintf('%s/fastvortErrConvg_eq%d.eps',baseDir,iEq);
                print('-depsc2', '-r300', fname2);
            end
                    
        end
    end
    

    % L2 norm error as a function of work unit
    for i = 1:fileNum
        workunitAF = h{i}(:).^-2*2/(1); 
        for iEq = 1:nEqns
            figure(iEq+nEqns)
            loglog(workunitAF,L2{i}(iEq,:),style(1,:),'linewidth',lWidth,'markersize',fSize)
            hold on
            %loglog(workunitFVM,L2FVM(iEq,:),style(2,:),'linewidth',lWidth,'markersize',fSize)
            loglog(workunitDG1,L2DG1(iEq,:),style(3,:),'linewidth',lWidth,'markersize',fSize)
            loglog(workunitDG2,L2DG2(iEq,:),style(4,:),'linewidth',lWidth,'markersize',fSize)

            %loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*L2{i}(iEq,1), 'k--')    % 1st order
            %loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*L2{i}(iEq,1), 'k-')    % 3rd order
            legend([legendNames],'location','best')
            hx = xlabel('"Work Units"'); 
            %hy = ylabel(sprintf('|%s|_2',pvNames{iEq}));
            hy = ylabel(['|\epsilon_{', pvNames{iEq}, '}|_2']);
            set(gca,'FontSize',fSize)
            set(hx,'FontSize',fSize)
            set(hy,'FontSize',fSize)

            if outputOn
                fname2 = [baseDir,'/','fastvortErrWorkUnit',num2str(iEq),'.eps'];
                %fname2 = sprintf('%s/fastvortErrWorkUnit_eq%d.eps',baseDir,iEq);
                print('-depsc2', '-r300', fname2);
            end
           
        end
    end

end

function [h, L1, L2] = readVortexError(file)
% READVORTEXERROR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = 3;
    
    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
        
    for lev = 1:totalLevs
        %h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        h(lev) = 1.0/sqrt(A(lev,2)-A(lev,1));   % cell size, 1/sqrt(dof)
        %h(lev) = 1.0/sqrt(3.0*A(lev,1));   % cell size, 1/sqrt(dof)
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
            
end

function [nCells, DOF] = readDOF(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;
%     nEqns = 4;
    
    % initialize output variables
    nCells = zeros(totalLevs,1); 
    DOF = zeros(totalLevs,1); 
        
    for lev = 1:totalLevs
        nCells(lev) = A(lev,1);   % cell size, 1/sqrt(dof)
        DOF(lev) = A(lev,2);   % cell size, 1/sqrt(dof)
    end
            
end

function [h, L1, L2] = readErrData(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;

    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
 
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
     
end

