function [] =  plotErrCompareAngle()
% use with plotErrConvergence
    close all;
    
    dir = '../cases';
    solType = 'BDEqual';
%     solType = 'BDEqualDetail';
    FN = [dir,'/',solType,'/errData.dat'];
    [hBND, L1BDEqual, L2BDEqual] = solErr(FN);
    
    solType = 'BDEqualSt1';    
%     solType = 'BDEqualSt1Detail';     
    FN = [dir,'/',solType,'/errData.dat'];
    [hBD, L1BDEqualSt1, L2BDEqualSt1] = solErr(FN);
    
    L1BDEqualbubble = abs(L1BDEqual - L1BDEqualSt1);
    L2BDEqualbubble = abs(L2BDEqual - L2BDEqualSt1);
    
    solType = 'BDFlow';      
%     solType = 'BDFlowDetail';     
    FN = [dir,'/',solType,'/errData.dat'];
    [hSt1, L1BDFlow, L2BDFlow] = solErr(FN);
    
    solType = 'BDFlowSt1';
%     solType = 'BDFlowSt1Detail';     
    FN = [dir,'/',solType,'/errData.dat'];
    [hSt1, L1BDFlowSt1, L2BDFlowSt1] = solErr(FN);
    
    L1BDFlowbubble = abs(L1BDFlow - L1BDFlowSt1);
    L2BDFlowbubble = abs(L2BDFlow - L2BDFlowSt1);
    
    solType = 'BND';         
%     solType = 'BNDDetail';     
    FN = [dir,'/',solType,'/errData.dat'];
    [hSt1, L1BND, L2BND] = solErr(FN);
    
    solType = 'BNDSt1';     
%     solType = 'BNDSt1Detail';     
    FN = [dir,'/',solType,'/errData.dat'];
    [hSt1, L1BNDSt1, L2BNDSt1] = solErr(FN);
        
    L1BNDbubble = abs(L1BND - L1BNDSt1);
    L2BNDbubble = abs(L2BND - L2BNDSt1);    
    
    
    degList = 40:0.1:50;
%     degList = 44:0.01:46;
    
    figure(1)
    h1 = semilogy((degList), L2BDEqual,'bo','markersize', 6,'linewidth', 1);
    hold on
%     h2 = semilogy((degList), L1BDEqualSt1,'b^','markersize', 12,'linewidth', 2);
    h3 = semilogy((degList), L2BDFlow,'rs','markersize', 6,'linewidth', 1);
%     h4 = semilogy((degList), L1BDFlowSt1,'rs','markersize', 12,'linewidth', 2);
    h5 = semilogy((degList), L2BND,'k<','markersize', 6,'linewidth', 1);
%     h6 = semilogy((degList), L1BNDSt1,'kh','markersize', 12,'linewidth', 2);
    grid on
    axis([degList(1), degList(end), 10e-3, 10e-2])
    xlabel('Degree'); ylabel(['|\epsilon|_2']);
    legend([h1, h3, h5], 'BDEqual', 'BDFlow', 'BND','location', 'best')   
    fname = [dir,'/','gaussianCompareSoln_deg_L1_large.eps'];
    print('-depsc2', fname);

    figure(2)
    h1 = semilogy((degList), L2BDEqualSt1,'bo','markersize', 6,'linewidth', 1);
    hold on
    h3 = semilogy((degList), L2BDFlowSt1,'rs','markersize', 6,'linewidth', 1);
    h5 = semilogy((degList), L2BNDSt1,'k<','markersize', 6,'linewidth', 1);
    grid on
    xlabel('Degree'); ylabel(['|\epsilon|_2']);
    legend([h1, h3, h5], 'BDEqualSt1', 'BDFlowSt1', 'BNDSt1','location', 'best')   
    fname = [dir,'/','gaussianCompareSoln_deg_L1_St1_large.eps'];
    print('-depsc2', fname);

%     figure(2)
%     h1 = semilogy((degList), L2BDEqual,'bo','markersize', 12,'linewidth', 2);
%     hold on
%     h2 = semilogy((degList), L2BDEqualSt1,'b^','markersize', 12,'linewidth', 2);
%     h3 = semilogy((degList), L2BDFlow,'r*','markersize', 12,'linewidth', 2);
%     h4 = semilogy((degList), L2BDFlowSt1,'rs','markersize', 12,'linewidth', 2);
%     h5 = semilogy((degList), L2BND,'k<','markersize', 12,'linewidth', 2);
%     h6 = semilogy((degList), L2BNDSt1,'kh','markersize', 12,'linewidth', 2);
%     grid on
%     xlabel('Degree'); ylabel(['|\epsilon|_2']);
%     legend([h1, h2, h3, h4, h5, h6], 'BDEqual', 'BDEqualSt1', 'BDFlow', 'BDFlowSt1', 'BND', 'BNDSt1','location', 'best')%     fname = [dir,'/','gaussCompareSoln_deg_L2.eps'];
% %     fname = [dir,'/','gaussianCompareSoln_deg_L2.eps'];
% %     print('-depsc2', fname);

%     figure(3)
%     h1 = semilogy((degList), L2BDEqualbubble,'bo','markersize', 12,'linewidth', 2);
%     hold on
%     h2 = semilogy((degList), L2BDFlowbubble,'r*','markersize', 12,'linewidth', 2);
%     h3 = semilogy((degList), L2BNDbubble,'k<','markersize', 12,'linewidth', 2);
%     grid on
%     xlabel('Degree'); ylabel(['|\epsilon|_2']);
%     legend([h1, h2, h3], 'BDEqual', 'BDFlow', 'BND','location', 'best')%     fname = [dir,'/','gaussCompareSoln_deg_L2.eps'];
% %     fname = [dir,'/','gaussianCompareSoln_deg_L2.eps'];
% %     print('-depsc2', fname);
% 


end


function [h, L1, L2] = solErr(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    L1 = zeros(totalLevs,1);
    L2 = zeros(totalLevs,1);
    
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        L1(lev) = A(lev,4);
        L2(lev) = A(lev,5);
    end

end
