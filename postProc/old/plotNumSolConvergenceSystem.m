function[] = plotNumSolConvergenceSystem()
% PLOTNUMERRCONVERGENCE(DIR,caseDir) plots numerical solution 
%   convergence for a series of solution files for system of equations
%
    close all;

%     baseDir = '../cases/euler/entWave_cons_unst';
%     baseDir = '../cases/euler/entWave_cons_unst';
%     baseDir = '../cases/euler/old/svortex_test';
    baseDir = '../cases/euler/mvortex_cons1_unst';
%     baseDir = '../cases/euler/mvortex_cons_exactflux_test_unst';
%     baseDir = '../cases/euler/mvortex_noncons_test';

    caseDir = {''};
%     caseDir = {'','/diagErr_t05'};
    
    % output flag
    outputOn = false;     
%     outputOn = true;

%     conserved variable 
    consVar = false; % primitive 
    consVar = true; % conserved variable
% 
    % variables names
    if consVar
        pvNames = {'\rho', '\rho u', '\rho v', '\rho E'};
    else
        pvNames = {'\rho', 'u', 'v', 'p'};
    end 
 
    % legend names
    legendNames = pvNames;
    
    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        if isempty(caseDir{i})
            if consVar
                FN = [baseDir,'/','conserverrData','.dat'];     % complete file location     
            else
                FN = [baseDir,'/','errData','.dat'];    
            end
        else
            if consVar
                FN = [baseDir,caseDir{i},'/','conserverrData','.dat'];     % complete file location
            else
                FN = [baseDir,caseDir{i},'/','errData','.dat'];     
            end
        end 
        [fh, fL1, fL2] = solErrSystem(FN);
        h{i} = fh;            % 1/dof^(-1/2)
        L1{i} = fL1;            % L1 norm of error
        L2{i} = fL2;            % L2 norm of error
    end

    [nEqns,totalLevs] = size(L1{1});

    % hard code for isentropic euler
    legendNames = legendNames(1:nEqns);
        
    if outputOn
        % open a file to store the convergence study data
        for i = 1:fileNum
            [fid] = fopen([baseDir,'/','errConvergence',caseDir{i},'.dat'],'w+');
            for iEq = 1:nEqns
                for lev = 1:totalLevs
                    if ( lev == 1 )
                        fprintf(fid, '\n %s \n', caseDir{i});
                        fprintf(fid, 'Equation %5d \n', iEq);
                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            'Level','h','L1Error','Order','L2Error','Order');
                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            '-----','-----','------------','-------','------------','-------');
                        fprintf(fid, '%5d %s %e %s %e %s %7s %s %e %s %7s %s\n', ...
                            lev,'&', h{i}(lev),'&',L1{i}(iEq,lev),'&','','&',L2{i}(iEq,lev),'&','','\\');
                    else
                        fprintf(fid, '%5d %s %e %s %e %s %7.4f %s %e %s %7.4f %s\n',...
                                lev,'&',h{i}(lev),'&',L1{i}(iEq,lev),'&',log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
                                L2{i}(iEq,lev),'&',log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
                    end
                end
            end
            fclose(fid);    
        end
    end
    
    for i = 1:fileNum
        for iEq = 1:nEqns
            for lev = 1:totalLevs             
                if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d \n', iEq);
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order')
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
                    fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h{i}(lev),L1{i}(iEq,lev),'',L2{i}(iEq,lev),'');
                else
                    fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                            lev,h{i}(lev),L1{i}(iEq,lev),log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
                            L2{i}(iEq,lev),log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
                end
            end
        end
    end
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    lWidth = 2;
    fSize = 18;
    % L1 norm error
    for i = 1:fileNum
        figure(1)
%         figure(i)
        for iEq = 1:nEqns
%             loglog(h{i}(:),L1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            plot(h{i}(3:end).^3,L1{i}(iEq,3:end),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on
            if ( iEq == nEqns )
%                 loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*L1{i}(1,1), 'k:')    % 1st order
%                 loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*L1{i}(1,1), 'k-')    % 3rd order
                legend([legendNames],'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_1');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if outputOn
                    fname1 = [baseDir,'/','euler',caseDir{i},'_errorL1.eps'];
                    print('-depsc2', '-r300', fname1);
                end
            end
        end
    end
 
    % L2 norm error
    for i = 1:fileNum
        figure(2)
%         figure(i+fileNum)        
        for iEq = 1:nEqns
%             loglog(h{i}(:),L2{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            plot(h{i}(3:end).^3,L2{i}(iEq,3:end),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on
            if ( iEq == nEqns )
%                 loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*L2{i}(1,1), 'k:')    % 1st order
%                 loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*L2{i}(1,1), 'k-')    % 3rd order
                legend([legendNames],'location','best')
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
                if outputOn
                    fname2 = [baseDir,'/','euler',caseDir{i},'_errorL2.eps'];
                    print('-depsc2', '-r300', fname2);
                end
            end
        end
    end
    
    
    stop
    % plot the difference in error of consecutive grid sizes
    for i = 1:fileNum
        figure(3)
        for iEq = 1:nEqns
            tempArray = [];
            for j = 1:length(h{i}(:))
                if j > 1
                    tempArray = [tempArray, abs(L1{i}(iEq,j-1)-L1{i}(iEq,j))];
                end
            end
            loglog(h{i}(2:end),tempArray,style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            if iEq == nEqns
                loglog(h{i}(:),1*(h{i}(:)).^3/(h{i}(2)^3)*L1{i}(1,2), 'k')    % 3rd order
                loglog(h{i}(:),1*(h{i}(:)).^2/(h{i}(2)^2)*L1{i}(1,2), 'k')    % 2rd order
                loglog(h{i}(:),1*(h{i}(:)).^1/(h{i}(2)^1)*L1{i}(1,2), 'k')    % 1rd order
            end
        end
        hx = xlabel('DOF^-^1^/^2'); hy = ylabel('\Delta|\epsilon|_1');
        hold off
        set(gca,'FontSize',fSize)
        set(hx,'FontSize',fSize)
        set(hy,'FontSize',fSize)
    end
    
% %     % plot the difference in error of consecutive grid sizes
%     for i = 1:fileNum
%         figure(4)
%         for iEq = 1:nEqns
%             tempArray = [];
%             for j = 1:length(h{i}(:))
%                 if j > 1
%                     tempArray = [tempArray, (L2{i}(iEq,j-1)-L2{i}(iEq,j))];
%                 end
%             end
%             loglog(h{i}(2:end),tempArray,style(iEq,:),'linewidth',lWidth,'markersize',fSize)
%             hold on;
%             if iEq == nEqns
%                 loglog(h{i}(:),5*(h{i}(:)).^3/(h{i}(2)^3)*L2{i}(1,2), 'k')    % 3rd order
%                 loglog(h{i}(:),5*(h{i}(:)).^2/(h{i}(2)^2)*L2{i}(1,2), 'k')    % 2rd order
%                 loglog(h{i}(:),5*(h{i}(:)).^1/(h{i}(2)^1)*L2{i}(1,2), 'k')    % 1rd order
%             end
%         end
%         hx = xlabel('DOF^-^1^/^2'); hy = ylabel('\Delta|\epsilon|_2');
%         hold off
%         set(gca,'FontSize',fSize)
%         set(hx,'FontSize',fSize)
%         set(hy,'FontSize',fSize)
%     end
    
    
end

function [h, L1, L2] = solErrSystem(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
        
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
            
end
