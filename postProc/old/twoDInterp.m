function [] = twoDInterp()

% evaluate interpolation value for a standard triangle
wavespeed = [1;0];
dt = 1/4;

% triangle 0
xnode(:,1) = [0,0];
xnode(:,2) = [1,0];
xnode(:,3) = [1,1];

[jac,detjac,jacInv] = evalJac(xnode);
[coeff] = reconCoeff(xnode);

% Fe1 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [2,-1,3];
for i = 1:3
    for j = 1:3
        u(i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
    end
end
[n1,l1] = faceNormalArea(xnode(:,2),xnode(:,3));
Fe1 = averageFlux(u,wavespeed)*n1'*l1;

% Fe2 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [3,-2,1];
for k = 1:2
    for i = 1:3
        for j = 1:3
            u(k,i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
        end
    end
end
[n2,l2] = faceNormalArea(xnode(:,3),xnode(:,1));
Fe2 = averageFlux(u,wavespeed)*n2'*l2;

% Fe3 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [1,-3,2];
for i = 1:3
    for j = 1:3
        u(i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
    end
end
[n3,l3] = faceNormalArea(xnode(:,1),xnode(:,2));
Fe3 = averageFlux(u,wavespeed)*n3'*l3;

u0n = 1/3*(coeff(2)+coeff(4)+coeff(6))
u0nplus1 = u0n - dt/detjac*(Fe1+Fe2+Fe3)

% triangle 1
xnode(:,1) = [0,0];
xnode(:,2) = [1,0];
xnode(:,3) = [1,1];

[jac,detjac,jacInv] = evalJac(xnode);
[coeff] = reconCoeff(xnode);

% Fe1 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [2,-1,3];
for i = 1:3
    for j = 1:3
        u(i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
    end
end
[n1,l1] = faceNormalArea(xnode(:,2),xnode(:,3));
Fe1 = averageFlux(u,wavespeed)*n1'*l1;

% Fe2 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [3,-2,1];
for k = 1:2
    for i = 1:3
        for j = 1:3
            u(k,i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
        end
    end
end
[n2,l2] = faceNormalArea(xnode(:,3),xnode(:,1));
Fe2 = averageFlux(u,wavespeed)*n2'*l2;

% Fe3 
timeArray = [0,0.5*dt,dt];
fluxNodeEdge = [1,-3,2];
for i = 1:3
    for j = 1:3
        u(i,j) = reconValue(fluxNodeEdge(j),timeArray(i),wavespeed,coeff,jacInv);
    end
end
[n3,l3] = faceNormalArea(xnode(:,1),xnode(:,2));
Fe3 = averageFlux(u,wavespeed)*n3'*l3;

u0n = 1/3*(coeff(2)+coeff(4)+coeff(6))
u0nplus1 = u0n - dt/detjac*(Fe1+Fe2+Fe3)


end

function [normal,length] = faceNormalArea(xL,xR)
    tangent = xR - xL;
    normal(1) = tangent(2)/norm(tangent);
    normal(2) = -tangent(1)/norm(tangent);
    length = norm(tangent);
end

function [avg] = averageFlux(u,wavespeed)
    f = wavespeed(1)*u;
    g = wavespeed(2)*u;    
    avg(1) = 1/36*( (f(1,1) + f(1,3) + f(3,1) + f(3,3)) + ...
          4*(f(1,2) + f(2,1) + f(2,3) + f(3,2)) + ...
          16*f(2,2) ) ;
    avg(2) = 1/36*( (g(1,1) + g(1,3) + g(3,1) + g(3,3)) + ...
          4*(g(1,2) + g(2,1) + g(2,3) + g(3,2)) + ...
          16*g(2,2) ) ;
end

function [jac,detjac,jacInv] = evalJac(xnode)
    % assume standard triangle coordinates are (0,0), (1,0), (0,1)
    x = xnode(1,:);
    y = xnode(2,:);
    jac = [x(2)-x(1), x(3)-x(1); y(2)-y(1), y(3)-y(1)];
    detjac = jac(1,1)*jac(2,2) - jac(1,2)*jac(2,1);
    jacInv = 1/detjac*[jac(2,2),-jac(1,2);-jac(2,1),jac(2,2)];
end

function [coeff] = reconCoeff(xnode)
    for inode = 1:3
        % node assignment
        coeff(2*(inode-1)+1) = uFunc(xnode(1,inode),xnode(2,inode));
        % edge assignment
        if ( inode == 1 )
            xedge = 0.5*(xnode(:,2)+xnode(:,3));
        elseif ( inode == 2 )            
            xedge = 0.5*(xnode(:,3)+xnode(:,1));
        elseif ( inode == 3 )
            xedge = 0.5*(xnode(:,1)+xnode(:,2));
        end
        coeff(2*(inode)) = uFunc(xedge(1),xedge(2));
    end
end

function [uVal] = uFunc(x,y)
    % analytic function
    uVal = x^2;
end

function [uInt] = reconValue(iNodeEdge,dt,wavespeed,coeff,jacInv)

% for iNodeEdge: nodes(1,2,3), edges(-1,-2,-3) 
% iNodeEdge = -2;
% [uInt] = reconValue(iNodeEdge,dt,wavespeed,coeff,jacInv)

    xiNode(:,1) = [0,0];
    xiNode(:,2) = [1,0];
    xiNode(:,3) = [0,1];
    xiEdge(:,1) = [0.5,0.5];
    xiEdge(:,2) = [0,0.5];
    xiEdge(:,3) = [0.5,0];

    if ( iNodeEdge > 0 )
        inode = iNodeEdge;
        xi0 = xiNode(:,inode) - dt*(jacInv*wavespeed);
    else
        iedge = -iNodeEdge;
        xi0 = xiEdge(:,iedge) - dt*(jacInv*wavespeed);
    end

    s1 = (1-xi0(1)-xi0(2));
    s2 = xi0(1);
    s3 = xi0(2);          
    phi(1) = s1*(2*s1-1);
    phi(2) = 4*s2*s3;
    phi(3) = s2*(2*s2-1);
    phi(4) = 4*s3*s1;
    phi(5) = s3*(2*s3-1);
    phi(6) = 4*s1*s2;   

    uInt = 0.0;
    for i = 1:6
        uInt = uInt + coeff(i)*phi(i);
    end
end