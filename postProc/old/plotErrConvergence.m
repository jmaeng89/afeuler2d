function[] = plotErrConvergence(dir,fileNames)
% PLOTERRCONVERGENCE(DIR,FILENAMES) plots the mesh refinement error 
%   convergence for a series of solution files
%
    close all;
    
    dir = '../cases';

    fileNames = {'chFixBurgErr','orgBurgErr'};
    
    solType = '';

    [~,fileNum] = size(fileNames);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [dir,'/',fileNames{i},'/','errData',solType,'.dat'];     % complete file location
        [fh, fL1, fL2] = solErr(FN);
        h(i,:) = fh;        % 1/dof^(-1/2)
        L1(i,:) = fL1;      % L1 norm of error
        L2(i,:) = fL2;      % L2 norm of error
    end
    
    [~,totalLevs] = size(h);
    
    % open a file to store the convergence study data
    [fid] = fopen([dir,'/','errConvergence',solType,'.dat'],'w+');
 
    for i = 1:fileNum
        for lev = 1:totalLevs
            if ( lev == 1 )
                fprintf(fid, '\n %s \n', fileNames{i});
                fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order');
                fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------');
                fprintf(fid, '%5d %s %e %s %e %s %7s %s %e %s %7s %s\n',lev,'&', h(i,lev),'&',L1(i,lev),'&','','&',L2(i,lev),'&','','\\');
            else
                fprintf(fid, '%5d %s %e %s %e %s %7.4f %s %e %s %7.4f %s\n',...
                        lev,'&',h(i,lev),'&',L1(i,lev),'&',log(L1(i,lev-1)/L1(i,lev))/log(h(i,lev-1)/h(i,lev)),'&', ...
                        L2(i,lev),'&',log(L2(i,lev-1)/L2(i,lev))/log(h(i,lev-1)/h(i,lev)), '\\');
            end

        end
    end
    fclose(fid);    
    
    for i = 1:fileNum
        for lev = 1:totalLevs
            if ( lev == 1 )
                fprintf('\n %s \n',fileNames{i})
                fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order')
                fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
                fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h(i,lev),L1(i,lev),'',L2(i,lev),'');
            else
                fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                        lev,h(i,lev),L1(i,lev),log(L1(i,lev-1)/L1(i,lev))/log(h(i,lev-1)/h(i,lev)), ...
                        L2(i,lev),log(L2(i,lev-1)/L2(i,lev))/log(h(i,lev-1)/h(i,lev)))
            end

        end
    end
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    lWidth = 2;
    fSize = 16;
    % L1 norm error
    figure(1)
    for i = 1:fileNum
        loglog(h(i,:),L1(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L1(1,1), 'k-.')    % 1st order
    loglog(h(1,:),(h(1,:)).^3/(h(1,1)^3)*L1(1,1), 'k--')    % 3rd order
    legend([fileNames, '1st','3rd'],'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['|\epsilon|_1']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname1 = [dir,'/','burg',solType,'_convgL1.eps'];
%     print('-depsc2', fname1);
    
    % L2 norm error
    figure(2)
    for i = 1:fileNum
        loglog(h(i,:),L2(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L2(1,1), 'k-.')    % 1st order
    loglog(h(1,:),(h(1,:)).^3/(h(1,1)^3)*L2(1,1), 'k--')    % 3rd order
    legend([fileNames, '1st','3rd'],'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['|\epsilon|_2']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname2 = [dir,'/','burg',solType,'_convgL2.eps'];
%     print('-depsc2', fname2);
    

end


function [h, L1, L2] = solErr(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    L1 = zeros(totalLevs,1);
    L2 = zeros(totalLevs,1);

    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        L1(lev) = A(lev,4);
        L2(lev) = A(lev,5);
    end

end

