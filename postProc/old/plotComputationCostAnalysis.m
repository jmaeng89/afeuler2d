function plotComputationCostAnalysis(dir,fileNames)
% PLOTERRCONVERGENCE(DIR,FILENAMES) plots the mesh refinement error 
%   convergence for a series of solution files
%
%   DIR is the directory path. FILENAMES contains names of data files 
%   in COLUMN structure format. 

    dir = '../cases/vort';
%     dir = 'cases/error/gaussVortGrid';
    fileNames = {'ChOrg/costBDBNU', 'ChOrg/costBDBU', 'ChOrg/costBNDBNU', 'ChOrg/costBNDBU', ...
                 'RD/costBDBNU', 'RD/costBDBU', 'RD/costBNDBNU', 'RD/costBNDBU'}; 
    
    [~,fileNum] = size(fileNames);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [dir,'/',fileNames{i},'.dat'];     % complete file location
        [fh, fL1, fIT] = readData(FN);
        h(i,:) = fh;                
        L1(i,:) = fL1;     
        L2(i,:) = fL1./fIT;
    end
    

    % open a file to store the convergence study data
    fid = fopen([dir,'/','costCompConvergence.dat'],'w');
    
    [~,totalLevs] = size(h);
    
    for i = 1:fileNum
        for lev = 1:totalLevs
            
            if ( lev == 1 )
                fprintf(fid, '\n %s \n',fileNames{i});
                fprintf(fid, '%5s  %12s   %12s  %7s   %12s  %7s  \n','Level','1/sqrt(DOF)','T(s)','Order','T_iter(s)','Order');
                fprintf(fid, '%5s  %12s   %12s  %7s   %12s  %7s \n','-----','-----','------------','-------','------------','-------');
                fprintf(fid, '%5d  %e   %e   %5s    %e   %5s  \n',lev,h(i,lev),L1(i,lev),'',L2(i,lev),'');
            else           
                fprintf(fid, '%5d  %e   %e  %7.4f   %e  %7.4f\n',...
                        lev,h(i,lev),L1(i,lev),log(L1(i,lev-1)/L1(i,lev))/log(h(i,lev-1)/h(i,lev)), ...
                         L2(i,lev),log(L2(i,lev-1)/L2(i,lev))/log(h(i,lev-1)/h(i,lev)));
            end

        end
    end
    fclose(fid)
    
    % plot solution convergence 
    style = ['-bo';'-rs';'-k<';'-g*'; ...
             ':bh';':rd';':kp';':g>'];
    lWidth = 2;
    fSize = 16;
    
    % Total computation time 
    figure(1)
    for i = 1:fileNum
        loglog(h(i,:),L1(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
%     loglog(h(1,:),(h(1,:)).^3/(h(1,1)^3)*L1(1,1), 'k--')    % 3rd order
%     loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L1(1,1), 'k--')    % 1st order
    legend(fileNames,'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['Total computation time (s)']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    fname1 = [dir,'/','Computation_convergenceTotalTime.eps'];
    print('-depsc2', fname1);
    
    % Total computation time 
    figure(2)
    for i = 1:fileNum
        loglog(h(i,:),L2(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
%     loglog(h(1,:),(h(1,:)).^3/(h(1,1)^3)*L1(1,1), 'k--')    % 3rd order
%     loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L1(1,1), 'k--')    % 1st order
    legend(fileNames,'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['Computation time per iteration (s)']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    fname2 = [dir,'/','Computation_convergenceTimePerIteration.eps'];
    print('-depsc2', fname2);
end

function [h, T, iter] = readData(file)
% SOLERR(FILE) returns the data contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    T = zeros(totalLevs,1);
    iter = zeros(totalLevs,1);
    
    for lev = 1:totalLevs
        h(lev) =  A(lev,3);   % cell size, 1/sqrt(dof)
        T(lev) = A(lev,5);      % computation cost 
        iter(lev) = A(lev,4);   % iterations
    end

end


