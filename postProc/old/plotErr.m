function plotErr(dir,minCell,nLevels,xMax,p)
% PLOTERR(DIR,MINCELL,NLEVELS,XMAX,P) plots the mesh refinement for a series of
%   solution files in directory DIR.
%
%   DIR may be specified as an array of directories, provided MINCELL and 
%   NCELLS is given for each directory as well.  XMAX is assumed to be constant
%   between the two directories

    if ( nargin < 5 )
        help plotErr
        return
    end
    
    % see if user is comparing multiple directories
    [nDir,~] = size(dir);
    if ( nDir > 1 )
        [n,~] = size(minCell);
        if ( n ~= nDir )
            fprintf('ERROR: must give minimum number of cells for each run (dir)\n')
            return
        end
        [n,~] = size(nLevels);
        if ( n ~= nDir )
            fprintf('ERROR: must give number of refinement levels for each run (dir)\n')
            return
        end
    end

    err = zeros(max(nLevels),nDir);
    dx = zeros(max(nLevels),nDir);
    for iDir = 1:nDir
        fprintf('\nError for files in directory "%s":\n',dir(iDir,:))
        
        %spacing on the coarsest level
        dxCoarse = xMax/minCell(iDir);

        for lev = 1:nLevels(iDir)
            nCells = minCell(iDir)*2^(lev-1);
            dx(lev,iDir) = dxCoarse/2^(lev-1);
            file = [dir(iDir,:),'/soln',num2str(nCells,'%0.5d'),'.dat'];

            err(lev,iDir) = burgErr(p,file);

            if ( lev == 1 )
                fprintf('%5s  %12s  %7s\n','Level','Error','Order')
                fprintf('%5s  %12s  %7s\n','-----','------------','-------')
                fprintf('%5d  %e\n',lev,err(lev,iDir));
            else
                fprintf('%5d  %e  %7.4f\n',...
                        lev,err(lev,iDir),log2(err(lev-1,iDir)/err(lev,iDir)))
            end
        end
        
        
    end
    
    % TODO: generalize for more than two directories
    style = ['b.-';'r.-'];
    fSize = 16;
    figure(1)
    clf
    loglog(dx(:,1),err(:,1),style(1,:),dx(:,2),err(:,2),style(2,:))
    hx = xlabel('\Delta x'); hy = ylabel(['|\epsilon|_',num2str(p)]);
    legend(dir)
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
end


function Lp = burgErr(p,file,plotSol)
% BURGERR(P,FILE) returns the L_P error norm for the solution contained in FILE
%   BURGERR(P,FILE,TRUE) plots the sampled solution and characteristics

    % turn plotting off by default
    if ( nargin < 3 )
        plotSol = false;
    end

    % floating-point precision of active-flux data
    prec = 'double';

    % read mesh from file
    fid = fopen(file,'r');
    nNodes = fread(fid,1,'int');
    nCells = nNodes - 1;
    x0 = fread(fid,nNodes,prec);
    
    % assume uniform spacing
    dx = (max(x0)-min(x0))/nCells;
    xCell = linspace(x0(1)+dx/2,x0(nNodes)-dx/2,nCells);
    
    % initial solution
    i0 = fread(fid,1,'int');
    t0 = fread(fid,1,prec);
    cellAvg0 = fread(fid,nCells,prec);
    nodeVal0 = fread(fid,nNodes,prec);
    
    % final solution
    iF = fread(fid,1,'int');
    tF = fread(fid,1,prec);
    cellAvgF = fread(fid,nCells,prec);
    nodeValF = fread(fid,nNodes,prec);
    
    fclose(fid);
    
    % initial (exact), and final cell reconstructions
    coeff0 = zeros(3,nCells);
    coeffF = zeros(3,nCells);
    for iCell = 1:nCells
        lNode = iCell;
        rNode = iCell+1;
        
        coeff0(:,iCell) = coeff( nodeVal0(lNode),cellAvg0(iCell),...
                                 nodeVal0(rNode) );
        coeffF(:,iCell) = coeff( nodeValF(lNode),cellAvgF(iCell),...
                                 nodeValF(rNode) );
    end
    
    % calculate cell widths at final time
    xF = zeros(nNodes,1);
    for iNode = 1:nNodes
        xF(iNode) = x0(iNode) + nodeVal0(iNode)*tF;
    end
    
    % calculate error in the domain
    nQuad = 4;
    xExact = zeros(nQuad*nCells,1);
    uExact = zeros(nQuad*nCells,1);
    uAF = zeros(nQuad*nCells,1);
    iEx = 0;
    error = 0.0;
    domainArea = 0.0;
    w = gaussWeights(nQuad);
    for iCell = 1:nCells
    
        lNode = iCell;
        rNode = iCell+1;
        
        xQuad = gaussLoc(nQuad,x0(lNode),x0(rNode));
        
        % map quadrature points to cells at t = 0
        charOwner = zeros(nQuad,1);
        errQuad = 0.0;
        for n = 1:nQuad
            
            % TODO: make this more efficient
            if ( xQuad(n) < xF(1) )
                charOwner(n) = 1;
            else
                i = 1;
                while xF(i) < xQuad(n)
                    i = i+1;
                end
                charOwner(n) = i-1;
            end
            
            % sample the exact solution
            xiQuad = (xQuad(n)-x0(charOwner(n)))./dx;
            xi0 = charOrigin(xiQuad,tF,dx,coeff0(:,charOwner(n)));
            
            iEx = iEx + 1;
            xExact(iEx) = xQuad(n);
            
            % assume that solution is constant at both ends of the domain
            if ( xi0 < 0.0 )
                uExact(iEx) = nodeVal0(1);
            elseif ( xi0 > 1.0 )
                uExact(iEx) = nodeVal0(nNodes);
            else
                uExact(iEx) = reconVal(xi0,coeff0(:,charOwner(n)));
            end
            
            % sample the active flux solution
            xi = (xQuad(n)-x0(iCell))/dx;
            uAF(iEx) = reconVal(xi,coeffF(:,iCell));
            
            % track the sums that are needed to calculate the p-norm
            errQuad = errQuad + w(n)*abs(uAF(iEx)-uExact(iEx))^p;
        end
        
        error = error + errQuad*dx;
        domainArea = domainArea + dx;
    end
    
    Lp = (error/domainArea)^(1.0/p);
    
    if ( plotSol )
        % plot exact and AF solution at quadrature points 
        figure(1)
            clf
            plot(xExact,uAF,'r^',xExact,uExact,'bv')
%             plot(xCell,cellAvg0,'b.',xCell,cellAvgF,'r.')
            legend('AF','Exact')

            pad = 0.1;
            yMin = min(min(uExact),min(uAF));
            yMax = max(max(uExact),max(uAF));
            ySize = yMax-yMin;
            axis([min(x0),max(x0),yMin-pad*ySize,yMax+pad*ySize])
            xlabel('x'); ylabel('u')

        % plot quadrature points and node characteristics
%         figure(2)
%             clf
%             hold on
%             for iNode = 1:nNodes
%                 plot([x0(iNode),xF(iNode)],[t0,tF],'b.-')
%             end
%             for iCell = 1:nCells
%                 x = gaussLoc(4,x0(iCell),x0(iCell+1));
%                 t = tF*ones(length(x),1);
%                 plot(x,t,'rs')
%             end
%             hold off
%             pad = 0.1;
%             axis([min(min(x0),min(xF)),max(max(x0),max(xF)),...
%                   t0-pad*(tF-t0),tF+pad*(tF-t0)])
%             xlabel('x'); ylabel('t')
    end
    
end


function c = coeff(uL,uBar,uR)
% COEFF(UL,UBAR,UR) returns the reconstruction coefficients given the left UL,
%   average UBAR, and right UR values

    c = zeros(3,1);
    
    c(1) = uL;
    c(2) = 0.25*( 6.0*uBar - uL - uR );
    c(3) = uR;
    
end


function xi0 = charOrigin(xi,t,dx,coeff)
% CHARORIGIN(XI,T,DX,COEFF) finds the origin of the characteristic passing 
%   through the point (XI,T) based on the cell width DX and reconstruction 
%   coefficients in COEFF

    a = 2*(coeff(1)-2*coeff(2)+coeff(3));
    b = -(3*coeff(1)-4*coeff(2)+coeff(3));
    r = t/dx;
    
    % sanity check
    rad = (b*r+1.0)^2 + 4*a*r*(xi-coeff(1)*r);
    if ( rad < 0 )
        fprintf('WARNING: unable to accurately find characterisitc origin.\n')
        xi0 = 0.0;
        return
    end
    
    xi0 = 2*(xi-coeff(1)*r) / ( (b*r+1) + sqrt(rad) );
end


function u = reconVal(xi,coeff)
% RECONVAL(XI,COEFF) returns the reconstruction value at XI based on
%   coefficients COEFF.  Assumes 1d data.

    phi = [(2.0*xi-1.0)*(xi-1.0);
            4.0*xi*(1.0-xi);
            xi*(2.0*xi-1.0);];
        
    u = 0.0;
    for i = 1:3
        u = u + coeff(i)*phi(i);
    end

end


function x = gaussLoc(nPts,a,b)
% GAUSSLOC(NPTS,A,B) returns the locations at which to evaluate a function over
%   the interval [A,B] for a numerical integration using NPTS points

    x = zeros(nPts,1);
    h = b-a;

    switch (nPts)
        case (4)
            x(1) = sqrt((3.0-2.0*sqrt(1.2))/7.0);
            x(2) = -x(1);
            x(3) = sqrt((3.0+2.0*sqrt(1.2))/7.0);
            x(4) = -x(3);
    end

    x(:) = a + 0.5*h*(x(:)+1.0);
end


function w = gaussWeights(nPts)
% GAUSSWEIGHTS(NPTS) returns the weights for each Gaussian quadrature point

    w = zeros(nPts,1);
    switch (nPts)
        case (4)
            w(1:2) = 0.5 + sqrt(30.0)/36.0;
            w(3:4) = 0.5 - sqrt(30.0)/36.0;
    end

end

