function plotErrConvergence(dir,fileNames)
% PLOTERRCONVERGENCE(DIR,FILENAMES) plots the mesh refinement error 
%   convergence for a series of solution files
%
%   DIR is the directory path. FILENAMES contains names of data files 
%   in COLUMN structure format. 
    close all;
    
    dir = '../cases/error';
%     fileNames = {'mult48/BDBNU', ...
%                  'mult49/BDBNU', ...
%                  'mult50/BDBNU', ...
%                  'mult51/BDBNU', ...
%                  'mult52/BDBNU'}; 
    fileNames = {'mult50/BDBNU'};     
    solType = 'errData';
          
%     degList = [atan(0.918259222659919882580936700833262875676155090332031250000000)*180/pi,...
%            atan(0.959583242871612451807550314697436988353729248046875000000000)*180/pi,...
%            atan(1.0)*180/pi, ...
%            atan(1.03961531346936197905961307696998119354248046875000000000000)*180/pi,...
%            atan(1.07851750101702115181012686662143096327781677246093750000000)*180/pi];
    degList = 45;
    [~,fileNum] = size(fileNames);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [dir,'/',fileNames{i},'/',solType,'.dat'];     % complete file location
        [fh, fL1, fL2] = solErr(FN);
        h(i,:) = fh;        % 1/dof^(-1/2)
        L1(i,:) = fL1;      % L1 norm of error
        L2(i,:) = fL2;      % L2 norm of error
    end
    
    [~,totalLevs] = size(h);
    
    % open a file to store the convergence study data
    [fid,mess] = fopen([dir,'/','errConvergence',solType,'.dat'],'w+');
 
    for i = 1:fileNum
        for lev = 1:totalLevs
            if ( lev == 1 )
                fprintf(fid, '\n %s \n', fileNames{i});
                fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order');
                fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------');
                fprintf(fid, '%5d %s %e %s %e %s %7s %s %e %s %7s %s\n',lev,'&', h(i,lev),'&',L1(i,lev),'&','','&',L2(i,lev),'&','','\\');
            else
                fprintf(fid, '%5d %s %e %s %e %s %7.4f %s %e %s %7.4f %s\n',...
                        lev,'&',h(i,lev),'&',L1(i,lev),'&',log(L1(i,lev-1)/L1(i,lev))/log(h(i,lev-1)/h(i,lev)),'&', ...
                        L2(i,lev),'&',log(L2(i,lev-1)/L2(i,lev))/log(h(i,lev-1)/h(i,lev)), '\\');
            end

        end
    end
    fclose(fid);    
    
    for i = 1:fileNum
        for lev = 1:totalLevs
            if ( lev == 1 )
                fprintf('\n %s \n',fileNames{i})
                fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order')
                fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
                fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h(i,lev),L1(i,lev),'',L2(i,lev),'');
            else
                fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                        lev,h(i,lev),L1(i,lev),log(L1(i,lev-1)/L1(i,lev))/log(h(i,lev-1)/h(i,lev)), ...
                        L2(i,lev),log(L2(i,lev-1)/L2(i,lev))/log(h(i,lev-1)/h(i,lev)))
            end

        end
    end
    % plot solution convergence 
    style = ['b.-';'r.-';'k.-';'g.-';'m.-'];
    lWidth = 2;
    fSize = 16;
    % L1 norm error
    figure(1)
    for i = 1:fileNum
        loglog(h(i,:),L1(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    loglog(h(1,:),(h(1,:)).^3/(h(1,1)^3)*L1(1,1), 'k--')    % 3rd order
    loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L1(1,1), 'k--')    % 1st order
    legend(fileNames,'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['|\epsilon|_1']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname1 = 'Gauss_convergence_L1.eps';
%     print('-depsc2', fname1);
    
    % L2 norm error
    figure(2)
    for i = 1:fileNum
        loglog(h(i,:),L2(i,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
        hold on
    end
    loglog(h(1,:),(h(1,:).^3/(h(1,1)^3))*L2(1,1), 'k--')    % 3rd order
    loglog(h(1,:),(h(1,:)).^1/(h(1,1)^1)*L2(1,1), 'k--')    % 1st order
    legend(fileNames,'location','best')
    hx = xlabel('DOF^-^1^/^2'); hy = ylabel(['|\epsilon|_2']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
%     fname2 = 'Gauss_convergence_L2.eps';
%     print('-depsc2', fname2);
    
    % L1 error versus degrees
    figure(3)
    for j = 1:totalLevs
        semilogy(degList(:),L1(:,j),'-o','linewidth',lWidth,'markersize',10)
        hold on
    end
    hx = xlabel('Degree'); hy = ylabel(['|\epsilon|_1']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    fname3 = [dir,'/','Soln_deg_L1',solType,'.eps'];
    print('-depsc2', fname3);
   
    % L2 error versus degrees
    figure(4)
    for j = 1:totalLevs
        semilogy(degList(:),L2(:,j),'-o','linewidth',lWidth,'markersize',10)
        hold on
    end 
    hx = xlabel('Degree'); hy = ylabel(['|\epsilon|_2']);
    hold off
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    fname4 = [dir,'/','Soln_deg_L2',solType,'.eps'];
    print('-depsc2', fname4);
    


end

function [h, L1, L2] = solErr(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % initialize output variables
    h = zeros(totalLevs,1); 
    L1 = zeros(totalLevs,1);
    L2 = zeros(totalLevs,1);
    
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        L1(lev) = A(lev,4);
        L2(lev) = A(lev,5);
    end

end


