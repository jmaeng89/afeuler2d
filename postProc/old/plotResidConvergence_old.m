function[] = plotResidConvergence(baseDir,fileNames)
% PLOTRESIDCONVERGENCE(BASEDIR,FILENAMES) plots the residual 
%   convergence history. 
%
%   J. Brad Maeng
%   3/31/2016 - created
%
    close all;
    
    baseDir = '../cases/linadv_test/gauss_unst_rec';
    baseDir = '../cases/linadv_test/gauss_unst_rec_old';
    baseDir = '../cases/linadv_test/gauss_unst_noncons';
    baseDir = '../cases/linadv_test/gauss_unst_rec2';
    baseDir = '../cases/linadv_test/gauss_unst_rec3';

    baseDir = '../cases/euler_test/svortex_rec1';
    baseDir = '../cases/linadv/sin_unst_rec_longt';
    fileNames = {'residunst032','residunst032_lt'};
    fileNames = {'residunst008','residunst008_lt','residunst008_lt1','residunst008_lt2','residunst008_lt3'};
    fileNames = {'residunst002','residunst008','residunst032','residunst128','residunst512'};

    %baseDir = '../cases/linadv_test/gauss_diag_rec_old';
    %baseDir = '../cases/euler_test/mvortex_diag';
    %baseDir = '../cases/linadv_test/gauss_diag_rec2';
    %baseDir = '../cases/linadv_test/gauss_diag_noncons';
    %baseDir = '../cases/linadv_test/gauss_diag_rec3';
    %baseDir = '../cases/linadv_test/gauss_diag_rec';
    %fileNames = {'residdiag002','residdiag008','residdiag032','residdiag128','residdiag512'};

    % output flag
    outputOn = false;
    %outputOn = true;

    [~,fileNum] = size(fileNames);
        
    % legend names
    %legendNames = {'\rho', 'u', 'v', 'p'};   
    legendNames = {'1\Delta x', '2 \Delta x', '2^2 \Delta x', '2^3 \Delta x','2^4\Delta x'};   
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        FN = [baseDir,'/',fileNames{i},'.dat'];     % complete file location
        [fIter, fT, fResid, fResidMax] = readResidSol(FN);
        iT{i} = fIter(2:end);
        t{i} = fT(2:end); 
        resid{i} = fResid(:,2:end);
        residMax{i} = fResidMax(:,2:end);  
    end
    
    [nEqns,totalLevs] = size(resid{1});
    nEqns = 1;

    style = ['bo-';'ro-';'ko-';'go-';'mo-'];
    style = ['b-';'r-';'k-';'g-';'m-'];
    lWidth = 2;
    fSize = 16;
    
    % u residual convergence history
    figure(1)
    for i = 1:fileNum
        for iEq = 1:nEqns
            %semilogy(t{i}(:),resid{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            loglog(t{i}(:),resid{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
            hold on
        end 
    end
    hx = xlabel('Time'); 
    hy = ylabel('|Resid|_1'); %hy = ylabel('|\epsilon|_1');
    legend([legendNames],'location','best')
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    for i = 1:fileNum
        loglog(t{i}(:),(t{i}(:)).^1/(t{1}(end)^1)*resid{i}(1,end),'k--','linewidth',lWidth) 
    end
    hold off
    if outputOn
        fname1 = [baseDir,'/','errorTimeHistoryL1.eps'];
        print('-depsc2', fname1);    
    end

    %% maximum residual convergence history
    %figure()
    %for i = 1:fileNum
    %    for iEq = 1:nEqns
    %        %semilogy(t{i}(:),residMax{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        loglog(t{i}(:),residMax{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
    %        %plot(t{i}(:),residMax{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on
    %    end 
    %end
    %hx = xlabel('Time'); 
    %hy = ylabel('|Resid|max'); % hy = ylabel('|discr|_1'); 
    %legend([legendNames],'location','best')
    %set(gca,'FontSize',fSize)
    %set(hx,'FontSize',fSize)
    %set(hy,'FontSize',fSize)
    %%for i = 1:fileNum
    %%    loglog(t{i}(:),(t{i}(:)).^1/(t{1}(end)^1)*residMax{i}(1,end),'k--','linewidth',lWidth) 
    %%end
    %hold off
    %if outputOn
    %    fname1 = [basedir,'/','discrepancyTimeHistoryL1.eps'];
    %    print('-depsc2', fname1);    
    %end

    %
    h = [2^-1, 2^-2, 2^-3, 2^-4, 2^-5];
    iEq = 1;
    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','err_node','Order','err_discr','Order')
    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
    for i = 1:fileNum
        if i == 1
            fprintf('%5d  %e   %e   %5s   %e    %7s \n',i,h(i),resid{i}(iEq,end),'',residMax{i}(iEq,end),'');
        else
            fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                i,h(i),resid{i}(iEq,end), ...
                log(resid{i-1}(iEq,end)/resid{i}(iEq,end))/log(h(i-1)/h(i)), ...
                residMax{i}(iEq,end), ... 
                log(residMax{i-1}(iEq,end)/residMax{i}(iEq,end))/log(h(i-1)/h(i)))
        end 

    end

end

function [fIter, fT, fResid, fResidMax] = readResidSol(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    nEqns = (cols-2)/2;
    
    % initialize output variables
    fIter = zeros(totalLevs,1); 
    fT = zeros(totalLevs,1);
    fResid = zeros(nEqns,totalLevs,1);
    fResidMax = zeros(nEqns,totalLevs,1);

    for lev = 1:totalLevs
        fIter(lev) = A(lev,1); 
        fT(lev) = A(lev,2);
        for iEq = 1:nEqns
            fResid(iEq,lev) = A(lev,2+iEq);
            fResidMax(iEq,lev) = A(lev,2+nEqns+iEq);
        end
    end

end
