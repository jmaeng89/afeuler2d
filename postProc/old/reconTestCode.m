function [qArr, qArrNew] = reconTestCode()
    
    close all;

    xiNode(1,:) = [0,0];
    xiNode(2,:) = [1,0];
    xiNode(3,:) = [0,1];
    xiEdge(1,:) = [0.5,0.5];
    xiEdge(2,:) = [0,0.5];
    xiEdge(3,:) = [0.5,0];

    for i = 1:3
        xi(1:2) = xiNode(i,:);
        q(2*i-1) = qFunc(xi(1),xi(2));

        xi(1:2) = xiEdge(i,:);
        q(2*i) = qFunc(xi(1),xi(2));
    end

    %q7 = qFunc(xi(1),xi(2));
    %qAvg = 9/20*q7 + 1.0/20*(q(1)+q(3)+q(5)) + 2/15*(q(2)+q(4)+q(6));
    qAvg = 1/3*(q(2)+q(4)+q(6)) + 0.01;
    q7 = 20/9*(qAvg - 1.0/20*(q(1)+q(3)+q(5)) - 2/15*(q(2)+q(4)+q(6)));
    %xi(1) = 1/3;
    %xi(2) = 1/3;
    %q7 = quadraticRecon(xi(1),xi(2),q(1:6),qAvg);
    qArr = [q,q7,qAvg];

    fx = (4.0*q(6) - 3.0*q(1)-q(3)) + (4.0*q(1) + 4.0*q(3) - 8.0*q(6))*(1.0/3.0) + (4.0*q(1) + 4.0*q(2) - ...
         4.0*q(4) - 4.0*q(6))*(1.0/3.0);

    fy = (4.0*q(4) - 3.0*q(1) - q(5)) + (4.0*q(1) + 4.0*q(2) - 4.0*q(4) - 4.0*q(6))*(1.0/3.0) + ...
         (4.0*q(1) + 4.0*q(5) - 8.0*q(4))*(1.0/3.0) ;

    fxx = 4.0*q(1) + 4.0*q(3) - 8.0*q(6);

    fyy = 4.0*q(1) + 4.0*q(5) - 8.0*q(4);

    fxy = 4.0*q(1) + 4.0*q(2) - 4.0*q(4) - 4.0*q(6);
    
    dq = qAvg-1/3*(q(2)+q(4)+q(6))
    qAvg-(9/20*quadraticRecon(1/3,1/3,q,qAvg)+1/20*(q(1)+q(3)+q(5))+2/15*(q(2)+q(4)+q(6)))
    qAvg-(3/4*quadraticRecon(1/3,1/3,q,qAvg)+1/12*(q(1)+q(3)+q(5)))
    for i = 1:3
        xi(1:2) = xiNode(i,:);
        xi(1) = xi(1) - 1/3;
        xi(2) = xi(2) - 1/3;
        %qNew(2*i-1) = q7 + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy;  
        %qNew(2*i-1) = qAvg + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy  ;  
        qNew(2*i-1) = q(2*i-1) + (dq);  

        xi(1:2) = xiEdge(i,:);
        xi(1) = xi(1) - 1/3;
        xi(2) = xi(2) - 1/3;
        %qNew(2*i) = q7 + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy;  
        %qNew(2*i) = qAvg + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy  ;  
        qNew(2*i) = q(2*i) + (dq);  
    end
    xi(1) = 1/3-1/3;
    xi(2) = 1/3-1/3;
    %qNew7 = qAvg + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy;
    qNew7 = q7 + xi(1)*fx + xi(2)*fy + 0.5*fxx*xi(1)*xi(1) + 0.5*fyy*xi(2)*xi(2) + xi(1)*xi(2)*fxy;
    %qNew7 = q(2*i-1) + 1/3*(dq);  
    %xi(1) = 1/3;
    %xi(2) = 1/3;
    %qNew7 = quadraticRecon(xi(1),xi(2),qNew(1:6),qAvg);

    %qNewAvg = 9.0/20.0*qNew7 + 1.0/20.0*(qNew(1)+qNew(3)+qNew(5)) + 2/15*(qNew(2)+qNew(4)+qNew(6));
    qNewAvg = 1/3*(qNew(2)+qNew(4)+qNew(6));

    qArrNew = [qNew,qNew7,qNewAvg];

    x = linspace(0,1,101);
    y = linspace(0,1,101);
    for i = 1:length(x)
        for j = 1:length(y)
            if ( x(i) + y(j) > 1 )
                U(i,j) = NaN;
                UNew(i,j) = NaN;
            else
                U(i,j) = quadraticRecon(x(i),y(j),q(1:6),qAvg);
                UNew(i,j) = quadraticRecon(x(i),y(j),qNew(1:6),qNewAvg);
            end
        end 
    end
    [X,Y] = meshgrid(x,y);

    figure(1)
    hold on;
    %surf(X,Y,U)
    mesh(X,Y,U);
    surf(X,Y,UNew)
    shading interp;
    hold off;

end 

function [q] = qFunc(x,y)
    q = x^2 + y^2;
end

function [u] = quadraticRecon(x,y,q,qAvg)
    s1 = 1.0 - (x+y);
    s2 = x;
    s3 = y;
            
    phi(1) = s1*(2.0*s1-1.0);
    phi(2) = 4.0*s2*s3;
    phi(3) = s2*(2.0*s2-1.0);
    phi(4) = 4.0*s3*s1;
    phi(5) = s3*(2.0*s3-1.0);
    phi(6) = 4.0*s1*s2;
    %phi(7:10) = 0.0;
    phi(7) = 27.0*s1*s2*s3;
    phi(8:10) = 0.0;

    %q(7) = 20/9*(qAvg-1/3*(q(2)+q(4)+q(6)));
    q(7) = 0;
    u = 0;
    for i = 1:7
        u = u + phi(i)*q(i) ;
    end 
end


