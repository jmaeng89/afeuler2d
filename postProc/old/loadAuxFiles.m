function[] = loadAuxFiles(baseDir,caseDir)
% LOADAUXFILES(BASEDIR,CASEDIR) loads auxilary files

    close all;
    
    %baseDir = '../cases/plesseuler/freestream/org_grid';
    baseDir = '../cases/plesseuler/freestream';
    %baseDir = '../cases/plesseuler/freestream/new_grid2';
    caseDir = {'debug/auxunstL003_debug.dat'}; 
    %caseDir = {'org_grid/auxunstL003.dat'}; 
    %caseDir = {'new_grid/auxunstL003_1.dat'}; 
    %caseDir = {'new_grid2/auxunstL003_2.dat'};
    %caseDir = {'org_grid_onlycons/auxunstL003.dat'};
    %caseDir = {'org_grid/auxunstL003.dat', 'new_grid/auxunstL003_1.dat' , ...
    %           'new_grid2/auxunstL003_2.dat', 'org_grid_onlycons/auxunstL003.dat'}; 
     
    for i = 1:size(caseDir,2)
        %filename = 'auxunstL003.dat';    % unstL003.grd
        %filename = 'auxunstL003_1.dat';  % unstL003_1.grd
        %filename = 'auxunstL003_2.dat';  % unstL003_2.grd
        filename = caseDir{i};
        FN = [baseDir,'/',filename];     % complete file location
        [fIter, fT, fV1, fV2, fV3, fV4, fV5, fV6] = readFile(FN);
        iT{i} = fIter;
        t{i} = fT; 

        V1{i} = fV1;
        V2{i} = fV2;
        V3{i} = fV3;
        V4{i} = fV4;
        V5{i} = fV5;
        V6{i} = fV6;
    end
        
    [nEqns,totalLevs] = size(V1{1});

    style = ['b-';'r-';'k-';'g-';'m-';'y-'];
    lWidth = 2;
    fSize = 18;

    %std(V1{1}(2,:))
    %std(V2{1}(2,:))
    %std(V4{1}(2,:))
    %std(V5{1}(2,:))
    %V1{1}(1,1) 
    %V2{1}(1,1)
    %V4{1}(1,1)
    %V5{4}(1,1)

    % choose equation to display
    figure(1)
    for i = 1:size(t,2)
        for iEq = 1
            semilogy(t{i}(:), abs(V1{i}(iEq,:)-V1{i}(iEq,1))/V1{i}(iEq,1), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            semilogy(t{i}(:), abs(V2{i}(iEq,:)-V2{i}(iEq,1))/V2{i}(iEq,1), style(iEq+2,:),'linewidth',lWidth,'markersize',fSize)
            %semilogy(t{i}(:), V1{i}(iEq,:), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            %semilogy(t{i}(:), V2{i}(iEq,:), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            %if iEq == nEqns
            %    hx = xlabel('t'); hy = ylabel('Density loc 1');
            %    set(gca,'FontSize',fSize)
            %    set(hx,'FontSize',fSize)
            %    set(hy,'FontSize',fSize)
            %    set(gca,'FontSize',fSize)
            %end
        end
    end
    hx = xlabel('t'); hy = ylabel('Density loc 1');
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    set(gca,'FontSize',fSize)

    figure(2)
    for i = 1:size(t,2)
        for iEq = 2
            semilogy(t{i}(:), abs(V1{i}(iEq,:)-V1{i}(iEq,1))/V1{i}(iEq,1), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            semilogy(t{i}(:), abs(V2{i}(iEq,:)-V2{i}(iEq,1))/V2{i}(iEq,1), style(iEq+2,:),'linewidth',lWidth,'markersize',fSize)
            %if iEq == nEqns
            %    hx = xlabel('t'); hy = ylabel('Velocity loc 1');
            %    set(gca,'FontSize',fSize)
            %    set(hx,'FontSize',fSize)
            %    set(hy,'FontSize',fSize)
            %    set(gca,'FontSize',fSize)
            %end
        end
    end
    hx = xlabel('t'); hy = ylabel('Velocity loc 1');
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    set(gca,'FontSize',fSize)

    stop
    figure(3)
    for i = 1:size(t,2)
        for iEq = 1
            semilogy(t{i}(:), abs(V4{i}(iEq,:)-V4{i}(iEq,1))/V4{i}(iEq,1), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            semilogy(t{i}(:), abs(V5{i}(iEq,:)-V5{i}(iEq,1))/V5{i}(iEq,1), style(iEq+2,:),'linewidth',lWidth,'markersize',fSize)
            %if iEq == nEqns
            %    hx = xlabel('t'); hy = ylabel('Density loc 2');
            %    set(gca,'FontSize',fSize)
            %    set(hx,'FontSize',fSize)
            %    set(hy,'FontSize',fSize)
            %    set(gca,'FontSize',fSize)
            %end
        end
    end
    hx = xlabel('t'); hy = ylabel('Density loc 2');
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    set(gca,'FontSize',fSize)

    figure(4)
    for i = 1:size(t,2)
        for iEq = 2
            semilogy(t{i}(:), abs(V4{i}(iEq,:)-V4{i}(iEq,1))/V4{i}(iEq,1), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            semilogy(t{i}(:), abs(V5{i}(iEq,:)-V5{i}(iEq,1))/V5{i}(iEq,1), style(iEq+2,:),'linewidth',lWidth,'markersize',fSize)
            %if iEq == nEqns
            %    hx = xlabel('t'); hy = ylabel('Velocity loc 2');
            %    set(gca,'FontSize',fSize)
            %    set(hx,'FontSize',fSize)
            %    set(hy,'FontSize',fSize)
            %    set(gca,'FontSize',fSize)
            %end
        end
    end
    hx = xlabel('t'); hy = ylabel('Velocity loc 2');
    set(gca,'FontSize',fSize)
    set(hx,'FontSize',fSize)
    set(hy,'FontSize',fSize)
    set(gca,'FontSize',fSize)

    %figure(3)
    %for i = 1:size(t,2)
    %    for iEq = 1:2
    %        semilogy(t{i}(:), V1{i}(iEq,:)+V4{i}(iEq,:), style(iEq,:),'linewidth',lWidth,'markersize',fSize)
    %        semilogy(t{i}(:), V2{i}(iEq,:)+V5{i}(iEq,:), style(iEq+2,:),'linewidth',lWidth,'markersize',fSize)
    %        hold on;
    %        if iEq == nEqns
    %            hx = xlabel('t'); hy = ylabel('V1');
    %            set(gca,'FontSize',fSize)
    %            set(hx,'FontSize',fSize)
    %            set(hy,'FontSize',fSize)
    %            set(gca,'FontSize',fSize)
    %        end
    %    end
    %end


% 	fname1 = [baseDir,'/','cons_resid.eps'];
% 	print('-depsc2', '-r300', fname1);

%     figure(2); 
%     for i = 1:size(t,2)
%         for iEq = 1:nEqns
%             plot(t{i}(:), uTempErr{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
% %             semilogy(t{i}(:), uTempErr{i}(iEq,:),style(i,:),'linewidth',lWidth,'markersize',fSize)
%             hold on;
%             if iEq == nEqns
%                 hx = xlabel('t'); hy = ylabel('\Sigma_{j}(u^n^+^1_j)^2 - (u^n_j)^2');
%                 set(gca,'FontSize',fSize)
%                 set(hx,'FontSize',fSize)
%                 set(hy,'FontSize',fSize)
%                 set(gca,'FontSize',fSize)
% 
%             end
%         end
%     end
                   
%	fname1 = [baseDir,'/','HJE_energy_Eq2_error.eps'];
%	print('-depsc2', '-r300', fname1);
                 

        
end

function [fIter, fT, fV1, fV2, fV3, fV4, fV5, fV6] = readFile(file)
% SOLERR(FILE) returns the L_1 and L_2 error norm for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);

    % determine the number of equations, 
    nEqns = 4; %(cols-2)/4    

    % initialize output variables
    fIter = zeros(1,totalLevs); 
    fT = zeros(1,totalLevs);
    fV1 = zeros(nEqns,totalLevs);
    fV2 = zeros(nEqns,totalLevs);
    fV3 = zeros(nEqns,totalLevs);
    fV4 = zeros(nEqns,totalLevs);
    fV5 = zeros(nEqns,totalLevs);
    fV6 = zeros(nEqns,totalLevs);

    for lev = 1:totalLevs
        fIter(lev) = A(lev,1); 
        fT(lev) = A(lev,2);
        for iEq = 1:nEqns
            fV1(iEq,lev) = A(lev,2+iEq);
            fV2(iEq,lev) = A(lev,2+iEq+nEqns);
            fV3(iEq,lev) = A(lev,2+iEq+2*nEqns);
            fV4(iEq,lev) = A(lev,2+iEq+3*nEqns);
            fV5(iEq,lev) = A(lev,2+iEq+4*nEqns);
            fV6(iEq,lev) = A(lev,2+iEq+5*nEqns);
        end
    end

end
