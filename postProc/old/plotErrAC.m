function[] = plotErrConvergenceSystem(baseDir,caseDir)
% PLOTERRAC(BASEDIR,CASEDIR) returns the error convergence
%   result from a series of mesh refinements. 
%
%   J. Brad Maeng
%   9/19/2016 - created 
%
    close all;

    %% acoustic
    baseDir = '../cases/acoustic_test/acpulse_unst_noncons';
    baseDir = '../cases/acoustic_test/acpulse_diag_noncons';
    %baseDir = '../cases/acoustic_test/acpulse_diag_rec1';
    %baseDir = '../cases/acoustic_test/acpulse_unst_rec1';
    %baseDir = '../cases/acoustic_test/acpulse_unst_rec0';
    %baseDir = '../cases/acoustic_test/acpulse_diag_rec0';

    caseDir = {''};

    % output flag
    outputOn = false;
    %outputOn = true;

    %% conserved variable 
    consVar = false; % primitive 
    consVar = true; % conserved variable
 
    % variables names
    if consVar
        pvNames = {'\rho', '\rho u', '\rho v', '\rho E'};
    else
        pvNames = {'\rho', 'u', 'v', 'p'};
    end 
 
    % legend names
    legendNames = pvNames;
    
    [~,fileNum] = size(caseDir);
    
    % Allocate variables for error and spacing
    for i = 1:fileNum
        %if isempty(caseDir{i})
        %    if consVar
        %        FN = [baseDir,'/','conserverrData','.dat'];     % complete file location     
        %    else
        %        FN = [baseDir,'/','errData','.dat'];    
        %    end
        %else
        %    if consVar
        %        FN = [baseDir,caseDir{i},'/','conserverrData','.dat'];     % complete file location
        %    else
        %        FN = [baseDir,caseDir{i},'/','errData','.dat'];     
        %        %FN = [baseDir,caseDir{i},'/','errData_normalized','.dat'];    
        %    end
        %end 

        FN = [baseDir,caseDir{i},'/','integralErrData','.dat'];     

        [fh, fL1, fL2] = readErrData(FN);
        h{i} = fh;            % 1/dof^(-1/2)
        L1{i} = fL1;            % L1 norm of error
        L2{i} = fL2;            % L2 norm of error

    end

    [nEqns,totalLevs] = size(L1{1});
    % refinement levels
    refLevel = 5;
    % hard code
    legendNames = legendNames(1:nEqns);

    nEqns = 3;
    % plot the functional error of 
    for i = 1:fileNum
        tempH = [];
        tempArrayL1 = [];
        tempArrayL2 = [];
        for iEq = 1:nEqns
            jInd = 1;
            for j = 1:length(h{i}(:))-1
                %if j > 1
                    tempH(jInd) = abs(h{i}(length(h{i}(:)))-h{i}(j));
                    tempArrayL1(iEq,jInd) = abs(L1{i}(iEq,length(h{i}(:)))-L1{i}(iEq,j));
                    tempArrayL2(iEq,jInd) = abs(L2{i}(iEq,length(h{i}(:)))-L2{i}(iEq,j));
                    jInd = jInd + 1;
                %end
            end
            %for j = 1:length(h{i}(:))
            %    if j > 1
            %        tempH(jInd) = h{i}(j);
            %        tempArrayL1(iEq,jInd) = abs(L1{i}(iEq,j-1)-L1{i}(iEq,j));
            %        tempArrayL2(iEq,jInd) = abs(L2{i}(iEq,j-1)-L2{i}(iEq,j));
            %        jInd = jInd + 1;
            %    end
            %end
        end
        deltah{i} = tempH;
        deltaL1{i} = tempArrayL1;
        deltaL2{i} = tempArrayL2;
    end 
    for i = 1:fileNum
        for iEq = 1:nEqns    
            for lev = 1:size(deltah{1},2)             
               if ( lev == 1 )
                    fprintf('\n %s \n',caseDir{i});
                    fprintf('Equation %5d \n', iEq);
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            'Level','h','L1Error','Order','L2Error','Order')
                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n', ...
                            '-----','-----','------------','-------','------------','-------')
                    fprintf('%5d  %e   %e   %5s   %e    %7s \n', ...
                        lev,deltah{i}(lev),deltaL1{i}(iEq,lev),'',deltaL2{i}(iEq,lev),'');
                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
                    fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
                        lev,h{i}(lev),deltaL1{i}(iEq,lev), ...
                        '       ', ...
                        deltaL2{i}(iEq,lev), ...
                        '       ')
                else
                    fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
                            lev,h{i}(lev),L1{i}(iEq,lev), ...
                            log(deltaL1{i}(iEq,lev-1)/deltaL1{i}(iEq,lev))/log(deltah{i}(lev-1)/deltah{i}(lev)), ...
                            L2{i}(iEq,lev), ... 
                            log(deltaL2{i}(iEq,lev-1)/deltaL2{i}(iEq,lev))/log(deltah{i}(lev-1)/deltah{i}(lev)) );
                end
            end
        end
    end

    hRef = h{1};
    % plot solution convergence 
    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
    style2 = ['bo-.';'rs-.';'k*-.';'gd-.';'m<-.'];
    lWidth = 2;
    fSize = 18;

    for i = 1:fileNum
        figure(3)
        for iEq = 1:nEqns
            loglog(deltah{i}(:),deltaL1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            if iEq == nEqns
                %loglog(deltah{i}(:),(deltah{i}(:)).^3/(deltah{i}(1)^3)*deltaL1{i}(1,1), 'k')    % 3rd order
                %loglog(deltah{i}(:),(deltah{i}(:)).^2/(deltah{i}(1)^2)*deltaL1{i}(1,1), 'k')    % 2rd order
                %loglog(deltah{i}(:),(deltah{i}(:)).^1/(deltah{i}(1)^1)*deltaL1{i}(1,1), 'k')    % 1rd order
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('\Delta|p\epsilon|_1');
                hold off
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
            end
        end
        hold off
    end
    
    % plot the difference in error of consecutive grid sizes
    for i = 1:fileNum
        figure(4)
        for iEq = 1:nEqns
            loglog(deltah{i}(:),deltaL2{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
            hold on;
            if iEq == nEqns
                %loglog(deltah{i}(:),(deltah{i}(:)).^3/(deltah{i}(1)^3)*deltaL2{i}(1,1), 'k')    % 3rd order
                %loglog(deltah{i}(:),(deltah{i}(:)).^2/(deltah{i}(1)^2)*deltaL2{i}(1,1), 'k')    % 2rd order
                %loglog(deltah{i}(:),(deltah{i}(:)).^1/(deltah{i}(1)^1)*deltaL2{i}(1,1), 'k')    % 1rd order%         
                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('\Delta|u\epsilon|_2');
                hold off
                set(gca,'FontSize',fSize)
                set(hx,'FontSize',fSize)
                set(hy,'FontSize',fSize)
            end
        end
    end
    
end

function [h, L1, L2] = readErrData(file)
% READERRDATA(FILE) returns the L1 and L2 norm errors for the solution 
%   contained in FILE

    A = load(file);

    [totalLevs, cols] = size(A);
    
    % determine the number of equations
    nEqns = (cols-3)/2;
    %nEqns = 4;
    
    % initialize output variables
    nCells = zeros(totalLevs,1);
    DOF = zeros(totalLevs,1);
    h = zeros(totalLevs,1); 
    L1 = zeros(nEqns,totalLevs);
    L2 = zeros(nEqns,totalLevs);
        
    for lev = 1:totalLevs
        h(lev) = A(lev,3);   % cell size, 1/sqrt(dof)
        nCells(lev) = A(lev,1);   % nCells
        DOF(lev) = A(lev,2);   % nCells + nEdges + nNodes
        for iEq = 1:nEqns
            L1(iEq,lev) = A(lev,3+iEq);
            L2(iEq,lev) = A(lev,3+iEq+nEqns);
        end
    end
            
end

     
%    if outputOn
%        % open a file to store the convergence study data
%        for i = 1:fileNum
%            if consVar
%                [fid] = fopen([baseDir,'/','conserverrConvergence',caseDir{i},'.dat'],'w+');
%            else
%                [fid] = fopen([baseDir,'/','errConvergence',caseDir{i},'.dat'],'w+');
%            end
%            for iEq = 1:nEqns
%                for lev = 1:totalLevs
%                    if ( lev == 1 )
%                        fprintf(fid, '\n %s \n', caseDir{i});
%                        fprintf(fid, 'Equation %5d, Variable %s\n', iEq, pvNames{iEq});
%                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
%                            'Level','h','L1Error','Order','L2Error','Order');
%                        fprintf(fid, '%5s  %12s   %12s  %7s  %12s  %7s\n', ...
%                            '-----','-----','------------','-------','------------','-------');
%                        fprintf(fid, '%5d %s %e %s %e %s %7s %s %e %s %7s %s\n', ...
%                            lev,'&', h{i}(lev),'&',L1{i}(iEq,lev),'&','','&',L2{i}(iEq,lev),'&','','\\');
%                    elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
%                        fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
%                            lev,h{i}(lev),L1{i}(iEq,lev), ...
%                            '       ', ...
%                            L2{i}(iEq,lev), ...
%                            '       ')
%                    else
%                        % latex tabular output
%                        fprintf(fid, '%5d %s %e %s %e %s %7.4f %s %e %s %7.4f %s\n',...
%                                lev,'&',h{i}(lev),'&',L1{i}(iEq,lev),'&', ...
%                                log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)),'&', ...
%                                L2{i}(iEq,lev),'&', ...
%                                log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), '\\');
%                    end
%                end
%            end
%            fclose(fid);    
%        end
%    end
%    
%    for i = 1:fileNum
%        for iEq = 1:nEqns
%            for lev = 1:totalLevs             
%               if ( lev == 1 )
%                    fprintf('\n %s \n',caseDir{i});
%                    fprintf('Equation %5d, Variable %s\n', iEq, pvNames{iEq});
%                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','Level','h','L1Error','Order','L2Error','Order')
%                    fprintf('%5s  %12s   %12s  %7s  %12s  %7s\n','-----','-----','------------','-------','------------','-------')
%                    fprintf('%5d  %e   %e   %5s   %e    %7s \n',lev,h{i}(lev),L1{i}(iEq,lev),'',L2{i}(iEq,lev),'');
%                elseif ( mod(lev,refLevel) == 1 && lev ~= 1)
%                    fprintf('%5d  %e   %e  %7s  %e  %7s\n',...
%                        lev,h{i}(lev),L1{i}(iEq,lev), ...
%                        '       ', ...
%                        L2{i}(iEq,lev), ...
%                        '       ')
%                else
%                    fprintf('%5d  %e   %e  %7.4f  %e  %7.4f\n',...
%                            lev,h{i}(lev),L1{i}(iEq,lev), ...
%                            log(L1{i}(iEq,lev-1)/L1{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)), ...
%                            L2{i}(iEq,lev), ... 
%                            log(L2{i}(iEq,lev-1)/L2{i}(iEq,lev))/log(h{i}(lev-1)/h{i}(lev)))
%                end
%            end
%        end
%    end
%    
%    hRef = h{1};
%    % plot solution convergence 
%    style = ['bo-';'rs-';'k*-';'gd-';'m<-'];
%    style2 = ['bo-.';'rs-.';'k*-.';'gd-.';'m<-.'];
%    lWidth = 2;
%    fSize = 18;
%
%    % L1 norm error
%    for i = 1:fileNum
%        figure(1)
%        %figure(i)
%        for iEq = 1:nEqns
%            if ( i == 1 )
%                loglog(h{i}(:),L1{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
%            else
%                loglog(h{i}(:),L1{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
%            end 
%            hold on
%            if ( iEq == nEqns && i == fileNum  )
%                loglog(hRef(:),0.01*(hRef(:)).^3/(hRef(1)^3)*L2{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
%                text(0.8*hRef(end),0.01*(hRef(end)).^3/(hRef(1)^3)*L2{i}(1,1),'3','FontSize',fSize)
%                legend([legendNames],'location','best')
%                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_1');
%                set(gca,'FontSize',fSize)
%                set(hx,'FontSize',fSize)
%                set(hy,'FontSize',fSize)
%                if outputOn
%                    if consVar
%                        fname1 = [baseDir,'/','cons_errorL1.eps'];
%                    else
%                        fname1 = [baseDir,'/','errorL1.eps'];
%                    end
%                    print('-depsc2', '-r300', fname1);
%                end
%            end
%        end
%    end
%    
%    % L2 norm error
%    for i = 1:fileNum
%        figure(2)
%        %figure(i+fileNum)        
%        for iEq = 1:nEqns
%            %figure(iEq)
%            if ( i == 1 ) 
%                loglog(h{i}(:),L2{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
%            else
%                loglog(h{i}(:),L2{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
%            end 
%            hold on
%            if ( iEq == nEqns && i == fileNum )
%                loglog(hRef(:),0.05*(hRef(:)).^3/(hRef(1)^3)*L2{i}(1,1),'k--','linewidth',lWidth)    % 3rd order
%                text(0.8*hRef(end),0.05*(hRef(end)).^3/(hRef(1)^3)*L2{i}(1,1),'3','FontSize',fSize)
%                legend([legendNames],'location','best')
%                hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
%                set(gca,'FontSize',fSize)
%                set(hx,'FontSize',fSize)
%                set(hy,'FontSize',fSize)
%                if outputOn
%                    if consVar
%                        fname2 = [baseDir,'/','cons_errorL2.eps'];
%                    else
%                        fname2 = [baseDir,'/','errorL2.eps'];
%                    end
%                    print('-depsc2', '-r300', fname2);
%                end
%            end
%        end
%    end
%    
%
%    %% L2 norm error convergence of extra variables
%    %for i = 1:fileNum
%    %    figure(3)
%    %    %for iEq = 1:nEqns
%    %    for iEq = 1:3
%    %        if ( i == 1 ) 
%    %            loglog(h{i}(:),L2_extra{i}(iEq,:),style(iEq,:),'linewidth',lWidth,'markersize',fSize)
%    %        else
%    %            loglog(h{i}(:),L2_extra{i}(iEq,:),style2(iEq,:),'linewidth',lWidth,'markersize',fSize)
%    %        end
%    %        hold on
%    %        if ( iEq == 3 && i == fileNum )
%    %            loglog(h{i}(:),(h{i}(:)).^1/(h{i}(1)^1)*L2_extra{i}(1,1),'k--','linewidth',lWidth)    % 1st order
%    %            loglog(h{i}(:),(h{i}(:)).^3/(h{i}(1)^3)*L2_extra{i}(1,1),'k-','linewidth',lWidth)    % 3rd order
%    %            legend([legendNames_extra],'location','best')
%    %            hx = xlabel('DOF^-^1^/^2'); hy = ylabel('|\epsilon|_2');
%    %            set(gca,'FontSize',fSize)
%    %            set(hx,'FontSize',fSize)
%    %            set(hy,'FontSize',fSize)
%    %            %if outputOn
%    %            %    %fname2 = [baseDir,'/','fastvortErrConvg_eq',iEq,'.eps'];
%    %            %    fname2 = sprintf('%s/fastvortErrConvg_eq%d.eps',baseDir,iEq);
%    %            %    print('-depsc2', '-r300', fname2);
%    %            %end
%    %        end 
%    %    end
%    %end
%
%
