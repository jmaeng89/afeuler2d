# plotErrConvergenceSystem.py
# returns the error convergence result from a series of mesh refinements
#
# J. Brad Maeng
# 1/11/2017 - Initial creation

import numpy as np
#import matplotlib.pyplot as plt
import os

def main():
    # base and case directories
    #baseDir = 'cases/euler_new_formulation/mvortex_unst';
    baseDir = 'cases/euler_nonconservative/mvortex_diag';

    # conservation
    consVar = True;
    consVar = False;

    #outputOn = True;
    outputOn = False;

    # Allocate variables for error and spacing
    if consVar:
        FN = baseDir+'/'+'conserverrData'+'.dat';
        var = (r'$\rho$', r'$\rho u$', r'$\rho v$', r'$\rho E$');
        figName = 'conserrorL2';
    else:
        FN = baseDir+'/'+'errData'+'.dat';
        var = (r'$\rho$', 'u', 'v', 'p');
        figName = 'errorL2';

    # read error data
    h, L1err, L2err = ReadErrData(FN);
    nEqns, totalLevs = np.shape(L2err);

    print(nEqns, totalLevs)

    refLevel = 5
    for iEq in xrange(0,nEqns):
        for lev in xrange(0,totalLevs):
            if lev == 0:
                print('Equation %5d, Variable %7s \n' % (iEq,var[iEq]) )
                print('%5s  %10s  %10s  %10s\n' % ('Level','h','L2Error','Order'))
                print('%5s  %10s  %10s  %10s\n' % ('-----','----------','----------','----------'))
                print('%5d  %6.4e  %6.4e  %7s\n' % (lev,h[lev],L2err[iEq,lev],''))
            elif ( np.mod(lev,refLevel) == 1 and lev != 1):
                print('%5d  %6.4e  %6.4e  %7s\n' % (lev,h[lev],L2err[iEq,lev],''))
            else:
                print('%5d  %6.4e  %6.4e  %7.4f\n' % (
                        lev, h[lev], L2err[iEq,lev],
                        np.log(L2err[iEq,lev-1]/L2err[iEq,lev])/np.log(h[lev-1]/h[lev]) ) )
    if outputOn:
        # output tabulated data
        fileOut = open('ErrConvergence.dat','w')
        for iEq in xrange(0,nEqns):
            for lev in xrange(0,totalLevs):
                if lev == 0:
                    fileOut.write('Equation %5d, Variable %7s \n' % (iEq,var[iEq]) )
                    fileOut.write('%5s  %10s  %10s  %10s\n' % ('Level','h','L2Error','Order'))
                    fileOut.write('%5s  %10s  %10s  %10s\n' % ('-----','----------','----------','----------'))
                    fileOut.write('%5d  %6.4e  %6.4e  %7s\n' % (lev,h[lev],L2err[iEq,lev],''))
                elif ( np.mod(lev,refLevel) == 1 and lev != 1):
                    fileOut.write('%5d  %6.4e  %6.4e  %7s\n' % (lev,h[lev],L2err[iEq,lev],''))
                else:
                    fileOut.write('%5d  %6.4e  %6.4e  %7.4f\n' % (
                            lev, h[lev], L2err[iEq,lev],
                            np.log(L2err[iEq,lev-1]/L2err[iEq,lev])/np.log(h[lev-1]/h[lev]) ) )
        fileOut.close()

    stop

    style = ('-bo','--rs',':k^','-.gv');
    fSize = 20;
    mSize = 24
    lWidth = 4;
    # L2 norm error plot
    plt.figure(1)
    for iEq in xrange(0,nEqns):
        plt.loglog(h,L2err[iEq,:],style[iEq],fillstyle='none',linewidth=lWidth,markersize=mSize,markeredgewidth=lWidth)

    plt.loglog(h,0.1*h**3.0/h[0]**3.0*L2err[0,0],'k')
    plt.ylabel(r'$\epsilon_{2}$',fontsize=fSize)
    plt.xlabel('h',fontsize=fSize)
    plt.xticks(fontsize=fSize)
    plt.yticks(fontsize=fSize)
    plt.legend(var,loc='best',markerscale=1.0,fontsize=fSize,numpoints=1)
    plt.show()
    if outputOn:
        plt.savefig(figName+'.eps', dpi='300', format=eps, frameon=False)


def ReadErrData(FN):
    #READERRDATA(FN) returns the L1 and L2 norm errors for the solution
    #  contained in FN

    data = np.genfromtxt(FN);

    totalLevs, cols = np.shape(data);

    # determine the number of equations
    nEqns = (cols-3)/2;

    fnCells = data[:,0];
    fDOF = data[:,1];
    fh = data[:,2];

    fL1err = np.zeros((nEqns,totalLevs));
    fL2err = np.zeros((nEqns,totalLevs));
    for lev in xrange(totalLevs):
        for iEq in xrange(nEqns):
            fL1err[iEq,lev] = data[lev,2+iEq];
            fL2err[iEq,lev] = data[lev,2+iEq+nEqns];

    return fh, fL1err, fL2err

if __name__ == '__main__':
    main()

