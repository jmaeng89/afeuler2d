function [xEdge, yEdge] = gridDisp(gridDir,gridFile)
% Reads and displays a AVUS/Cobalt formatted (*.grd) mesh from a file.
% Jungyeoul Brad Maeng
% 9/6/2013
    % check for input arguments
    if nargin ~= 2
        gridDir = '';
        gridFile = 'diag002';
    end
    
    % file open
    fid = fopen([gridDir,gridFile,'.grd'], 'r');

    % read line by line to obtain information about the mesh
    line1 = str2num(fgetl(fid));
    line2 = str2num(fgetl(fid));

    % assign line1 and line2 information to variables
    nDim = line1(1); nZones = line1(2); nPatches = line1(3);
    nNodes = line2(1); nFaces = line2(2); nCells = line2(3); 
    maxNodesPerFace = line2(4); maxFacesPerCell = line2(5);

    nCells
    
    % read X = (x, y) coordinates for each node
    for iNode = 1:nNodes

        X(:,iNode) = str2num(fgetl(fid));

    end

    % Face information

    for iFace = 1:nFaces

        tempLine = str2num(fgetl(fid));

        switch nDim

            case 1    
                nodesPerFace(1,iFace) = tempLine(1);        % number of nodes in face
                faceNodes(1,iFace) = tempLine(2);       % nodes consisting face
                faceCells(1:2,iFace) = tempLine(3:4);        % current cell, neighbor cell

            case 2   
                nodesPerFace(1,iFace) = tempLine(1);        % number of nodes in face
                faceNodes(1:2,iFace) = tempLine(2:3);       % nodes consisting face
                faceCells(1:2,iFace) = tempLine(4:5);       % current cell, neighbor cell
                        
        end

    end
    fclose(fid);

    
    cellFaces = zeros(maxFacesPerCell,nCells);
    % plot mesh
    figure(1)
    clf
    hold on
    axis('equal')
    % go around cellInfo and plot face of current cell
    for iFace = 1:nFaces

        switch nDim

            case 1

                xEdge = [X(faceNodes(1,iFace))];
                yEdge = [X(faceNodes(1,iFace))];
                plot(xEdge, yEdge,'b-')

            case 2

                lCell = faceCells(1,iFace);  % current cell number
                rCell = faceCells(2,iFace);  % neighbor cell number 
                xEdge = [X(1,faceNodes(1,iFace)), X(1,faceNodes(2,iFace))];
                yEdge = [X(2,faceNodes(1,iFace)), X(2,faceNodes(2,iFace))];
                
                plot( xEdge, yEdge, 'b-');

                % faces contained in a cell
                for iSide = 1:2
                    jCell = faceCells(iSide,iFace);   
                    if ( jCell > 0 )
                        if cellFaces(1,jCell) == 0
                            cellFaces(1,jCell) = iFace;
                        else
                            jFace = 2;
                            while ( abs(cellFaces(jFace,jCell)) > 0 )
                                jFace = jFace + 1;
                            end
                            cellFaces(jFace,jCell) = iFace;
                        end
                    end
                end
                
                % plot neighboring cell info on faces                
                text( mean(xEdge), mean(yEdge), num2str(iFace) ) % face number
                % plot boundary numbers for cells located at the boundary
                %if ( rCell < 0 ) 
                %    text( mean(xEdge), mean(yEdge), num2str(rCell) ) % face number
                %end

        end
%     axis('square')
    end
    
    % plot node numbers
    for iNode = 1:nNodes
        text( X(1,iNode), X(2,iNode), num2str(iNode), 'color', 'r') 
    end
 
    % plot cell numbers
    for iCell = 1:nCells
        for jFace = 1:2 
            for iFace = 1:maxFacesPerCell
                if ( iFace == 1 && jFace == 1 )
                    xFace = [X(1,faceNodes(jFace,cellFaces(iFace,iCell)))];
                    yFace = [X(2,faceNodes(jFace,cellFaces(iFace,iCell)))];
                else
                    xFace = [xFace, X(1,faceNodes(jFace,cellFaces(iFace,iCell)))];
                    yFace = [yFace, X(2,faceNodes(jFace,cellFaces(iFace,iCell)))];
                end
            end
        end

        text( mean(xFace), mean(yFace), num2str(iCell), 'color', 'b' ) % cell number 
     end
    
end
    
