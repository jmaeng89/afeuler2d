#-------------------------------------------------------------------------------
# @ purpose
#  Run fortran generated error executable
#
# @ history
#  8 October 2013 - Initial creation (Maeng)
#
# required libraries
import os, sys, numpy, subprocess

# local variables
#numFiles = 7 # number of grid refinements
numFiles = 5 # number of grid refinements
#numFiles = 4 # number of grid refinements
caseDir = 'cases/euler_test/mvortex_diagL' # directory containing cases,drivers
# for loop to run executables
for i in range(0, numFiles):
    print('%d',i)
    #j = ((2*4**(i)))
    #j = i # + 2
    j = i # + 2
    caseNum = '%03d' % j
    caseName = caseDir+'/diagL'+caseNum+'.drv'	# case name

    # run main executable
    #subprocess.call(['./afEuler',caseName])
    # run error executable
    subprocess.call(['./afError',caseName])

