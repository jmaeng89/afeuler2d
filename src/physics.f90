!-------------------------------------------------------------------------------
!> @purpose 
!>  Physics routines that are independent of solution method
!>
!> @author
!>  Timothy A. Eymann
!>
module physics

    use solverVars, only: FP
    implicit none

    integer, parameter :: ADVECTION_FLUX = 1,   & !< flag for advection flux
                          ACOUSTIC_FLUX = 5,    & !< flag for acoustic flux 
                          PLESSEULER_FLUX = 2,  & !< flag for pressureless Euler flux
                          ISENTEULER_FLUX = 3,  & !< flag for isentropic Euler flux
                          EULER_FLUX = 4          !< flag for Euler flux

    integer, parameter :: DENSITY = 1,  & !< density index
                          UVEL = 2,     & !< x-component of velocity
                          VVEL = 3,     & !< y-component of velocity
                          PRESSURE = 4    !< pressure index
    
    real(FP), parameter :: gam = 1.4_FP !< ratio of specific heats for air

    real(FP), parameter :: isentropicConst = 1.0_FP

    real(FP), parameter :: Rgas = 287.15_FP !< air gas constant [J/kg/K]

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert a vector of primitive values to conservative values.  Assumes
!>  Euler equation
!>
!> @history
!>  14 May 2013 - Initial creation
!>  26 January 2015 - Modified for pressureless euler (Maeng)
!>  16 February 2015 - Added multidimension case (Maeng)
!>  9 October 2015 - Euler equations (Maeng)
!>
function prim2conserv(nDim,nEqns,q)

    implicit none

    ! Interface vairables
    integer, intent(in) :: nDim,    & !< problem dimension 
                           nEqns      !< number of fields in state vector
    
    real(FP), intent(in) :: q(nEqns) !< primitive state vector

    ! Function variable
    real(FP) :: prim2conserv(nEqns)

    ! Local variables
    real(FP) :: velMag !< square of velocity magnitude

    prim2conserv(:) = 0.0_FP
    ! pressureless euler equations
    select case ( nDim )
        
        case ( 1 )

            prim2conserv(1) = q(1)
            prim2conserv(2) = q(1)*q(2)

        case ( 2 )

            if ( nEqns == 3 ) then
                ! barotropic, isentropic Euler
                prim2conserv(1) = q(DENSITY)
                prim2conserv(2) = q(DENSITY)*q(UVEL)
                prim2conserv(3) = q(DENSITY)*q(VVEL)
            else
                ! Euler
                !velMag = dot_product(q(UVEL:VVEL),q(UVEL:VVEL))
                velMag = q(UVEL)*q(UVEL) + q(VVEL)*q(VVEL)
                prim2conserv(1) = q(DENSITY)
                prim2conserv(2) = q(DENSITY)*q(UVEL)
                prim2conserv(3) = q(DENSITY)*q(VVEL)
                prim2conserv(4) = q(PRESSURE)/(gam-1.0_FP) + &
                                    0.5_FP*q(DENSITY)*velMag
            end if

    end select

end function prim2conserv
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert a vector of constervative values to primitive values.  Assumes
!>  Euler equation
!>
!> @history
!>  14 May 2013 - Initial creation
!>  26 January 2015 - Modified for pressureless euler (Maeng)
!>
function conserv2prim(nDim,nEqns,q)

    implicit none

    ! Interface vairables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of fields in state vector
    
    real(FP), intent(in) :: q(nEqns) !< conservative state vector

    ! Function variable
    real(FP) :: conserv2prim(nEqns)

    conserv2prim(:) = 0.0_FP
    select case ( nDim )
        
        case ( 1 )

            conserv2prim(1) = q(1)
            conserv2prim(2) = q(2)/q(1)

        case ( 2 )

            if ( nEqns == 3 ) then
                ! pressureless Euler, isentropic Euler
                conserv2prim(1) = q(1)
                conserv2prim(2) = q(2)/q(1)
                conserv2prim(3) = q(3)/q(1)
            else
                ! full Euler
                conserv2prim(DENSITY) = q(1)
                conserv2prim(UVEL) = q(2)/q(1)
                conserv2prim(VVEL) = q(3)/q(1)
                conserv2prim(PRESSURE) = (gam-1.0_FP)*(q(4) - &
                                            0.5_FP*dot_product(q(2:3), &
                                                        q(2:3))/q(1))
            end if

    end select

end function conserv2prim
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert a vector of primitive values to parameter vectors.
!>
!> @history
!>  29 December 2015 - Initial creation (Maeng)
!>
function prim2param(nDim,nEqns,q)

    implicit none

    ! Interface vairables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of fields in state vector
    
    real(FP), intent(in) :: q(nEqns) !< conservative state vector

    ! Function variable
    real(FP) :: prim2param(nEqns)

    real(FP) :: H   !< total specific enthalpy

    select case ( nDim )
        
        case ( 1 )

            prim2param(1) = sqrt(q(1))
            prim2param(2) = sqrt(q(1))*q(2)

        case ( 2 )

            if ( nEqns == 3 ) then
                ! barotropic Euler/ pressureless
                prim2param(1) = sqrt(q(1))
                prim2param(2) = sqrt(q(1))*q(2)
                prim2param(3) = sqrt(q(1))*q(3)
            else
                ! Euler
                H = enthalpy(nEqns,q)  
                prim2param(1) = sqrt(q(1))
                prim2param(2) = sqrt(q(1))*q(2)
                prim2param(3) = sqrt(q(1))*q(3)
                prim2param(4) = sqrt(q(1))*H
            end if

    end select

end function prim2param
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the speed of sound from a primitive state vector
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  13 May 2015 - Barotropic Euler equation (Maeng)
!>
function soundSpeed(nEqns,qPrim)

    use solverVars, only: eps
    implicit none
    
    ! Interface variables
    integer, intent(in) :: nEqns !< number of equations

    real(FP), intent(in) :: qPrim(nEqns) !< primitive state vector

    ! Function variable
    real(FP) :: soundSpeed

    ! Local variables
    real(FP) :: p,  & !< pressure
                rho   !< density

#ifdef VERBOSE
    !if ( ( qPrim(DENSITY) <= eps ) .or. &
    !     ( qPrim(PRESSURE)/qPrim(DENSITY) < 0.0_FP ) ) then
    if ( ( qPrim(DENSITY) <= eps ) ) then
        write(*,*)
        write(*,'(x,a)') 'WARNING: Sound speed calculation will fail.'
        write(*,'(5x,a,*(e14.6e2))') 'state: ', qPrim(:)
        read(*,*) 
    end if
#endif

    rho = qPrim(DENSITY)
    p = qPrim(PRESSURE)

    if ( ( rho <= 1.0*eps ) ) then
        write(*,*)
        write(*,'(x,a)') 'WARNING: Sound speed calculation will fail &
            due to negative density.'
        !write(*,'(5x,a,*(e14.6e2))') 'state: ', qPrim(:)
        !read(*,*) 
        ! modify density by small positive value
        rho = 1e2*eps
    else if ( ( p <= 1.0*eps ) ) then
        write(*,*)
        write(*,'(x,a)') 'WARNING: Sound speed calculation will fail &
            due to negative pressure.'
        !write(*,'(5x,a,*(e14.6e2))') 'state: ', qPrim(:)
        !read(*,*) 
        ! modify pressure by small positive value
        p = 1e2*eps
    end if

    ! Isentropic Euler
    !rho = qPrim(DENSITY)
    !p = isentropicConst*(rho**(gam))
    !soundSpeed = sqrt(gam*p/rho)

    ! Euler equations
    soundSpeed = sqrt(gam*p/rho)

end function soundSpeed
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate specific total energy from a primitive state vector
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>
function energy(nEqns,qPrim)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< number of equations

    real(FP), intent(in) :: qPrim(nEqns) !< primitive state vector

    ! Function variable
    real(FP) :: energy


#ifdef VERBOSE
    if ( qPrim(DENSITY) == 0.0_FP ) then
        write(*,*)
        write(*,'(x,a)') 'WARNING: energy calculation will fail.'
        write(*,'(5x,a,*(e14.6e2))') 'state: ', qPrim(:)
        read(*,*) 
    end if
#endif

    energy = 1.0_FP/(gam-1.0_FP)*qPrim(PRESSURE)/qPrim(DENSITY) + &
             0.5_FP*dot_product(qPrim(UVEL:VVEL),qPrim(UVEL:VVEL))

end function energy
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate specific total enthalpy from a primitive state vector
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>
function enthalpy(nEqns,qPrim)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< number of equations

    real(FP), intent(in) :: qPrim(nEqns) !< primitive state vector

    ! Function variable
    real(FP) :: enthalpy

    enthalpy = energy(nEqns,qPrim) + qPrim(PRESSURE)/qPrim(DENSITY)

end function enthalpy
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate an analytical flux from primitive state vector.\n
!>  Assumes standard Euler ordering of primitive equations.
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  13 January 2015 - Modified ADVECTION_FLUX for 2D Burgers' equations (Maeng)
!>  1 February 2015 - Added pressure-less euler fluxes (Maeng)
!>  16 February 2015 - Modified to incorporate multidimensional euler flux (Maeng)
!>
function flux( nDim,nEqns,fluxType,q,lam )

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           fluxType   !< equation type

    real(FP), intent(in) :: q(nEqns) !< state

    real(FP), intent(in), optional :: lam(nDim) !< linear wave speed

    ! Function variable
    real(FP) :: flux(nDim,nEqns)
    
    ! Local variables
    integer :: iDir       !< direction index

    real(FP) :: p,    & !< pressure 
                H       !< total specific enthalpy

    flux = 0.0_FP
    
    select case ( fluxType )

        case ( ADVECTION_FLUX )

            ! linear advection
            if ( present(lam) ) then
                flux(1,:) = (/ q(DENSITY)*lam(1), &
                               q(UVEL)*lam(1), &
                               q(VVEL)*lam(1), &
                               q(PRESSURE)*lam(1) /)
                
                flux(2,:) = (/ q(DENSITY)*lam(2), &
                               q(UVEL)*lam(2), &
                               q(VVEL)*lam(2), &
                               q(PRESSURE)*lam(2) /)
            else
                ! if velocity is specified by equations 2 and 3,
                ! then do not update equations 2 and 3.
                flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                               0.0_FP, &
                               0.0_FP, &
                               q(PRESSURE)*q(UVEL) /)
                
                flux(2,:) = (/ q(DENSITY)*q(VVEL), &
                               0.0_FP, &
                               0.0_FP, &
                               q(PRESSURE)*q(VVEL) /)

            end if

        case ( ACOUSTIC_FLUX )

            ! pressure
            p = isentropicConst*abs(q(DENSITY))**(gam)

            ! nonlinear acoustic system 
            flux(1,:) = (/ q(UVEL), &
                           p, &
                           0.0_FP, &
                           0.0_FP /)
            
            flux(2,:) = (/ q(VVEL), &
                           0.0_FP, &
                           p, &
                           0.0_FP /)

        case ( PLESSEULER_FLUX )

            ! conservative pressureless euler fluxes
            if ( nDim == 2 ) then 
                flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(VVEL), &
                               0.0_FP /)
                
                flux(2,:) = (/ q(DENSITY)*q(VVEL), &
                               q(DENSITY)*q(VVEL)*q(UVEL), &
                               q(DENSITY)*q(VVEL)*q(VVEL), &
                               0.0_FP/)
            else
                ! nDim == 1
                flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                               q(DENSITY)*q(UVEL)*q(UVEL) /) 
            end if

        case ( ISENTEULER_FLUX )

            if ( nDim == 1 ) then
                write(*,'(a)') 'ERROR: One dimensional solver is not implemented'
                write(*,*) 
                stop
            end if

            ! isentropic euler fluxes 
            p = isentropicConst*q(DENSITY)**(gam)

            flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                           q(DENSITY)*q(UVEL)*q(UVEL) + p, &
                           q(DENSITY)*q(UVEL)*q(VVEL), &
                           0.0_FP /)

            flux(2,:) = (/ q(DENSITY)*q(VVEL), &
                           q(DENSITY)*q(VVEL)*q(UVEL), &
                           q(DENSITY)*q(VVEL)*q(VVEL) + p, &
                           0.0_FP /)

        case ( EULER_FLUX )

            ! conservative barotropic euler fluxes
            if ( nDim == 1 ) then
                write(*,'(a)') 'ERROR: One dimensional solver is not implemented'
                write(*,*) 
                stop
            end if
                         
            !full Euler fluxes
            H = enthalpy(nEqns,q) ! specific total enthalpy
            flux(1,:) = (/ q(DENSITY)*q(UVEL), &
                           q(DENSITY)*q(UVEL)*q(UVEL) + q(PRESSURE), &
                           q(DENSITY)*q(UVEL)*q(VVEL), &
                           q(DENSITY)*q(UVEL)*H /)
            
            flux(2,:) = (/ q(DENSITY)*q(VVEL), &
                           q(DENSITY)*q(UVEL)*q(VVEL), &
                           q(DENSITY)*q(VVEL)*q(VVEL) + q(PRESSURE), &
                           q(DENSITY)*q(VVEL)*H /)

        case default
            write(*,'(a,i0,a)') 'ERROR: Flux type = ',fluxtype,' not supported.'
            stop
    
    end select

end function flux
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return Jacobian matrices for Euler equations
!>
!> @history
!>  29 December 2015 - Initial creation (Maeng)
!>
subroutine eulerJacobian( nDim,nEqns,u,A,B )

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    real(FP), intent(in) :: u(nEqns)  !< conservative states

    real(FP), intent(out) :: A(nEqns,nEqns),    & !< x Jacobian
                             B(nEqns,nEqns)       !< y Jacobian
    
    ! Local variables
    real(FP) :: k1,       & !< const
                H           !< specific total enthalpy

    k1 = (gam-1.0_FP)
    H = enthalpy(nEqns,u) ! specific total enthalpy

    ! A(U) = dF/dU
    A(1,1) = 0.0_FP
    A(1,2) = 1.0_FP
    A(1,3) = 0.0_FP
    A(1,4) = 0.0_FP
    A(2,1) = k1/2.0_FP*(u(2)**2.0_FP + u(3)**2.0_FP)/(u(1)**2.0_FP) - &
             (u(2)**2.0_FP)/(u(1)**2.0_FP)  
    A(2,2) = -k1*u(2)/u(1) + 2.0_FP*u(2)/u(1)
    A(2,3) = -k1*u(3)/u(1)
    A(2,4) = k1
    A(3,1) = -u(2)*u(3)/(u(1)**2.0_FP)
    A(3,2) = u(3)/u(1)
    A(3,3) = u(2)/u(1)
    A(3,4) = 0.0_FP
    A(4,1) = -gam*(u(2)*u(4))/(u(1)**2.0_FP) + &
             k1*(u(2)**3.0_FP + u(2)*u(3)**2.0_FP)/(u(1)**3.0_FP) 
    A(4,2) = gam*u(4)/u(1) - &
             k1/2.0_FP*(3.0_FP*u(2)**2.0_FP + u(3)**2.0_FP)/(u(1)**2.0_FP) 
    A(4,3) = -k1*(u(2)*u(3))/(u(1)**2.0_FP) 
    A(4,4) = gam*u(2)/u(1) 
 
    ! B(U) = dG/dU
    B(1,1) = 0.0_FP
    B(1,2) = 0.0_FP
    B(1,3) = 1.0_FP
    B(1,4) = 0.0_FP
    B(2,1) = -u(2)*u(3)/(u(1)**2.0_FP) 
    B(2,2) = u(3)/u(1) 
    B(2,3) = u(2)/u(1)
    B(2,4) = 0.0_FP
    B(3,1) = k1/2.0_FP*(u(2)**2.0_FP + u(3)**2.0_FP)/(u(1)**2.0_FP) - &
             (u(3)**2.0_FP)/(u(1)**2.0_FP)
    B(3,2) = -k1*u(2)/u(1) 
    B(3,3) = -k1*u(3)/u(1) + 2.0_FP*u(3)/u(1) 
    B(3,4) = k1 
    B(4,1) = -gam*(u(3)*u(4))/(u(1)**2.0_FP) + &
             k1*(u(2)**2.0_FP*u(3) + u(3)**3.0_FP)/(u(1)**3.0_FP) 
    B(4,2) = -k1*(u(2)*u(3))/(u(1)**2.0_FP) 
    B(4,3) = gam*u(4)/u(1) - &
             k1/2.0_FP*(u(2)**2.0_FP + 3.0_FP*u(3)**2.0_FP)/(u(1)**2.0_FP) 
    B(4,4) = gam*u(3)/u(1) 

end subroutine eulerJacobian
!-------------------------------------------------------------------------------
end module physics
!-------------------------------------------------------------------------------
