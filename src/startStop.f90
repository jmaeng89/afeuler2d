!-------------------------------------------------------------------------------
!> @purpose 
!>  Start-up and shut-down routines
!>
!> @author
!>  Timothy A. Eymann
!>
module startStop

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Start simulation
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>  14 May 2013 - Adapt for Euler re-write
!>  27 April 2016 - Time control module implemented (Maeng)
!>
subroutine startUp

    use solverVars
    use meshUtil, only: nDim, nCells, connect, nEdges, &
                        cellVolume 
    use inputOutput, only: getDriverFile, parseDriverFile, parseBCfile, &
                           openFiles, histFile, bcFileName, &
                           solInput, inputFileName
    use boundaryConditions, only: setNodeEdgeCellCount
    use reconstruction
    use timeControl
    use update

    implicit none

    ! Local variables
    !real(FP) :: dtMax !< maximum time step

    integer :: iCell,   & !< cell index
               iFace      !< face index
    real(FP) :: temp
    
    ! Set constants to precision of machine
    call calcConstants

    ! Gather inputs
    call getDriverFile
    call parseDriverFile ! initialize mesh variables

    ! Establish mesh connectivity and set edgeCoord values 
    call connect

    ! Flag boundary elements
    call parseBCfile

    ! set node/edgeCellCount
    !call setNodeEdgeCellCount

    ! Initialize solution values
    if ( .not. solInput ) then
        ! Initialize with specified analytic function
        call initPointVals
        call initCellAverages(nDim,nEqns)
    else
        ! Initialize with existing data
        nodeData = nodeDataN
        edgeData = edgeDataN
        cellAvg = cellAvgN
        primAvg = primAvgN
    end if

    ! Initialize time step parameters. nIter, cfl and dtIn 
    call initTimeStep(nIter,cfl,dtIn)

    if ( dtIn > dtMax ) then
        write(*,*)
        write(*,*) 'ERROR: Time step is too large.'
        write(*,'(7x,a,e12.4e2)') '       dtMax: ', dtMax
        write(*,'(7x,a,e12.4e2)') '        dtIn: ', dtIn
        stop
    else
        !write(*,'(7x,a,e12.4e2)') '       dtMax: ',dtMax
        write(*,'(7x,a,e12.4e2)') '        dtIn: ', dtIn
        write(*,'(7x,a,e12.4e2)') '  Final time: ', tFinal
        write(*,'(7x,a,f8.4)')    '         CFL: ', cfl  
        write(*,'(7x,a,i0)')      ' Init. nIter: ', nIter
        write(*,*)
    end if

    if ( .not. solInput ) then
        write(*,'(2a)') '   Initial solution: ', trim(adjustl(initSolnType))
    else
        write(*,'(3a)') '   Continued from "', trim(adjustl(inputFileName)), '"'
        write(*,'(2a)') '   Initial solution: ', trim(adjustl(initSolnType))
    end if

    write(*,'(2a)') ' Governing equation: ', trim(adjustl(govEqn))
    write(*,'(2a)') ' Boundary file name: ', trim(adjustl(bcFileName))
    write(*,*)

    ! Open files for output
    call openFiles

    ! write header to histFile
    if ( .not. solInput ) then
        write(histFile,'(2a)') '#  Initial solution: ', trim(adjustl(initSolnType))
    else
        write(histFile,'(3a)') '#  Continued from "', trim(adjustl(inputFileName)), '"'
        write(histFile,'(2a)') '#  Initial solution: ', trim(adjustl(initSolnType))
    end if

    write(histFile,'(2a)') '#Governing equation: ', trim(adjustl(govEqn))
    write(histFile,'(a)') '# '
    !write(histFile,'(a,e12.4e2)') '#      dtMax: ', dtMax
    write(histFile,'(a,e12.4e2)') '#       dtIn: ', dtIn
    write(histFile,'(a,e12.4e2)') '# Final time: ', tFinal
    write(histFile,'(a,f8.4)')    '#        CFL: ', cfl !dtIn/dtMax
    write(histFile,'(a,i0)')      '#      nIter: ', nIter

end subroutine startUp
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gracefully shutdown code
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>
subroutine shutDown(t0,tf)

    use solverVars, only: nIter
    use inputOutput, only: histFile, deallocateMeshVars, closeFiles
    implicit none

    ! Interface variables
    real(FP), intent(in) :: t0, & !< starting time
                            tf    !< ending time


    ! write time to history file
    write(histFile,'(a)') '#'
    write(histFile,'(a,e24.16e2,a)' ) '# Total solution time: ',tf-t0,' seconds'
    write(histFile,'(a,e24.16e2,a)' ) '#  Time per iteration: ',(tf-t0)/dble(nIter), &
                                     ' seconds'
    
    ! free allocated memory
    call deallocateMeshVars

    ! Close output files
    call closeFiles

end subroutine shutDown
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize point values at nodes and edges
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation
!>
subroutine initPointVals

    use solverVars, only: nEqns
    use analyticFunctions
    use meshUtil, only: nDim, nNodes, nEdges, &
                        nodeCoord, edgeCoord
    use update, only: nodeDataN, nodeData, edgeDataN, edgeData 

    implicit none

    ! Local variables
    integer :: iNode,   & !< node index
               iEdge      !< edge index

    do iNode = 1, nNodes
        nodeDataN(:,iNode) = evalFunction(nDim,nEqns,nodeCoord(:,iNode))
    end do
    
    do iEdge = 1, nEdges
        edgeDataN(:,iEdge) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge))
    end do
   
    nodeData(:,:) = nodeDataN(:,:)
    edgeData(:,:) = edgeDataN(:,:)

end subroutine initPointVals
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize cell averages use numerical integration.  Code uses Dunavant
!>  points and weights.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial Creation (Eymann)
!>  17 March 2015 - Primitive and conservative variables added (Maeng)
!>  15 April 2015 - Consistent intialization for primitive variables (Maeng)
!>
subroutine initCellAverages(nDim,nEqns)

    use update, only: cellAvg, cellAvgN, &
                      primAvg, primAvgN, &
                      eqFlag, ADVECTION_EQ, ACOUSTIC_EQ, &
                      EULER_EQ, ISENTEULER_EQ, PLESSEULER_EQ
    use meshUtil, only: nCells, nEdges, ref2cart, cellVolume, &
                        faceCells, cellFaces
    use mathUtil, only: dunavantLoc, gaussQuadLoc, numAverage, &
                        symTriLoc, symTriWt, numAverageSymTri
    use physics, only: prim2conserv
    use analyticFunctions
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns      !< number of equations

    ! Local variables
    integer :: iEq,     & !< equatoin index
               nPts,    & !< number of integration points
               iCell,   & !< cell index
               lCell,   & !< cell index
               rCell,   & !< cell index
               iEdge,   & !< edge index
               iPoint     !< integration point

    real(FP), allocatable :: xi(:,:),       & !< evaluation coordinates (in reference space)
                             qPrim(:,:),    & !< sampled primitive variable vector
                             qCons(:,:)       !< sampled conserved state vector
    
    real(FP) :: x(nDim)   !< evaluation points in Cartesian space

    if ( nDim == 2 ) then
        nPts = 6 
    else
        nPts = 5
    end if

    allocate( xi(nDim,nPts), qPrim(nEqns,nPts), qCons(nEqns,nPts) )
    xi = 0.0_FP

    ! Keep dimension check outside of cell loop for efficiency
    if ( nDim == 1 ) then
        xi(1,:) = gaussQuadLoc(nPts)
        do iCell = 1, nCells
            qPrim = 0.0_FP
            qCons = 0.0_FP
            do iPoint = 1, nPts
                x(:) = ref2cart(nDim,iCell,0.5_FP*(xi(:,iPoint)+1.0_FP))
                qPrim(:,iPoint) = evalFunction(nDim,nEqns,x(:))
                if ( eqFlag == ADVECTION_EQ .or. eqFlag == ACOUSTIC_EQ ) then
                    ! primitive variables for non-Euler system
                    qCons(:,iPoint) = qPrim(:,iPoint)
                else
                    ! conserved cell variables
                    qCons(:,iPoint) = prim2conserv(nDim,nEqns,qPrim(:,iPoint)) 
                end if
            end do
            do iEq = 1, nEqns
                ! conserved cell state variables
                cellAvgN(iEq,iCell) = numAverage(nDim,nPts,qCons(iEq,:))
                primAvgN(iEq,iCell) = numAverage(nDim,nPts,qPrim(iEq,:))
            end do
        end do
    else
        xi(:,:) = dunavantLoc(nPts) ! dunavant points for high-order integration
        do iCell = 1, nCells
            qPrim = 0.0_FP
            qCons = 0.0_FP
            do iPoint = 1, nPts
                x(:) = ref2cart(nDim,iCell,xi(:,iPoint))
                qPrim(:,iPoint) = evalFunction(nDim,nEqns,x(:))
                if ( eqFlag == ADVECTION_EQ ) then
                    ! primitive variables for non-Euler system
                    qCons(:,iPoint) = qPrim(:,iPoint)
                else
                    ! conserved cell state variables
                    qCons(:,iPoint) = prim2conserv(nDim,nEqns,qPrim(:,iPoint))
                end if
            end do
            do iEq = 1, nEqns
                ! high order accuarate average calculation - using Dunavant
                cellAvgN(iEq,iCell) = numAverage(nDim,nPts,qCons(iEq,:))
                primAvgN(iEq,iCell) = numAverage(nDim,nPts,qPrim(iEq,:))
            end do
        end do

    end if

    deallocate( xi, qPrim, qCons )

    cellAvg(:,:) = cellAvgN(:,:) 
    primAvg(:,:) = primAvgN(:,:)

end subroutine initCellAverages
!-------------------------------------------------------------------------------
end module startStop
