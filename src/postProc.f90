!-------------------------------------------------------------------------------
!> @purpose 
!>  Common post-processing routines
!>
!> @author
!>  Timothy A. Eymann
!>
module postProc

    use solverVars, only: FP

    real(FP) :: r_cutoff = 4.0_FP !< cutoff radius for error evaluation domain

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return quadratic reconstruction coefficients 
!>      defined on Lagrange basis points
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 April 2016 - Initial creation
!>
function quadIntData(nDim,nEqns,nPts,nCells)

    use reconstruction, only: reconValue
    use update, only: updateReconData
    use mathUtil, only: dunavantLoc, gaussQuadLoc, symTriLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of points
                           nCells     !< number of cells

    ! Function variable
    real(FP) :: quadIntData(nEqns,nPts,nCells)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iPt,     & !< quadrature point index
               iCell      !< cell index

    real(FP) :: xi(nDim,nPts)       !< evaluation coordinates (in reference space)

    ! use latest values
    call updateReconData()

    ! set locations
    select case ( nDim )

      case ( 1 )
        xi(1,:) = gaussQuadLoc(nPts)
        do iCell = 1, nCells
            ! assume natural reconstruction
            do iPt = 1, nPts
                do iEq = 1, nEqns
                    ! convert Gaussian quadrature location to interval from [0,1]
                    xi(1,iPt) = 0.5*(xi(1,iPt) + 1.0)
                    quadIntData(iEq,iPt,iCell) = &
                            reconValue(nDim,iEq,iCell,xi(1,iPt))
                end do
            end do
        end do

      case ( 2 )
        ! 2d quadrature inside element
        xi(:,:) = symTriLoc(nPts)
        do iCell = 1, nCells
            ! natural reconstruction, with bubble function 
            do iPt = 1, nPts
                do iEq = 1, nEqns
                    quadIntData(iEq,iPt,iCell) = &
                        reconValue(nDim,iEq,iCell,xi(:,iPt))
                end do
            end do
        end do
      
    end select

end function quadIntData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gather data at Dunavant points (internal quadrature points) for each cell
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  3 Sep. 2015 - Included updateRecondata for latest reconstruction 
!>                                                      values (Maeng)
!>
function intData(nDim,nEqns,nPts,nCells,nBasis)

    use reconstruction, only: reconValue
    use update, only: updateReconData
    use mathUtil, only: dunavantLoc, gaussQuadLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    ! Function variable
    real(FP) :: intData(nEqns,nPts,nCells)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iCell,   & !< cell index
               iPt        !< integration point

    real(FP) :: xi(nDim,nPts)       !< evaluation coordinates (in reference space)

    real(FP) :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    ! use latest values
    call updateReconData()

    ! set locations
    select case ( nDim ) 

      case ( 1 )
        xi(1,:) = gaussQuadLoc(nPts)
        do iCell = 1, nCells
            ! assume natural reconstruction
            do iPt = 1, nPts
                do iEq = 1, nEqns
                    ! convert Gaussian quadrature location to interval from [0,1]
                    xi(1,iPt) = 0.5*(xi(1,iPt) + 1.0)
                    intData(iEq,iPt,iCell) = &
                            reconValue(nDim,iEq,iCell,xi(1,iPt))
                end do
            end do
        end do

      case ( 2 )
       ! 2d quadrature inside element
        xi(:,:) = dunavantLoc(nPts)
        do iCell = 1, nCells
            ! natural reconstruction, with bubble function 
            do iPt = 1, nPts
                do iEq = 1, nEqns
                    intData(iEq,iPt,iCell) = &
                        reconValue(nDim,iEq,iCell,xi(:,iPt))
                end do
            end do
        end do
    
    end select

    !! if basis functions are specified, we're working with 2d xflow file
    !if ( present(nBasis) ) then
    !    !xi(:,:) = dunavantLoc(nPts)
    !        do iPt = 1, nPts
    !            intData(iEq,iPt,iCell) = &
    !                xReconValue(nDim,nBasis,iEq,iCell,xi(:,iPt))
    !        end do
    !    end do
    !    cycle ! fix this
    !end if

end function intData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Compute error norm between numerical cell average and "exact" cell average
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  18 November 2015 - Initial creation
!>
function cellAvgError(nDim,nEqns,nCells,cellAvg0,cellAvg,norm)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells     !< number of cells

    real(FP), intent(in) :: cellAvg0(nEqns,nCells),& !< numerical
                            cellAvg(nEqns,nCells), & !< exact data
                            norm                     !< error norm

    ! Function variable
    real(FP) :: cellAvgError(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    ! loop through equations
    do iEq = 1, nEqns
        errSum = 0.0_FP
        area = 0.0_FP

        ! error norm
        select case ( nDim )

          case ( 1 )
            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                ! to truncate outside domain,
                !if ( x >= -1.0_FP .and. x <= 1.0_FP ) then
                    errSum = errSum + cellVolume(iCell)* &
                            abs(cellAvg(iEq,iCell)-cellAvg0(iEq,iCell))**norm
                    area = area + cellVolume(iCell)

                !end if
            end do

          case ( 2 )

            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                ! radial position
                r = sqrt(x**2.0_FP + y**2.0_FP)

                ! to truncate outside domain,
                !if ( r <= r_cutoff ) then
                    errSum = errSum + cellVolume(iCell)* &
                            abs(cellAvg(iEq,iCell)-cellAvg0(iEq,iCell))**norm
                    area = area + cellVolume(iCell)
                !end if
            end do

        end select
        cellAvgError(iEq) = (errSum/area)**(1.0_FP/norm)
    end do

end function cellAvgError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Compute error norm between cell reconstructions at first and final iteration 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  30 July 2012 - Add variable norm
!>  26 October 2012 - Add multiple equations
!>  27 April 2016 - reconstruction coefficients included (Maeng)
!>
function cellReconError(nDim,nEqns,nPts,nCells,intData0,intData,norm,nBasis)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid
    use mathUtil, only: dunavantWt, gaussQuadWt, symTriWt
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells,  & !< number of cells
                           nPts       !< number of integration points

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: intData0(nEqns,nPts,nCells),& !< initial data
                            intData(nEqns,nPts,nCells), & !< final data
                            norm                          !< error norm

    ! Function variable
    real(FP) :: cellReconError(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                w(nPts),    & !< quadrature weights
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    do iEq = 1, nEqns
        errSum = 0.0_FP
        area = 0.0_FP

        select case ( nDim ) 

          case ( 1 ) 
            w(:) = gaussQuadWt(nPts)
            do iCell = 1, nCells

                x = cellCentroid(1,iCell)
                ! to truncate outside domain,
                !if ( x >= -1.0_FP .and. x <= 1.0_FP ) then
                    quad = 0.0
                    do iPt = 1, nPts
                        quad = quad + w(iPt) * &
                               abs(intData0(iEq,iPt,iCell) - &
                                   intData(iEq,iPt,iCell))**norm
                    end do

                    ! factor of 1/2 accounts for interval shift
                    ! Integration normalization factor is 0.5
                    errSum = errSum + cellVolume(iCell)*(0.5_FP*quad)
                    area = area + cellVolume(iCell)
                !end if
            end do

          case ( 2 )

            if ( present(nBasis) ) then
                ! 2nd order integration weights
                w(:) = symTriWt(nPts)
            else
                ! 2d quadrature inside element
                w(:) = dunavantWt(nPts)
            end if

            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                r = sqrt(x**2.0_FP + y**2.0_FP)

                ! to truncate outside domain,
                !if ( r <= r_cutoff ) then
                    quad = 0.0_FP
                    do iPt = 1, nPts
                        quad = quad + w(iPt) * &
                               abs(intData(iEq,iPt,iCell) - &
                                   intData0(iEq,iPt,iCell))**norm
                    end do
                    errSum = errSum + cellVolume(iCell)*(1.0_FP*quad)  
                    area = area + cellVolume(iCell)
                !end if
            end do

        end select
        cellReconError(iEq) = (errSum/area)**(1.0_FP/norm)
    end do

end function cellReconError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find error in vertex and edge values
!>  Takes quadratic reconstruction coefficients
!>
!> @history
!>  15 December 2016 - Initial creation
!>
subroutine cellNodeEdgeError(nDim,nEqns,nPts,nCells,quadData0,quadData,nodeErr,edgeErr)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells,  & !< number of cells
                           nPts       !< number of integration points

    real(FP), intent(in) :: quadData0(nEqns,nPts,nCells),& !< initial data
                            quadData(nEqns,nPts,nCells)    !< final data

    real(FP), intent(inout) :: nodeErr(nEqns),  & !< node error
                               edgeErr(nEqns)     !< edge error

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad1,      & !< quadrature sum
                quad2,      & !< quadrature sum
                errSum1,    & !< sum of error terms
                errSum2,    & !< sum of error terms
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    ! error norm
    do iEq = 1, nEqns
        errSum1 = 0.0_FP
        errSum2 = 0.0_FP
        area = 0.0_FP

        select case ( nDim ) 

          case ( 2 ) 
            do iCell = 1, nCells
                x = cellCentroid(1,iCell)
                y = cellCentroid(2,iCell)
                r = sqrt(x**2.0_FP + y**2.0_FP)
    
                ! to truncate outside domain
                if ( r <= r_cutoff ) then
                    quad1 = 0.0_FP
                    quad2 = 0.0_FP
                    do iPt = 1,3
                        ! node
                        quad1 = quad1 + &
                            1.0_FP/3.0_FP*abs(quadData(iEq,2*iPt-1,iCell)-quadData0(iEq,2*iPt-1,iCell))
                        ! edge
                        quad2 = quad2 + &
                            1.0_FP/3.0_FP*abs(quadData(iEq,2*iPt,iCell)-quadData0(iEq,2*iPt,iCell))
                    end do
                    errSum1 = errSum1 + cellVolume(iCell)*(quad1)  
                    errSum2 = errSum2 + cellVolume(iCell)*(quad2)  
                    area = area + cellVolume(iCell)
                end if
            end do

          case default
            write(*,*) 'ERROR: only available in 2D.'
            stop

        end select

        nodeErr(iEq) = (errSum1/area)
        edgeErr(iEq) = (errSum2/area)
    end do

end subroutine cellNodeEdgeError
!-------------------------------------------------------------------------------
!> @purpose 
!>  Isentropic vortex problem 
!>  Evaluate error at extrema value location of a specified variable.
!>  For vortex problem, locate the center of vortex by finding minimum pressure.
!>  Then find the relative error compared to the initial value.
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  29 March 2016 - Initial creation
!>  26 April 2016 - Accurate minimum point location evaluation - Newton-Rhapson
!>
subroutine isentVortexExtrema(nDim,nEqns,nCells,qPt,xMin)

    use solverVars, only: eps
    use meshUtil, only: cellCentroid, ref2cart, cellFaces
    use update, only: primAvgN, edgeDataN, updateReconData
    use reconstruction, only: reconValue, gradVal
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells     !< number of cells

    real(FP), intent(out) :: qPt(nEqns),    & !< minimum pressure location values 
                             xMin(nDim)       !< location of minimum pressure

    ! Local variables
    integer, parameter :: DENSITY = 1,  & !< density
                          UVEL = 2,     & !< velocity
                          VVEL = 3,     & !< velocity
                          PRESSURE = 4    !< pressure

    integer :: iCell,   & !< cell index
               iEq,     & !< equation index
               iter,    & !< iteration index
               iPt,     & !< point index
               iPtMin,  & !< minimum point index
               iCellMin   !< minimum cell index

    real(FP) :: r,              & !< radius from the center of domain 
                rSearch,        & !< search radius
                pMinTemp,       & !< minimum velocity, temp 
                pMin              !< minimum velocity 

    real(FP) :: tol,            & !< tolerance
                ds,             & !< step size
                xiG(nDim),      & !< reference coord
                xi(nDim),       & !<    
                gradP1(nDim),   & !< pressure gradient
                gradP2(nDim),   & !< pressure gradient
                f, g, det,      &
                dfdx,           & !<
                dfdy,           & !<
                dgdx,           & !<
                dgdy

    real(FP) :: coeff((nDim+2)*(nDim+3)/(2*(3-nDim))) !< function coefficients
                
    tol = eps
    ds = 1.0_FP*sqrt(abs(tol))

    ! use latest values
    call updateReconData()

    ! search radius, be sure to check the relative mesh size
    rSearch = 4.0_FP

    pMin = 1.0e06
    xMin = 5.0_FP
    iPtMin = 0
    iCellMin = 0
    ! loop around and locate cell containing minimum pressure
    do iCell = 1, nCells
        ! define search radius and skip anything outside of it
        r = sqrt((cellCentroid(1,iCell))**2.0_FP + &
                 (cellCentroid(2,iCell))**2.0_FP)
        if ( r >= rSearch ) cycle 

        pMinTemp = pMin
        !pMin = min(pMin, abs(primAvgN(PRESSURE,iCell)))
        pMin = min(pMin, &
            abs( 1.0_FP/3.0_FP*(edgeDataN(PRESSURE,cellFaces(1,iCell)) + &
                                edgeDataN(PRESSURE,cellFaces(2,iCell)) + &
                                edgeDataN(PRESSURE,cellFaces(3,iCell)) ) ) )
        if ( pMin /= pMinTemp ) then
            iPtMin = iPt
            iCellMin = iCell ! this cell contains vortex core
        end if
    end do

    ! find xi inside quadratic element in reference coordinate
    xiG = 0.5_FP
    do iter = 1,50

        ! evaluate pressure gradient
        gradP1 = gradVal(nDim,PRESSURE,iCellMin,xiG)
        f = gradP1(1)
        g = gradP1(2)

        ! jacobian - derivatives of gradients of pressure
        xi(1) = xiG(1) + ds
        xi(2) = xiG(2)
        gradP1 = gradVal(nDim,PRESSURE,iCellMin,xi)
        xi(1) = xiG(1) - ds
        gradP2 = gradVal(nDim,PRESSURE,iCellMin,xi)
        dfdx = (gradP1(1) - gradP2(1))/(2.0_FP*ds)
        dgdx = (gradP1(2) - gradP2(2))/(2.0_FP*ds)
            
        xi(1) = xiG(1)
        xi(2) = xiG(2) + ds
        gradP1 = gradVal(nDim,PRESSURE,iCellMin,xi)
        xi(2) = xiG(2) - ds
        gradP2 = gradVal(nDim,PRESSURE,iCellMin,xi)
        dfdy = (gradP1(1) - gradP2(1))/(2.0_FP*ds)
        dgdy = (gradP1(2) - gradP2(2))/(2.0_FP*ds)

        det = dfdx*dgdy - dfdy*dgdx
        if ( abs(det) < tol ) then
            write(*,*) 'Singular jacobian matrix for pressure derivatives'
            stop
        end if

        ! new coordinate
        xi(1) = xiG(1) - 1.0_FP/det*(f*dgdy - g*dfdy)
        xi(2) = xiG(2) - 1.0_FP/det*(-f*dgdx + g*dfdx)

        ! check convergence of coordinate
        if ( abs(xi(1)-xiG(1)) < ds .and. abs(xi(2)-xiG(2)) < ds ) exit
        xiG = xi

    end do

    xMin = ref2cart(nDim,iCellMin,xiG)
    do iEq = 1,nEqns
        !coeff(:) = recData(iEq,:,iCellMin) ! reconstruction coefficients
        qPt(iEq) = reconValue(nDim,iEq,iCellMin,xiG)
    end do
    !write(*,*) iCellMin, xiG, xMin, iter

end subroutine isentVortexExtrema
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return numerical solution averaged over specified domain
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  22 March 2016 - Initial creation
!>  14 September 2016 - Modified a bit for nonlinear acoustic error estimation
!>
function cellReconAvgVal(nDim,nEqns,nPts,nCells,intData,norm,nBasis)

    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
                        cellCentroid, ref2cart
    use mathUtil, only: dunavantWt, gaussQuadWt, symTriWt, &
                        dunavantLoc, symTriLoc
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimension number
                           nEqns,   & !< number of equations
                           nCells,  & !< number of cells
                           nPts       !< number of integration points

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: intData(nEqns,nPts,nCells), & !< final data
                            norm                          !< error norm

    ! Function variable
    real(FP) :: cellReconAvgVal(nEqns)

    ! Local variables
    integer :: iCell,   & !< cell index
               iPt,     & !< point index
               iEq        !< equation index

    real(FP) :: area,       & !< domain area
                quad,       & !< quadrature sum
                errSum,     & !< sum of error terms
                w(nPts),    & !< quadrature weights
                x,          & !< centroid x coordinate
                y,          & !< centroid y coordinate
                r

    real(FP) :: gaussFunc,       & 
                xi(nDim,nPts),   & !< quadrature weights
                xcart(nDim)

    do iEq = 1, nEqns

        errSum = 0.0_FP
        area = 0.0_FP

        ! 2d quadrature inside element
        if ( present(nBasis) ) then
            w(:) = symTriWt(nPts)
            xi(:,:) = symTriLoc(nPts)
        else
            w(:) = dunavantWt(nPts)
            xi(:,:) = dunavantLoc(nPts)
        end if

        do iCell = 1, nCells
            !x = cellCentroid(1,iCell)
            !y = cellCentroid(2,iCell)
            !r = sqrt(x**2.0_FP + y**2.0_FP)
            
            ! to truncate outside domain,
            !if ( x >= 0.0_FP .and. x <= 1.0_FP ) then
            !if ( r <= 0.5_FP ) then

            quad = 0.0_FP
            do iPt = 1, nPts
                ! for nonlinear acoustic error estimation
                xCart = ref2cart(nDim,iCell,xi(:,iPt))
                gaussFunc = exp(-0.5_FP*(xCart(1)**2.0_FP + xCart(2)**2.0_FP)) 
                ! for nonlinear acoustic error estimation

                quad = quad + gaussFunc* &
                       abs(intData(iEq,iPt,iCell))**norm

                !quad = quad + w(iPt) * &
                !       abs(intData(iEq,iPt,iCell))**norm

            end do
            errSum = errSum + cellVolume(iCell)*(1.0_FP*quad)  
            area = area + cellVolume(iCell)
            !end if

        end do

        cellReconAvgVal(iEq) = (errSum/area)**(1.0_FP/norm)
    end do

end function cellReconAvgVal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Solve linear equations by interpolating to characteristic origins 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 December 2016 - Initial creation
!>
function interpSol(nDim,nEqns,nPts,nCells,finalSol,tf,nBasis)

    use solverVars, only: eps
    use meshUtil, only: ref2cart, xMin, xMax
    use mathUtil, only: dunavantLoc, gaussQuadLoc, symTriLoc
    use analyticFunctions, only: evalFunction

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: finalSol(nEqns,nPts,nCells),    & !< final solution from simulation 
                            tf                                !< final time

    ! Function variable
    real(FP) :: interpSol(nEqns,nPts,nCells)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iDir,    & !< dimension index
               iCell,   & !< cell index
               iPt        !< integration point

    real(FP) :: xi(nDim,nPts),  & !< reference coorindate
                xCart(nDim),    & !< physical coordinate
                xCartNew(nDim), & !< physical coordinate
                xCartTemp(nDim),& !< physical coordinate
                deltaX(nDim),   & !< bounds
                xiTemp(nDim)

    
    ! quadrature points
    if ( nDim == 2 ) then
        if ( present(nBasis) ) then
            xi(:,:) = symTriLoc(nPts)
        else
            xi(:,:) = dunavantLoc(nPts)
        end if
    else
        ! nDim == 1
        xi(1,:) = gaussQuadLoc(nPts)
    end if

    ! set bounds
    deltaX(1) = xMax(1) - xMin(1)
    deltaX(2) = xMax(2) - xMin(2)

    ! linear advection solution finding routine
    do iCell = 1, nCells
        do iPt = 1, nPts
            if ( nDim == 1 ) then
                ! convert Gaussian quadrature location to interval from [0,1]
                xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                xCartTemp(:) = ref2cart(nDim,iCell,xiTemp)
            else
                xCartTemp(:) = ref2cart(nDim,iCell,xi(:,iPt))
            end if

            ! functions with exact temporal solutions available  
            xCart(1) = xCartTemp(1)-finalSol(2,iPt,iCell)*tf
            xCart(2) = xCartTemp(2)-finalSol(3,iPt,iCell)*tf
            ! x-coord
            if ( xCart(1) > xMax(1) ) then
                xCartNew(1) = mod((xCart(1)-xMin(1)),deltaX(1)) + xMin(1) 
            else if ( xCart(1) < xMin(1) ) then
                xCartNew(1) = xMax(1) - mod((xMax(1)-xCart(1)),deltaX(1)) 
            else
                xCartNew(1) = xCart(1)
            end if
            ! y-coord
            if ( xCart(2) > xMax(2) ) then
                xCartNew(2) = mod((xCart(2)-xMin(2)),deltaX(2)) + xMin(2) 
            else if ( xCart(2) < xMin(2) ) then
                xCartNew(2) = xMax(2) - mod((xMax(2)-xCart(2)),deltaX(2)) 
            else
                xCartNew(2) = xCart(2)
            end if

            interpSol(:,iPt,iCell) = evalFunction(nDim,nEqns,xCartNew,t=tf)

        end do
    end do

end function interpSol
!-------------------------------------------------------------------------------
!> @purpose 
!>  Solve nonlinear equations iteratively for characteristic origins/velocities 
!>  for pressureless Euler equations
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  31 August 2015 - Initial creation
!>
function iterateSol(nDim,nEqns,nPts,nCells,finalSol,tf,nBasis)

    use solverVars, only: eps
    use meshUtil, only: ref2cart, xMin, xMax
    use mathUtil, only: dunavantLoc, gaussQuadLoc, symTriLoc
    use iterativeFunctions

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           nPts,    & !< number of integration points
                           nCells     !< number of cells

    integer, intent(in), optional :: nBasis !< number of basis functions

    real(FP), intent(in) :: finalSol(nEqns,nPts,nCells),    & !< final solution from simulation 
                            tf                                !< final time

    ! Function variable
    real(FP) :: iterateSol(nEqns,nPts,nCells)
  
    ! Local variables
    integer :: iEq,     & !< equation index
               iDir,    & !< dimension index
               iCell,   & !< cell index
               iPt        !< integration point

    real(FP) :: xi(nDim,nPts),  & !< reference coorindate
                xCart(nDim),    & !< physical coordinate
                xiTemp(nDim)

    
    ! quadrature points
    if ( nDim == 2 ) then
        if ( present(nBasis) ) then
            xi(:,:) = symTriLoc(nPts)
        else
            xi(:,:) = dunavantLoc(nPts)
        end if
    else
        ! nDim == 1
        xi(1,:) = gaussQuadLoc(nPts)
    end if

    ! iterate for solutions
    do iPt = 1, nPts
        do iCell = 1, nCells
            xCart(:) = ref2cart(nDim,iCell,xi(:,iPt))
            if ( nDim == 1 ) then
                ! convert Gaussian quadrature location to interval from [0,1]
                xiTemp(1) = 0.5*(xi(1,iPt) + 1.0)
                xCart(:) = ref2cart(nDim,iCell,xiTemp)
            end if
            ! pressureless euler equations, iterated implicit solution
            iterateSol(:,iPt,iCell) = pLessEulerSol(nDim,nEqns, &
                    finalSol(:,iPt,iCell),xCart,tf)
        end do
    end do

end function iterateSol
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate internal value using xFlow basis functions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  22 June 2012 - Initial creation
!>  1 August 2012 - Add p=1 basis function
!>
function xReconValue(nDim,nBasis,iEq,iCell,xi)

    use reconstruction, only: reconData
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iEq,     & !< equation index
                           iCell,   & !< element index
                           nBasis     !< number of basis functions

    real, intent(in) :: xi(nDim) !< reference coordinate

    ! Function variable
    real(FP) :: xReconValue

    ! Local variable
    integer :: iPhi !< basis index

    real(FP) :: phi(nBasis) !< basis value

    phi(:) = 0.0
    select case ( nBasis )
        case (3)
            phi(1) = 1.0 - xi(1) - xi(2)
            phi(2) = xi(1)
            phi(3) = xi(2)

        case (6)
            phi(1) = 1.0 - 3.0*xi(1) + 2.0*xi(1)**2 - 3.0*xi(2) + &
                     4.0*xi(1)*xi(2) + 2.0*xi(2)**2
            phi(2) = 4.0*(xi(1) - xi(1)**2 - xi(1)*xi(2))
            phi(3) = -xi(1) + 2.0*xi(1)**2
            phi(4) = 4.0*(xi(2) - xi(2)**2 - xi(1)*xi(2))
            phi(5) = 4.0*xi(1)*xi(2)
            phi(6) = 2.0*xi(2)**2 - xi(2)
    end select

    xReconValue = 0.0
    do iPhi = 1, nBasis
        xReconValue = xReconValue + phi(iPhi)*reconData(iEq,iPhi,iCell)
    end do

end function xReconValue
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read data from an output file produced by xflow
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 June 2012 - Initial creation
!>
subroutine readXflowData(fileUnit,nBasis)

    use reconstruction, only: reconData
    use meshUtil, only: nCells
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    integer, intent(out), optional :: nBasis !< number of basis functions

    ! Local variables
    character(1) :: header !< file metadata
    
    integer :: line,    & !< line index
               iCell,   & !< cell index
               nBas,    & !< number of basis functions
               p          !< order (assumed to be same for all cells)

    read(fileUnit,'(a)',advance='yes') (header, line=1,11)
    do iCell = 1, nCells
        read(fileUnit,*) p, nBas
        read(fileUnit,*) reconData(1,1:nBas,iCell)
    end do

    if ( present(nBasis) ) then
        nBasis = nBas
    end if

end subroutine readXflowData
!-------------------------------------------------------------------------------
end module
!-------------------------------------------------------------------------------
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Evaluate reference solution associated with edge points
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  17 January 2016 - Initial creation
!!>
!function edgeAvgError(nDim,nEqns,nCells,nFaces,edgeAvg,tf,norm)
!
!    use meshUtil, only: nodeCoord, cellCentroid, cellVolume, &
!                        faceNormal, faceNodes, faceCells
!    use mathUtil, only: gaussQuadLoc, gaussQuadWt, &
!                        bilinQuadriLoc, bilinQuadriJac
!    use physics, only: prim2conserv
!    use analyticFunctions
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,    & !< problem dimension
!                           nEqns,   & !< number of equations
!                           nCells,  & !< number of cells
!                           nFaces     !< number of faces
!
!    real(FP), intent(in) :: edgeAvg(nEqns,nFaces), & !< numerical data
!                            norm                     !< error norm
!    real(FP), intent(in) :: tf  !< solution time
!
!    ! Function variable
!    real(FP) :: edgeAvgError(nEqns)
!  
!    ! Local variables
!    integer :: iEq,     & !< equation index
!               iFace,   & !< face index
!               iCell,   & !< cell index
!               lCell,   & !< cell index
!               rCell,   & !< cell index
!               lNode,   & !< local node index
!               rNode,   & !< local node index
!               iPoint,  & !< integration point
!               jPoint     !< integration point
!
!    real(FP) :: xCart(nDim,4),  & !< node locations, physical
!                xDiff(nDim),    & !< difference in coordinate
!                fNorm(nDim),    & !< face normal
!                errSum(nEqns),  & !< error sum
!                area              !< domain area
!
!    integer, parameter :: nPts = 6 !< number of quadrature points
!
!    real(FP) :: xi(nPts),  & !< quadrature locations
!                wt(nPts),       & !< quadrature weights
!                xiQuad(nDim),   & !< reference quadrature location in 2D
!                xQuad(nDim), & !< quadrature physical coord
!                Jacobian(nDim,nDim),  &  !< coodinate transformation matrix
!                detJac,         & !< determinant of jacobian
!                temp(nEqns), & !< function values
!                funcVal(nEqns), & !< function values
!                refSol(nEqns)
!
!    ! 2d quadrature inside element - (-1,1) x (-1,1)
!    xi(:) = gaussQuadLoc(nPts)
!    wt(:) = gaussQuadWt(nPts)
!
!    do iFace = 1, nFaces
!
!        lCell = faceCells(1,iFace)
!        rCell = faceCells(2,iFace) 
!        if ( rCell < 0 ) cycle ! do not evaluate errors on boundary cells/edges
!        rNode = faceNodes(2,iFace) 
!        lNode = faceNodes(1,iFace) 
!
!        xDiff(:) = nodeCoord(:,rNode) - nodeCoord(:,lNode)
!        fNorm(1) = -xDiff(2)/norm2(xDiff(:))
!        fNorm(2) =  xDiff(1)/norm2(xDiff(:))
!
!        ! do not need to flip
!        !if ( dot_product(fNorm,faceNormal(:,iFace)) /= 1.0_FP ) then
!        !    ! flipped lNode and rNode
!        !    rNode = faceNodes(1,iFace)
!        !    lNode = faceNodes(2,iFace)
!        !end if
!
!        ! assign quadrilateral nodes 
!        ! lCell centroid, lNode, rCell centroid, rNode
!        ! counter clock-wise ordering of physical coordinates
!        xCart(:,1) = cellCentroid(:,lCell) 
!        xCart(:,2) = nodeCoord(:,lNode) 
!        xCart(:,3) = cellCentroid(:,rCell) 
!        xCart(:,4) = nodeCoord(:,rNode) 
!
!        ! find average reference solution
!        refSol(:) = 0.0_FP
!        jacobian(:,:) = 0.0_FP
!        do iPoint = 1, nPts
!            do jPoint = 1, nPts
!                xiQuad(1) = xi(iPoint) 
!                xiQuad(2) = xi(jPoint) 
!
!                !xQuad(:) = quadriLoc(nDim,xCart,xiQuad)
!                !Jacobian(:,:) = quadriJac(nDim,xCart,xQuad)
!                xQuad(:) = bilinQuadriLoc(nDim,xCart,xiQuad)
!                Jacobian(:,:) = bilinQuadriJac(nDim,xCart,xQuad)
!                detJac = abs(jacobian(1,1)*jacobian(2,2) - &
!                    jacobian(2,1)*jacobian(1,2))
!
!                temp(:) = evalFunction(nDim,nEqns,xQuad,t=tf)
!                funcVal(:) = prim2conserv(nDim,nEqns,temp)
!
!                ! average in the reference space, spans (0,1)x(0,1)
!                refSol(:) = refSol(:) + &
!                    (0.5_FP*wt(iPoint))*(0.5_FP*wt(jPoint))*funcVal(:)
!
!            end do
!        end do
!
!        area = area + 0.5_FP*(cellVolume(lCell)+cellVolume(rCell))
!
!        errSum = errSum + 0.5_FP*(cellVolume(lCell)+cellVolume(rCell))* &
!                    abs(refSol(:) - edgeAvg(:,iFace))**norm
!
!    end do
!
!    edgeAvgError(:) = (errSum/area)**(1.0/norm)
!
!end function edgeAvgError
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Compute error norm between specified cell reconstructions at first and 
!!>  final iteration by exactly interpolating
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  5 November 2014 - Initial creation
!!>
!function cellReconErrorPoint(nDim,nEqns,nPts,nCells,recData0,recData,norm)
!
!    use solverVars, only: waveSpeed, nIter, dtIn
!    use meshUtil, only: cellDetJac, cellVolume, faceArea, cellFaces, &
!                        findCell, cart2ref
!    use mathUtil, only: dunavantWt, gaussQuadWt
!    use reconstruction, only: reconValue
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,    & !< dimension number
!                           nEqns,   & !< number of equations
!                           nCells,  & !< number of cells
!                           nPts       !< number of integration points
!
!    real(FP), intent(in) :: recData0(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells),& !< initial data
!                            recData(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells), & !< final data
!                            norm                          !< error norm
!
!    ! Function variable
!    real(FP) :: cellReconErrorPoint(nEqns)
!
!    ! Local variables
!    integer :: iCell,    & !< cell index
!               iPt,      & !< point index
!               iEq,      & !< equation index
!               x0Cell(4),&!< cells containing points of interest at initial iter
!               xfCell(4)  !< cells containing points of interest at final iter
!
!    real(FP) :: area,       & !< domain area
!                quad,       & !< quadrature sum
!                quad1,      & !< quadrature sum
!                quad2,      & !< quadrature sum
!                errSum,     & !< sum of error terms
!                w(nPts),    & !< quadrature weights
!                wGauss2D(4),& !< 2D gaussian quadrature weights
!                xi(nDim),   & !< reference interpolation location
!                x0Pts(nDim,4),&!< points of interest at initial iteration 
!                xfPts(nDim,4)  !< points of interest at final iteration 
!
!    ! hard code necessary variables
!    ! 2D gaussian quadrature weights
!    wGauss2D(:) = 1.0
!
!    ! Sample points for 2D gaussian quadarture
!    x0Pts(:,1) = (/ 0.25/sqrt(3.0), 0.25/sqrt(3.0) /) 
!    x0Pts(:,2) = (/ -0.25/sqrt(3.0), 0.25/sqrt(3.0) /) 
!    x0Pts(:,3) = (/ -0.25/sqrt(3.0), -0.25/sqrt(3.0) /) 
!    x0Pts(:,4) = (/ 0.25/sqrt(3.0), -0.25/sqrt(3.0) /) 
!    
!    do iCell = 1, 4
!        x0Cell(iCell) = findCell(nDim,100,x0Pts(:,iCell))
!
!        ! find the coordinates corresponding to the advected coordinate
!        xfPts(1,iCell) = x0Pts(1,iCell) + waveSpeed(1)*dtIn*nIter 
!        xfPts(2,iCell) = x0Pts(2,iCell) + waveSpeed(2)*dtIn*nIter 
!
!        !Find the advected cells
!        xfCell(iCell) = findCell(nDim,100,xfPts(:,iCell))
!    end do
!
!    do iEq = 1, nEqns
!
!        errSum = 0.0
!        area = 0.0
!        ! error norm
!        ! 2d quadrature inside element
!        quad1 = 0.0
!        quad2 = 0.0
!        do iPt = 1,4
!            ! cell average calculation
!            xi(:) = cart2ref(nDim,x0Cell(iPt),x0Pts(:,iPt))
!            quad1 = quad1 + wGauss2D(iPt) * &
!                    reconValue(nDim,iEq,x0Cell(iPt),xi(:),recData0(iEq,:,x0Cell(iPt)))
!            xi(:) = cart2ref(nDim,xfCell(iPt),xfPts(:,iPt))
!            quad2 = quad2 + wGauss2D(iPt) * &
!                    reconValue(nDim,iEq,xfCell(iPt),xi(:),recData(iEq,:,xfCell(iPt)))
!        end do
!        ! compare cell averages
!        quad = abs(quad2 - quad1)**(norm)
!
!        ! 1/16 for shift of domain from [-1,1;-1,1] to [-0.25,0.25;-0.25,0.25]
!        ! 8 to make square domain area
!        errSum = errSum + (1.0/16.0)*(0.25)*quad
!        area = area + (0.25)
!
!        cellReconErrorPoint(iEq) = (errSum/area)**(1.0/norm)
!    end do
!
!end function cellReconErrorPoint
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Return number of iterations in afSolver binary file
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  26 July 2012 - Initial Creation
!!>
!function getIterNum(fileUnit)
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: fileUnit !< file unit for binary data
!
!    ! Function variable
!    integer :: getIterNum
!
!    ! Local variables
!    integer :: iErr !< error code
!
!    iErr = 0
!    getIterNum = 0
!    
!    rewind(fileUnit)
!
!    call skipConnectivity(fileUnit)
!    do while ( iErr == 0 )
!        iErr = skipData(fileUnit)
!        if ( iErr == 0 ) getIterNum = getIterNum + 1
!    end do
!
!end function getIterNum
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Skip connectivity data in binary file
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  26 July 2012 - Initial Creation
!!>
!subroutine skipConnectivity(fileUnit)
!
!    use meshUtil, only: nDim, nNodes, nEdges, nCells, maxNodesPerCell
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: fileUnit !< file unit for binary data
!
!    ! Local variables
!    integer :: i,       & !< counter
!               iNode,   & !< node index
!               iEdge,   & !< edge index
!               iCell,   & !< cell index
!               dumInt     !< dummy integer variable
!
!    real :: dumReal !< dummy real variable
!
!    read(fileUnit) (dumInt, i=1,8) 
!    do iNode = 1, nNodes
!        read(fileUnit) (dumReal, i=1,nDim)
!    end do
!    do iEdge = 1, nEdges
!        read(fileUnit) (dumReal, i=1,nDim)
!    end do
!    do iCell = 1, nCells
!        read(fileUnit) (dumInt, i=1,2*maxNodesPerCell)
!    end do
!
!end subroutine skipConnectivity
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Skip iteration data from an output file produced by afSolver
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  19 April 2012 - Initial creation
!!>
!function skipData(fileUnit)
!
!    use solverVars, only: nEqns
!    use meshUtil, only: nCells, nEdges, nNodes
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: fileUnit !< file unit number
!
!    ! Function variable
!    integer :: skipData
!
!    ! Local variables
!    integer :: dumInt,  & !< dummy integer variable
!               iCell,   & !< cell index
!               iNode,   & !< node index
!               iEdge,   & !< edge index
!               iEq        !< equation index
!
!    real :: dumReal !< dummy real variable
!
!    read(fileUnit, iostat=skipData) dumInt, dumReal
!    
!    do iCell = 1, nCells
!        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
!    end do
!
!    do iCell = 1, nCells
!        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
!    end do
!    
!    do iNode = 1, nNodes
!        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
!    end do
!    
!    do iEdge = 1, nEdges
!        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
!    end do
!
!    !do iNode = 1, nNodes
!    !    read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
!    !end do
!    !
!    !do iEdge = 1, nEdges
!    !    read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
!    !end do
!
!    !do iEdge = 1, nEdges
!    !    read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
!    !end do
!
!end function skipData
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Read data from an output file produced by afSolver
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  19 April 2012 - Initial creation
!!>
!subroutine readData(fileUnit,solIter,solTime)
!
!    use solverVars, only: nEqns
!    use update, only: primAvgN, cellAvgN, & 
!                      nodeDataN, edgeDataN, edgeAvgN, &
!                      nodeDataDB, edgeDataDB
!    use meshUtil, only: nCells, nNodes, nEdges
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: fileUnit !< file unit number
!
!    integer, intent(out), optional :: solIter !< solution iteration
!
!    real, intent(out), optional :: solTime 
!
!    ! Local variables
!    integer :: dumInt,  & !< dummy integer variable
!               iCell,   & !< cell index
!               iNode,   & !< node index
!               iEdge,   & !< edge index
!               iEq        !< equation index
!
!    real :: dumReal !< dummy real variable
!
!    if ( (present(solIter)) .and. (present(solTime)) ) then
!        read(fileUnit) solIter, solTime
!    else
!        read(fileUnit) dumInt, dumReal
!    end if
!
!    do iCell = 1, nCells
!        read(fileUnit) (cellAvgN(iEq,iCell), iEq=1,nEqns) 
!    end do
!    
!    do iCell = 1, nCells
!        read(fileUnit) (primAvgN(iEq,iCell), iEq=1,nEqns) 
!    end do
!    
!    do iNode = 1, nNodes
!        read(fileUnit) (nodeDataN(iEq,iNode), iEq=1,nEqns)
!    end do
!    
!    do iEdge = 1, nEdges
!        read(fileUnit) (edgeDataN(iEq,iEdge), iEq=1,nEqns)
!    end do
!
!    !do iNode = 1, nNodes
!    !    read(fileUnit) (nodeDataDB(iEq,iNode), iEq=1,nEqns)
!    !end do
!    !
!    !do iEdge = 1, nEdges
!    !    read(fileUnit) (edgeDataDB(iEq,iEdge), iEq=1,nEqns)
!    !end do
!
!    !do iEdge = 1, nEdges
!    !    read(fileUnit) (edgeAvgN(iEq,iEdge), iEq=1,nEqns)
!    !end do
!
!end subroutine readData

