!-------------------------------------------------------------------------------
!> @purpose 
!>  Functions for boundary conditions
!/n>
!> @author
!>  Timothy A. Eymann
!>
module boundaryConditions

    use solverVars, only: inLength, FP

    integer, parameter :: internalID = 0,   & !< internal node/edge
                          periodicID = 1,   & !< periodic bc type
                          outflowID = 2,    & !< outflow
                          wallID = 3,       & !< wall 
                          specFuncID = 4,   & !< use specified function
                          initSolnID = 5      !< use initial solution function

    integer, allocatable :: patchType(:),   & !< bc associated with each id
                            patchID(:),     & !< array of patch id numbers
                            edgePatch(:),   & !< patch associated to edge
                            nodeBC(:),      & !< bc type associated with node
                            edgeBC(:),      & !< bc type associated with edge midpt
                            pNodePair(:,:), & !< periodic node pair
                            pEdgePair(:)      !< periodic edge pair

    integer, allocatable :: nodeCellCount(:),   & !< count of cells connected to vertex 
                            edgeCellCount(:)      !< count of cells connected to edge, 2

    character(inLength) :: bcFunction !< specified boundary function

contains
!-------------------------------------------------------------------------------
!> @purpose
!>  Update node and edge values using specified boundary conditions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial creation
!>  13 December 2013 - Adaptation for 2D linear advection (Maeng)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  25 January 2017 - Assumed shape array for states (Maeng)
!>
subroutine applyBC(nEqns,iCell,iNodeEdge,signal,q,dt,updated,iter)

    use solverVars, only: tSim
    use meshUtil, only: cellNodes, cellFaces

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< local node or edge index

    real(FP), intent(in) :: signal(nEqns), & !< change in state at node/edge
                            dt               !< time step

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    integer, intent(in), optional :: iter     !< iteration number

    ! Local variables
    integer :: iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: t         !< simulation time

    ! calculate the simulation time
    t = tSim + dt

    if ( iNodeEdge > 0 ) then

        iNode = cellNodes(iNodeEdge,iCell)

        select case ( nodeBC(iNode) )

            case ( periodicID )
                ! need updated 
                call applyPeriodicSignal(nEqns,iCell,iNodeEdge,signal,q,updated)
            !case ( rotPeriodicID )
            !call applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

            case ( outflowID )
                call outflowBC(nEqns,iCell,iNodeEdge,q,updated)

            case ( wallID )
                call wallBC(nEqns,iCell,iNodeEdge,signal,q,updated)

            case ( specFuncID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t,updated)

            case ( initSolnID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t,updated)

            case default
                continue

        end select

    else

        iEdge = cellFaces(abs(iNodeEdge),iCell)

        select case ( edgeBC(iEdge) )

            case ( periodicID )
                call applyPeriodicSignal(nEqns,iCell,iNodeEdge,signal,q,updated)
            !case ( rotPeriodicID )
            !call applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

            case ( outflowID )
                call outflowBC(nEqns,iCell,iNodeEdge,q,updated)

            case ( wallID )
                call wallBC(nEqns,iCell,iNodeEdge,signal,q,updated)

            case ( specFuncID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t,updated)

            case ( initSolnID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t,updated)

            case default
                continue

        end select

    end if

end subroutine applyBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Update acoustics node and edge values using specified boundary conditions
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 May 2016 - Initial creation
!>  25 January 2017 - Assumed shape array for states (Maeng)
!>
subroutine applyAcBC(nEqns,iCell,iNodeEdge,signal,q,dt,updated,iter)

    use solverVars, only: tSim
    use meshUtil, only: cellNodes, cellFaces

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node or edge local index

    real(FP), intent(in) :: signal(nEqns), & !< change in state at node/edge
                            dt               !< time step

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    integer, intent(in), optional :: iter     !< iteration number

    ! Local variables
    integer :: iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: t         !< simulation time

    ! calculate the simulation time
    t = tSim + dt

    if ( iNodeEdge > 0 ) then

        iNode = cellNodes(iNodeEdge,iCell)

        select case ( nodeBC(iNode) )

            case ( periodicID )
                ! Do not pass updated flag
                call applyPeriodicAcSignal(nEqns,iCell,iNodeEdge,signal,q)

            case ( wallID )
                call wallAcBC(nEqns,iCell,iNodeEdge,signal,q)

            case ( specFuncID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t)

            case ( outflowID )
                call outflowBC(nEqns,iCell,iNodeEdge,q)

            case default
                continue

        end select

    else

        iEdge = cellFaces(abs(iNodeEdge),iCell)

        select case ( edgeBC(iEdge) )

            case ( periodicID )
                ! Do not pass updated flag
                call applyPeriodicAcSignal(nEqns,iCell,iNodeEdge,signal,q)

            case ( wallID )
                call wallAcBC(nEqns,iCell,iNodeEdge,signal,q)

            case ( specFuncID )
                call specifyBC(nEqns,iCell,iNodeEdge,q,t)

            case ( outflowID )
                call outflowBC(nEqns,iCell,iNodeEdge,q)

            case default
                continue

        end select

    end if

end subroutine applyAcBC
!-------------------------------------------------------------------------------
!> @purpose 
!>  Search opposite side of domain to find periodic neighbor cell.  Routine
!>  assumes a rectangular grid aligned with Cartesian axes.  Prime candidate
!>  for efficiency improvements
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 March 2012 - Initial creation
!>  7 June 2012 - Add code for storing node neighbors
!>
subroutine setPeriodicNeighbor(perPatches)

    use meshUtil
    use solverVars, only: iLeft, iRight, eps
    implicit none

    ! Interface variables
    integer, intent(in) :: perPatches(2)  !< list of patches in periodic bound

    ! Local variables
    integer, parameter :: ix = 1,   & !< index for x direction
                          iy = 2      !< index for y direction

    real(FP), parameter :: coordTol = 1.0e-10_FP !< tolerance for coordinate matches

    integer :: iFace,       & !< face id
               jFace,       & !< face id
               iNode,       & !< node index
               gNode,       & !< global node index
               oppNode,     & !< opposite node global index
               iPatch,      & !< patch id
               matchDim       !< dimension to match

    real(FP) :: x(nDim,maxNodesPerFace) !< coordinates of face nodes


    ! flag node and edge bc
    do iFace = 1, nFaces

        if ( (faceCells(iRight,iFace) == -perPatches(1)) .or. &
             (faceCells(iRight,iFace) == -perPatches(2)) ) then

            edgePatch(iFace) = abs(faceCells(iRight,iFace))
            edgeBC(iFace) = periodicID
            do iNode = 1, maxNodesPerFace
                nodeBC(faceNodes(iNode,iFace)) = periodicID
            end do

        end if

    end do

    ! match up faces across domain
    select case ( nDim )

        case (1)
            ! assumes ordered numbering of cells
            faceCells(iRight,1) = -nCells
            faceCells(iRight,nFaces) = -1

            pNodePair(1,nNodes) = 1
            pNodePair(1,1) = nNodes

            pEdgePair(nFaces) = 1
            pEdgePair(1) = nFaces

        case (2)
            do iFace = 1, nFaces-1
                if ( (faceCells(iRight,iFace) == -perPatches(1)) .or. &
                     (faceCells(iRight,iFace) == -perPatches(2)) ) then

                    if ( faceCells(iRight,iFace) == -perPatches(1)) then
                        iPatch = perPatches(2)
                    else
                        iPatch = perPatches(1)
                    end if

                    x(:,:) = nodeCoord(:,faceNodes(:,iFace))

                    ! determine if edge is horizontal or vertical
                    ! assumes that all edges and nodes are aligned with cartesian coordinate
                    !if ( abs(x(ix,1)-x(ix,2)) <= 10.0*eps ) then
                    if ( abs(x(ix,1)-x(ix,2)) <= coordTol ) then
                        ! edge is vertical
                        matchDim = iy
                    else
                        ! edge is horizontal
                        matchDim = ix
                    end if

                    do jFace = iFace+1, nFaces
                        if ( faceCells(iRight,jFace) == -iPatch ) then

                            ! if edge coordinates match store neighbor info
                            !   ASSUMES EQUAL SPACING ALONG EDGES
                            if ( abs(edgeCoord(matchDim,iFace) - &
                                     edgeCoord(matchDim,jFace)) <= coordTol ) then

                                ! set neighbor cell
                                faceCells(iRight,iFace) = &
                                    -faceCells(iLeft,jFace)

                                faceCells(iRight,jFace) = &
                                    -faceCells(iLeft,iFace)

                                ! store periodic edges
                                pEdgePair(iFace) = jFace
                                pEdgePair(jFace) = iFace

                                ! store periodic nodes
                                do iNode = 1, 2
                                    gNode = faceNodes(iNode,iFace)
                                    oppNode = faceNodes(3-iNode,jFace)

                                    if ( pNodePair(1,gNode) == 0 ) then
                                        pNodePair(1,gNode) = oppNode
                                    elseif ( pNodePair(1,gNode) /= oppNode ) then
                                        pNodePair(2,gNode) = oppNode
                                    end if

                                    if ( pNodePair(1,oppNode) == 0 ) then
                                        pNodePair(1,oppNode) = gNode
                                    elseif ( pNodePair(1,oppNode) /= gNode ) then
                                        pNodePair(2,oppNode) = gNode
                                    end if

                                end do
                            end if
                        end if

                    end do


                end if
            end do

    end select

    !do iFace = 61,62
    !write(*,*) iFace, pEdgePair(iFace) !pNodePair(2,faceNodes(1,iFace)), pNodePair(2,faceNodes(2,iFace))
    !end do
    !do iNode = 1,8
    !if ( iNode = 3 ) then 
    !iNode = 3
        !write(*,*) iNode, pNodePair(1,iNode), pNodePair(2,iNode)
    !end if

end subroutine setPeriodicNeighbor
!-------------------------------------------------------------------------------
!> @purpose 
!>  Ensure periodic assignament worked
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 October 2012 - Initial creation
!>
subroutine testPeriodicAssignment

    use solverVars, only: eps
    use meshUtil, only: nFaces, nNodes, edgeCoord, xMax, nDim
    implicit none

    ! Local variables
    integer, parameter :: ix = 1,   & !< x-coordinate
                          iy = 2      !< y-coordinate

    integer :: iFace,   & !< face index
               jFace,   & !< face index
               iNode,   & !< node index
               jNode,   & !< node count
               matchDim   !< matching dimension


    do iFace = 1, nFaces
        if ( edgeBC(iFace) == periodicID ) then
            if ( pEdgePair(iFace) == 0 ) then
                write(*,'(a,i0,a)') 'ERROR: edge ',iFace, &
                                    ' has no periodic match'

                write(*,*) 'edge coordinates: ',edgeCoord(:,iFace)
                write(*,*) 'possible candidates...'

                if ( (abs(edgeCoord(1,iFace))-xMax(1)) <= eps ) then
                    matchDim = iy
                else
                    matchDim = ix
                end if

                do jFace = 1, nFaces
                    if (( edgeBC(jFace) == periodicID ) .and. &
                       ( abs( edgeCoord(matchDim,iface) - &
                              edgeCoord(matchDim,jface)) <= 1.0e-6 ) ) then
                        write(*,'(i0,3e18.6e2)') jFace, edgeCoord(:,jFace), &
                                   abs(edgeCoord(matchDim,iFace) - &
                                       edgeCoord(matchDim,jFace))
                    end if

                end do
                stop
            end if
        end if
    end do
    jNode = 0
    do iNode = 1, nNodes
        if ( nodeBC(iNode) == periodicID ) then
            if ( pNodePair(1,iNode) == 0 ) then
                write(*,'(a,i0,a)') 'ERROR: node ',iNode, &
                                    ' has no periodic match'
                stop
            elseif ( nDim > 1 ) then
                if ( pNodePair(2,iNode) /= 0 ) then
                    jNode = jNode + 1
                end if
            end if
        end if
    end do
    if ( nDim > 1 ) then
        if ( jNode < 4 ) then
            write(*,*) 'WARNING: some corner nodes only have one match.'
            !stop
        end if
    end if

end subroutine testPeriodicAssignment
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply rotating flow signal to periodically-matched nodes and edges
!>  by manipulating velocity signals. Only work for systems with velocities.
!>
!> @history
!>  9 November 2015 - Initial creation (Maeng)
!>
subroutine setNodeEdgeCellCount

    use meshUtil, only: nDim, nCells, cellNodes, cellFaces, &
                        maxNodesPerCell, maxFacesPerCell
    implicit none

    ! Local variables
    integer :: iEdge,   & !< edge index
               iNode,   & !< node index
               jNode,   & !< node index
               kNode,   & !< node index
               gEdge,   & !< global edge index
               gNode,   & !< global node index
               pEdge,   & !< periodic edge index
               pNode,   & !< periodic node index
               pNode2,  & !< periodic node index
               iCell      !< cell index

    logical :: cornerUp

    edgeCellCount = 0
    nodeCellCount = 0
    do iCell = 1, nCells

        do iEdge = 1, maxFacesPercell
            gEdge = cellFaces(iEdge,iCell)
            edgeCellCount(gEdge) = edgeCellCount(gEdge) + 1 

            ! take care of periodic edge 
            if ( edgeBC(gEdge) == periodicID ) then
                pEdge = pEdgePair(gEdge)
                if ( pEdge > 0 ) edgeCellCount(pEdge) = edgeCellCount(pEdge) + 1 
            else

            end if
        end do

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeCellCount(gNode) = nodeCellCount(gNode) + 1

            ! take care of periodic node
            if ( nodeBC(gNode) == periodicID ) then
                cornerUp = .false.
                do jNode = 1,nDim
                    pNode = pNodePair(jNode,gNode)
                    if ( pNode > 0 ) then
                        nodeCellCount(pNode) = nodeCellCount(pNode) + 1
                        do kNode = 1,nDim
                            pNode2 = pNodePair(kNode,pNode)
                            if ( (pNode2 > 0) .and. (pNode2 /= gNode) .and. &
                                (.not. cornerUp) ) then
                                nodeCellCount(pNode2) = nodeCellCount(pNode2) + 1
                                cornerUp = .true.
                            end if
                        end do
                    end if
                end do
            end if
        end do

    end do

end subroutine setNodeEdgeCellCount
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply rotating flow signal to periodically-matched nodes and edges
!>  by manipulating velocity signals. Only work for systems with velocities.
!>
!> @history
!>  9 November 2015 - Initial creation (Maeng)
!>
subroutine applyRotatingPeriodicSignal(nEqns,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim, nodeCoord, edgeCoord

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout) :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: jNode,   & !< node index
               kNode,   & !< node index
               pEdge,   & !< matching periodic edge
               pNode,   & !< matching periodic node
               pNode2     !< second matching periodic node

    logical :: cornerUpdated !< flag that signal propagated to corner

    cornerUpdated = .false.
        
    if ( iNodeEdge > 0 ) then ! apply signal to nodes
        cornerUpdated = .false.

        ! corner nodes have more than one periodic neigbor
        do jNode = 1,nDim
            pNode = pNodePair(jNode,iNodeEdge)
            
            if ( (pNode > 0) .and. (updated(iNodeEdge)) ) then

                ! change the sign of signals for boundaries for rotating flow 
                if ( nodeCoord(1,iNodeEdge) == nodeCoord(1,pNode) ) then
                    ! x-coord matching
                    q(1,pNode) = q(1,pNode) + signal(1)
                    q(2,pNode) = q(2,pNode) - signal(2)
                    q(3,pNode) = q(3,pNode) + signal(3)
                    q(4,pNode) = q(4,pNode) + signal(4)
                else
                    ! y-coord matching
                    q(1,pNode) = q(1,pNode) + signal(1)
                    q(2,pNode) = q(2,pNode) + signal(2)
                    q(3,pNode) = q(3,pNode) - signal(3)
                    q(4,pNode) = q(4,pNode) + signal(4)
                end if
                updated(pNode) = .true.
                do kNode = 1,nDim
                    pNode2 = pNodePair(kNode,pNode)
                    ! make sure that neighbors of neigbors only updated once
                    if ( ( pNode2 /= iNodeEdge ) .and. ( pNode2 > 0 ) .and.&
                         ( .not. cornerUpdated) ) then
                        q(1,pNode2) = q(1,pNode2) + signal(1)
                        q(2,pNode2) = q(2,pNode2) - signal(2)
                        q(3,pNode2) = q(3,pNode2) - signal(3)
                        q(4,pNode2) = q(4,pNode2) + signal(4)
                        cornerUpdated = .true.
                        updated(pNode2) = .true.
                    end if
                end do
            end if
        end do
    else ! apply signal to edges

        pEdge = pEdgePair(abs(iNodeEdge))
        if ( pEdge > 0 .and. updated(abs(iNodeEdge)) ) then

            ! change the sign of signals for boundaries for rotating flow 
            if ( edgeCoord(1,abs(iNodeEdge)) == edgeCoord(1,pEdge) ) then
                ! x-coord matching
                q(1,pEdge) = q(1,pEdge) + signal(1)
                q(2,pEdge) = q(2,pEdge) - signal(2)
                q(3,pEdge) = q(3,pEdge) + signal(3)
                q(4,pEdge) = q(4,pEdge) + signal(4)
            else
                ! y-coord matching
                q(1,pEdge) = q(1,pEdge) + signal(1)
                q(2,pEdge) = q(2,pEdge) + signal(2)
                q(3,pEdge) = q(3,pEdge) - signal(3)
                q(4,pEdge) = q(4,pEdge) + signal(4)
            end if
            updated(pEdge) = .true.
        end if

    end if

end subroutine applyRotatingPeriodicSignal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply signal to periodically-matched nodes and edges
!>
!> @history
!>  17 May 2012 - Initial creation (Eymann)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  6 October 2015 - updated flag for node/edge to eliminate duplicate update
!>  25 January 2017 - Assumed shape array for states (Maeng)
!>
subroutine applyPeriodicSignal(nEqns,iCell,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim, cellNodes, cellFaces

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: jNode,   & !< node index
               kNode,   & !< node index
               pEdge,   & !< matching periodic edge
               pNode,   & !< matching periodic node
               pNode2     !< second matching periodic node

    integer :: iNode,   & !< node index
               iEdge      !< edge index

    integer :: gNodeEdge  !< global node/edge index 

    logical :: cornerUpdated = .false. !< flag that signal propagated to corner
        
    if ( iNodeEdge > 0 ) then ! apply signal to nodes
        cornerUpdated = .false.

        iNode = cellNodes(iNodeEdge,iCell) 
        
        ! corner nodes have more than one periodic neigbor
        do jNode = 1,nDim
            pNode = pNodePair(jNode,iNode)
            
            if ( (pNode > 0) .and. (updated(iNode)) ) then
                q(:,pNode) = q(:,pNode) + signal
                updated(pNode) = .true.
                do kNode = 1,nDim
                    pNode2 = pNodePair(kNode,pNode)
                    
                    ! make sure that neighbors of neigbors only updated once
                    if ( ( pNode2 /= iNode ) .and. ( pNode2 > 0 ) .and.&
                         ( .not. cornerUpdated) ) then
                        q(:,pNode2) = q(:,pNode2) + signal
                        cornerUpdated = .true.
                        updated(pNode2) = .true.
                    end if
                end do
            end if
        end do

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell) 

        pEdge = pEdgePair(iEdge)
        if ( pEdge > 0 .and. updated(iEdge) ) then
            q(:,pEdge) = q(:,pEdge) + signal
            updated(pEdge) = .true.
        end if

    end if

end subroutine applyPeriodicSignal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply ACOUSTIC signal to periodically-matched nodes and edges
!>
!> @history
!>  4 May 2016 - Initial creation (Maeng)
!>
subroutine applyPeriodicAcSignal(nEqns,iCell,iNodeEdge,signal,q)

    use meshUtil, only: nDim, cellNodes, cellFaces

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge local index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state

    ! Local variables
    integer :: jNode,   & !< node index
               kNode,   & !< node index
               pEdge,   & !< matching periodic edge
               pNode,   & !< matching periodic node
               pNode2     !< second matching periodic node

    integer :: iNode,   & !< node index
               iEdge      !< edge index

    integer :: gNodeEdge  !< global node/edge index 
              
    logical :: cornerUpdated = .false. !< flag that signal propagated to corner
        
    if ( iNodeEdge > 0 ) then ! apply signal to nodes
        cornerUpdated = .false.

        iNode = cellNodes(iNodeEdge,iCell) 

        ! corner nodes have more than one periodic neigbor
        do jNode = 1,nDim
            pNode = pNodePair(jNode,iNode)
            
            !if ( (pNode > 0) .and. (updated(iNodeEdge)) ) then
            if ( (pNode > 0) ) then
                q(:,pNode) = q(:,pNode) + signal
                !updated(pNode) = .true.
                do kNode = 1,nDim
                    pNode2 = pNodePair(kNode,pNode)
                    
                    ! make sure that neighbors of neigbors only updated once
                    if ( ( pNode2 /= iNode ) .and. ( pNode2 > 0 ) .and.&
                         ( .not. cornerUpdated) ) then
                        q(:,pNode2) = q(:,pNode2) + signal
                        cornerUpdated = .true.
                        !updated(pNode2) = .true.
                    end if
                end do
            end if
        end do

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell) 

        pEdge = pEdgePair(iEdge)
        !if ( pEdge > 0 .and. updated(abs(iNodeEdge)) ) then
        if ( pEdge > 0 ) then
            q(:,pEdge) = q(:,pEdge) + signal
            !updated(pEdge) = .true.
        end if

    end if

end subroutine applyPeriodicAcSignal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update node and edge values using a specified function
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 July 2012 - Initial creation
!>  13 December 2013 - Adaptation for 2D linear advection (Maeng)
!>  25 February 2015 - pointer for state variables (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  25 January 2017 - Assumed shape array for states (Maeng)
!>
subroutine specifyBC(nEqns,iCell,iNodeEdge,q,t,updated)

    use analyticFunctions
    use meshUtil, only: nDim, nodeCoord, edgeCoord, cellNodes, cellFaces
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge index

    real(FP), intent(inout) :: q(:,:) !< state 

    real(FP), intent(in) :: t  !< simulation time at n+1

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< node index
               iEdge      !< edge index

    integer :: gNodeEdge    !< global node/edge index

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = cellNodes(iNodeEdge,iCell) 

        q(:,iNode) = evalFunction(nDim,nEqns,nodeCoord(:,iNode),t, &
                        bcFunction)

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell)

        q(:,iEdge) = evalFunction(nDim,nEqns,edgeCoord(:,iEdge),t, &
                        bcFunction)

        if ( present(updated) ) updated(iEdge) = .true.

    end if


end subroutine specifyBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Outflow BC.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  20 July 2012 - Initial creation\n
!>  30 October 2012 - Reset to use cell averages
!>  13 December 2013 - Adaptation for 2D linear advection case (Maeng)
!>  6 October 2015 - pointer for updated flag (Maeng)
!>  25 January 2017 - Assumed shape array for states (Maeng)
!>
subroutine outflowBC(nEqns,iCell,iNodeEdge,q,updated)

    use analyticFunctions
    use meshUtil, only: nDim, nodeCoord, edgeCoord, cellNodes, cellFaces
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge index

    real(FP), intent(inout) :: q(:,:) !< state 

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< local node index
               iEdge      !< local edge index

    integer :: gNodeEdge  !< global node/edge index 

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = cellNodes(iNodeEdge,iCell) 

        !q(:,iNode) = q(:,iNode)

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell) 

        !q(:,iEdge) = q(:,iEdge)

        if ( present(updated) ) updated(iEdge) = .true.

    end if


end subroutine outflowBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Wall BC
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 January 2016 - Initial creation
!>
subroutine wallBC(nEqns,iCell,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim, nodeCoord, edgeCoord, cellNodes, cellFaces
    use physics, only: DENSITY, UVEL, VVEL, PRESSURE

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state 

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer :: iNode,   & !< local node index
               iEdge      !< local edge index

    integer :: gNodeEdge       !< global node/edge index

    real(FP) :: curvature,      & !< radius of curvature of geometry
                normalVec(nDim)   !< normal vector 

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = cellNodes(iNodeEdge,iCell) 

        ! FIXME: uncertain about advection wall BC,
        ! For now, leave them unchanged from charac. interpolation
        q(:,iNode) = q(:,iNode) - signal(:)

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell) 

        q(:,iEdge) = q(:,iEdge) - signal(:)

        if ( present(updated) ) updated(iEdge) = .true.

    end if

end subroutine wallBC
!-------------------------------------------------------------------------------
!> @purpose
!>  Wall BC for acoustic operation
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  2 December 2016 - Initial creation
!>
subroutine wallAcBC(nEqns,iCell,iNodeEdge,signal,q,updated)

    use meshUtil, only: nDim, nodeCoord, edgeCoord, faceNodes, &
                        cellNodes, cellFaces, xMin, xMax, &
                        maxNodesPerFace
    use physics, only: DENSITY, UVEL, VVEL, PRESSURE

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge      !< node/edge index

    real(FP), intent(in) :: signal(nEqns) !< change in state at node/edge

    real(FP), intent(inout) :: q(:,:) !< state 

    logical, intent(inout), optional :: updated(:) !< flag that node/edge updated

    ! Local variables
    integer, parameter :: ix = 1,   & !< index for x direction
                          iy = 2      !< index for y direction

    real(FP), parameter :: coordTol = 1.0e-10_FP !< tolerance for coordinate matches

    integer :: iDir,    & !< dimension 
               iNode,   & !< local node index
               iEdge,   & !< local edge index
               matchDim   !< dimension to match

    real(FP) :: x(nDim,maxNodesPerFace) !< coordinates of face nodes

    if ( iNodeEdge > 0 ) then ! apply signal to nodes

        iNode = cellNodes(iNodeEdge,iCell)

        ! find the face vector associated with adjacent face/edge 
        x(:,1) = nodeCoord(:,iNode)
    
        ! determine if edge is horizontal or vertical
        ! assumes that all edges and nodes are aligned with cartesian coordinate
        matchDim = 0
        do iDir = 1,nDim
            if ( ( abs( xMin(iDir)-x(iDir,1) ) <= coordTol ) .or. & 
                 ( abs( xMax(iDir)-x(iDir,1) ) <= coordTol ) ) then
                matchDim = matchDim + iDir
            end if
        end do
             
        if ( matchDim == 1 ) then
            ! node is vertical
            q(1,iNode) = q(1,iNode) + signal(1)
            q(2,iNode) = q(2,iNode) - signal(2)
            q(3,iNode) = q(3,iNode) + signal(3)
            q(4,iNode) = q(4,iNode) + signal(4)
        else if ( matchDim == 2 ) then
            ! node is horizontal
            q(1,iNode) = q(1,iNode) + signal(1)
            q(2,iNode) = q(2,iNode) + signal(2)
            q(3,iNode) = q(3,iNode) - signal(3)
            q(4,iNode) = q(4,iNode) + signal(4)
        else  
            ! node is corner
            q(1,iNode) = q(1,iNode) + 2.0_FP*signal(1)
            q(2,iNode) = q(2,iNode) - signal(2)
            q(3,iNode) = q(3,iNode) - signal(3)
            q(4,iNode) = q(4,iNode) + 2.0_FP*signal(4)
        end if

        if ( present(updated) ) updated(iNode) = .true.

    else ! apply signal to edges

        iEdge = cellFaces(abs(iNodeEdge),iCell)

        ! there are two types of edges
        x(:,:) = nodeCoord(:,faceNodes(:,iEdge))

        ! determine if edge is horizontal or vertical
        ! assumes that all edges and nodes are aligned with cartesian coordinate
        if ( abs(x(ix,1)-x(ix,2)) <= coordTol ) then
            ! edge is vertical
            q(2,iEdge) = q(2,iEdge) - signal(2) ! taketh away
            q(3,iEdge) = q(3,iEdge) + signal(3)
        else
            ! edge is horizontal
            q(2,iEdge) = q(2,iEdge) + signal(2)
            q(3,iEdge) = q(3,iEdge) - signal(3) ! no normal component of velocity
        end if
        q(1,iEdge) = q(1,iEdge) + signal(1)
        q(4,iEdge) = q(4,iEdge) + signal(4)

        if ( present(updated) ) updated(iEdge) = .true.

    end if

end subroutine wallAcBC
!-------------------------------------------------------------------------------
end module boundaryConditions
!-------------------------------------------------------------------------------

! old BC routines
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Fill acoustics node/edge connectivity arrays with independent periodic
!!>  nodes and edges.
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  12 May 2015 - Initial creation.
!!>
!subroutine setPeriodicAcNodeEdge
!
!    use solverVars, only: iLeft, iRight
!    use meshUtil, only: nNodes, nEdges, nFaces, nCells, nDim, &
!                        cellFaces, faceCells, cellNodes, &
!                        maxFacesPerCell, maxNodesPerCell, &
!                        acNodeCells, acEdgeCells, acFaceCells
!
!    implicit none
!
!    ! Local variables
!    integer :: iCell,   & !< cell index
!               jCell,   & !< cell index
!               iFace,   & !< face index
!               jFace,   & !< face index
!               iNode,   & !< node index
!               jNode,   & !< node index
!               jjNode,  & !< node index
!               kNode      !< node index
!
!    integer :: iDir, pEdge, pNode, pNode1, pNode2, &
!               pNodeCorner1, pNodeCorner2, pNodeCorner3, pNodeCorner4
!
!    acEdgeCells = 0
!    acFaceCells = 0
!    acNodeCells = 0
!
!    ! Loop over faces to find acEdgeCells, acFaceCells
!    do iFace = 1, nFaces
!
!        ! only when edgeBC flag has periodic element 
!        pEdge = pEdgePair(iFace)  ! periodic edge index
!            
!        ! find acEdgeCells acFaceCells to be used in ACOUSTICS edge update
!        select case ( nDim )
!
!            case (1)
!            ! 2 cells sharing the edge at all times
!                jCell = 1
!                do iCell = 1,nCells
!                    do jFace = 1, maxFacesPerCell
!                        if ( cellFaces(jFace,iCell) == iFace ) then
!                            ! global cell number 
!                            acEdgeCells(1,iFace,jCell) = iCell
!                            ! local edge index
!                            acEdgeCells(2,iFace,jCell) = jFace
!                            jCell = jCell + 1
!                            ! there are only two neighboring cells
!                            if ( jCell > 2 ) exit
!                        end if
!                    end do
!                end do
!
!                ! set periodic boundary edges
!                iDir = 2 
!                ! right element index
!                if ( (acEdgeCells(1,iFace,iDir) == 0) ) then
!                    ! periodic cell index for iFace, jCell
!                    ! by definition faceCells has current element in iLeft index
!                    jCell = faceCells(iLeft,pEdge)
!                    acEdgeCells(1,iFace,iDir) = jCell 
!                    ! now find local edge
!                    do jFace = 1, maxFacesPerCell 
!                        if ( cellFaces(jFace,jCell) == pEdgePair(iFace) ) then
!                            acEdgeCells(2,iFace,iDir) = jFace  
!                        end if
!                    end do
!                end if
!
!            case (2)
!            ! 2 cells sharing the edge at all times
!                jCell = 1 ! local index 
!                do iCell = 1,nCells
!                    do jFace = 1, maxFacesPerCell
!                        if ( cellFaces(jFace,iCell) == iFace ) then
!                            ! global cell number 
!                            acEdgeCells(1,iFace,jCell) = iCell
!                            ! local edge index
!                            acEdgeCells(2,iFace,jCell) = jFace
!                            jCell = jCell + 1
!                            ! there are only two neighboring cells
!                            if ( jCell > 2 ) exit
!                        end if
!                    end do
!                end do
!
!                ! set PERIODIC boundary edges
!                iDir = 2
!                ! right element index
!                if ( (acEdgeCells(1,iFace,iDir) == 0) ) then
!                    ! periodic cell index for iFace, jCell
!                    ! by definition faceCells has current element in iLeft index
!                    jCell = faceCells(iLeft,pEdge)
!                    acEdgeCells(1,iFace,iDir) = jCell 
!                    ! now find local edge
!                    do jFace = 1, maxFacesPerCell 
!                        if ( cellFaces(jFace,jCell) == pEdgePair(iFace) ) then
!                            acEdgeCells(2,iFace,iDir) = jFace  
!                        end if
!                    end do
!                end if
!
!        end select
!        
!    end do
!    ! 1 and 2d acFaceCells == acEdgeCells
!    acFaceCells(:,:,:) = acEdgeCells(:,:,:) 
!
!    ! node to cell mapping (needed to start neighbor search)
!    do iNode = 1, nNodes
!
!        ! find acNodeCells to be used in ACOUSTICS nodal update
!        select case ( nDim )
!
!            case (1)
!            ! 2 cells sharing the node at all times
!                pNode1 = pNodePair(1,iNode)  ! periodic node index in x
!                kNode = 1
!                do iCell = 1,nCells
!                    do jNode = 1, maxNodesPerCell
!                        if ( cellNodes(jNode,iCell) == iNode ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                            ! there are only two neighboring cells
!                            if ( kNode > 2 ) exit
!                        else if ( cellNodes(jNode,iCell) == pNode1 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                            ! there are only two neighboring cells
!                            if ( kNode > 2 ) exit
!                        end if
!                    end do
!                end do
!
!            case (2)
!                ! many cells sharing the node
!                ! TODO: how many cells share the node? 
!                ! should fix this
!                pNode1 = pNodePair(1,iNode)  ! periodic node index in x
!                pNode2 = pNodePair(2,iNode)  ! periodic node index in y
!
!                !! for corner nodes, there are more than one periodic neighbor
!                ! there are four corner nodes
!                if ( pNode1 > 0 .and. pNode2 > 0 ) then 
!                    pNodeCorner1 = pNodePair(1,pNode1) 
!                    pNodeCorner2 = pNodePair(2,pNode1) 
!                    pNodeCorner3 = pNodePair(1,pNode2) 
!                    pNodeCorner4 = pNodePair(2,pNode2) 
!                else
!                    pNodeCorner1 = 0 
!                    pNodeCorner2 = 0 
!                    pNodeCorner3 = 0 
!                    pNodeCorner4 = 0 
!                end if
!
!                kNode = 1 ! local index, the third index of acNodeCells = neighboring cell index 
!                do iCell = 1,nCells
!                    do jNode = 1, maxNodesPerCell
!                        if ( cellNodes(jNode,iCell) == iNode ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!
!                        ! for PERIODIC neighbor nodes
!                        else if ( cellNodes(jNode,iCell) == pNode1 ) then
!                            ! neighbor 1
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                        else if ( cellNodes(jNode,iCell) == pNode2 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!
!                        ! for CORNER nodes, for proper periodic boundary in corner nodes
!                        else if ( cellNodes(jNode,iCell) == pNodeCorner1 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                        else if ( cellNodes(jNode,iCell) == pNodeCorner2 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                        else if ( cellNodes(jNode,iCell) == pNodeCorner3 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                        else if ( cellNodes(jNode,iCell) == pNodeCorner4 ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,kNode) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,kNode) = jNode
!                            kNode = kNode + 1
!                        end if
!
!                    end do
!                end do
!
!        end select
!
!    end do
!
!    !! debug: write to file the connectivities
!    !open (unit=43210,file="acEdgeWorks.dat",action="write",status="replace")
!    !do iFace = 1,nFaces
!    !write (43210,'(i4,i4,i4,i4,i4)') iFace, acEdgeCells(1,iFace,1),acEdgeCells(1,iFace,2)
!    !end do 
!    !close (43210)
!
!    !! debug: write to file the connectivities
!    !open (unit=43211,file="acNodeWorks.dat",action="write",status="replace")
!    !do iNode = 1,nNodes
!    !    write (43211,'(i4,10i4)') iNode, acNodeCells(1,iNode,:)
!    !end do 
!    !close (43211)
!    
!end subroutine setPeriodicAcNodeEdge
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Fill acoustics node/edge connectivity arrays with boundary specific connectivity
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  24 March 2015 - Initial creation.
!!>
!subroutine setAcNodeEdgeBC
!
!    use solverVars, only: iLeft, iRight
!    use meshUtil, only: nNodes, nEdges, nFaces, nCells, nDim, &
!                        cellFaces, faceCells, cellNodes, &
!                        maxFacesPerCell, maxNodesPerCell, &
!                        acNodeCells, acEdgeCells, acFaceCells
!
!    implicit none
!
!    ! Local variables
!    integer :: iCell,   & !< cell index
!               jCell,   & !< cell index
!               iFace,   & !< face index
!               jFace,   & !< face index
!               iNode,   & !< node index
!               jNode,   & !< node index
!               jjNode,  & !< node index
!               kNode,   & !< node index
!               iCount,  & !< counter
!               jCount     !< counter
!
!    integer :: iDir, pEdge, pNode, pNode1, pNode2, &
!               pNodeCorner1, pNodeCorner2, pNodeCorner3, pNodeCorner4
!
!    ! Loop over faces to find acEdgeCells, acFaceCells
!    do iFace = 1, nFaces
!
!        ! only when edgeBC flag has periodic element 
!        pEdge = pEdgePair(iFace)  ! periodic edge index
!        ! find acEdgeCells acFaceCells to be used in ACOUSTICS edge update
!        select case ( nDim )
!
!            case (1)
!            
!                ! set periodic boundary edges
!                iDir = 2 
!                ! right element index
!                if ( (acEdgeCells(1,iFace,iDir) == 0 ) ) then
!
!                    if ( edgeBC(iFace) == periodicID ) then
!                        ! periodic cell index for iFace, jCell
!                        ! by definition faceCells has current element in iLeft index
!                        jCell = faceCells(iLeft,pEdge)
!                        acEdgeCells(1,iFace,iDir) = jCell 
!                        ! now find local edge
!                        do jFace = 1, maxFacesPerCell 
!                            if ( cellFaces(jFace,jCell) == pEdgePair(iFace) ) then
!                                acEdgeCells(2,iFace,iDir) = jFace  
!                            end if
!                        end do
!
!                    else
!                        ! TODO: other boundaries
!                        acEdgeCells(1,iFace,iDir) = -edgeBC(iFace)  
!                        acEdgeCells(2,iFace,iDir) = 0  
!                    end if
!                end if
!
!            case (2)
!            
!                ! set PERIODIC boundary edges
!                iDir = 2
!                ! right element index
!                if ( (acEdgeCells(1,iFace,iDir) == 0) ) then
!
!                    if ( edgeBC(iFace) == periodicID ) then
!                        ! periodic cell index for iFace, jCell
!                        ! by definition faceCells has current element in iLeft index
!                        jCell = faceCells(iLeft,pEdge)
!                        acEdgeCells(1,iFace,iDir) = jCell 
!                        ! now find local edge
!                        do jFace = 1, maxFacesPerCell 
!                            if ( cellFaces(jFace,jCell) == pEdgePair(iFace) ) then
!                                acEdgeCells(2,iFace,iDir) = jFace  
!                            end if
!                        end do
!                    else
!                        ! TODO: other boundaries
!                        acEdgeCells(1,iFace,iDir) = -edgeBC(iFace)  
!                        acEdgeCells(2,iFace,iDir) = 0  
!                    end if
!                end if
!
!        end select
!        
!    end do
!    ! 1 and 2d acFaceCells == acEdgeCells
!    acFaceCells(:,:,:) = acEdgeCells(:,:,:) 
!
!    ! node to cell mapping (needed to start neighbor search)
!    do iNode = 1, nNodes
!
!        ! find acNodeCells to be used in ACOUSTICS nodal update
!        select case ( nDim )
!
!            case (1)
!                ! 2 cells sharing the node at all times
!                pNode1 = pNodePair(1,iNode)  ! periodic node index in x
!                iCount = 2
!                if ( acNodeCells(1,iNode,iCount) == 0 ) then
!                    if ( nodeBC(iNode) == periodicID ) then
!                        do iCell = 1,nCells
!                            do jNode = 1, maxNodesPerCell
!                                if ( cellNodes(jNode,iCell) == pNode1 ) then
!                                    ! global cell number 
!                                    acNodeCells(1,iNode,iCount) = iCell
!                                    ! local node index
!                                    acNodeCells(2,iNode,iCount) = jNode
!                                end if
!                            end do
!                        end do
!                    else
!                        ! TODO: other boundaries
!                        acNodeCells(1,iFace,iCount) = -nodeBC(iNode)  
!                        acNodeCells(2,iFace,iCount) = 0  
!                    end if
!                end if
!
!            case (2)
!                ! many cells sharing the node
!                ! TODO: how many cells share the node? 
!                ! should fix this
!                pNode1 = pNodePair(1,iNode)  ! periodic node index in x
!                pNode2 = pNodePair(2,iNode)  ! periodic node index in y
!
!                !! for corner nodes, there are more than one periodic neighbor
!                ! there are four corner nodes
!                if ( pNode1 > 0 .and. pNode2 > 0 ) then 
!                    pNodeCorner1 = pNodePair(1,pNode1) 
!                    pNodeCorner2 = pNodePair(2,pNode1) 
!                    pNodeCorner3 = pNodePair(1,pNode2) 
!                    pNodeCorner4 = pNodePair(2,pNode2) 
!                else
!                    pNodeCorner1 = 0 
!                    pNodeCorner2 = 0 
!                    pNodeCorner3 = 0 
!                    pNodeCorner4 = 0 
!                end if
!
!                ! loop through all local index and determine which part is not 
!                ! FIXME: we don't know exactly how many cells surround a node
!                ! this could potentially be a problem
!                do iCount = 2, 10
!                    ! Assuming 1 node is filled up...
!                    if ( acNodeCells(1,iNode,iCount) == 0 ) then
!                        if ( nodeBC(iNode) == periodicID ) then
!                            cellLoop: do iCell = 1,nCells
!                                ! make sure to skip over elements that were assigned previously
!                                do jCount = 1,iCount
!                                    if ( acNodeCells(1,iNode,jCount) == iCell ) cycle cellLoop
!                                end do
!
!                                do jNode = 1, maxNodesPerCell
!                                    ! for PERIODIC neighbor nodes
!                                    if ( cellNodes(jNode,iCell) == pNode1 ) then
!                                        ! neighbor 1
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!                                    else if ( cellNodes(jNode,iCell) == pNode2 ) then
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!
!                                    ! for CORNER nodes, for proper periodic boundary in corner nodes
!                                    else if ( cellNodes(jNode,iCell) == pNodeCorner1 ) then
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!                                    else if ( cellNodes(jNode,iCell) == pNodeCorner2 ) then
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!                                    else if ( cellNodes(jNode,iCell) == pNodeCorner3 ) then
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!                                    else if ( cellNodes(jNode,iCell) == pNodeCorner4 ) then
!                                        ! global cell number 
!                                        acNodeCells(1,iNode,iCount) = iCell
!                                        ! local node index
!                                        acNodeCells(2,iNode,iCount) = jNode
!                                    end if
!
!                                end do
!                            end do cellLoop        
!                        else
!                            ! TODO: other boundaries
!                            acNodeCells(1,iNode,iCount) = -nodeBC(iNode)  
!                            acNodeCells(2,iNode,iCount) = 0  
!                        end if
!                    end if
!                end do
!        end select
!
!    end do
!
!    !! debug: write to file the connectivities
!    !open (unit=43210,file="acEdge.dat",action="write",status="replace")
!    !do iFace = 1,nFaces
!    !write (43210,'(i4,i4,i4,i4,i4)') iFace, acEdgeCells(1,iFace,1),acEdgeCells(1,iFace,2)
!    !end do 
!    !close (43210)
!
!    !! debug: write to file the connectivities
!    !open (unit=43211,file="acNode.dat",action="write",status="replace")
!    !do iNode = 1,nNodes
!    !    write (43211,'(i4,10i4)') iNode, acNodeCells(1,iNode,:)
!    !end do 
!    !close (43211)
!
!end subroutine setAcNodeEdgeBC

