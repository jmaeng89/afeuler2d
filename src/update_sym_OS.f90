!-------------------------------------------------------------------------------
!> @purpose 
!>  Routines that advance the solution in time
!>  Advection updates using Characteristic Tracing scheme
!>
!> @history
!>  3 October 2014 - Implementation of RD scheme (Maeng)
!>  26 January 2015 - Started looking into pressure-less Euler 2d (Maeng)
!>  5 May 2015 - Barotropic Euler 2d (Maeng)
!>  9 October 2015 - Isentropic Euler equations 2d (Maeng)
!>  16 January 2017 - Full Euler equations 2d (Maeng)
!>  24 January 2017 - Experiment with operator splitting forms (Maeng)
!>  
module update

    use solverVars, only: nEqns, dtIn, FP
    use meshUtil, only: nDim, nCells, nEdges, nNodes
    implicit none

    ! Shared module variables 
    integer, parameter :: ADVECTION_EQ = 0, & !< flag for linear advection solution
                          ACOUSTIC_EQ = 4,  & !< flag for nonlinear acoustic solution
                          PLESSEULER_EQ = 1,& !< flag for pressureless Euler solution
                          ISENTEULER_EQ = 2,& !< flag for isentropic Euler solution
                          EULER_EQ = 3        !< flag for Euler solution

    integer :: eqFlag   !< flag for equation type

    real(FP), allocatable :: cellAvg(:,:),    & !< conserved state variable at centroids
                             cellAvgN(:,:),   & !< conserved variable at timestep "n"
                             primAvg(:,:),    & !< primitive variable average at cell centroids
                             primAvgN(:,:),   & !< primitive variable average at timestep "n"
                             edgeDataN(:,:),  & !< edge solution at iteration n
                             edgeData(:,:),   & !< edge solution at iteration n+1
                             edgeDataH(:,:),  & !< edge solution at iteration n+1/2
                             nodeDataN(:,:),  & !< node solution at iteration n
                             nodeData(:,:),   & !< node solution at iteration n+1
                             nodeDataH(:,:)     !< node solution at iteration n+1/2

    real(FP), allocatable :: residMax(:),   & !< max flux residual 
                             residNorm(:),  & !< L1 norm of flux residual over all cells
                             primMin(:),    & !< minimum of primitive variables
                             primNorm(:),   & !< L1 norm of primitive variables
                             conservNorm(:)   !< L1 norm of conserved variables

    real(FP), allocatable :: edgeChOrg(:,:),    & !< edge char. org. 
                             nodeChOrg(:,:)       !< node char. org.

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D AF advection system solver   
!>
!> @history
!>  22 February 2017 - Initial creation (Maeng)
!>
subroutine advectionSystem(nDim,nEqns,iter)

    use physics, only: ADVECTION_FLUX, PLESSEULER_FLUX  
    use meshUtil, only: nodeCoord, edgeCoord   

    implicit none

    !> Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           iter       !< iteration number

    ! Set all values equal in time
    nodeDataH = nodeDataN
    nodeData = nodeDataN
    edgeDataH = edgeDataN
    edgeData = edgeDataN

    ! initialize ChOrg with code and edge coordinates
    nodeChOrg(:,:) = nodeCoord(:,:)
    edgeChOrg(:,:) = edgeCoord(:,:)

    select case ( eqFlag )

        case ( ADVECTION_EQ ) 
            ! linear advection equations 
            ! nonconservative update
            call advectionUpdate( nDim,nEqns,iter )
            
            ! conservation stage
            call updateCells( nDim,nEqns,nCells,ADVECTION_FLUX, &
                              cellAvgN,cellAvg )

        case ( PLESSEULER_EQ )
            ! pressureless euler 
            ! nonconservative update
            call advectionUpdate( nDim,nEqns,iter )
            nodeData(4,:) = 1.0_FP
            edgeData(4,:) = 1.0_FP
            nodeDataH(4,:) = 1.0_FP
            edgeDataH(4,:) = 1.0_FP

            ! conservation stage
            call updateCells( nDim,nEqns,nCells,PLESSEULER_FLUX, &
                              cellAvgN,cellAvg )

    end select

    call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                          nodeData,edgeData,cellAvg,primAvg )

    ! Store for next time step
    edgeDataN = edgeData 
    nodeDataN = nodeData 
    cellAvgN = cellAvg 
    primAvgN = primAvg 

    ! check conservation
    call conservationCheck()

end subroutine advectionSystem
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D AF Euler solver   
!>  Second-order accurate Lax-Wendroff type expansion for solving 
!>      nonlinear advection and acoustic operations. 
!>
!> @history
!>  9 October 2015 - Initial creation (Maeng)
!>  16 January 2017 - Stable discretization form (aco->adv) implemented (Maeng)
!>
subroutine eulerUpdate(nDim,nEqns,iter)

    use solverVars, only: nIter
    use physics, only: ISENTEULER_FLUX, EULER_FLUX, &
                       isentropicConst, gam 
    use meshUtil, only: nodeCoord, edgeCoord   

    implicit none

    !> Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations
                           iter       !< iteration number

    integer :: iNode, iEdge

    ! Set all values equal in time
    nodeDataH = nodeDataN
    nodeData = nodeDataN
    edgeDataH = edgeDataN
    edgeData = edgeDataN

    ! initialize ChOrg with code and edge coordinates
    nodeChOrg(:,:) = nodeCoord(:,:)
    edgeChOrg(:,:) = edgeCoord(:,:)

    select case ( eqFlag )

      ! staggered operator splitting
      case ( EULER_EQ )
        ! euler equations 

        if ( mod(iter,2) == 1 ) then
            ! ASYMMETRIC Operation 0 : Aco -> Adv
            ! acoustics operator
            call acousticsUpdate0( nDim,nEqns,iter )
            ! advection operator
            call advectionUpdate0( nDim,nEqns,iter )

            
        else 
            ! ASYMMETRIC Operation 1 : Adv -> Aco
            ! advection operator
            call advectionUpdate1( nDim,nEqns,iter )

            ! acoustics operator
            call acousticsUpdate1( nDim,nEqns,iter )

        end if

        !! temporal limited nodal points
        !do iNode = 1, nNodes
        !    call limiter(nodeDataN(:,iNode),nodeDataH(:,iNode),nodeData(:,iNode))
        !end do
        !do iEdge = 1, nEdges
        !    call limiter(edgeDataN(:,iEdge),edgeDataH(:,iEdge),edgeData(:,iEdge))
        !end do

        !! isentropic pressure
        !nodeDataH(4,:) = isentropicConst*nodeDataH(1,:)**gam
        !nodeData(4,:) = isentropicConst*nodeData(1,:)**gam
        !edgeDataH(4,:) = isentropicConst*edgeDataH(1,:)**gam
        !edgeData(4,:) = isentropicConst*edgeData(1,:)**gam

        ! conservation stage
        call updateCells( nDim,nEqns,nCells,EULER_FLUX,cellAvgN,cellAvg )
                
    end select

    call conserv2primAvg( nDim,nEqns,nNodes,nEdges,nCells, &
                          nodeData,edgeData,cellAvg,primAvg )

    ! Store for next time step
    edgeDataN = edgeData 
    nodeDataN = nodeData 
    cellAvgN = cellAvg 
    primAvgN = primAvg 

    ! check conservation
    call conservationCheck()

end subroutine eulerUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Limiter 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  14 March 2017 - Initial creation
!>
subroutine limiter(qN,qH,q)
   
    use solverVars, only: eps
    implicit none

    ! Interface variables
    real(FP), intent(in) :: qN(:),  & !< time step "n"
                            qH(:)     !< time step "n+1/2"

    real(FP), intent(inout) :: q(:)   !< time step "n+1"

    ! Local variables
    integer :: iEq,     & !< equation index
               n

    real(FP) :: dq,     & 
                dqL,    & !< difference between "n" and "n+1/2"
                dqR,    & !< difference between "n+1/2" and "n+1"
                R,  RNew

    n = size(qN)

    do iEq = 1, n
        dq  = q(iEq) - qN(iEq)
        dqL = qH(iEq) - qN(iEq)
        dqR = q(iEq) - qH(iEq)
        if ( abs(dqR) <= eps ) then 
            R = 10.0_FP
        else
            R = dqL/dqR 
        end if
        ! minmod
        RNew = max(0.0_FP, min(1.0_FP,R))
        ! superbee
        !RNew = max(0.0_FP, min(2.0_FP*R,1.0_FP),min(R,2.0_FP))
        !q(iEq) = RNew*dq + qN(iEq)
        q(iEq) = RNew*dqR + qH(iEq)
    end do

end subroutine limiter
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update values of reconData array
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  13 June 2012 - Initial creation
!>
subroutine updateReconData()

    use meshUtil, only: cellFaces, cellNodes
    use reconstruction, only: reconData, reconDataH, reconDataF
    implicit none

    ! Local variables
    integer :: iCell,       & !< cell index
               iEq

    ! set reconstruction coefficients
    do iCell = 1, nCells
        do iEq = 1, nEqns
            ! time step "n" reconstruction coefficients
            reconData(iEq,:,iCell) = coefficients(nDim,iEq,iCell)
        end do
    end do

end subroutine updateReconData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Update values of reconDataH and reconDataF arrays
!>  Notice that they are QUADRATIC coefficients
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  25 January 2017 - Initial creation
!>
subroutine updateReconDataQuad(qNodeH,qEdgeH,qNode,qEdge)

    use meshUtil, only: cellFaces, cellNodes
    use reconstruction, only: reconData, reconDataH, reconDataF

    implicit none

    ! Interface variables
    real(FP), intent(in) :: qNodeH(:,:),   & !< node data 
                            qEdgeH(:,:),   & !< edge data
                            qNode(:,:),    &
                            qEdge(:,:)     

    ! Local variables
    integer :: iCell,       & !< cell index
               iEq

    ! set reconstruction coefficients
    do iCell = 1, nCells
        do iEq = 1, nEqns
            ! reconstruction coefficients of partially evaluated solutions
            ! HALF time step reconstruction coefficients, quadratic
            reconDataH(iEq,1,iCell) = qNodeH(iEq,cellNodes(1,iCell))
            reconDataH(iEq,3,iCell) = qNodeH(iEq,cellNodes(2,iCell))
            reconDataH(iEq,5,iCell) = qNodeH(iEq,cellNodes(3,iCell))
            reconDataH(iEq,2,iCell) = qEdgeH(iEq,cellFaces(1,iCell))
            reconDataH(iEq,4,iCell) = qEdgeH(iEq,cellFaces(2,iCell))
            reconDataH(iEq,6,iCell) = qEdgeH(iEq,cellFaces(3,iCell))
            reconDataH(iEq,7:10,iCell) = 0.0_FP ! no bubble function

            ! FULL time step reconstruction coefficients, quadratic
            reconDataF(iEq,1,iCell) = qNode(iEq,cellNodes(1,iCell))
            reconDataF(iEq,3,iCell) = qNode(iEq,cellNodes(2,iCell))
            reconDataF(iEq,5,iCell) = qNode(iEq,cellNodes(3,iCell))
            reconDataF(iEq,2,iCell) = qEdge(iEq,cellFaces(1,iCell))
            reconDataF(iEq,4,iCell) = qEdge(iEq,cellFaces(2,iCell))
            reconDataF(iEq,6,iCell) = qEdge(iEq,cellFaces(3,iCell))
            reconDataF(iEq,7:10,iCell) = 0.0_FP ! no bubble function
        end do
    end do

end subroutine updateReconDataQuad
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply acoustics operator. 
!>  Import functions and subroutines written in C from srcAcoustics directory.
!>  Requires partially update advection data.
!>  
!>  Acoustics corrections require different attention from advection updates
!>  as the operator is elliptic in nature. 
!>  All nodal and edge data must be updated at the same time
!>  to reduce chance of corrupting reconstruction for other nodes.
!>
!> @history
!>  7 May 2015 - Initial creation (Maeng)
!>  4 May 2016 - Modularized acoustics correction (Maeng)
!>  16 January 2017 - Incorporated updateReconData subroutine (Maeng)
!>
subroutine acousticsUpdate(nDim,nEqns,iter)

    use reconstruction, only: reconData
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord
    use boundaryConditions, only: applyAcBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge              !< global edge index

    real(FP) :: cellRecon(nEqns,nDim*(nDim+1)+1), & !< cell quadratic reconstruction 
                nodeAcSignal(nEqns,3),            & !< signals on node
                edgeAcSignal(nEqns,3),            & !< signals on edge 
                cellNodeCoord(nDim,3)               !< cell vertex

    ! set cell reconstruction coefficients
    call updateReconData()
    
    ! acoustics solver with cell loop based update 
    do iCell = 1, nCells
        
        ! set cell node coordinates 
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            cellNodeCoord(:,iNode) = nodeCoord(:,gNode) 
        end do

        ! half time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconData(:,1:7,iCell)
        call acSignalUpdate(nEqns,nDim,iCell,0.5_FP*dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeDataH,0.5_FP*dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeDataH,0.5_FP*dtIn)
        end do

        ! full time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconData(:,1:7,iCell)
        call acSignalUpdate(nEqns,nDim,iCell,dtIn,cellRecon, &
                            nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeData(:,gNode) = nodeData(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeData,dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeData(:,gEdge) = edgeData(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeData,dtIn)
        end do

    end do

end subroutine acousticsUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply acoustics operator. 
!>  Import functions and subroutines written in C from srcAcoustics directory.
!>  Requires partially update advection data.
!>  
!>  Acoustics corrections require different attention from advection updates
!>  as the operator is elliptic in nature. 
!>  All nodal and edge data must be updated at the same time
!>  to reduce chance of corrupting reconstruction for other nodes.
!>
!> @history
!>  7 May 2015 - Initial creation (Maeng)
!>  4 May 2016 - Modularized acoustics correction (Maeng)
!>  16 January 2017 - Incorporated updateReconData subroutine (Maeng)
!>
subroutine acousticsUpdate0(nDim,nEqns,iter)

    use reconstruction, only: reconData
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord
    use boundaryConditions, only: applyAcBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge              !< global edge index

    real(FP) :: cellRecon(nEqns,nDim*(nDim+1)+1), & !< cell quadratic reconstruction 
                nodeAcSignal(nEqns,3),            & !< signals on node
                edgeAcSignal(nEqns,3),            & !< signals on edge 
                cellNodeCoord(nDim,3)               !< cell vertex

    ! set cell reconstruction coefficients
    call updateReconData()
    
    ! acoustics solver with cell loop based update 
    do iCell = 1, nCells
        
        ! set cell node coordinates 
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            cellNodeCoord(:,iNode) = nodeCoord(:,gNode) 
        end do

        ! half time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconData(:,1:7,iCell)
        call acSignalValue(nEqns,nDim,iCell,0.5_FP*dtIn,cellRecon, &
                           nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeDataH,0.5_FP*dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeDataH,0.5_FP*dtIn)
        end do

        ! full time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconData(:,1:7,iCell)
        call acSignalValue(nEqns,nDim,iCell,dtIn,cellRecon, &
                           nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeData(:,gNode) = nodeData(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeData,dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeData(:,gEdge) = edgeData(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeData,dtIn)
        end do

    end do

end subroutine acousticsUpdate0
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply acoustics operator. 
!>  Import functions and subroutines written in C from srcAcoustics directory.
!>  Requires partially update advection data.
!>  
!>  Acoustics corrections require different attention from advection updates
!>  as the operator is elliptic in nature. 
!>  All nodal and edge data must be updated at the same time
!>  to reduce chance of corrupting reconstruction for other nodes.
!>
!> @history
!>  7 May 2015 - Initial creation (Maeng)
!>  4 May 2016 - Modularized acoustics correction (Maeng)
!>  16 January 2017 - Incorporated updateReconData subroutine (Maeng)
!>
subroutine acousticsUpdate1(nDim,nEqns,iter)

    use reconstruction, only: reconDataH, reconDataF
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord
    use boundaryConditions, only: applyAcBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge              !< global edge index

    real(FP) :: cellRecon(nEqns,nDim*(nDim+1)+1), & !< cell quadratic reconstruction 
                nodeAcSignal(nEqns,3),            & !< signals on node
                edgeAcSignal(nEqns,3),            & !< signals on edge 
                cellNodeCoord(nDim,3)               !< cell vertex

    ! set cell quadratic reconstruction coefficients
    call updateReconDataQuad(nodeDataH,edgeDataH,nodeData,edgeData)

    ! acoustics solver with cell loop based update 
    do iCell = 1, nCells
        
        ! set cell node coordinates 
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            cellNodeCoord(:,iNode) = nodeCoord(:,gNode) 
        end do

        ! half time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconDataH(:,1:7,iCell)
        call acSignalValue(nEqns,nDim,iCell,0.5_FP*dtIn,cellRecon, &
                           nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeDataH,0.5_FP*dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeDataH,0.5_FP*dtIn)
        end do

        ! full time step
        nodeAcSignal = 0.0_FP
        edgeAcSignal = 0.0_FP
        cellRecon = reconDataF(:,1:7,iCell)
        call acSignalValue(nEqns,nDim,iCell,dtIn,cellRecon, &
                           nodeAcSignal,edgeAcSignal,cellNodeCoord)
        ! apply signals
        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)
            nodeData(:,gNode) = nodeData(:,gNode) + nodeAcSignal(:,iNode) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,iNode,nodeAcSignal(:,iNode), &
                           nodeData,dtIn)
        end do
        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)
            edgeData(:,gEdge) = edgeData(:,gEdge) + edgeAcSignal(:,iEdge) 
            ! apply boundary condition
            call applyAcBC(nEqns,iCell,-iEdge,edgeAcSignal(:,iEdge), &
                           edgeData,dtIn)
        end do

    end do

end subroutine acousticsUpdate1
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply advection operator using Characteristic Origin Tracing scheme.
!>  Advection speed is determined by node/edge velocity
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  4 September 2013 - Linear advection constant wave speed is added (Maeng)
!>  13 January 2015 - 2D Burgers' equations (Maeng) 
!>  1 February 2015 - 2D Pressureless Euler equations (Maeng) 
!>  5 May 2015 - 2D Euler Equations (Maeng)
!>  16 January 2017 - Order of operators changed; acoustics operator is applied first
!>`     advection operator takes in quadratic reconstruction. (Maeng)
!>
subroutine advectionUpdate(nDim,nEqns,iter,lam)

    use reconstruction, only: reconDataH, reconDataF
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord, edgeCoord
    use boundaryConditions, only: applyBC, nodeBC, edgeBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               neglectedEdgesH, & !< number of edges without an update
               neglectedNodesH, & !< number of nodes without an update
               neglectedEdges,  & !< number of edges without an update
               neglectedNodes     !< number of nodes without an update

    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
                       nodeUpdated(nNodes),  & !< flag that a node has been updated
                       edgeUpdated(nEdges)     !< flag that an edge has been updated

    real(FP) :: dt,             & !< time step 
                vel(nDim),      & !< local advection velocity 
                signal(nEqns)     !< cell's contribution to node/edge value

    nodeUpdatedH = .false.
    edgeUpdatedH = .false.
    nodeUpdated = .false.
    edgeUpdated = .false.

    dt = dtIn

    ! Set reconstruction coefficients for primitive variables
    call updateReconData()

    do iCell = 1,nCells

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)

            ! HALF time step local velocity  
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = nodeDataN(2:3,gNode) ! Adv->Aco
                vel(:) = nodeDataH(2:3,gNode) ! Aco->Adv
            end if

            ! half time step
            !signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
            !                0.5_FP*dt,vel,nodeDataN(:,gNode), &
            !                nodeUpdatedH(gNode) )
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            0.5_FP*dt,vel,nodeDataH(:,gNode), &
                            nodeUpdatedH(gNode),cIn=reconDataH(:,:,iCell) )
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeDataH,0.5_FP*dt, &
                                nodeUpdatedH)

            ! FULL time step local velocity
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = nodeDataN(2:3,gNode) ! Adv->Aco
                vel(:) = nodeData(2:3,gNode) ! Aco->Adv
            end if

            ! full time step
            !signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
            !                dt,vel,nodeDataN(:,gNode), &
            !                nodeUpdated(gNode) )
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            dt,vel,nodeData(:,gNode), &
                            nodeUpdated(gNode),cIn=reconDataF(:,:,iCell) )
            nodeData(:,gNode) = nodeData(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeData,dt, &
                                nodeUpdated)

        end do

        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)

            ! HALF time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = edgeDataN(2:3,gEdge) ! Adv->Aco
                vel(:) = edgeDataH(2:3,gEdge) ! Aco->Adv
            end if

            ! half time step
            !signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
            !                0.5_FP*dt,vel,edgeDataN(:,gEdge), &
            !                edgeUpdatedH(gEdge) )
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            0.5_FP*dt,vel,edgeDataH(:,gEdge), &
                            edgeUpdatedH(gEdge),cIn=reconDataH(:,:,iCell) )
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeDataH,0.5_FP*dt, &
                                edgeUpdatedH)

            ! FULL time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = edgeDataN(2:3,gEdge) ! Adv->Aco
                vel(:) = edgeData(2:3,gEdge) ! Aco->Adv
            end if

            ! full time step
            !signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
            !                dt,vel,edgeDataN(:,gEdge),  &
            !                edgeUpdated(gEdge) )
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            dt,vel,edgeData(:,gEdge),  &
                            edgeUpdated(gEdge),cIn=reconDataF(:,:,iCell) )
            edgeData(:,gEdge) = edgeData(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeData,dt, &
                                edgeUpdated)
        
        end do

    end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Specific inflow
    !!
    !! 18 February 2016
    !!
    !! use this for solid body rotation where inflow is needed 
    !!
    !do gNode = 1, nNodes
    !    if ( .not. nodeUpdatedH(gNode) ) then
    !       nodeDataH(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeDataH(:,gNode),nodeCoord(:,gNode),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. nodeUpdated(gNode) ) then
    !       nodeData(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeData(:,gNode),nodeCoord(:,gNode),tSim+dtIn)
    !    end if
    !end do
    !do gEdge = 1, nEdges
    !    if ( .not. edgeUpdatedH(gEdge) ) then
    !       edgeDataH(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeDataH(:,gEdge),edgeCoord(:,gEdge),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. edgeUpdated(gEdge) ) then
    !       edgeData(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeData(:,gEdge),edgeCoord(:,gEdge),tSim+dtIn)
    !    end if
    !end do
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! nullify pointers
    !nullify( edgePtr, nodePtr, updatedPtr )
                
    ! print out neglected node/edge
    neglectedEdgesH = nEdges - count( edgeUpdatedH )
    neglectedNodesH = nNodes - count( nodeUpdatedH )
    neglectedEdges = nEdges - count( edgeUpdated )
    neglectedNodes = nNodes - count( nodeUpdated )
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
    end if
    
    ! Check for edges/nodes without an update
#ifdef VERBOSE
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine advectionUpdate
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply advection operator using Characteristic Origin Tracing scheme.
!>  Advection speed is determined by node/edge velocity
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  4 September 2013 - Linear advection constant wave speed is added (Maeng)
!>  13 January 2015 - 2D Burgers' equations (Maeng) 
!>  1 February 2015 - 2D Pressureless Euler equations (Maeng) 
!>  5 May 2015 - 2D Euler Equations (Maeng)
!>  16 January 2017 - Order of operators changed; acoustics operator is applied first
!>`     advection operator takes in quadratic reconstruction. (Maeng)
!>
subroutine advectionUpdate0(nDim,nEqns,iter,lam)

    use reconstruction, only: reconDataH, reconDataF
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord, edgeCoord
    use boundaryConditions, only: applyBC, nodeBC, edgeBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iEq,             & !< equation index 
               iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               neglectedEdgesH, & !< number of edges without an update
               neglectedNodesH, & !< number of nodes without an update
               neglectedEdges,  & !< number of edges without an update
               neglectedNodes     !< number of nodes without an update

    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
                       nodeUpdated(nNodes),  & !< flag that a node has been updated
                       edgeUpdated(nEdges)     !< flag that an edge has been updated

    real(FP) :: dt,             & !< time step 
                vel(nDim),      & !< local advection velocity 
                signal(nEqns)     !< cell's contribution to node/edge value

    nodeUpdatedH = .false.
    edgeUpdatedH = .false.
    nodeUpdated = .false.
    edgeUpdated = .false.

    dt = dtIn

    ! set cell quadratic reconstruction coefficients
    call updateReconDataQuad(nodeDataH,edgeDataH,nodeData,edgeData)

    do iCell = 1,nCells

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)

            ! HALF time step local velocity  
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = nodeDataH(2:3,gNode) ! Aco->Adv
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            0.5_FP*dt,vel,nodeDataH(:,gNode), &
                            nodeUpdatedH(gNode),cIn=reconDataH(:,:,iCell) )
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeDataH,0.5_FP*dt, &
                                nodeUpdatedH)

            ! FULL time step local velocity
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = nodeData(2:3,gNode) ! Aco->Adv
            end if

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            dt,vel,nodeData(:,gNode), &
                            nodeUpdated(gNode),cIn=reconDataF(:,:,iCell) )
            nodeData(:,gNode) = nodeData(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeData,dt, &
                                nodeUpdated)

        end do

        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)

            ! HALF time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = edgeDataH(2:3,gEdge) ! Aco->Adv
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            0.5_FP*dt,vel,edgeDataH(:,gEdge), &
                            edgeUpdatedH(gEdge),cIn=reconDataH(:,:,iCell) )
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeDataH,0.5_FP*dt, &
                                edgeUpdatedH)

            ! FULL time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                vel(:) = edgeData(2:3,gEdge) ! Aco->Adv
            end if

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            dt,vel,edgeData(:,gEdge),  &
                            edgeUpdated(gEdge),cIn=reconDataF(:,:,iCell) )
            edgeData(:,gEdge) = edgeData(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeData,dt, &
                                edgeUpdated)
        
        end do

    end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Specific inflow
    !!
    !! 18 February 2016
    !!
    !! use this for solid body rotation where inflow is needed 
    !!
    !do gNode = 1, nNodes
    !    if ( .not. nodeUpdatedH(gNode) ) then
    !       nodeDataH(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeDataH(:,gNode),nodeCoord(:,gNode),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. nodeUpdated(gNode) ) then
    !       nodeData(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeData(:,gNode),nodeCoord(:,gNode),tSim+dtIn)
    !    end if
    !end do
    !do gEdge = 1, nEdges
    !    if ( .not. edgeUpdatedH(gEdge) ) then
    !       edgeDataH(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeDataH(:,gEdge),edgeCoord(:,gEdge),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. edgeUpdated(gEdge) ) then
    !       edgeData(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeData(:,gEdge),edgeCoord(:,gEdge),tSim+dtIn)
    !    end if
    !end do
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !! nullify pointers
    !nullify( edgePtr, nodePtr, updatedPtr )
                
    ! print out neglected node/edge
    neglectedEdgesH = nEdges - count( edgeUpdatedH )
    neglectedNodesH = nNodes - count( nodeUpdatedH )
    neglectedEdges = nEdges - count( edgeUpdated )
    neglectedNodes = nNodes - count( nodeUpdated )
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
    end if
    
    ! Check for edges/nodes without an update
#ifdef VERBOSE
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine advectionUpdate0
!-------------------------------------------------------------------------------
!> @purpose 
!>  Apply advection operator using Characteristic Origin Tracing scheme.
!>  Advection speed is determined by node/edge velocity
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  4 September 2013 - Linear advection constant wave speed is added (Maeng)
!>  13 January 2015 - 2D Burgers' equations (Maeng) 
!>  1 February 2015 - 2D Pressureless Euler equations (Maeng) 
!>  5 May 2015 - 2D Euler Equations (Maeng)
!>  16 January 2017 - Order of operators changed; acoustics operator is applied first
!>`     advection operator takes in quadratic reconstruction. (Maeng)
!>
subroutine advectionUpdate1(nDim,nEqns,iter,lam)

    use reconstruction, only: reconData
    use meshUtil, only: maxNodesPerCell, cellNodes, &
                        maxFacesPerCell, cellFaces, &
                        nodeCoord, edgeCoord
    use boundaryConditions, only: applyBC, nodeBC, edgeBC

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< number of equations in system
                           iter       !< iteration number

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iCell,           & !< cell index in loop
               iNode,           & !< local node index within cell
               iEdge,           & !< local edge index witn cell
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               neglectedEdgesH, & !< number of edges without an update
               neglectedNodesH, & !< number of nodes without an update
               neglectedEdges,  & !< number of edges without an update
               neglectedNodes     !< number of nodes without an update

    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
                       nodeUpdated(nNodes),  & !< flag that a node has been updated
                       edgeUpdated(nEdges)     !< flag that an edge has been updated

    real(FP) :: dt,             & !< time step 
                vel(nDim),      & !< local advection velocity 
                signal(nEqns)     !< cell's contribution to node/edge value

    nodeUpdatedH = .false.
    edgeUpdatedH = .false.
    nodeUpdated = .false.
    edgeUpdated = .false.

    dt = dtIn

    ! Set reconstruction coefficients for primitive variables
    call updateReconData()

    do iCell = 1,nCells

        do iNode = 1, maxNodesPerCell
            gNode = cellNodes(iNode,iCell)

            ! HALF time step local velocity  
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = nodeDataN(2:3,gNode) ! Adv->Aco
                vel(:) = nodeDataH(2:3,gNode) ! Adv->Aco
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            0.5_FP*dt,vel,nodeDataH(:,gNode), &
                            nodeUpdatedH(gNode),cIn=reconData(:,:,iCell) )
            nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeDataH,0.5_FP*dt, &
                                nodeUpdatedH)

            ! FULL time step local velocity
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = nodeDataN(2:3,gNode) ! Adv->Aco
                vel(:) = nodeData(2:3,gNode) ! Adv->Aco
            end if

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,iNode, &
                            dt,vel,nodeData(:,gNode), &
                            nodeUpdated(gNode),cIn=reconData(:,:,iCell) )
            nodeData(:,gNode) = nodeData(:,gNode) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,iNode,signal,nodeData,dt, &
                                nodeUpdated)

        end do

        do iEdge = 1, maxFacesPerCell
            gEdge = cellFaces(iEdge,iCell)

            ! HALF time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = edgeDataN(2:3,gEdge) ! Adv->Aco
                vel(:) = edgeDataH(2:3,gEdge) ! Adv->Aco
            end if

            ! half time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            0.5_FP*dt,vel,edgeDataH(:,gEdge), &
                            edgeUpdatedH(gEdge),cIn=reconData(:,:,iCell) )
            edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeDataH,0.5_FP*dt, &
                                edgeUpdatedH)

            ! FULL time step local velocity 
            if ( present(lam) ) then
                ! linear advection
                vel(:) = lam(:)
            else
                !vel(:) = edgeDataN(2:3,gEdge) ! Adv->Aco
                vel(:) = edgeData(2:3,gEdge) ! Adv->Aco
            end if

            ! full time step
            signal(:) = signalValue( nEqns,nDim,iCell,-iEdge, &
                            dt,vel,edgeData(:,gEdge),  &
                            edgeUpdated(gEdge),cIn=reconData(:,:,iCell) )
            edgeData(:,gEdge) = edgeData(:,gEdge) + signal
            ! apply boundary condition
            call applyBC(nEqns,iCell,-iEdge,signal,edgeData,dt, &
                                edgeUpdated)
        
        end do

    end do

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! Specific inflow
    !!
    !! 18 February 2016
    !!
    !! use this for solid body rotation where inflow is needed 
    !!
    !do gNode = 1, nNodes
    !    if ( .not. nodeUpdatedH(gNode) ) then
    !       nodeDataH(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeDataH(:,gNode),nodeCoord(:,gNode),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. nodeUpdated(gNode) ) then
    !       nodeData(:,gNode) = pLessEulerSol(nDim,nEqns,&
    !           nodeData(:,gNode),nodeCoord(:,gNode),tSim+dtIn)
    !    end if
    !end do
    !do gEdge = 1, nEdges
    !    if ( .not. edgeUpdatedH(gEdge) ) then
    !       edgeDataH(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeDataH(:,gEdge),edgeCoord(:,gEdge),tSim+0.5_FP*dtIn)
    !    end if
    !    if ( .not. edgeUpdated(gEdge) ) then
    !       edgeData(:,gEdge) = pLessEulerSol(nDim,nEqns,&
    !           edgeData(:,gEdge),edgeCoord(:,gEdge),tSim+dtIn)
    !    end if
    !end do
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! print out neglected node/edge
    neglectedEdgesH = nEdges - count( edgeUpdatedH )
    neglectedNodesH = nNodes - count( nodeUpdatedH )
    neglectedEdges = nEdges - count( edgeUpdated )
    neglectedNodes = nNodes - count( nodeUpdated )
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
    end if
    
    ! Check for edges/nodes without an update
#ifdef VERBOSE
    if ( ( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 ) .or. &
         ( neglectedEdgesH > 0 ) .or. ( neglectedNodesH > 0 ) ) then
        write(*,*)
        write(*,*) 'Adevection update'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodesH,' nodes and ', &
                   neglectedEdgesH,' edges at half time have not been updated.'
        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
                   neglectedEdges,' edges at full time have not been updated.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iNode = 1, nNodes
            if ( .not. nodeUpdated(iNode) ) then
                write(*,'(i7,2e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
                                            nodeBC(iNode)
                !if ( nodebc(iNode) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine advectionUpdate1
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the signal sent from a cell at a specified time
!>
!> @history
!>  26 June 2012 - Initial creation (Eymann)
!>  14 May 2013 - Use local advection speed (Eymann)
!>  1 February 2015 - Pressure-less Euler system. Velocity divergence
!>      correction for density (Maeng)
!>  5 October 2015 - Incorporated a mandatory update flag, and onEdge flag
!>      no longer used. 
!>  9 October 2015 - Euler equations in 2d (Maeng)
!>  11 November 2015 - Remove velocity divergence effect from pressureless Euler
!>      for the implementation of Euler equations (Maeng)
!>  16 January 2017 - Euler nonlinear interaction terms included (Maeng) 
!>
function signalValue(nEqns,nDim,iCell,iNodeEdge,dt,lam,qN,updated,iter,cIn)
   
    use solverVars, only: eps
    use reconstruction, only: reconValue, gradVal
    use meshUtil, only: cellFaces, faceCells, cellNodes, ref2cart
    use physics, only: gam

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations in system
                           nDim,    & !< number of dimensions
                           iCell,   & !< cell index
                           iNodeEdge  !< local node index (negative for edge)

    real(FP), intent(in) :: dt,                 & !< timestep for signal
                            lam(nDim),          & !< local advection speed
                            qN(nEqns)             !< state at time n

    logical, intent(inout) :: updated !< flag that update should be applied

    integer, intent(in), optional :: iter

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    ! Function variables
    real(FP) :: signalValue(nEqns)

    ! Local variables
    integer :: iEq,     & !< equation index
               onEdge     !< edge containing char. origin

    integer :: gFace,   & !< global face index
               gNode,   &
               pEdge,   & !< periodic edge
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: tol,                & !< tolerance
                xi0(nDim),          & !< char origin in ref space
                qXi0(nEqns),        & !< state at char. origin
                cVel(nDim),         & !< corrected wave speed
                gradU(nDim,nDim),   & !< velocity gradient
                divSignal,          & !< divergence effect on density signal 
                divU                  !< velocity divergence

    real(FP) :: xiL(nDim),  &
                xiR(nDim),  &  
                xCart(nDim)    

    logical :: inCell = .false.     !< flag that signal originates in cell

    tol = eps
    signalValue = 0.0_FP
    qXi0 = 0.0_FP

    ! corrected advection velocity
    select case ( eqFlag ) 
      case ( ADVECTION_EQ )
        cVel = lam
      !case ( EULER_EQ, ISENTEULER_EQ )
      !  ! nonlinear advection for Euler system
      !  ! notice cIn
      !  cVel = corrWaveSpeedEuler(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)
      case default
        ! nonlinear advection - velocity correction
        cVel = corrWaveSpeed(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)
    end select

    call charOrigin(nDim,iCell,iNodeEdge,dt,cVel,xi0,inCell,onEdge)

    lCell = iCell
    rCell = iCell
    xiL = xi0
    xiR = xi0
    ! only modify values if signal for wave originates in cell
    if ( ( inCell ) .and. ( .not. updated ) ) then  

        select case ( eqFlag )
        
          case ( ADVECTION_EQ ) 
            ! original characteristic origin tracing
            ! do not update other variables, in case of linear advection
            qXi0(1) = reconValue(nDim,1,iCell,xi0(:))
            signalValue(1) = qXi0(1) - qN(1)

            if ( abs(signalValue(1)) <= tol ) signalValue(1) = 0.0_FP
          
          case ( PLESSEULER_EQ )

            do iEq = 1,nEqns
                ! original characteristic origin tracing
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:))

                if ( iEq == 1 ) then 
                    ! density correction by divergence of vel.
                    divSignal = 0.0_FP
                    if ( nDim == 2 ) then
                        ! velocity gradients
                        gradU(1,:) = gradVal(nDim,2,iCell,xi0) ! grad u
                        gradU(2,:) = gradVal(nDim,3,iCell,xi0) ! grad v
                        ! divergence of velocity
                        !divU = velocityDiv( nDim, iCell, xi0, dt )
                        divU = gradU(1,1) + gradU(2,2)
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU) + &
                                    dt*dt*(gradU(1,1)*gradU(2,2) - gradU(1,2)*gradU(2,1)))
                        ! signal value
                        signalValue(iEq) = divSignal - qN(iEq)
                    else    ! nDim == 1
                        !divU = velocityDiv( nDim, iCell, xi0, dt )
                        divU = gradU(1,1)
                        divSignal = qXi0(iEq)/(1.0_FP + dt*(divU))
                        signalValue(iEq) = divSignal - qN(iEq)
                    end if
                else    
                    ! velocities
                    signalValue(iEq) = qXi0(iEq) - qN(iEq) 
                end if
                if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
            end do

          case ( EULER_EQ, ISENTEULER_EQ )

            ! interpolation at the characteristic origin
            do iEq = 1,nEqns

                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:),cIn(iEq,:))
                ! advection operator
                signalValue(iEq) = qXi0(iEq) - qN(iEq)

                if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
            end do

          case default

            do iEq = 1,nEqns
                ! original characteristic origin tracing
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:))

                ! advection operator
                signalValue(iEq) = qXi0(iEq) - qN(iEq)

                if ( abs(signalValue(iEq)) <= tol ) signalValue(iEq) = 0.0_FP
            end do

        end select

        ! characteristic origin
        xCart = ref2cart(nDim,iCell,xi0)
        if ( iNodeEdge > 0 ) then
            nodeChOrg(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        else
            edgeChOrg(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        end if

        !xCart = ref2cart(nDim,lCell,xiL)
        !if ( iNodeEdge > 0 ) then
        !    nodeChOrgL(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        !else
        !    edgeChOrgL(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        !end if
        !xCart = ref2cart(nDim,rCell,xiR)
        !if ( iNodeEdge > 0 ) then
        !    nodeChOrgR(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        !else
        !    edgeChOrgR(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        !end if

        ! mark node/edge as updated
        updated = .true.
    end if
    
end function signalValue
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the signal sent from a cell at a specified time
!>
!> @history
!>  26 June 2012 - Initial creation (Eymann)
!>  14 May 2013 - Use local advection speed (Eymann)
!>  1 February 2015 - Pressure-less Euler system. Velocity divergence
!>      correction for density (Maeng)
!>  5 October 2015 - Incorporated a mandatory update flag, and onEdge flag
!>      no longer used. 
!>  9 October 2015 - Euler equations in 2d (Maeng)
!>  11 November 2015 - Remove velocity divergence effect from pressureless Euler
!>      for the implementation of Euler equations (Maeng)
!>  16 January 2017 - Euler nonlinear interaction terms included (Maeng) 
!>
function signalValue0(nEqns,nDim,iCell,iNodeEdge,dt,lam,qN,updated,iter,cIn)
   
    use solverVars, only: eps
    use reconstruction, only: reconValue, gradVal
    use meshUtil, only: cellFaces, faceCells, cellNodes, ref2cart
    use physics, only: gam

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations in system
                           nDim,    & !< number of dimensions
                           iCell,   & !< cell index
                           iNodeEdge  !< local node index (negative for edge)

    real(FP), intent(in) :: dt,                 & !< timestep for signal
                            lam(nDim),          & !< local advection speed
                            qN(nEqns)             !< state at time n

    logical, intent(inout) :: updated !< flag that update should be applied

    integer, intent(in), optional :: iter

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    ! Function variables
    real(FP) :: signalValue0(nEqns)

    ! Local variables
    integer :: iEq,     & !< equation index
               onEdge     !< edge containing char. origin

    integer :: gFace,   & !< global face index
               gNode,   &
               pEdge,   & !< periodic edge
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: tol,                & !< tolerance
                xi0(nDim),          & !< char origin in ref space
                qXi0(nEqns),        & !< state at char. origin
                cVel(nDim),         & !< corrected wave speed
                gradU(nDim,nDim),   & !< velocity gradient
                divSignal,          & !< divergence effect on density signal 
                divU,               & !< velocity divergence
                qCorr(nEqns),       & !< nonlinear corrections for Euler
                gradP(nDim)           !< velocity gradient

    real(FP) :: xiL(nDim),  &
                xiR(nDim),  &  
                xCart(nDim)    

    logical :: inCell = .false.     !< flag that signal originates in cell

    tol = eps
    signalValue0 = 0.0_FP
    qXi0 = 0.0_FP

    ! corrected advection velocity
    select case ( eqFlag ) 
      case ( EULER_EQ )
        ! nonlinear advection for Euler system
        ! notice cIn
        cVel = corrWaveSpeedEuler0(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)
    end select

    call charOrigin(nDim,iCell,iNodeEdge,dt,cVel,xi0,inCell,onEdge)

    lCell = iCell
    rCell = iCell
    xiL = xi0
    xiR = xi0
    ! only modify values if signal for wave originates in cell
    if ( ( inCell ) .and. ( .not. updated ) ) then  

        select case ( eqFlag )
        
          case ( EULER_EQ )

            do iEq = 1,nEqns
                ! interpolation at the characteristic origin
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:),cIn(iEq,:))

                ! advection operator
                signalValue0(iEq) = qXi0(iEq) - qN(iEq)

                if ( abs(signalValue0(iEq)) <= tol ) signalValue0(iEq) = 0.0_FP
            end do

          case default
            write(*,*) 'ERROR: undefined equation type'
            stop
            
        end select

        ! characteristic origin
        xCart = ref2cart(nDim,iCell,xi0)
        if ( iNodeEdge > 0 ) then
            nodeChOrg(:,cellNodes(iNodeEdge,iCell)) = xCart(:)
        else
            edgeChOrg(:,cellFaces(abs(iNodeEdge),iCell)) = xCart(:)
        end if

        ! mark node/edge as updated
        updated = .true.
    end if
    
end function signalValue0
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the signal sent from a cell at a specified time
!>
!> @history
!>  26 June 2012 - Initial creation (Eymann)
!>  14 May 2013 - Use local advection speed (Eymann)
!>  1 February 2015 - Pressure-less Euler system. Velocity divergence
!>      correction for density (Maeng)
!>  5 October 2015 - Incorporated a mandatory update flag, and onEdge flag
!>      no longer used. 
!>  9 October 2015 - Euler equations in 2d (Maeng)
!>  11 November 2015 - Remove velocity divergence effect from pressureless Euler
!>      for the implementation of Euler equations (Maeng)
!>  16 January 2017 - Euler nonlinear interaction terms included (Maeng) 
!>
function signalValue1(nEqns,nDim,iCell,iNodeEdge,dt,lam,qN,updated,iter,cIn)
   
    use solverVars, only: eps
    use reconstruction, only: reconValue, gradVal
    use meshUtil, only: cellFaces, faceCells, cellNodes, ref2cart
    use physics, only: gam

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns,   & !< number of equations in system
                           nDim,    & !< number of dimensions
                           iCell,   & !< cell index
                           iNodeEdge  !< local node index (negative for edge)

    real(FP), intent(in) :: dt,                 & !< timestep for signal
                            lam(nDim),          & !< local advection speed
                            qN(nEqns)             !< state at time n

    logical, intent(inout) :: updated !< flag that update should be applied

    integer, intent(in), optional :: iter

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    ! Function variables
    real(FP) :: signalValue1(nEqns)

    ! Local variables
    integer :: iEq,     & !< equation index
               onEdge     !< edge containing char. origin

    integer :: gFace,   & !< global face index
               gNode,   &
               pEdge,   & !< periodic edge
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: tol,                & !< tolerance
                xi0(nDim),          & !< char origin in ref space
                qXi0(nEqns),        & !< state at char. origin
                cVel(nDim),         & !< corrected wave speed
                gradU(nDim,nDim),   & !< velocity gradient
                divSignal,          & !< divergence effect on density signal 
                divU,               & !< velocity divergence
                qCorr(nEqns),       & !< nonlinear corrections for Euler
                gradP(nDim)           !< velocity gradient

    real(FP) :: xiL(nDim),  &
                xiR(nDim),  &  
                xCart(nDim)    

    logical :: inCell = .false.     !< flag that signal originates in cell

    tol = eps
    signalValue1 = 0.0_FP
    qXi0 = 0.0_FP

    ! corrected advection velocity
    select case ( eqFlag ) 
      case ( EULER_EQ )
        ! nonlinear advection for Euler system
        ! notice cIn
        cVel = corrWaveSpeedEuler1(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)
    end select

    call charOrigin(nDim,iCell,iNodeEdge,dt,cVel,xi0,inCell,onEdge)

    ! only modify values if signal for wave originates in cell
    if ( ( inCell ) .and. ( .not. updated ) ) then  

        select case ( eqFlag )
        
          case ( EULER_EQ )

            do iEq = 1,nEqns
                ! interpolation at the characteristic origin
                qXi0(iEq) = reconValue(nDim,iEq,iCell,xi0(:),cIn(iEq,:))

                ! advection operator
                signalValue1(iEq) = qXi0(iEq) - qN(iEq)

                if ( abs(signalValue1(iEq)) <= tol ) signalValue1(iEq) = 0.0_FP
            end do

          case default
            write(*,*) 'Error: undefined equation type'
            stop

        end select

        ! mark node/edge as updated
        updated = .true.
    end if
    
end function signalValue1
!-------------------------------------------------------------------------------
!> @purpose
!>  Calculate the origin of characteristic intersecting a space-time face
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  8 May 2012 - Initial creation\n
!>  23 May 2012 - Add Burgers equation\n
!>  3 October 2012 - Add flag for origin lying on interface
!>  4 November 2014 - Add tolerance on origins (Maeng)
!>
subroutine charOrigin(nDim,iCell,iNodeEdge,dt,lam,xi0,inCell,onEdge)

    use solverVars, only: eps
    use meshUtil, only: cellInvJac, xiNode, xiEdge
    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,             & !< time at interface
                            lam(nDim)         !< average velocity in cell

    real(FP), intent(out) :: xi0(nDim) !< characterisitic origin

    logical, intent(out) :: inCell !< flag that cell contains characteristic

    integer, intent(out) :: onEdge !< local index of edge for char. origin

    !< Local variables
    real(FP) :: xi(nDim),     & !< reference coordinate of interface
                xiTemp(nDim), &
                tol

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    ! find originTemp
    xiTemp(:) = -1.0_FP*dt*matmul(cellInvJac(:,:,iCell),lam(:))  
    xi0(:) = xi(:) + xiTemp(:) 
    
    ! determine if char. origin is inside cell
    inCell = .false.
    onEdge = 0
    select case ( nDim )

        case (1)
            ! zero tolerance check
            if ( abs(xi0(1)) <= tol ) xi0(1) = 0.0_FP

            if ( (( xi0(1) > 0.0_FP ) .and. ( xi0(1) < 1.0_FP )) ) then
                inCell = .true.
            elseif ( xi0(1) == 1.0_FP ) then
                inCell = .true.
                onEdge = 2
            elseif ( xi0(1) == 0.0_FP ) then
                inCell = .true.
                onEdge = 1
            end if

        case (2)
            !! zero tolerance check
            !if ( abs(xi0(1)) <= tol ) xi0(1) = 0.0_FP
            !if ( abs(xi0(2)) <= tol ) xi0(2) = 0.0_FP

            ! Cartesian coordinate
            if ( (abs(xi0(1)) <= tol) .and. &
                 ((xi0(2) >= 0.0_FP) .and. (xi0(2) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 2
            elseif ( (abs(xi0(2)) <= tol) .and. &
                     ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 3
            elseif ( (abs(sum(xi0(:))-1.0_FP) <= tol ) .and. &
                     ((xi0(1) >= 0.0_FP) .and. (xi0(1) <= 1.0_FP)) ) then
                inCell = .true.
                onEdge = 1
            elseif ( ((xi0(1) > 0.0_FP) .and. (xi0(1) < 1.0_FP)) .and. &
                 ((xi0(2) > 0.0_FP ) .and. (sum(xi0(:)) < 1.0_FP)) ) then
                inCell = .true.
                onEdge = 0
            end if

    end select

end subroutine charOrigin
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 January 2015 - Initial creation
!>  24 February 2015 - Included second order correction term
!>  16 April 2015 - Exact correction terms added for comparison
!>
function corrWaveSpeed(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: cellInvJac, xiNode, xiEdge, ref2cart
    use reconstruction, only: gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations 
                           nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< function coefficients

    !< Function variable
    real(FP) :: corrWaveSpeed(nDim)  

    !< Local variables
    integer :: iDir       !< dimension index

    real(FP) :: tol,                    & !< tolerance
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                corrMat(nDim,nDim),     & !< correction matrix
                corrMattemp(nDim,nDim), & !< correction matrix, temp
                detMat                    !< determinant of matrix

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    select case ( nDim )

        case ( 1 )

            ! evaluate gradient 
            gradU(1,:) = gradVal( nDim, 2, iCell, xi, cIn(2,:) )

            ! Approximation of geometric series 
            corrWaveSpeed(:) = lam(:) - lam(:)*gradU(1,:)*dt  

            ! Exact correction
            !corrMat(1,:) = 1.0_FP + 1.0_FP*dt*gradU(1,:) 
            !corrWaveSpeed(:) = lam(:)/corrMat(1,:)  

        case ( 2 ) 

            ! evaluate velocity gradient at the org. 
            gradU(1,:) = gradVal( nDim, 2, iCell, xi, cIn(2,:) ) 
            gradU(2,:) = gradVal( nDim, 3, iCell, xi, cIn(3,:) ) 

            ! Matrix multiplication
            corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt  
            corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt  

            !! Exact correction by inverting matrix
            !corrMat(:,:) = 1.0_FP*dt*gradU(:,:)
            !! add Identity matrix
            !corrMat(1,1) = 1.0_FP + corrMat(1,1) 
            !corrMat(2,2) = 1.0_FP + corrMat(2,2) 
            !! assign the matrix to temporary variable
            !corrMatTemp(:,:) = corrMat(:,:)
            !! invert the matrix
            !corrMat(:,:) = 0.0_FP
            !detMat = (corrMatTemp(1,1)*corrMatTemp(2,2) - &
            !          corrMatTemp(1,2)*corrMatTemp(2,1))
            !if ( abs(detMat) <= tol ) then
            !    ! in case corrMat is sigular
            !    !corrWaveSpeed(:) = lam(:)
            !    corrWaveSpeed(1) = lam(1) - (lam(1)*gradU(1,1) + &
            !        lam(2)*gradU(1,2))*dt  
            !    corrWaveSpeed(2) = lam(2) - (lam(1)*gradU(2,1) + &
            !        lam(2)*gradU(2,2))*dt  
            !else
            !    ! invert corrMat
            !    corrMat(1,1) = (1.0_FP/detMat)*corrMatTemp(2,2) 
            !    corrMat(1,2) = -(1.0_FP/detMat)*corrMatTemp(1,2) 
            !    corrMat(2,1) = -(1.0_FP/detMat)*corrMatTemp(2,1) 
            !    corrMat(2,2) = (1.0_FP/detMat)*corrMatTemp(1,1) 
            !    ! corrected waveSpeed
            !    corrWaveSpeed(:) = matmul(corrMat(:,:),lam(:))  
            !end if

    end select

end function corrWaveSpeed
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity with pressure 
!>  gradient correction
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 August 2016 - Initial creation
!>
function corrWaveSpeedEuler(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: xiNode, xiEdge, ref2cart
    use reconstruction, only: reconValue, gradVal
    use physics, only: gam, isentropicConst 

    implicit none

    !< Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations 
                           nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional ::  &
                    cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    !< Function variable
    real(FP) :: corrWaveSpeedEuler(nDim)  

    !< Local variables
    real(FP) :: tol,                    & !< tolerance
                rho,                    & !< density
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                gradP(nDim)               !< gradient of pressure

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    gradU(1,:) = gradVal(nDim,2,iCell,xi,cIn(2,:))
    gradU(2,:) = gradVal(nDim,3,iCell,xi,cIn(3,:))

    rho = 0.0_FP
    rho = reconValue(nDim,1,iCell,xi,cIn(1,:))
    
    gradP(:) = 0.0_FP 
    select case ( eqFlag )
        
        case ( EULER_EQ )
            gradP(:) = gradVal(nDim,4,iCell,xi,cIn(4,:)) 

        case ( ISENTEULER_EQ )
            gradP(:) = gradVal(nDim,1,iCell,xi,cIn(1,:)) 

            ! pressure gradient is replaced by density gradient in isentropic Euler
            gradP(1) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(1)
            gradP(2) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(2)

    end select

    !! Approximation of matrix multiplication
    !    ! for Advection --> Acoustic
    !    corrWaveSpeedEuler(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt - &
    !        0.5_FP*(gradP(1)/rho)*dt
    !    corrWaveSpeedEuler(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt - &
    !        0.5_FP*(gradP(2)/rho)*dt
        ! for Acoustic --> Advection
        ! notice the sign in front of pressure gradients
        corrWaveSpeedEuler(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt + &
            0.5_FP*(gradP(1)/rho)*dt
        corrWaveSpeedEuler(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt + &
            0.5_FP*(gradP(2)/rho)*dt

end function corrWaveSpeedEuler
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity with pressure 
!>  gradient correction
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 August 2016 - Initial creation
!>
function corrWaveSpeedEuler0(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: xiNode, xiEdge, ref2cart
    use reconstruction, only: reconValue, gradVal
    use physics, only: gam, isentropicConst 

    implicit none

    !< Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations 
                           nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional ::  &
                    cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    !< Function variable
    real(FP) :: corrWaveSpeedEuler0(nDim)  

    !< Local variables
    real(FP) :: tol,                    & !< tolerance
                rho,                    & !< density
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                gradP(nDim)               !< gradient of pressure

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    gradU(1,:) = gradVal(nDim,2,iCell,xi,cIn(2,:))
    gradU(2,:) = gradVal(nDim,3,iCell,xi,cIn(3,:))

    rho = 0.0_FP
    rho = reconValue(nDim,1,iCell,xi,cIn(1,:))
    
    gradP(:) = 0.0_FP 
    select case ( eqFlag )
        
        case ( EULER_EQ )
            gradP(:) = gradVal(nDim,4,iCell,xi,cIn(4,:)) 

        case ( ISENTEULER_EQ )
            gradP(:) = gradVal(nDim,1,iCell,xi,cIn(1,:)) 

            ! pressure gradient is replaced by density gradient in isentropic Euler
            gradP(1) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(1)
            gradP(2) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(2)

    end select

    ! Approximation of matrix multiplication
    ! for Acoustic --> Advection
    ! notice the sign in front of pressure gradients
    corrWaveSpeedEuler0(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt + &
        0.5_FP*(gradP(1)/rho)*dt
    corrWaveSpeedEuler0(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt + &
        0.5_FP*(gradP(2)/rho)*dt

end function corrWaveSpeedEuler0
!-------------------------------------------------------------------------------
!> @purpose
!>  First order correction for nonlinear advection velocity with pressure 
!>  gradient correction
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  5 August 2016 - Initial creation
!>
function corrWaveSpeedEuler1(nEqns,nDim,iCell,iNodeEdge,dt,lam,cIn)

    use solverVars, only: eps, tSim
    use meshUtil, only: xiNode, xiEdge, ref2cart
    use reconstruction, only: reconValue, gradVal
    use physics, only: gam, isentropicConst 

    implicit none

    !< Interface variables
    integer, intent(in) :: nEqns,       & !< number of equations 
                           nDim,        & !< problem dimension
                           iCell,       & !< cell index
                           iNodeEdge      !< origin node

    real(FP), intent(in) :: dt,         & !< time step
                            lam(nDim)     !< waveSpeed

    real(FP), intent(in), optional ::  &
                    cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) 

    !< Function variable
    real(FP) :: corrWaveSpeedEuler1(nDim)  

    !< Local variables
    real(FP) :: tol,                    & !< tolerance
                rho,                    & !< density
                xi(nDim),               & !< reference coordinate of interface
                gradU(nDim,nDim),       & !< gradient of vector u
                gradP(nDim)               !< gradient of pressure

    tol = eps

    if ( iNodeEdge > 0 ) then
        xi(:) = xiNode(:,iNodeEdge)
    else
        xi(:) = xiEdge(:,abs(iNodeEdge))
    end if

    gradU(:,:) = 0.0_FP 
    gradU(1,:) = gradVal(nDim,2,iCell,xi,cIn(2,:))
    gradU(2,:) = gradVal(nDim,3,iCell,xi,cIn(3,:))

    rho = 0.0_FP
    rho = reconValue(nDim,1,iCell,xi,cIn(1,:))
    
    gradP(:) = 0.0_FP 
    select case ( eqFlag )
        
        case ( EULER_EQ )
            gradP(:) = gradVal(nDim,4,iCell,xi,cIn(4,:)) 

        case ( ISENTEULER_EQ )
            gradP(:) = gradVal(nDim,1,iCell,xi,cIn(1,:)) 

            ! pressure gradient is replaced by density gradient in isentropic Euler
            gradP(1) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(1)
            gradP(2) = isentropicConst*gam*rho**(gam-1.0_FP)*gradP(2)

    end select

    ! Approximation of matrix multiplication
    ! for Advection --> Acoustic
    corrWaveSpeedEuler1(1) = lam(1) - (lam(1)*gradU(1,1)+lam(2)*gradU(1,2))*dt - &
        0.5_FP*(gradP(1)/rho)*dt
    corrWaveSpeedEuler1(2) = lam(2) - (lam(1)*gradU(2,1)+lam(2)*gradU(2,2))*dt - &
        0.5_FP*(gradP(2)/rho)*dt

end function corrWaveSpeedEuler1
!-------------------------------------------------------------------------------
!> @purpose 
!>  Velocity divergence accounting for the first order term
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  24 September 2015 - Initial creation (Maeng)
!>
function velocityDiv( nDim, iCell, xi, dt )

    use reconstruction, only: reconValue, gradVal

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           iCell          !< cell index

    real(FP), intent(in) :: xi(nDim),   & !< evaluation location 
                            dt            !< time

    !< Function variable
    real(FP) :: velocityDiv

    !< Local variables
    real(FP) :: gradU(nDim,nDim) !< gradient of velocity vector

    velocityDiv = 0.0_FP
    select case ( nDim ) 
        
        case (1)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! ux

            ! velocity divergence
            velocityDiv = gradU(1,1) 

        case (2)
            ! gradient of velocity vector
            gradU(1,:) = gradVal(nDim,2,iCell,xi) ! grad u
            gradU(2,:) = gradVal(nDim,3,iCell,xi) ! grad v 

            ! without second order gradients
            ! divergence of velocity
            ! velocity divergence evaluated at char. org contains second order derivatives
            ! no need to evaluate it again
            velocityDiv = (gradU(1,1)+gradU(2,2))

    end select

end function velocityDiv
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use fluxes to update cell values
!>
!> @history
!>  17 May 2013 - Initial creation (Eymann)
!>  10 April 2015 - Updated for pressureless Euler (Maeng) 
!>
subroutine updateCells( nDim,nEqns,nCells,fluxType,qCellN,qCell,lam )
    
    use solverVars, only: tSim, iRight, iLeft
    use meshUtil, only: faceCells, faceNormal, faceArea, cellVolume, &
                        cellFaces, faceNodes, ref2cart, totalVolume, &
                        faceNormalArea, nodeCoord
    
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,     & !< problem dimension
                           nEqns,    & !< number of equations
                           nCells,   & !< number of cells
                           fluxType    !< flux used for update

    real(FP), intent(in) :: qCellN(nEqns,nCells)       !< state at time n

    real(FP), intent(inout) :: qCell(nEqns,nCells)     !< state at time n+1

    real(FP), intent(in), optional :: lam(nDim)

    ! Local variables
    integer :: iEq,     & !< equation index
               iEdge,   & !< edge index
               iNode,   &
               gEdge,   &
               lNode,   & !< node index
               rNode,   & !< node index
               iCell,   & !< cell index
               lCell,   & !< left cell index
               rCell      !< right cell index

    real(FP) :: faceFlux(nDim,nEqns),   & !< flux at edge
                resid(nEqns,nCells),    & !< cell residual
                residNormSum(nEqns),    & !< residual norm storage
                dR(nEqns)                 !< change in residual value

    real(FP) :: fNorm(nDim),  &
                fArea
                
    !write(*,*) 'conservation stage'
    qCell(:,:) = qCellN(:,:)
    resid(:,:) = 0.0_FP
    do iEdge = 1, nEdges
        ! left and right cell index
        lCell = faceCells(iLeft,iEdge)
        rCell = faceCells(iRight,iEdge)

        ! evaluate average flux over a face
        faceFlux(:,:) = averageFlux( nDim,nEqns,fluxType,iEdge,lam )

        ! numerical face flux
        do iEq = 1,nEqns
            dR(iEq) = dot_product(faceFlux(:,iEq),faceNormal(:,iEdge)) * &
                      faceArea(iEdge)
        end do

        ! TODO: cell update counter?
        resid(:,lCell) = resid(:,lCell) - dR(:)/cellVolume(lCell)
        qCell(:,lCell) = qCell(:,lCell) - dtIn*dR(:)/cellVolume(lCell)
        if ( rCell > 0 ) then
            resid(:,rCell) = resid(:,rCell) + dR(:)/cellVolume(rCell)
            qCell(:,rCell) = qCell(:,rCell) + dtIn*dR(:)/cellVolume(rCell)
        end if
        
    end do

    ! Residual calculation 
    !residNormSum(:) = 0.0_FP
    !residMax(:) = 0.0_FP
    !! TODO: select a range from which the residual is taken
    !do iEq = 1, nEqns
    !    do iCell = 1, nCells
    !        residNormSum(iEq) = residNormSum(iEq) + &
    !            cellVolume(iCell)*abs(resid(iEq,iCell))**(1.0_FP)
    !        residMax(iEq) = max(residMax(iEq),abs(resid(iEq,iCell)))
    !    end do
    !end do
    !do iEq = 1, nEqns
    !    residNorm(iEq) = (residNormSum(iEq)/totalVolume)**(1.0_FP/1.0_FP)   
    !end do

end subroutine updateCells
!-------------------------------------------------------------------------------
!> @purpose
!>  Use node and interface values to calculate an average flux
!>
!> @history
!>  14 May 2013 - Initial creation (Eymann)
!>  10 January 2014 - l/rNode correction for 1D (Maeng)
!>  11 March 2015 - 1D flux calculation (Maeng)
!>
function averageFlux(nDim,nEqns,fluxType,iEdge,lam)

    use meshUtil, only: faceNodes, faceCells
    use physics, only: flux
    use mathutil, only: simpson2d
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           nEqns,       & !< equation for flux
                           fluxType,    & !< flux equation
                           iEdge          !< interface

    real(FP), intent(in), optional :: lam(nDim) !< advection speed

    ! Function variable
    real(FP) :: averageFlux(nDim,nEqns)

    ! Local variables
    integer :: lNode,   & !< left face node
               rNode,   & !< right face node
               iCell,   &
               lCell,   &
               iDir,    & !< direction index
               iEq,     & !< equation index
               i,       & !< row index
               j          !< column index

    real(FP) :: fluxQuad(3,3,nDim,nEqns),   & !< analytical fluxes at quadrature points
                fluxSlice(3,3)                !< flux values for given dimen and eqn

    select case ( nDim )

        case (1)
            lNode = faceNodes(1,iEdge)
            rNode = faceNodes(1,iEdge)

        case (2)
            ! assuming ccw ordering of node
            lNode = faceNodes(2,iEdge)
            rNode = faceNodes(1,iEdge)

    end select

    ! The 'flux' function expects primitive state vectors !
    if ( present(lam) ) then
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode),lam )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge),lam )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode),lam )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode),lam )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge),lam )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode),lam )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode),lam )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge),lam )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode),lam )
    else
        fluxQuad(1,1,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,lNode) )
        fluxQuad(1,2,:,:) = flux( nDim,nEqns,fluxType,edgeData(:,iEdge) )
        fluxQuad(1,3,:,:) = flux( nDim,nEqns,fluxType,nodeData(:,rNode) )
        fluxQuad(2,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,lNode) )
        fluxQuad(2,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataH(:,iEdge) )
        fluxQuad(2,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataH(:,rNode) )
        fluxQuad(3,1,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,lNode) )
        fluxQuad(3,2,:,:) = flux( nDim,nEqns,fluxType,edgeDataN(:,iEdge) )
        fluxQuad(3,3,:,:) = flux( nDim,nEqns,fluxType,nodeDataN(:,rNode) )
    end if

    ! TODO: optimize loop for speed
    averageFlux(:,:) = 0.0_FP
    do iDir = 1,nDim
        do iEq = 1,nEqns
            fluxSlice(:,:) = 0.0_FP
            do i = 1,3
                do j = 1,3
                    fluxSlice(i,j) = fluxQuad(i,j,iDir,iEq)
                end do 
            end do
            averageFlux(iDir,iEq) = simpson2d( fluxSlice(:,:) )
        end do
    end do

end function averageFlux
!-------------------------------------------------------------------------------
!> @purpose 
!>  Loop around the domain and evaluate conservation
!>
!> @history
!>  25 November 2014 - Initial creation (Maeng)
!>  30 October 2015 - simplified
!>
subroutine conservationCheck()

    use meshUtil, only: faceCells, cellVolume, totalVolume, cellFaces
    use physics, only: prim2conserv

    implicit none

    ! Local variables
    integer :: iCell,   & !< cell index
               iEdge,   & !< edge index
               iNode,   & !< node index
               iEq        !< equation index
    
    real(FP) :: q(nEqns),       & 
                primQ(nEqns),   & !< primitive variable sum
                consQ(nEqns)      !< conserved variable sum

    q(:) = 1e6
    primQ(:) = 0.0_FP
    consQ(:) = 0.0_FP
    do iEq = 1,nEqns
        do iCell = 1,nCells
            primQ(iEq) = primQ(iEq) + (primAvg(iEq,iCell))*cellVolume(iCell) 
            consQ(iEq) = consQ(iEq) + (cellAvg(iEq,iCell))*cellVolume(iCell) 
        end do
        primNorm(iEq) = (primQ(iEq)/totalVolume)**(1.0_FP)
        conservNorm(iEq) = (consQ(iEq)/totalVolume)**(1.0_FP)

        !do iNode = 1,nNodes
        !    q(iEq) = min(q(iEq), nodeData(iEq,iNode) )
        !end do
        !do iEdge = 1,nEdges
        !    q(iEq) = min(q(iEq), edgeData(iEq,iEdge) )
        !end do
        !primMin(iEq) = q(iEq)
    end do

end subroutine conservationCheck
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate primitive cell average from conservative variables 
!>  for coupled systems. The primitive cell average are used for 
!>  constructing cell reconstruction coefficients.
!>  
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 March 2015 - Initial Creation
!>  21 September 2015 - Implementation of a new conservative 
!>      variable conversion for 1D pressureless euler
!>  23 September 2015 - Implementation of a new conservative
!>      variable conversion for 2D pressureless euler
!>  9 October 2015 - 2d euler equations, polytropic EOS for p
!>
subroutine conserv2primAvg(nDim,nEqns,nNodes,nEdges,nCells, &
                                    qNode,qEdge,qCons,qPrim)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces
    use physics, only: isentropicConst, gam

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nNodes,  & !< number of nodes
                           nEdges,  & !< number of edges
                           nCells,  & !< number of cells
                           nEqns      !< number of equations

    real(FP), intent(in) :: qNode(nEqns,nNodes),  & !< nodeData 
                            qEdge(nEqns,nEdges),  & !< edgeData
                            qCons(nEqns,nCells)     !< conservative state variable

    real(FP), intent(out) :: qPrim(nEqns,nCells)  !< primitive state variable

    !< Local variables
    integer :: iEq,     & !< equation index
               node1,   &
               node2,   &
               node3,   &
               iCell      !< cell index

    real(FP) :: qAvg(nEqns),  & !< local cell average
                rDiff(nDim),  & !< density derivatives
                uDiff(nDim),  & !< u derivatives
                vDiff(nDim),  & !< v derivatives
                sCorrU,       & !< second order vel. correction for conservation 
                sCorrV,       & !< second order vel. correction for conservation 
                sCorrE          !< second order energy correction for conservation 

    real(FP) :: primavgs(nEqns), dU(nEQns)

    qPrim(:,:) = 0.0_FP
    select case ( nDim )

        case (1)
            ! new method
            do iCell = 1,nCells
                node1 = cellNodes(1,iCell)
                node2 = cellNodes(2,iCell)
                sCorrU = 1.0_FP/12.0_FP*(qNode(1,node2)-qNode(1,node1))*&
                                        (qNode(2,node2)-qNode(2,node1)) 
                qAvg(1) = qCons(1,iCell) 
                qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU)
                qPrim(:,iCell) = qAvg(:) 
            end do

        case (2)

            if ( (eqFlag == ISENTEULER_EQ) .or. (eqFlag == PLESSEULER_EQ) ) then
                ! pressureless euler/isentropic euler equations; rho, u, v
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    rDiff(1) = qNode(1,node2)-qNode(1,node1) ! drho/dxi
                    rDiff(2) = qNode(1,node3)-qNode(1,node1) ! drho/deta
                    uDiff(1) = qNode(2,node2)-qNode(2,node1)
                    uDiff(2) = qNode(2,node3)-qNode(2,node1)
                    vDiff(1) = qNode(3,node2)-qNode(3,node1)
                    vDiff(2) = qNode(3,node3)-qNode(3,node1)
                    ! correction for velocity
                    sCorrU = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*uDiff(1)-uDiff(2)) + &
                        rDiff(2)*(2.0_FP*uDiff(2)-uDiff(1)) )
                    sCorrV = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*vDiff(1)-vDiff(2)) + &
                        rDiff(2)*(2.0_FP*vDiff(2)-vDiff(1)) )
                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU) ! u average
                    qAvg(3) = 1.0_FP/qCons(1,iCell)*(qCons(3,iCell)-sCorrV) ! v average
                    qPrim(1:3,iCell) = qAvg(1:3)
                end do
            else if ( (eqFlag == EULER_EQ) ) then
                ! full Euler equations
                do iCell = 1,nCells
                    node1 = cellNodes(1,iCell)
                    node2 = cellNodes(2,iCell)
                    node3 = cellNodes(3,iCell)
                    rDiff(1) = qNode(1,node2)-qNode(1,node1) ! drho/dxi
                    rDiff(2) = qNode(1,node3)-qNode(1,node1) ! drho/deta
                    uDiff(1) = qNode(2,node2)-qNode(2,node1)
                    uDiff(2) = qNode(2,node3)-qNode(2,node1)
                    vDiff(1) = qNode(3,node2)-qNode(3,node1)
                    vDiff(2) = qNode(3,node3)-qNode(3,node1)

                    ! correction for velocity
                    sCorrU = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*uDiff(1)-uDiff(2)) + &
                        rDiff(2)*(2.0_FP*uDiff(2)-uDiff(1)) )
                    sCorrV = 1.0_FP/36.0_FP*( &
                        rDiff(1)*(2.0_FP*vDiff(1)-vDiff(2)) + &
                        rDiff(2)*(2.0_FP*vDiff(2)-vDiff(1)) )
                    sCorrE = 1.0_FP/36.0_FP*( &
                        qNode(1,node1)*((uDiff(1)-uDiff(2))**2.0_FP + uDiff(1)*uDiff(2) + &
                                        (vDiff(1)-vDiff(2))**2.0_FP + vDiff(1)*vDiff(2)) )

                    qAvg(1) = qCons(1,iCell) ! rho average
                    qAvg(2) = 1.0_FP/qCons(1,iCell)*(qCons(2,iCell)-sCorrU) ! u average
                    qAvg(3) = 1.0_FP/qCons(1,iCell)*(qCons(3,iCell)-sCorrV) ! v average
                    qAvg(4) = (gam-1.0_FP)*(qCons(4,iCell) - &
                               0.5_FP*(qCons(2,iCell)*qCons(2,iCell) + &
                               qCons(3,iCell)*qCons(3,iCell))/qCons(1,iCell)-sCorrE) ! pressure average 
                    qPrim(1:4,iCell) = qAvg(1:4) 

                    !! 2/23/2017: alternative way - test
                    !! change in conservative vars
                    !dU(1) = qCons(1,iCell) - 1.0_FP/3.0_FP*( &
                    !    qEdge(1,cellFaces(1,iCell))+qEdge(1,cellFaces(2,iCell))+qEdge(1,cellFaces(3,iCell))) 
                    !dU(2) = qCons(2,iCell) - 1.0_FP/3.0_FP*( &
                    !    qEdge(1,cellFaces(1,iCell))*qEdge(2,cellFaces(1,iCell)) +&
                    !    qEdge(1,cellFaces(2,iCell))*qEdge(2,cellFaces(2,iCell)) + &
                    !    qEdge(1,cellFaces(3,iCell))*qEdge(2,cellFaces(3,iCell)) ) 
                    !dU(3) = qCons(3,iCell) - 1.0_FP/3.0_FP*( &
                    !    qEdge(1,cellFaces(1,iCell))*qEdge(3,cellFaces(1,iCell)) +&
                    !    qEdge(1,cellFaces(2,iCell))*qEdge(3,cellFaces(2,iCell)) + &
                    !    qEdge(1,cellFaces(3,iCell))*qEdge(3,cellFaces(3,iCell)) ) 
                    !dU(4) = qCons(4,iCell) - 1.0_FP/3.0_FP*( &
                    !    (qEdge(4,cellFaces(1,iCell))/(gam-1.0) + 0.5_FP*&
                    !     qEdge(1,cellFaces(1,iCell))*(qEdge(2,cellFaces(1,iCell))**2.0_FP+&
                    !                                  qEdge(3,cellFaces(1,iCell))**2.0_FP)) + &
                    !    (qEdge(4,cellFaces(2,iCell))/(gam-1.0) + 0.5_FP*&
                    !     qEdge(1,cellFaces(2,iCell))*(qEdge(2,cellFaces(2,iCell))**2.0_FP+ &
                    !                                  qEdge(3,cellFaces(2,iCell))**2.0_FP)) + &
                    !    (qEdge(4,cellFaces(3,iCell))/(gam-1.0) + 0.5_FP*&
                    !     qEdge(1,cellFaces(3,iCell))*(qEdge(2,cellFaces(3,iCell))**2.0_FP+ &
                    !                                  qEdge(3,cellFaces(3,iCell))**2.0_FP)) )
                    !
                    !! primitive numerical average
                    !primavgs(1) = 1.0_FP/3.0_FP*(qEdge(1,cellFaces(1,iCell))+&
                    !                    qEdge(1,cellFaces(2,iCell))+qEdge(1,cellFaces(3,iCell))) 
                    !primavgs(2) = 1.0_FP/3.0_FP*(qEdge(2,cellFaces(1,iCell))+&
                    !                    qEdge(2,cellFaces(2,iCell))+qEdge(2,cellFaces(3,iCell))) 
                    !primavgs(3) = 1.0_FP/3.0_FP*(qEdge(3,cellFaces(1,iCell))+&
                    !                    qEdge(3,cellFaces(2,iCell))+qEdge(3,cellFaces(3,iCell))) 
                    !primavgs(4) = 1.0_FP/3.0_FP*(qEdge(4,cellFaces(1,iCell))+&
                    !                    qEdge(4,cellFaces(2,iCell))+qEdge(4,cellFaces(3,iCell))) 

                    !! bubble functions in velocity and pressure
                    !sCorrU = (dU(2)/primavgs(1) - primavgs(2)/primavgs(1)*dU(1) ) 
                    !sCorrV = (dU(3)/primavgs(1) - primavgs(3)/primavgs(1)*dU(1) ) 
                    !sCorrE = ( (gam-1.0_FP)* &
                    !    (0.5*(primavgs(2)*primavgs(2) + primavgs(3)*primavgs(3))*(dU(1)) - &
                    !          primavgs(2)*(dU(2)) - primavgs(3)*dU(3) + dU(4) ) )

                    !qAvg(1) = qCons(1,iCell) ! rho average
                    !qAvg(2) = primavgs(2) + sCorrU
                    !qAvg(3) = primavgs(3) + sCorrV
                    !qAvg(4) = primavgs(4) + sCorrE 
                    !qPrim(1:4,iCell) = qAvg(1:4) 
                end do
            else
                ! scalar conservation laws. Linear advection
                qPrim(:,:) = qCons(:,:)
            end if

    end select

end subroutine conserv2primAvg
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients for each equation for a 
!>  given cell.
!>  
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial Creation\n
!>  8 May 2012 - Extend to 1-D
!>  6 January 2016 - Modified to get rid of bubble function (Maeng)
!>
function coefficients(nDim,iEq,iCell)

    use solverVars, only: eps
    use meshUtil, only: cellNodes, cellFaces
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iEq,     & !< number of equations
                           iCell      !< cell index

    ! Function variable
    real(FP) :: coefficients((nDim+2)*(nDim+3)/(2*(3-nDim)))

    ! Local variables
    real(FP) :: uPoint(nDim*(nDim+1)),  & !< vector of edge and node values
                uAvg                      !< cell average

    coefficients = 0.0_FP

    uAvg = primAvgN(iEq,iCell)

    select case ( nDim )

        case (1)
           uPoint(1) = edgeDataN(iEq,cellFaces(1,iCell))
           uPoint(2) = edgeDataN(iEq,cellFaces(2,iCell))

        case (2)
           uPoint(1) = nodeDataN(iEq,cellNodes(1,iCell))
           uPoint(3) = nodeDataN(iEq,cellNodes(2,iCell))
           uPoint(5) = nodeDataN(iEq,cellNodes(3,iCell))

           uPoint(2) = edgeDataN(iEq,cellFaces(1,iCell))
           uPoint(4) = edgeDataN(iEq,cellFaces(2,iCell))
           uPoint(6) = edgeDataN(iEq,cellFaces(3,iCell))

    end select

    ! set reconstruction coefficients
    coefficients(:) = coeffFromVal(nDim,uAvg,uPoint)

end function coefficients
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the reconstruction coefficients \f$c_i$\f given point values and 
!>  conserved quantity\n
!>  \n
!>  1d Equation (in reference coordinates):\n
!>  \f{equation*}{ 
!>      u(\xi) = c_1 \left(1-2\xi\right)\left(1-\xi\right) + 
!>               c_2 4\xi\left(1-\xi\right) + 
!>               c_3 \xi\left(2\xi-1\right)
!>  \f}
!>  \n
!>  2d Equation (in reference coordinates):\n 
!>  See basis function definitions in 'reconValue' function
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  4 June 2012 - Initial Creation
!>  27 July 2013 - Add tolerance for bubble coefficient
!>
function coeffFromVal(nDim,uAvg,uPoint)

    use solverVars, only: eps
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim !< problem dimension

    real(FP), intent(in) :: uAvg,                 & !< primitive average
                            uPoint(nDim*(nDim+1))   !< point values

    ! Function variable
    real(FP) :: coeffFromVal((nDim+2)*(nDim+3)/(2*(3-nDim)))

    ! Local variables
    real(FP), parameter :: tol = 2.0e-10_FP !< tolerance for bubble coeff (FIXME)

    real(FP) :: uNumAvg


    coeffFromVal(:) = 0.0_FP
    select case ( nDim )

        case (1)
            coeffFromVal(1) = uPoint(1)
            coeffFromVal(2) = 0.25_FP*(6.0_FP*uAvg-uPoint(1)-uPoint(2))
            coeffFromVal(3) = uPoint(2)

        case (2)

            coeffFromVal(1) = uPoint(1)
            coeffFromVal(2) = uPoint(2)
            coeffFromVal(3) = uPoint(3)
            coeffFromVal(4) = uPoint(4)
            coeffFromVal(5) = uPoint(5)
            coeffFromVal(6) = uPoint(6)

            ! FIXME: What is a good value for bubble function tolerance
            uNumAvg = (1.0_FP/3.0_FP)*(uPoint(2)+uPoint(4)+uPoint(6)) ! 2nd order accurate average
            ! bubble function
            coeffFromVal(7) = (20.0_FP/9.0_FP)*(uAvg - uNumAvg)
            if ( abs(coeffFromVal(7)) <= 1.0_FP*eps ) coeffFromVal(7) = 0.0_FP
            coeffFromVal(8:10) = 0.0_FP

    end select

end function coeffFromVal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Test 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  21 April 2015 - Initial creation
!>
subroutine test()

    use solverVars, only: eps, nIter
    use meshUtil, only: nDim, cellNodes, cellFaces, ref2cart, &
                        nodeCoord, edgeCoord, &
                        faceNodes, faceCells, &
                        cellInvJac, cellJac, cellVolume, &
                        faceNormal, faceArea, &
                        cellCentroid, faceNormalArea
    use reconstruction, only: reconValue, gradVal, secondGradVal
    use boundaryConditions, only: nodeBC, edgeBC

    implicit none

    !> Local variables
    integer :: iEq,     & !< equation index 
               iCell,   & !< cell index
               iFace,   & !< face index
               iEdge,   & !< edge index
               iNode      !< node index

    real(FP) :: xi(nDim),           & 
                sumNormal(nDim),    &
                vel(nDim),          &
                gradient(nDim),     &
                gradU(nDim,nDim),   &
                sGradU(nDim,nDim),  &
                sGradV(nDim,nDim)

    write(*,*) "fortran "
    write(*,*) nCells
    write(*,*) nEqns
    write(*,*) shape(nodeDataN), size(nodeDataN)
    write(*,*) shape(nodeBC), size(nodeBC)
    write(*,*) nodeDataN(:,124)
    write(*,*) nodeBC(5)

    !!! check second order gradient evaluation routines !!!
    !iCell = 111
    !xi = (/1.0_FP, 0.0_FP/)
    !write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)

    !vel(1) = reconValue( nDim,2,iCell,xi )
    !vel(2) = reconValue( nDim,3,iCell,xi )
    !write(*,*) 'U'
    !write(*,*) vel

    !gradU(1,:) = gradVal( nDim, 2, iCell, xi ) 
    !!write(*,*) 'dU/dXi'
    !!gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(1,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(1,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(1,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !gradU(2,:) = gradVal( nDim, 3, iCell, xi ) 
    !!write(*,*) gradient
    !!gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !!gradU(2,1) = gradient(1)*cellInvJac(1,1,iCell) + gradient(2)*cellInvJac(2,1,iCell)! du/dx
    !!gradU(2,2) = gradient(1)*cellInvJac(1,2,iCell) + gradient(2)*cellInvJac(2,2,iCell)! du/dy
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !write(*,*) 
    !write(*,*) 'dU/dX'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) gradU(2,1), gradU(2,2)

    !sGradU = secondGradVal( nDim,2,iCell,xi )
    !write(*,*) 'd2U/dX2'
    !write(*,*) sGradU(1,1), sGradU(1,2)
    !write(*,*) sGradU(2,1), sGradU(2,2)

    !sGradV = secondGradVal( nDim,3,iCell,xi )
    !write(*,*) 'd2V/dX2'
    !write(*,*) sGradV(1,1), sGradV(1,2)
    !write(*,*) sGradV(2,1), sGradV(2,2)

    !! check gradient evaluation routines !!!
    !iCell = 10
    !xi = (/1.0_FP, 0.0_FP/)
    !write(*,*) 'x, y',  ref2cart(nDim,iCell,xi)
    !write(*,*) 'eps', eps
    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !write(*,*) 'dU/dXi'
    !write(*,*) gradient
    !gradU(1,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! du/dx, du/dy
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !write(*,*) gradient
    !gradU(2,:) = matmul(gradient,cellInvJac(:,:,iCell)) ! dv/dx, dv/dy
    !write(*,*) 
    !write(*,*) 'cellInvJac'
    !write(*,*) cellInvJac(1,1,iCell), cellInvJac(1,2,iCell)
    !write(*,*) cellInvJac(2,1,iCell), cellInvJac(2,2,iCell)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)

    !gradient = gradVal( nDim, 2, iCell, xi ) 
    !gradU(1,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(1,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !gradient = gradVal( nDim, 3, iCell, xi ) 
    !!gradU(2,:) = matmul(cellInvJac(:,:,iCell),gradient) ! dv/dx, dv/dy
    !gradU(2,1) = cellInvJac(1,1,iCell)*gradient(1) + &
    !    cellInvJac(2,1,iCell)*gradient(2)
    !gradU(2,2) = cellInvJac(1,2,iCell)*gradient(1) + &
    !    cellInvJac(2,2,iCell)*gradient(2)    
    !write(*,*)
    !write(*,*)
    !write(*,*) 'dU/dXi * cellInvJac by manual multiplication'
    !write(*,*) 'du/dx, du/dy'
    !write(*,*) gradU(1,1), gradU(1,2)
    !write(*,*) 'dv/dx, dv/dy'
    !write(*,*) gradU(2,1), gradU(2,2)
    !! check gradient evaluation routines !!!
    
end subroutine test
!-------------------------------------------------------------------------------
end module update
