!-------------------------------------------------------------------------------
!> @purpose 
!>  Module including stability analysis used for Active Flux methods
!>
!> @history
!>  15 April 2016 - Initial creation (Maeng)
!>  
module stabilityUtil

    use solverVars, only: DP, FP    

    implicit none

    ! Shared module variables 
    real(FP), allocatable ::nodeDataN(:,:)     !< node solution at timestep "n"

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Autocorrelation to determine a source of instability that is
!>  not random 
!>
!> @history
!>  15 April 2016 - Initial creation (Maeng)
!>
subroutine autoCorrelate()

    use update

    implicit none



end subroutine autoCorrelate
!-------------------------------------------------------------------------------
end module stabilityUtil
!-------------------------------------------------------------------------------
