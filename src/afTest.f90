!-------------------------------------------------------------------------------
!> @purpose 
!>  Master program for 2d active flux test program
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  3 May 2016 - Initial creation
!>
program afTest

    use solverVars, only: nEqns, nIter, FP, tSim, dtIn, tFinal
    use startStop, only: startUp, shutDown
    use meshUtil, only: nDim, nEdges, nNodes, nCells
    use inputOutput, only: initTecPlotFile, outputVizMesh, &
                           outputVizSolution, solOutputFreq, &
                           outputBinary, outputAux, outputVizChOrg, &
                           outputResidual, dumpDataInquiry, shutDownInquiry
    use timeControl, only: updateDt
    use update, only: eulerUpdate, test

    implicit none

    ! Local variables
    logical :: switch

    integer :: iter,    & !< iteration number
               iZone      !< zone number

    real(FP) :: t0, & !< starting time stamp
                tf    !< final time

    ! Initialize simulation
    call startUp

    ! Output mesh 
    call initTecPlotFile
    call outputVizMesh
    call outputVizSolution(0,tSim)
    call outputBinary(0,tSim)
    call outputResidual(0,tSim)
    !call outputVizChOrg(1,dtIn)
    !call outputAux(0,tSim)

    call cpu_time(t0)
    iZone = 2
    ! Main loop
    !do while ( tSim <= tFinal ) ! if dtIn varies    
    do iter = 1, nIter

        ! Euler equations
        !call eulerUpdate(nDim,nEqns,iter)

        ! test functions
        call test()
        
        ! update simulation time
        tSim = tSim + dtIn
        !! update time if time step should vary
        !call updateDt(dtIn)

        ! update reconstruction with NEW data and plot
        write(*,'(a,i0,4x,a,e24.15e2)') 'Finished iteration: ',iter, 'Solution time: ', tSim
        if (( mod(iter,solOutputFreq) == 0 ) .or. ( iter == nIter )) then
            call outputVizSolution(iter,tSim)
            call outputBinary(iter,tSim)
            iZone = iZone + 1
            !call outputVizChOrg(iZone,dtIn)

            ! aux data output
            !if ( iter == nIter ) call outputAux(iter,tSim)
            !call outputAux(iter,tSim)
        end if
        ! output residual history
        call outputResidual(iter,tSim)

        ! data dump file is created if 'dump' is created within the directory
        call dumpDataInquiry(iter,tSim)
        ! terminate simulation early if 'shutdown' is created within the directory 
        call shutDownInquiry(switch)
        if ( switch ) then 
            ! wrap up final solution and exit the iteration loop
            call outputVizSolution(iter,tSim)
            call outputBinary(iter,tSim)
            call outputResidual(iter,tSim)
            ! exit iteration loop and shut down
            exit
        end if

    end do

    call cpu_time(tf)

    ! Shut down
    call shutDown(t0,tf)

end program afTest
!-------------------------------------------------------------------------------
