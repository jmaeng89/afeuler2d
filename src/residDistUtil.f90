!-------------------------------------------------------------------------------
!> @purpose 
!>  Utilities for residual/fluctuation distribution scheme
!>
!> @history
!>  29 December 2015 - Initial creation (Maeng)
!>  
module residDistUtil

    use solverVars, only: nEqns, dtIn, DP, FP
    use meshUtil, only: nDim, nCells, nEdges, nNodes
    use update
    implicit none

    ! RD parameters
    integer, parameter :: nSubCells = 4 !< number of subcells in a cell

    integer, parameter, dimension(4,3) :: subCellNodes = &
                                          reshape((/1,2,3,-1, &
                                                   -3,-1,-2,-2, &
                                                   -2,-3,-1,-3/),(/4,3/))
    ! Notice the columnwise array allocation in Fortran

contains
!-------------------------------------------------------------------------------
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Apply advection operator using Residual Distribution scheme.  
!!>
!!> @history
!!>  18 September 2014 - Initial creation (Maeng)
!!>
!subroutine advectionUpdateRD(nDim,nEqns,iter,lam)
!
!    use meshUtil, only: maxNodesPerCell, cellNodes, &
!                        maxFacesPerCell, cellFaces, &
!                        nodeCoord, edgeCoord, &
!                        faceNodes
!
!    use boundaryConditions, only: applyBC, nodeBC, edgeBC
!
!    implicit none
!
!    ! Interface variables
!    integer, intent(in) :: nDim,    & !< problem dimension
!                           nEqns,   & !< number of equations in system
!                           iter       !< iteration number
!
!    real(FP), intent(in), optional :: lam(nDim) !< advection speed
!
!    ! Local variables
!    integer :: iCell,           & !< cell index in loop
!               iNode,           & !< local node index
!               iEdge,           & !< local edge index
!               gNode,           & !< global node index
!               gEdge,           & !< global edge index
!               iSubCell,        & !< subcell index
!               iNodeEdge,       & !< local node/edge index
!               neglectedEdges,  & !< number of edges without an update
!               neglectedNodes     !< number of nodes without an update
!
!    logical, target :: nodeUpdatedH(nNodes), & !< flag that a node has been updated
!                       edgeUpdatedH(nEdges), & !< flag that an edge has been updated
!                       nodeUpdated(nNodes),  & !< flag that a node has been updated
!                       edgeUpdated(nEdges)     !< flag that an edge has been updated
!
!    logical, pointer :: updatedPtr         !< node/edge updated flag
!
!    real(FP) :: velBar(nDim),       & !< average velocity in cell
!                signal(nEqns)         !< signal
!
!    nodeUpdated = .false.
!    edgeUpdated = .false.
!
!    ! Set reconstruction coefficients for primitive variables
!    call updateReconData()
!
!    do iCell = 1, nCells
!
!        if ( present(lam) ) then
!            ! Linear advection wave speed, same in all cells
!            velBar(:) = lam(:)
!        else
!            ! nonlinear advection, set this accordingly 
!            velBar(:) = primAvgN(2:nDim+1,iCell)
!        end if
!
!        ! subcell loop to apply residual distribution for advection
!        do iSubCell = 1,nSubCells
!    
!            do iNodeEdge = 1, maxNodesPerCell 
!                ! one inflow case
!                if ( subCellNodes(iSubCell,iNodeEdge) > 0 ) then
!                    ! node update
!                    gNode = cellNodes(subCellNodes(iSubCell,iNodeEdge),iCell)
!                    
!                    ! half time step
!                    signal(:) = residSignalValue(nDim,nEqns,iCell, &
!                            subCellNodes(iSubCell,:),iNodeEdge,velBar,0.5_FP*dtIn)
!
!                    nodeDataH(:,gNode) = nodeDataH(:,gNode) + signal(:)
!
!                    ! apply boundary condition
!                    nodePtr => nodeDataH
!                    call applyBC(nEqns,nNodes,gNode,signal,nodePtr,0.5_FP*dtIn,iter)           
!
!                    ! full time step
!                    signal(:) = residSignalValue(nDim,nEqns,iCell, &
!                            subCellNodes(iSubCell,:),iNodeEdge,velBar,dtIn,nodeUpdated(gNode))
!                        
!                    nodeData(:,gNode) = nodeData(:,gNode) + signal(:)
!
!                    ! apply boundary condition
!                    nodePtr => nodeData
!                    call applyBC(nEqns,nNodes,gNode,signal,nodePtr,dtIn, &
!                                        iter,nodeUpdated)           
!
!                else 
!                    ! edge update
!                    gEdge = cellFaces(abs(subCellNodes(iSubCell,iNodeEdge)),iCell)
!
!                    ! half time step
!                    signal(:) = residSignalValue(nDim,nEqns,iCell, &
!                            subCellNodes(iSubCell,:),iNodeEdge,velBar,0.5_FP*dtIn)
!
!                    edgeDataH(:,gEdge) = edgeDataH(:,gEdge) + signal(:)
!
!                    ! apply boundary condition
!                    edgePtr => edgeDataH
!                    call applyBC(nEqns,nEdges,-gEdge,signal,edgePtr,0.5_FP*dtIn, &
!                                        iter)
!
!                    ! full time step
!                    signal(:) = residSignalValue(nDim,nEqns,iCell, &
!                            subCellNodes(iSubCell,:),iNodeEdge,velBar,dtIn,edgeUpdated(gEdge))
!
!                    edgeData(:,gEdge) = edgeData(:,gEdge) + signal(:)
!
!                    ! apply boundary condition
!                    edgePtr => edgeData
!                    call applyBC(nEqns,nEdges,-gEdge,signal,edgePtr,dtIn, &
!                                        iter,edgeUpdated)
!
!                end if
!            end do
!        end do
!    end do
!
!    ! Check for edges/nodes without an update
!#ifdef VERBOSE
!    neglectedEdges = nEdges - count( edgeUpdated )
!    neglectedNodes = nNodes - count( nodeUpdated )
!    if (( neglectedEdges > 0 ) .or. ( neglectedNodes > 0 )) then
!        write(*,*)
!        write(*,'(2(a,i0),a)') 'WARNING: ',neglectedNodes,' nodes and ', &
!                   neglectedEdges,' edges have not been updated.'
!        write(*,*)
!        write(*,'(a7,2a12,a4)') 'Node','x','y','bc'
!        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
!        do iNode = 1, nNodes
!            if ( .not. nodeUpdated(iNode) ) then
!                write(*,'(i7,e12.4e2,i4)') iNode,nodeCoord(:,iNode), &
!                                            nodeBC(iNode)
!            end if
!        end do
!        write(*,*)
!        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
!        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
!        do iEdge = 1, nEdges
!            if ( .not. edgeUpdated(iEdge) ) then
!                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
!                                            edgeBC(iEdge)
!            end if
!        end do
!        write(*,*)
!    end if
!#endif
!end subroutine advectionUpdateRD
!-------------------------------------------------------------------------------
!> @purpose 
!>  Returns update signal only for one inflow subcell
!>
!> @history
!>  24 September 2014 - Initial creation (Maeng)
!>
function residSignalValue(nDim,nEqns,iCell,nodeEdge,iNodeEdge, &
            lam,dt,updated) result(signal) 

    use solverVars, only: eps
    use reconstruction, only: reconData
    use meshUtil, only: cellInvJac, cellFaces, cellNodes, faceCells, &
                        maxNodesPerCell

    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,        & !< problem dimension
                           nEqns,       & !< number of equations
                           iCell,       & !< cell index
                           iNodeEdge,   & !< node/edge local index
                           nodeEdge(maxNodesPerCell) !< node/edge in subcell

    real(FP), intent(in) :: dt,              & !< time step
                            lam(nDim)    !< wave speed

    logical, intent(inout), optional :: updated !< flag that update should be applied

    !< Function variable
    real(FP) :: signal(nEqns)        !< node signal

    !< Local variables
    integer :: iEq,         & !< equation index
               onEdge,      & !< edge containing char. origin
               gFace,       & !< global face index
               iNodeEdge1,  & !< iNode + 1
               iNodeEdge2,  & !< iNode + 2
               outNodeEdge    !< outflow node/edge index

    real(FP) :: xiD(nDim),          & !< interpolation location 
                xi0(nDim),          & !< Characteristic origin
                quadTerm,           & !< quadratic update term 
                bubbleTerm,         & !< bubble function term
                resid(nSubCells),   & !< subcell residuals
                k(maxNodesPerCell), & !< upstream indicator
                nf(nDim,maxNodesPerCell)  !< face normal

    logical :: inCell = .false.       !< incell flag

    real(FP) :: cI((nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    signal = 0.0_FP

    call charOrigin(nDim,iCell,nodeEdge(iNodeEdge),dt,lam,xi0,inCell,onEdge)

    ! tie-breaker when origin lies directly on an edge
    if ( onEdge > 0 ) then
        gFace = cellFaces(onEdge,iCell)
        ! cell with smallest index updates value
        if ( iCell == minval( abs(faceCells(:,gFace)) ) ) then
            inCell = .true.
        else
            inCell = .false.
        end if
    end if

    ! check k coefficient for inflow/outflow determination in subcell 
    nf = vertNorm(nDim,iCell,nodeEdge(:))
    k(1) = 0.5_FP*dot_product(lam,nf(:,1))
    k(2) = 0.5_FP*dot_product(lam,nf(:,2))
    k(3) = 0.5_FP*dot_product(lam,nf(:,3))
    outNodeEdge = nodeFlowCheck(k)

    if ( inCell .and. (outNodeEdge == iNodeEdge) ) then
        ! inCell check and downstream node/edge check

        do iEq = 1,nEqns

            ! bubble coefficient update
            cI = reconData(iEq,:,iCell)
            !cI(7) = 0.0_FP
            bubbleTerm = cI(7)*(27.0_FP*xi0(1)*xi0(2)*(1.0_FP-xi0(1)-xi0(2)))

            ! interpolation location
            xiD(:) = -1.0_FP*dt*matmul(cellInvJac(:,:,iCell),lam(:))
            ! subcell residuals
            resid = residualCalc(nDim,iEq,iCell,xiD) 

            quadTerm = 2.0_FP*xiD(1)*(resid(2) - resid(1)) + &
                       2.0_FP*xiD(2)*(resid(3) - resid(1)) ! quadratic update term

            ! find local index for edge left and right of node
            iNodeEdge1 = mod((iNodeEdge+1),3)
            if ( iNodeEdge1 == 0 ) iNodeEdge1 = 3
            iNodeEdge2 = mod((iNodeEdge+2),3)
            if ( iNodeEdge2 == 0 ) iNodeEdge2 = 3

            if ( nodeEdge(iNodeEdge) > 0) then
                ! node signal
                signal(iEq) = 3.0_FP*resid(nodeEdge(iNodeEdge)) - &
                            resid(abs(nodeEdge(iNodeEdge1))) - &
                            resid(abs(nodeEdge(iNodeEdge2))) + &
                            resid(4) + quadTerm + bubbleTerm
            else
                ! edge signal
                signal(iEq) = -1.0_FP*resid(abs(nodeEdge(iNodeEdge))) +&
                            resid(abs(nodeEdge(iNodeEdge1))) + &
                            resid(abs(nodeEdge(iNodeEdge2))) + &
                            resid(4) + quadTerm + bubbleTerm
            end if

        end do
        
        if ( present(updated) ) updated = .true.

    end if 

end function residSignalValue
!-------------------------------------------------------------------------------
!> @purpose 
!>  Evaluate subcell residuals for the cell
!>
!> @history
!>  18 September 2014 - Initial creation (Maeng)
!>  11 October 2014 - Added cellCoefficients routine
!>
function residualCalc(nDim,iEq,iCell,xi0,cIn) result(resid)

    use reconstruction, only: reconData
    use meshUtil, only: cellFaces, cellNodes

    implicit none

    !< Interface variables
    integer,intent(in) :: nDim,   & !< problem dimension
                          iEq,    & !< equation index
                          iCell     !< cell index

    real(FP), intent(in) :: xi0(nDim)   !< interpolation location

    real(FP), intent(in), optional :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim)))

    !< Function variable
    real(FP) :: resid(nSubCells)

    !< Local variables
    real(FP) :: c((nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    ! Allocate cell reconstruction coefficients
    if ( .not. present(cIn) ) then
        c(:) = reconData(iEq,:,iCell)  
    else 
        c(:) = cIn(:)
    end if
    
    ! Calculate subresiduals
    resid(1) = xi0(1)*(c(6)-c(1)) + &
               xi0(2)*(c(4)-c(1)) ! subresid 1 
    resid(2) = xi0(1)*(c(3)-c(6)) + &
               xi0(2)*(c(2)-c(6)) ! subresid 2 
    resid(3) = xi0(1)*(c(2)-c(4)) + &
               xi0(2)*(c(5)-c(4)) ! subresid 3 
    resid(4) = xi0(1)*(c(2)-c(4)) + &
               xi0(2)*(c(2)-c(6)) ! subresid 4   
    
end function
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find the scaled outward normal of vertex in a subcell. 
!>
!> @history
!>  24 September 2014 - Initial creation (Maeng)
!>
function vertNorm(nDim,iCell,nodeEdge)

    use solverVars, only: eps
    use meshUtil, only: maxFacesPerCell, maxNodesPerCell, &
                        cellFaces, nodeCoord, edgeCoord, &
                        faceNodes, cellNodes
    use mathUtil, only: pointInTri

    implicit none

    !> Interface variables
    integer :: nDim,         & !< number of dimensions
               iCell,        & !< cell index
               nodeEdge(maxNodesPerCell)     !< node/edge index for subcell

    !> Function variable
    real(FP) :: vertNorm(nDim,maxNodesPerCell) !< vertex inward normals

    !> Local variables
    integer :: iNodeEdge,   & !< current local node/edge index
               gNodeEdge,   & !< global node/edge index
               lNodeEdge,   & !< left local node/edge
               rNodeEdge      !< right local node/edge

    real(FP) :: xNodeEdge1(nDim),   & !< node/edge coord
                xNodeEdge2(nDim),   & !< node edge coord 
                xDiff(nDim)           !< difference in coord

    do iNodeEdge = 1,maxNodesPerCell
        ! find local index for edge left and right of node
        ! counter-clockwise orderiing for left and right node index
        lNodeEdge = mod((iNodeEdge+1),3)
        if ( lNodeEdge == 0 ) lNodeEdge = 3
        rNodeEdge = mod((iNodeEdge+2),3)
        if ( rNodeEdge == 0 ) rNodeEdge = 3       

        ! node/edge global index and coordinates 
        if ( nodeEdge(lNodeEdge) > 0 ) then  ! node index
            gNodeEdge = cellNodes(nodeEdge(lNodeEdge),iCell)
            xNodeEdge1(:) = nodeCoord(:,gNodeEdge) 
        else ! edge index 
            gNodeEdge = cellFaces(abs(nodeEdge(lNodeEdge)),iCell) 
            xNodeEdge1(:) = edgeCoord(:,gNodeEdge) 
        end if

        if ( nodeEdge(rNodeEdge) > 0 ) then  ! node index
            gNodeEdge = cellNodes(nodeEdge(rNodeEdge),iCell)
            xNodeEdge2(:) = nodeCoord(:,gNodeEdge) 
        else ! edge index 
            gNodeEdge = cellFaces(abs(nodeEdge(rNodeEdge)),iCell) 
            xNodeEdge2(:) = edgeCoord(:,gNodeEdge) 
        end if              
        xDiff(:) = xNodeEdge2(:)-xNodeEdge1(:)

        ! inward scaled normal from vertex to opposite face
        ! outward scaled normal of edge, to neighboring cell
        ! This has a different sign from the RD formulation
        vertNorm(1,iNodeEdge) = xDiff(2) 
        vertNorm(2,iNodeEdge) = -xDiff(1)
    end do
   
end function vertNorm
!-------------------------------------------------------------------------------
!> @purpose 
!>  Returns the index of downstream node, or inflow edge (which is opposite of 
!>  downstream node). If two inflow edges, returns upstream node with negative
!>  sign. 
!>  Always returns the index of node that is opposite of 1 inflow or 1 outflow edge!
!>  1 inflow edge - 2 outflow edges
!>  2 inflow edges - 1 outflow edge
!>
!> @history
!>  2 October 2014 - Initial creation (Maeng)
!>  20 October 2014 - k conditioning function included
!>
function nodeFlowCheck(k) result(iDownNode) 

    use solverVars, only: eps
    use meshUtil, only: maxNodesPerCell

    implicit none

    ! Interface variables
    real(FP) :: k(maxNodesPerCell) !< vertex normal 

    !> Function variable
    integer :: iDownNode !< Downstream node index

    !> Local variables
    integer :: iNode,       & !< local node index
               downCount,   & !< downstream node counter
               iUpNode        !< upstream node index

    downCount = 0
    do iNode = 1,maxNodesPerCell
        ! Downstream node index is the same as inflow edge index 
        ! TODO: Need to establish the best tolerance for k
        if (  ( k(iNode) ) < -1.0_FP*eps ) then 
            ! Downstream node - outflow side 
            downCount = downCount + 1
            iDownNode = iNode
        else
            ! Upstream node - inflow side
            ! INCLUDES streamline edge
            iUpNode = iNode
        end if
    end do
    
    ! IF two inflow edges, return upstream node index
    if ( downCount > 1 ) iDownNode = -iUpNode

end function nodeFlowCheck 
!-------------------------------------------------------------------------------
!> @purpose 
!>  Vector conditioner
!>
!> @author
!>  Brad Maeng
!>
!> @history
!>  20 October 2014 - Initial creation 
!>
function vecCond(k,kLen)
    
    use solverVars, only: eps

    implicit none
    
    !> Interface variables
    integer, intent(in) :: kLen         !< length of k vector

    real(FP), intent(inout) :: k(kLen)     !< k vector

    !> Output variable
    real(FP) :: vecCond(kLen)

    !> Local variables
    integer :: iLen     !< vector index

    real(FP) :: tol

    tol = eps

    do iLen = 1,kLen
    
        if ( abs(k(iLen)) <= tol ) then
            ! allow room for zeroes
            vecCond(iLen) = 0.0_FP
        else
            vecCond(iLen) = k(iLen)
        end if

    end do
    
end function vecCond
!-------------------------------------------------------------------------------
!> @purpose 
!>  Damp advection bubble function value and correct edge values to maintain
!>  conservation of the entire computational domain
!>
!> @author
!>  Brad Maeng
!>
!> @history
!>  18 September 2014 - Initial creation
!>  3 October 2014 - Bubble function tolerance included
!>  26 November 2014 - Finalized to keep conservation before and after damping
!>
subroutine bubbleDamp(lam,iter)

    use solverVars, only: eps, inLength, iLeft, iRight

    use meshUtil, only: nDim, cellVolume, cellNodes, cellFaces, &
                        faceNodes, faceCells, edgeCoord, &
                        maxFacesPerCell, maxNodesPerCell

    use boundaryConditions, only: applyBC, nodeBC, edgeBC, pEdgePair

    use reconstruction, only: reconData
    
    implicit none

    !> Interface variables
    real(FP), intent(in), optional :: lam(nDim)

    integer, intent(in), optional :: iter           !< iteration

    !> Local variables
    integer :: iEq,     & !< equation index
               iCell,   & !< cell index
               iNode,   & !< local node index
               iEdge,   & !< local edge index
               edge1,   & !< cell edge 1 
               edge2,   & !< cell edge 2
               lCell,   & !< current cell index
               rCell1,  & !< neighbor cell index 1
               rCell2,  & !< neighbor cell index 2
               gFace,   & !< global face index
               gFace1,  & !< global face index 1
               gFace2,  & !< global face index 2
               neglectedEdges     !< number of edges without an update

    integer :: countUpdatedEdges(nEdges)   !< number of bubble updated edges

    logical, target :: edgeUpdated(nEdges)    !< edge update flag

    logical, pointer :: updatedPtr(:)         !< node/edge updated flag pointer

    real(FP) :: waveSpeed(nDim),    & !< wave speed 
                cDamp,              & !< damping coefficient
                sRef,               & !< edge signal weight for conservation
                gamma1,             & !< edge signal weight 1
                gamma2,             & !< edge signal weight 2
                signal(nEqns),      & !< bubble coefficient change 
                bubbleCoef(nEqns),  & !< bubble coefficient 
                k(maxNodesPerCell), & !< scaled vertex normal
                nf(nDim,maxNodesPerCell) !< vertex normals into opposite edge

    integer, parameter, dimension(3) :: &
                nodes = (/ 1, 2, 3 /) !< local node index

    cDamp = 1.0_FP   ! damping coefficient
    countUpdatedEdges(:) = 0
    edgeUpdated(:) = .false.

    ! Bubble function only exists for 2 or higher dimensional problems
    if ( nDim < 2 ) then
        write(*,*) 'Bubble coefficient doesn not exist for 1d problems.'
        return
    end if
    
    do iCell = 1,nCells
        if ( present(lam) ) then
            waveSpeed(:) = lam(:)
        else
            waveSpeed(1) = 1.0_FP/3.0_FP*(sum(edgeData(2,cellFaces(:,iCell)))) 
            waveSpeed(2) = 1.0_FP/3.0_FP*(sum(edgeData(3,cellFaces(:,iCell)))) 
        end if

        ! Calculate the signal to change the bubble function
        bubbleCoef(:) = reconData(:,7,iCell) 
        signal(:) = cDamp*bubbleCoef(:) 
        ! check k coefficient for inflow/outflow determination 
        nf = vertNorm(nDim,iCell,nodes)
        k(1) = 0.5_FP*dot_product(waveSpeed,nf(:,1))
        k(2) = 0.5_FP*dot_product(waveSpeed,nf(:,2))
        k(3) = 0.5_FP*dot_product(waveSpeed,nf(:,3))

        ! node index for inflow edge check
        iNode = nodeFlowCheck(k)
        if ( iNode > 0 ) then 
            ! one inflow, two outflow - contributions to two outflow edges
            gFace = cellFaces(iNode,iCell) ! face index opposite of iNode
            lCell = faceCells(iLeft,gFace) ! current cell index
            if ( lCell /= iCell ) lCell = faceCells(iRight,gFace) ! flip side
            ! FIXME: need to flip the order of faceCells if faceNormal flips

            ! outflow edge local and global indices
            edge1 = mod((iNode+1),3) ! outflow edge 1
            if ( edge1 == 0 ) edge1 = 3
            edge2 = mod((iNode+2),3) ! outflow edge 2
            if ( edge2 == 0 ) edge2 = 3       
            gFace1 = cellFaces(edge1,lCell) ! outflow edge 1
            gFace2 = cellFaces(edge2,lCell) ! outflow edge 2
            rCell1 = faceCells(iRight,gFace1) ! neighbor cell of outflow edge 1
            if ( rCell1 > 0 .and. rCell1 == iCell ) then
                rCell1 = faceCells(iLeft,gFace1) ! flip side
            elseif ( rCell1 <= 0 .and. pEdgePair(gFace1) > 0 ) then 
                rCell1 = faceCells(iLeft,pEdgePair(gFace1)) ! periodic boundary
            else
                rCell1 = lCell !0 Think about what to do
            end if
            rCell2 = faceCells(iRight,gFace2) ! neighbor cell of outflow edge 2
            if ( rCell2 > 0 .and. rCell2 == iCell ) then
                rCell2 = faceCells(iLeft,gFace2) ! flip side
            elseif ( rCell2 <= 0 .and. pEdgePair(gFace2) > 0 ) then
                rCell2 = faceCells(iLeft,pEdgePair(gFace2)) ! periodic boundary
            else
                rCell2 = lCell !0
            end if

            ! edge weight, approach 1
            !gamma1 = abs(k(edge2))/abs(k(iNode)) ! edge weight 1
            !if ( abs(gamma1) <= eps ) gamma1 = 0.0_FP
            !gamma2 = abs(k(edge1))/abs(k(iNode)) ! edge weight 2 
            !if ( abs(gamma2) <= eps ) gamma2 = 0.0_FP

            !! edge weight, approach 2
            !gamma1 = abs(k(edge1))/abs(k(iNode)) ! edge weight 1
            !if ( abs(gamma1) <= eps ) gamma1 = 0.0_FP
            !gamma2 = abs(k(edge2))/abs(k(iNode)) ! edge weight 2 
            !if ( abs(gamma2) <= eps ) gamma2 = 0.0_FP

            ! edge weight, equi distribution approach
            gamma1 = 0.5_FP ! edge weight 1
            gamma2 = 0.5_FP ! edge weight 2

            !if ( iCell == 1 .or. iCell ==2 ) then
            !    write(*,*) iCell, '1 inflow', gFace1, gamma1, gFace2, gamma2
            !end if

            ! edge weight for conservation
            sRef = (27.0_FP/20.0_FP)/( 2.0_FP*gamma1*(1.0_FP + &
                cellVolume(rCell1)/cellVolume(lCell)) + &
                2.0_FP*gamma2*(1.0_FP+cellVolume(rCell2)/cellVolume(lCell)) )
            
            ! Apply changes to outflow edge 1
            edgeDataN(:,gFace1) = edgeDataN(:,gFace1) + &
                    (gamma1*sRef)*signal(:)
            countUpdatedEdges(gFace1) = countUpdatedEdges(gFace1)+1
            edgeUpdated(gFace1) = .true.
            edgePtr => edgeDataN
            updatedPtr => edgeUpdated
            ! Apply boundary condition, periodic BC
            call applyBC(nEqns,-gFace1,(gamma1*sRef)*signal(:),&
                    edgePtr,dtIn,iter,updatedPtr)

            ! Apply changes to outflow edge 2
            edgeDataN(:,gFace2) = edgeDataN(:,gFace2) + &
                    (gamma2*sRef)*signal(:) 
            countUpdatedEdges(gFace2) = countUpdatedEdges(gFace2)+1
            edgeUpdated(gFace2) = .true.
            ! Apply boundary condition, periodic BC 
            call applyBC(nEqns,-gFace2,(gamma2*sRef)*signal(:), &
                    edgePtr,dtIn,iter,updatedPtr)

            do iEq = 1,nEqns
                ! update bubble coefficient in neighbor cells
                reconData(iEq,:,rCell1) = coefficients(nDim,iEq,rCell1)
                reconData(iEq,:,rCell2) = coefficients(nDim,iEq,rCell2)
            end do

            !write(*,*) 'one inflow cell:', iCell
        else
            ! two inflow and one outflow - bubble contribute to ONE OUTFLOW edge only
            gFace = cellFaces(abs(iNode),iCell) ! face index opposite of iNode
            lCell = faceCells(iLeft,gFace)
            if ( lCell /= iCell ) lCell = faceCells(iRight,gFace) ! flip side

            rCell1 = faceCells(iRight,gFace)
            if ( rCell1 > 0 .and. rCell1 == iCell ) then
                rCell1 = faceCells(iLeft,gFace) ! flip side
            elseif ( rCell1 <= 0 .and. pEdgePair(gFace) > 0 ) then 
                rCell1 = faceCells(iLeft,pEdgePair(gFace)) ! periodic boundary
            else
                rCell1 = lCell !0
            end if

            gamma1 = 1.0_FP  ! edge weight 1
            ! edge weight for conservation
            sRef = (27.0_FP/20.0_FP)/( gamma1*(1.0_FP+cellVolume(rCell1)/cellVolume(lCell)) )

            !if ( iCell == 1 .or. iCell ==2 ) then
            !    write(*,*) iCell, '2 inflow', gFace, gamma1
            !end if

            ! FIXME: SMART implementation of stopping bubble damping if bubble function
            ! is smaller than the tolerance

            ! Apply changes to outflow edge 1
            edgeDataN(:,gFace) = edgeDataN(:,gFace) + &
                (gamma1*sRef)*signal(:)
            edgeUpdated(gFace) = .true.
            edgePtr => edgeDataN
            updatedPtr => edgeUpdated
            countUpdatedEdges(gFace) = countUpdatedEdges(gFace)+1
            ! Apply boundary condition, periodic BC
            call applyBC(nEqns,-gFace,(gamma1*sRef)*signal(:), &
                    edgePtr,dtIn,iter,updatedPtr)
            
            do iEq = 1,nEqns
                ! update bubble coefficient in neighbor cells
                reconData(iEq,:,rCell1) = coefficients(nDim,iEq,rCell1)
            end do 

        end if

        !update reconData in current cell
        do iEq = 1, nEqns
            reconData(iEq,:,lCell) = coefficients(nDim,iEq,lCell)
        end do

    end do

    !! check for edge update due to bubble coefficient damping
    !write(*,*) 'edge number, updated'
    !do iEdge = 1,nEdges
    !    write(*,*) iEdge, countUpdatedEdges(iEdge)
    !end do
    !write(*,*) sum(countUpdatedEdges)
    !write(*,*) (countUpdatedEdges)

    ! Set all values equal in time
    edgeData = edgeDataN
    edgeDataH = edgeDataN

    ! Check for edges/nodes without an update
#ifdef VERBOSE
    neglectedEdges = nEdges - count( edgeUpdated )
    if (( neglectedEdges > 0 ) ) then
        write(*,*)
        write(*,'(2(a,i0),a)') 'WARNING: ', &
                   neglectedEdges,' edges have not been updated in Bubble damp.'
        write(*,*)
        write(*,'(a7,2a12,a4)') 'Edge','x','y','bc'
        write(*,'(a7,2a12,a4)') '------','-----------','-----------','---'
        do iEdge = 1, nEdges
            if ( .not. edgeUpdated(iEdge) ) then
                write(*,'(i7,2e12.4e2,i4)') iEdge,edgeCoord(:,iEdge), &
                                            edgeBC(iEdge)
                !if ( edgebc(iedge) == 0 ) then
                !    read(*,*)
                !end if
            end if
        end do
        write(*,*)
    end if
#endif

end subroutine bubbleDamp
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Eliminate advection bubble function value and correct edge values to maintain
!!>  conservation of the element
!!>
!!> @author
!!>  Brad Maeng
!!>
!!> @history
!!>  26 November 2014 - Initial creation
!!>
!subroutine bubbleDelete()
!
!    use solverVars, only: eps, iLeft, iRight, waveSpeed
!    use meshUtil, only: nDim, cellVolume, cellFaces, &
!                        cellNodes, faceCells, faceArea, &
!                        maxFacesPerCell, maxNodesPerCell
!    use boundaryConditions, only: pEdgePair
!    use reconstruction, only: reconData
!
!    implicit none
!
!    !> Local variables
!    integer :: iEq,     & !< equation index
!               iCell,   & !< cell index
!               iEdge,   & !< local edge index
!               rCell,   & !< neighbor cell index 1
!               gFace,   & !< global face index
!               gNode,   & !< global face index
!               inflowNode !< local inflow node index
!
!    real(FP) :: sRef,               & !< edge signal weight for conservation
!                uAvg,               & !< current cell average
!                sEdge(maxFacesPerCell),& !< edge length for weights
!                signal(nEqns),      & !< bubble coefficient change 
!                bubbleCoef(nEqns),  & !< bubble coefficient 
!                uPoint(nDim*(nDim+1)),& !< vector of edge and node values
!                k(maxNodesPerCell), & !< scaled vertex normal
!                nf(nDim,maxNodesPerCell) !< vertex normals into opposite edge
!
!    integer, parameter, dimension(3) :: &
!                nodes = (/ 1, 2, 3 /) !< local node index
!
!    ! Bubble function only exists for 2 or higher dimensional problems
!    if ( nDim < 2 ) then
!        write(*,*) 'Bubble coefficient doesn not exist for 1d problems.'
!        return
!    end if
!    
!    do iEq = 1, nEqns
!        do iCell = 1,nCells
!            ! Calculate the signal to change the bubble function
!            bubbleCoef(iEq) = reconData(iEq,7,iCell)
!
!            ! check k coefficient for inflow/outflow determination 
!            nf = vertNorm(nDim,iCell,nodes)
!            k(1) = 0.5_FP*dot_product(waveSpeed,nf(:,1))
!            k(2) = 0.5_FP*dot_product(waveSpeed,nf(:,2))
!            k(3) = 0.5_FP*dot_product(waveSpeed,nf(:,3))
!
!            ! node index for inflow edge check
!            inflowNode = nodeFlowCheck(k)
!
!            sEdge = 0
!            do iEdge = 1,maxFacesPerCell
!                if ( (k(iEdge)) <= eps ) then  
!                    ! outflow edges, including streamline edge
!                    sEdge(iEdge) = abs(k(iEdge)) 
!                else 
!                    sEdge(iEdge) = abs(k(iEdge))
!                end if
!            end do
!
!            sRef = sum(sEdge)
!            ! bubble function delete signal
!            signal(iEq) = ((27.0_FP/20.0_FP)/(sRef))*bubbleCoef(iEq) 
!            uPoint = 0.0_FP
!            ! apply the signal to the edge
!            do iEdge = 1,maxFacesPerCell
!                gFace = cellFaces(iEdge,iCell)
!                gNode = cellNodes(iEdge,iCell)
!                ! node, no need to change
!                !uPoint(2*(iEdge-1)+1) = nodeDataN(iEq,gNode) 
!                uPoint(2*(iEdge-1)+1) = reconData(iEq,2*(iEdge-1)+1,iCell) 
!                ! edge adjust 
!                !uPoint(2*iEdge) = edgeDataN(iEq,gFace) + sEdge(iEdge)*signal(iEq)
!                uPoint(2*iEdge) = reconData(iEq,2*iEdge,iCell) + sEdge(iEdge)*signal(iEq)
!            end do
!            uAvg = cellAvgN(iEq,iCell)
!            !update reconData in current cell
!            reconData(iEq,:,iCell) = coeffFromVal(nDim,uAvg,uPoint)
!        end do
!    end do
!    
!end subroutine bubbleDelete
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Damp advection bubble function value and correct edge values to maintain
!!>  conservation of the entire computational domain
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  20 October 2015 - Initial creation
!!>
!subroutine bubbleDamp(nDim,nEqns)
!
!    use solverVars, only: eps, inLength, iLeft, iRight
!    use meshUtil, only: cellVolume, cellNodes, cellFaces, &
!                        faceNodes, faceCells, faceNormal, &
!                        nodeCoord, edgeCoord, &
!                        maxNodesPerCell
!    use boundaryConditions, only: applyBC, nodeBC, edgeBC, pEdgePair
!    use reconstruction, only: reconData
!    use mathUtil, only: pointInTri
!
!    implicit none
!
!    !> Interface variables
!    integer, intent(in) :: nDim,    & !< problem dimension
!                           nEqns      !< number of equations 
!
!    !> Local variables
!    integer :: iEq,     & !< equation index
!               iCell,   & !< cell index
!               iNode,   & !< local node index
!               gNode      !< global node index
!
!    real(FP) :: cDamp,              & !< damping coefficient
!                sRef,               & !< element volume
!                lam(nDim),          & !< wave speed
!                diss(nEqns),        & !< dissipation
!                qNodeVal(nEqns,3),  &
!                xDiff(nDim),        & !< edge length vector
!                xVert(nDim,3),      & !< edge length vector
!                xN(nDim),      & !< edge length vector
!                uAvg,               & !< cell average
!                uPoint(nDim*(nDim+1)),  & !< vector of edge and node values
!                k(maxNodesPerCell)    !< node weight      
!
!    cDamp = 0.01_FP   ! damping coefficient
!
!    ! Bubble function only exists for 2 or higher dimensional problems
!    if ( nDim < 2 ) then
!        write(*,*) 'Bubble coefficient doesn not exist for 1d problems.'
!        return
!    end if
!    
!    do iCell = 1,nCells
!
!        sRef = cellVolume(iCell) ! cell volume
!        do iNode = 1,maxNodesPerCell
!            xDiff(:) = nodeCoord(:,faceNodes(2,iNode)) - &
!                       nodeCoord(:,faceNodes(1,iNode))
!            ! should I check if the normal is pointing out ?
!            xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
!            xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
!            xN(:) = edgeCoord(:,iNode) - dtIn*faceNormal(:,iNode)
!            if ( .not. pointInTri(xN,xVert) ) then
!                ! I guess flip the order of node coord
!                xDiff(:) = nodeCoord(:,faceNodes(1,iNode)) - &
!                           nodeCoord(:,faceNodes(2,iNode))
!            end if
!            lam(1) = waveSpeed(1)
!            lam(2) = -waveSpeed(2)
!            k(iNode) = -1.0_FP*dot_product(lam,xDiff)/(2.0_FP*sRef) 
!        end do
!        ! evaluate dissipation value
!        diss(:) = k(1)*k(1)*nodeData(:,cellNodes(1,iCell)) + &
!                  k(2)*k(2)*nodeData(:,cellNodes(2,iCell)) + &
!                  k(3)*k(3)*nodeData(:,cellNodes(3,iCell)) + &
!                  2.0_FP*k(1)*k(2)*edgeData(:,cellFaces(1,iCell)) + &
!                  2.0_FP*k(2)*k(3)*edgeData(:,cellFaces(2,iCell)) + &
!                  2.0_FP*k(3)*k(1)*edgeData(:,cellFaces(3,iCell))
!
!        ! apply dissipation to node values
!        do iNode = 1,maxNodesPerCell
!            gNode = cellNodes(iNode,iCell) 
!            !qNodeVal(:,iNode) = nodeDataN(:,gNode) - &
!            !        cDamp*k(iNode)*k(iNode)*diss(:)
!            nodeData(:,gNode) = nodeData(:,gNode) - &
!                    cDamp*k(iNode)*k(iNode)*diss(:)
!        end do
!
!        !! do I update reconData ? or not?????
!        !do iEq = 1,nEqns
!        !    uAvg = cellAvgN(iEq,iCell)
!        !    uPoint(1) = qNodeVal(iEq,1)
!        !    uPoint(3) = qNodeVal(iEq,2)
!        !    uPoint(5) = qNodeVal(iEq,3)
!        !    uPoint(2) = edgeDataN(iEq,cellFaces(1,iCell))
!        !    uPoint(4) = edgeDataN(iEq,cellFaces(2,iCell))
!        !    uPoint(6) = edgeDataN(iEq,cellFaces(3,iCell))
!        !    reconData(iEq,:,iCell) = coeffFromVal(nDim,uAvg,uPoint) 
!        !    !reconData(iEq,:,iCell) = coefficients(nDim,iEq,iCell)
!        !end do
!
!    end do
!
!    !! Set all values equal in time
!    nodeDataN = nodeData
!    nodeDataH = nodeData
!
!end subroutine bubbleDamp
!-------------------------------------------------------------------------------
end module residDistUtil
!-------------------------------------------------------------------------------
