!-------------------------------------------------------------------------------
!> @purpose 
!>  Master program for 2d active flux linear acoustic equations solver
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  13 November 2015 - Initial creation
!>
program afLinAcoustics

    use solverVars, only: nEqns, nIter, FP, tSim, dtIn
    use startStop, only: startUp, shutDown, maxDt
    use meshUtil, only: nDim, nEdges, nNodes, nCells
    use inputOutput, only: outputVizMesh, outputVizSolution, solOutputFreq, &
                           outputBinary, outputAux, outputVizChOrg
    use update, only: linacousticsUpdate, &
                      nodeDataDBAdv, nodeDataDBAco, nodeGradAco, &
                      edgeDataDBAdv, edgeDataDBAco, edgeGradAco, &
                      nodeData, edgeData, nodeDataH, edgeDataH

    implicit none

    ! Local variables
    integer :: iter !< iteration number

    integer :: iZone

    real(FP) :: t0, & !< starting time stamp
                tf    !< final time

    real(FP) :: dtInTemp

    ! simulation time initialization
    tSim = 0.0_FP

    ! Initialize simulation
    call startUp

    !! the initial data for debugging purpose  
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    dtInTemp = dtIn
    !! set time step to zero for visualizing 
    dtIn = 0.0_FP
    edgeDataDBAdv(:,:) = edgeData(:,:) 
    nodeDataDBAdv(:,:) = nodeData(:,:) 
    edgeDataDBAco(:,:) = edgeData(:,:) 
    nodeDataDBAco(:,:) = nodeData(:,:) 
    edgeGradAco(:,:) = 0.0_FP 
    nodeGradAco(:,:) = 0.0_FP 
    nodeDataH(:,:) = nodeData(:,:)
    edgeDataH(:,:) = edgeData(:,:)
    ! set time step back
    dtIn = dtInTemp
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ! Output mesh 
    call outputVizMesh
    call outputVizSolution(0,tSim)
    call outputBinary(0,tSim)

    call cpu_time(t0)

    ! Main loop
    do iter = 1, nIter

        ! isentropic Euler equations
        call linAcousticsUpdate(nDim,nEqns,iter)
        
        tSim = tSim + dtIn

        iZone = iZone + 1
        ! update reconstruction with NEW data and plot
        write(*,'(a,i0)') 'Finished iteration: ',iter
        if (( mod(iter,solOutputFreq) == 0 ) .or. ( iter == nIter )) then
            call outputVizSolution(iter,tSim)
            call outputBinary(iter,tSim)
        end if

    end do

    call cpu_time(tf)

    ! Shut down
    call shutDown(t0,tf)

end program afLinAcoustics
!-------------------------------------------------------------------------------
