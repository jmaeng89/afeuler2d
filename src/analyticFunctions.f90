!!-------------------------------------------------------------------------------
!!>  Contains initialization functions
!>
!> @author
!>  Timothy A. Eymann
!>  
!> @history
!>  10 April 2015 - Combined evalFunctions for simplicity (Maeng)
!>
module analyticFunctions

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Select which function to evaluate (might be slow since case select is
!>  within loop)
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  13 March 2012 - Initial Creation
!>  7 April 2015 - Combined Steady and Unsteady (Maeng)
!>  6 May 2015 - Began working on euler 2d (Maeng)
!>
function evalFunction(nDim,nEqns,x,t,funcName)

    use solverVars, only: initSolnType, inLength
    implicit none

    ! Interface variables
    character(inLength), intent(in), optional :: funcName !< name of function

    integer, intent(in) :: nDim,    & !< dimension of x-array
                           nEqns      !< number of equations in system

    real(FP), intent(in) :: x(nDim) !< function evaluation point

    real(FP), intent(in), optional :: t !< time

    ! Function variable
    real(FP) :: evalFunction(nEqns)

    ! Local variable
    character(inLength) :: func!< local variable for function name

    real(FP) :: tSol   !< solution evaluation time

    if ( .not. present(t) ) then
        tSol = 0.0_FP
    else
        tSol = t
    end if

    if ( present(funcName) ) then
        func = funcName
    else
        func = initSolnType
    end if

    select case ( trim(adjustl(func)) )

        case ( 'constVec' )
            evalFunction = constVec(nEqns,x(1),x(2))

        case ( 'gaussian' )
            evalFunction = gaussian(nEqns,x(1),x(2),tSol)

        case ( 'sinusoid' )
            evalFunction = sinusoid(nEqns,x(1),x(2),tSol)

        case ( 'testWave' )
            evalFunction = testWave(nEqns,x(1),x(2),tSol)

        case ( 'burgers' )
            evalFunction = burgers(nEqns,x(1),x(2),tSol)

        case ( 'simpWave' )
            evalFunction = simpleWave(nEqns,x(1),x(2),tSol)

        case ( 'testLin' ) 
            evalFunction = testLinWave(nEqns,x(1),x(2),tSol)

        case ( 'testQuad' ) 
            evalFunction = testQuadWave(nEqns,x(1),x(2),tSol)

        ! nonlinear acoustic cases
        case ( 'acPulse' )
            evalFunction = acPulse(nEqns,x(1),x(2),tSol)

        ! pressureless Euler cases
        case ( 'sin2' )
            evalFunction = sinusoid2(nEqns,x(1),x(2),tSol)

        case ( 'solidRot' )
            evalFunction = solidRotation(nEqns,x(1),x(2))

        case ( 'plEuEx2')
            evalFunction = plEulerExact2d(nEqns,x(1),x(2),tSol)

        case ( 'plEu2dt1' )
            evalFunction = plEuler2d_v1(nEqns,x(1),x(2))

        case ( 'plEu2dt2' )
            evalFunction = plEuler2d_v2(nEqns,x(1),x(2))

        ! Euler/Isentropic Euler cases
        case ( 'entWave' )
            evalFunction = entropyWave(nEqns,x(1),x(2),tSol)

        case ( 'fastvort' )
            evalFunction = fastVortex(nEqns,x(1),x(2))

        case ( 'slowvort' )
            evalFunction = slowVortex(nEqns,x(1),x(2)) ! this case is bonkers

        case ( 'mvortex')
            evalFunction = movingVortex(nEqns,x(1),x(2),tSol)
     
        case ( 'svortex')
            evalFunction = steadyVortex(nEqns,x(1),x(2),tSol)

        case ( 'mvortv2')
            evalFunction = movingVortexV2(nEqns,x(1),x(2),tSol)
     
        case ( 'svortv2')
            evalFunction = steadyVortexV2(nEqns,x(1),x(2),tSol)

        case ( 'sod1D' )
            evalFunction = sodProblem1D(nEqns,x(1),x(2),tSol)

        case ( 'sod2D' )
            evalFunction = sodProblem2D(nEqns,x(1),x(2),tSol)

        case default
            write(*,'(3a)') 'ERROR: "',trim(adjustl(func)), &
                            '" is not a valid function.'
            stop

    end select

end function evalFunction
!-------------------------------------------------------------------------------
!> @purpose 
!>  Constant vector for testing systems
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  25 October 2012 - Initial Creation
!>
function constVec(nEqns,x,y)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y     !< y coord

    ! Function variable
    real(FP) :: constVec(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    constVec(1) = 1.0_FP ! + dble(iEq)
    constVec(2) = 1.0_FP ! + dble(iEq)
    constVec(3) = 1.0_FP ! + dble(iEq)
    constVec(4) = 1.0_FP ! + dble(iEq)

    !constVec(1) = 1.0_FP ! + dble(iEq)
    !constVec(2) = x**2.0 + 2.*y**2.0 - x*y + y!0.5_FP*x*y - 1.0_FP*y**2.0 ! + dble(iEq)
    !constVec(3) = -0.5*x**2.0 + 3.*y**2.0 + 5.0*x*y + x! + dble(iEq)
    !constVec(4) = 1.0_FP ! + dble(iEq)

end function constVec
!-------------------------------------------------------------------------------
!> @purpose 
!>  Gaussian pulse 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 August 2016 - Initial Creation
!>
function gaussian(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: gaussian(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    real(FP) :: c = 0.5_FP

    gaussian(:) = exp( -c*(x)**2.0_FP - c*(y)**2.0_FP ) 

end function gaussian
!-------------------------------------------------------------------------------
!> @purpose 
!>  Sinusoidal wave 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 August 2016 - Initial Creation
!>
function sinusoid(nEqns,x,y,t)

    use solverVars, only: pi, waveSpeed
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: sinusoid(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    sinusoid(:) = cos( (2.0_FP*pi)*(x-waveSpeed(1)*t)/5.0_FP ) 

end function sinusoid
!-------------------------------------------------------------------------------
!> @purpose 
!>  flat Sinusoidal wave 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  16 August 2016 - Initial Creation
!>
function sinusoid2(nEqns,x,y,t)

    use solverVars, only: pi, waveSpeed
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< time

    ! Function variable
    real(FP) :: sinusoid2(nEqns)

    sinusoid2(1) = 0.02_FP + 0.01_FP*sin( (2.0_FP*pi)/10.0_FP*(x) ) 
    sinusoid2(2) = 0.02_FP + 0.01_FP*sin( (2.0_FP*pi)/10.0_FP*(x) ) 
    sinusoid2(3) = 0.00_FP + 0.0_FP
    sinusoid2(4) = 0.02_FP + 0.01_FP*sin( (2.0_FP*pi)/10.0_FP*(x) ) 

end function sinusoid2
!-------------------------------------------------------------------------------
!> @purpose 
!>  TEST
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 March 2016 - Initial Creation
!>
function testWave(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y, & !< input coordinates
                            t       !< time

    ! Function variable
    real(FP) :: testWave(nEqns)

    testWave(1) = 1.0_FP/3.0_FP*(x-1.0*t)**2.0_FP + 1.0_FP/3.0_FP
    testWave(2) = 1.0_FP  
    testWave(3) = 1.0_FP 
    testWave(4) = 1.0_FP 

end function testWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  Burgers' equation initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  6 December 2015 - Initial Creation
!>
function burgers(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: burgers(nEqns)

    ! Local variables
    real(FP) :: u,      & !< solution 
                A,      & !< constant
                k,      & !< constant
                ts,     & !< constant
                xc        !< constant 

    ! Expansion fan
    A = 1.0_FP   !< amplitude
    k = 1.0_FP   !< decay coefficient
    xc = 0.0_FP  !< location where expansion wave is centered 
    u = A*tanh(k*(x-xc))

    !! compression wave
    !ts = 1.0_FP  !< shock formation time
    !u = 5.0_FP/(2.0_FP*pi*ts)*sin(2.0_FP*pi*x/5.0_FP)

    burgers(1) = 0.0_FP
    burgers(2) = u 
    burgers(3) = 0.0_FP 
    burgers(4) = 0.0_FP 

end function burgers
!-------------------------------------------------------------------------------
!> @purpose 
!>  Simple wave solution for compressible Euler
!>  
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 December 2015 - Initial Creation
!>
function simpleWave(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: simpleWave(nEqns)

    ! Local variables
    real(FP) :: aInf,   & !< freestream speed of sound
                rhoInf, & !< freestream density
                pInf,   & !< freestream pressure
                MInf,   & !< freestream Mach number
                uaInf,  & !< velocity over aInf
                VBurg(nEqns) !< solution to simple waveBurgers' equation

    rhoInf = 1.0_FP
    pInf = isentropicConst*rhoInf**(gam)
    aInf = sqrt(gam*pInf/rhoInf)
    MInf = 0.0_FP

    ! u+a
    VBurg = burgers(nEqns,x,y,t)
    ! this is u/aInf
    uaInf = 2.0_FP/(gam+1.0_FP)*(VBurg(2)/aInf -  &
        (1.0_FP - (gam-1.0_FP)/2.0_FP*MInf)) ! this is u/aInf

    ! rho for u+a
    simpleWave(1) = rhoInf*(1.0_FP + 0.5_FP*(gam-1.0_FP)* &
        (uaInf-MInf))**(2.0_FP/(gam-1.0_FP))
    ! rho for u-a
    !simpleWave(1) = rhoInf*(1.0_FP - 0.5_FP*(gam-1.0_FP)* &
    !    (uaInf-MInf))**(2.0_FP/(gam-1.0_FP))
    !! rho when gam = 1
    !simpleWave(1) = rhoInf*(exp(uaInf-MInf))
    ! u
    simpleWave(2) = aInf*uaInf 
    simpleWave(3) = 0.0_FP 
    simpleWave(4) = pInf*(1.0_FP + 0.5_FP*(gam-1.0_FP)* &
        (uaInf-MInf))**(2.0_FP*gam/(gam-1.0_FP)) 

end function simpleWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  One dimensional simple wave solution for compressible Euler - linear problem
!>  u = a = rho for gam = 3.0 and isentropicConst = 1.0/3.0
!>  k > 0 - expansion wave
!>  k < 0 - compression wave then shock at t=1/k
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  3 November 2015 - Initial Creation
!>
function testLinWave(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, & 
                            y, & !< input coordinates
                            t    !< time

    ! Function variable
    real(FP) :: testLinWave(nEqns)

    ! Local variables
    real(FP) :: xL,     & !< left coordinate of wave
                xR,     & !< right coordinate of wave
                uL,     & !< left wave initial value
                uR,     & !< simple wave initial value
                A,      & !< y offset
                k,      & !< slope
                wspeed, & !< wave speed
                waveLen   !< half wave length    

    A = 3.0_FP
    waveLen = 2.0_FP ! half wave length

    uL = A-1.0_FP
    uR = A+1.0_FP

    ! in simple wave solutions, u = a. So u = F(x-2ut) is solved.
    wspeed = 2.0_FP ! 1 for burgers', 2 for simple wave
    xL = -waveLen + uL*wspeed*t 
    xR =  waveLen + uR*wspeed*t 
    ! density
    if ( x < xL ) then 
        testLinWave(1) = uL 
    else if ( x >= xL .and. x < xR ) then
        k = (uR-uL)/(xR-xL)
        testLinWave(1) = k*x + (uR - k*xR) 
    else
        testLinWave(1) = uR  
    end if
    testLinWave(2) = testLinWave(1) 
    testLinWave(3) = 0.0_FP 
    testLinWave(4) = isentropicConst*testLinWave(1)**gam 

end function testLinWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  One dimensional simple wave test for compressible Euler - quadratic problem
!>  u = a = rho for gam = 3 and isentropicConst = 1.0/3.0
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  25 November 2015 - Initial Creation
!>
function testQuadWave(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y, & !< input coordinates
                            t       !< time

    ! Function variable
    real(FP) :: testQuadWave(nEqns)

    ! Local variables
    real(FP) :: A, B, C,    & !< coefficients in quadratic function
                waveLen,    & !< half wave length
                wspeed,     & !< wave speed
                u1, u2,     & !< wave speed at various locations
                u3, u4,     &       
                x1, x2,     & !< wave speed at various locations
                x3, x4        

    waveLen = 4.0_FP 
    u1 = 1.0_FP
    u2 = 5.0_FP/4.0_FP
    u3 = 5.0_FP/4.0_FP
    u4 = 1.0_FP

    ! in simple wave solutions, u = a. So u = F(x-2ut) is solved
    ! in burgers' equation, u = F(x-ut) is solved
    wspeed = 2.0_FP ! 1 for burgers', 2 for simple wave
    x1 = -waveLen + u1*wspeed*t
    x2 = -0.5_FP*waveLen + u2*wspeed*t
    x3 =  0.5_FP*waveLen + u3*wspeed*t
    x4 =  waveLen + u4*wspeed*t
    ! density
    if ( x < x1 ) then 
        testQuadWave(1) = 1.0_FP 

    else if ( x >= x1 .and. x < x2 ) then
        A = 1.0_FP/16.0_FP
        B = 0.5_FP
        C = 2.0_FP
        ! simple wave
        testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(4.0_FP*A*x*t + 2.0_FP*B*t + 1.0_FP + &
            sqrt(-16.0_FP*A*C*t*t + 4.0_FP*B*B*t*t + 8.0_FP*A*t*x + 4.0_FP*B*t + 1.0_FP)) 
        !! for Burgers' equation
        !testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
        !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))

    else if ( x >= x2 .and. x < x3 ) then
        A = -1.0_FP/16.0_FP
        B = 0.0_FP
        C = 3.0_FP/2.0_FP
        ! simple wave
        testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(4.0_FP*A*x*t + 2.0_FP*B*t + 1.0_FP + &
            sqrt(-16.0_FP*A*C*t*t + 4.0_FP*B*B*t*t + 8.0_FP*A*t*x + 4.0_FP*B*t + 1.0_FP))
        !! for Burgers' equation
        !testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
        !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))

    else if ( x >= x3 .and. x < x4 ) then
        A = 1.0_FP/16.0_FP
        B = -0.5_FP
        C = 2.0_FP        
        ! simple wave
        testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(4.0_FP*A*x*t + 2.0_FP*B*t + 1.0_FP + &
            sqrt(-16.0_FP*A*C*t*t + 4.0_FP*B*B*t*t + 8.0_FP*A*t*x + 4.0_FP*B*t + 1.0_FP)) 
        !! for Burgers' equation
        !testQuadWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
        !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))

    else

        testQuadWave(1) = 1.0_FP

    end if
    testQuadWave(2) = testQuadWave(1)  
    testQuadWave(3) = 0.0_FP 
    testQuadWave(4) = isentropicConst*testQuadWave(1)**gam 

end function testQuadWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  nonlinear acoustic initial condition
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  13 September 2016 - Initial Creation
!>
function acPulse(nEqns,x,y,t)

    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y, & !< input coordinates
                            t       !< time

    ! Function variable
    real(FP) :: acPulse(nEqns)

    ! Local variable
    real(FP) :: c = 2.0_FP

    !acPulse(1) = 1.0_FP + 2.0_FP*exp(-c*(x**2.0_FP + y**2.0_FP))
    acPulse(1) = 1.0_FP + 1.0_FP*exp(-c*(x**2.0_FP + y**2.0_FP))
    acPulse(2) = 0.0_FP  
    acPulse(3) = 0.0_FP 
    acPulse(4) = isentropicConst*acPulse(1)**gam 

end function acPulse
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for testing barotropic euler systems 2d
!>  Circular flow
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  6 May 2015 - Initial Creation
!>
function barotropicTest1(nEqns,x,y)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y     !< y coord

    ! Local variable 
    real(FP) :: r,      & !< radial position
                rhoInf, & !< density at infinity
                k,      & !< coefficient
                x0        !< shifted vortex center

    ! Function variable
    real(FP) :: barotropicTest1(nEqns)

    k = 2.0_FP
    x0 = 0.0_FP
    r = sqrt((x-x0)**2.0_FP + y**2.0_FP)

    rhoInf = 1.0_FP

    barotropicTest1(1) = ( rhoInf**(gam-1.0_FP) -  &
                (((gam-1.0_FP)*exp(-2.0_FP*k*r**2.0_FP))/  &
                (4.0_FP*gam*k*isentropicConst)) )**(1.0_FP/(gam-1.0_FP))
    barotropicTest1(2) = -1.0_FP - y*exp(-k*r**2.0_FP) 
    barotropicTest1(3) = 1.0_FP + (x-x0)*exp(-k*r**2.0_FP)
    barotropicTest1(4) = isentropicConst*( rhoInf**(gam-1.0_FP) -  &
                (((gam-1.0_FP)*exp(-2.0_FP*k*r**2.0_FP))/  &
                (4.0_FP*gam*k*isentropicConst)) )**(gam/(gam-1.0_FP))
    !barotropicTest1(1) = 0.0
    !barotropicTest1(2) = 0.0
    !barotropicTest1(3) = 0.0
    !barotropicTest1(4) = 0.0

end function barotropicTest1
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D Isentropic Euler moving vortex
!>  Velocity is cut off to 0 at rOff
!>
!> @history
!>  30 March 2016 - Initial Creation
!>
function movingVortexV2(nEqns,x,y,t)

    use solverVars, only: eps, pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                rOff,   & !< cut off position
                theta,  & !< angle
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                rho0,   & !< reference density
                rhoInf, & !< freestream density
                vmag,   & !< velocity magnitude
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    ! Function variable
    real(FP) :: movingVortexV2(nEqns)

    rOff = 4.0_FP
    ep = 3.8_FP
    rho0 = 10.5_FP 
    rhoInf = 0.977695_FP !< machine precision freestream evaluated from 
                         !< Mathematica
    uInf = 1.0_FP
    vInf = 0.0_FP
    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    theta = 0.0_FP
    if ( r > 2.0_FP*eps ) theta = atan2(yc,xc)

    if ( r >= rOff ) then
        movingVortexV2(1) = rhoInf
        movingVortexV2(2) = uInf - 0.0_FP
        movingVortexV2(3) = vInf + 0.0_FP 
        movingVortexV2(4) = isentropicConst*( movingVortexV2(1) )**(gam)
    else
        ! velocity magnitude calculation
        vmag = ep*(r/rOff)*(1.0_FP - (r/rOff)**2.0_FP)**3.0_FP*exp(-(r/rOff)**2.0_FP)  
        movingVortexV2(1) = ( rho0**(gam-1.0_FP) - &
            (gam-1.0_FP)/(isentropicConst*gam)*(ep**2.0_FP)/(16.0_FP*rOff**12.0_FP) * &
            exp(-2.0_FP*(r/rOff)**2.0_FP)*( 4.0_FP*r**12.0_FP - &
            12.0_FP*r**10.0_FP*rOff**2.0_FP + 30.0_FP*r**8.0_FP*rOff**4.0_FP - &
            20.0_FP*r**6.0_FP*rOff**6.0_FP + 30.0_FP*r**4.0_FP*rOff**8.0_FP + &
            6.0_FP*r**2.0_FP*rOff**10.0_FP + 7.0_FP*rOff**12.0_FP ))**(1.0_FP/(gam-1.0_FP))
        movingVortexV2(2) = uInf - vmag*sin(theta) 
        movingVortexV2(3) = vInf + vmag*cos(theta)
        movingVortexV2(4) = isentropicConst*( movingVortexV2(1) )**(gam)
    end if

end function movingVortexV2
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D Isentropic Euler moving vortex - version 2
!>  Given radially dependent velocity profile, we find the density and pressure
!>   that satisfy isentropic relation.
!>
!> @history
!>  30 March 2016 - Initial Creation
!>
function steadyVortexV2(nEqns,x,y,t)

    use solverVars, only: eps, pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                rOff,   & !< cut off position
                theta,  & !< angle
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                rho0,   & !< reference density
                rhoInf, & !< freestream density
                vmag,   & !< velocity magnitude
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    ! Function variable
    real(FP) :: steadyVortexV2(nEqns)

    rOff = 4.0_FP
    ep = 3.8_FP
    rho0 = 10.5_FP 
    rhoInf = 0.977695_FP !< machine precision freestream evaluated from 
                         !< Mathematica
    uInf = 0.0_FP
    vInf = 0.0_FP
    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    theta = 0.0_FP
    if ( r > 2.0_FP*eps ) theta = atan2(yc,xc)

    if ( r >= rOff ) then
        steadyVortexV2(1) = rhoInf
        steadyVortexV2(2) = uInf - 0.0_FP
        steadyVortexV2(3) = vInf + 0.0_FP 
        steadyVortexV2(4) = isentropicConst*( steadyVortexV2(1) )**(gam)
    else
        ! velocity magnitude calculation
        vmag = ep*(r/rOff)*(1.0_FP - (r/rOff)**2.0_FP)**3.0_FP*exp(-(r/rOff)**2.0_FP)  
        steadyVortexV2(1) = ( rho0**(gam-1.0_FP) - &
            (gam-1.0_FP)/(isentropicConst*gam)*(ep**2.0_FP)/(16.0_FP*rOff**12.0_FP) * &
            exp(-2.0_FP*(r/rOff)**2.0_FP)*( 4.0_FP*r**12.0_FP - &
            12.0_FP*r**10.0_FP*rOff**2.0_FP + 30.0_FP*r**8.0_FP*rOff**4.0_FP - &
            20.0_FP*r**6.0_FP*rOff**6.0_FP + 30.0_FP*r**4.0_FP*rOff**8.0_FP + &
            6.0_FP*r**2.0_FP*rOff**10.0_FP + 7.0_FP*rOff**12.0_FP ))**(1.0_FP/(gam-1.0_FP))
        steadyVortexV2(2) = uInf - vmag*sin(theta) 
        steadyVortexV2(3) = vInf + vmag*cos(theta)
        steadyVortexV2(4) = isentropicConst*( steadyVortexV2(1) )**(gam)
    end if

end function steadyVortexV2
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D Isentropic Euler moving vortex
!>
!> @author
!>  Doreen Fan
!>
!> @history
!>  20 June 2015 - Initial Creation
!>
function movingVortex(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst, Rgas
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                Tratio, & !< T/TInf
                M,      & !< Mach number
                aInf,   & !< freestream speed of sound 
                TInf,   & !< freestream temperature 
                pInf,   & !< freestream pressure 
                rInf,   & !< freestream density
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    ! Function variable
    real(FP) :: movingVortex(nEqns)

    ep = 5.0_FP
    pInf = 1.0_FP
    TInf = 1.0_FP !398.208_FP
    aInf = 1.0_FP !sqrt(gam*Rgas*TInf) ! isentropic speed of sound
    rInf = 1.0_FP !pInf/(TInf*Rgas)

    !M = 0.5_FP
    !uInf = M*aInf
    uInf = 1.0_FP
    vInf = 0.0_FP

    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)

    ! original condition
    Tratio = ( 1.0_FP - &
              (((gam-1.0_FP)*(ep**2.0_FP)*exp(1.0_FP-r**2.0_FP))/  &
              (8.0_FP*gam*pi**2.0_FP)) )
    movingVortex(1) = rInf*(Tratio**(1.0_FP/(gam-1.0_FP)))
    movingVortex(2) = uInf - ep*yc*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi) 
    movingVortex(3) = vInf + ep*xc*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)
    movingVortex(4) = pInf*(Tratio**(gam/(gam-1.0_FP)))

end function movingVortex
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D Isentropic Euler stationary vortex
!>
!> @author
!>  Doreen Fan
!>
!> @history
!>  20 Jund 2015 - Initial Creation
!>
function steadyVortex(nEqns,x,y,t)

    use solverVars, only: pi
    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Local variable 
    real(FP) :: r,      & !< radial position
                ep,     & !< vortex strength
                xc,     & !< vortex location
                yc,     & !< vortex location
                x0,     & !< vortex initial location
                y0,     & !< vortex initial location
                uInf,   & !< freestream velocity, x
                vInf      !< freestream velocity, y

    ! Function variable
    real(FP) :: steadyVortex(nEqns)

    ep = 5.0_FP
    uInf = 0.0_FP
    vInf = 0.0_FP
    x0 = 0.0_FP
    y0 = 0.0_FP
    xc = x - x0 - uInf*t
    yc = y - y0 - vInf*t
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    
    ! original condition
    steadyVortex(1) = ( 1.0_FP -  &
                (((gam-1.0_FP)*(ep**2.0_FP)*exp(1.0_FP-r**2.0_FP))/  &
                (8.0_FP*gam*pi**2.0_FP)) )**(1.0_FP/(gam-1.0_FP))
    steadyVortex(2) = uInf - ep*yc*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi) 
    steadyVortex(3) = vInf + ep*xc*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)
    steadyVortex(4) = isentropicConst*( steadyVortex(1) )**(gam)

    !! derivatives of steadyVortex variables
    !r = sqrt((x)**2.0_FP + (y)**2.0_FP)
    !! drhodx 
    !ep**2.0/(4.0*gam*pi**2.0)*x*exp(1.0-r**2.0)* &
    !    (1.0-(gam-1.0)/(8.0*gam*pi**2.0)*ep**2.0*exp(1.0-r**2.0))**(1.0/(gam-1.0)-1.0)
    !! drhody 
    !ep**2.0/(4.0*gam*pi**2.0)*y*exp(1.0-r**2.0)* &
    !    (1.0-(gam-1.0)/(8.0*gam*pi**2.0)*ep**2.0*exp(1.0-r**2.0))**(1.0/(gam-1.0)-1.0)
    !! dudx
    !x*y*ep/(2.0*pi)*exp(0.5*(1.0-r**2.0))
    !! dudy
    !(y*y-1.0)*ep/(2.0*pi)*exp(0.5*(1.0-r**2.0))
    !! dvdx
    !(1.0-x*x)*ep/(2.0*pi)*exp(0.5*(1.0-r**2.0))
    !! dvdy
    !-x*y*ep/(2.0*pi)*exp(0.5*(1.0-r**2.0))

    !! 11/14/2015 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !! debugging isentropic Euler using steady vortex, time independent solution
    !! define variables
    !gam = 1.400_FP
    !r = sqrt((xCart(1))**2.0_FP + (xCart(2))**2.0_FP)
    !ep = 1.0_FP
    !rho = ( 1.0_FP -  &
    !        (((gam-1.0_FP)*(ep**2.0_FP)*exp(1.0_FP-r**2.0_FP))/  &
    !        (8.0_FP*gam*pi**2.0_FP)) )**(1.0_FP/(gam-1.0_FP))
    !vel(1) = - ep*xCart(2)*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)
    !vel(2) =   ep*xCart(1)*exp(0.5_FP*(1.0_FP-r**2.0_FP))/(2.0_FP*pi)
    !drho(1) = ep**2.0_FP/(4.0_FP*gam*pi**2.0)*xCart(1)*exp(1.0_FP-r**2.0_FP)* &
    !    (1.0_FP - &
    !    (gam-1.0_FP)/(8.0_FP*gam*pi**2.0_FP)*ep**2.0_FP* &
    !    exp(1.0_FP-r**2.0_FP))**(1.0_FP/(gam-1.0_FP)-1.0_FP)
    !drho(2) = ep**2.0_FP/(4.0_FP*gam*pi**2.0)*xCart(2)*exp(1.0_FP-r**2.0_FP)* &
    !    (1.0_FP - &
    !    (gam-1.0_FP)/(8.0_FP*gam*pi**2.0_FP)*ep**2.0_FP* &
    !    exp(1.0_FP-r**2.0_FP))**(1.0_FP/(gam-1.0_FP)-1.0_FP)
    !du(1) = xCart(1)*xCart(2)*ep/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
    !du(2) = (xCart(2)*xCart(2)-1.0_FP)*ep/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
    !dv(1) = (1.0_FP-xCart(1)*xCart(1))*ep/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
    !dv(2) = -xCart(1)*xCart(2)*ep/(2.0_FP*pi)*exp(0.5_FP*(1.0_FP-r**2.0_FP))
    !c2 = gam*rho**(gam-1.0_FP) ! speed of sound

    !! analytic quantities
    !iterateSol(1,iPoint,iCell) = rho - tf*(vel(1)*drho(1) + vel(2)*drho(2) + &
    !    rho*(du(1)+dv(2)))   
    !iterateSol(2,iPoint,iCell) = vel(1) - tf*(vel(1)*du(1) + vel(2)*du(2) + &
    !    1.0_FP/rho*c2*(drho(1)))
    !iterateSol(3,iPoint,iCell) = vel(2) - tf*(vel(1)*dv(1) + vel(2)*dv(2) + &
    !    1.0_FP/rho*c2*(drho(2)))
    !iterateSol(4,iPoint,iCell) = 0.0_FP ! hold off on pressure until later  
    !! 11/14/2015 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end function steadyVortex
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D entropy wave, exact simple wave solution
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  12 October 2015 - Initial Creation
!>
function entropyWave(nEqns,x,y,t)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y, & !< input coordinates
                            t       !< time

    ! Function variable
    real(FP) :: entropyWave(nEqns)

    ! Local variables
    real(FP) :: amp,    & !< amplitude
                rhoInf, & !< density at infinity
                uInf,   & !< velocity at infinity
                vInf,   &
                pInf      !< pressure at infinity

    rhoInf = 2.0_FP
    uInf = 1.0_FP
    vInf = 0.5_FP
    pInf = 2.0_FP

    amp = 1.0_FP/3.0_FP
    !entropyWave(1) = rhoInf + amp*sin(2.0_FP*pi/5.0_FP*(x+y-(uInf+vInf)*t))  
    !entropyWave(1) = rhoInf + amp*sin(2.0_FP*pi/5.0_FP*((x-uInf*t)+(y-vInf*t)))  
    !entropyWave(1) = rhoInf + amp*sin(2.0_FP*pi/5.0_FP*((x-uInf*t)-(y-vInf*t)))  
    !entropyWave(1) = rhoInf + amp*sin(2.0_FP*pi/5.0_FP*((x-uInf*t)+(y-vInf*t)))  
    entropyWave(1) = rhoInf + amp*sin(2.0_FP*pi/5.0_FP*((x-uInf*t)))  
    entropyWave(2) = uInf
    entropyWave(3) = vInf
    entropyWave(4) = pInf

    !waveLen = 4.0_FP 
    !u1 = 1.0_FP
    !u2 = 5.0_FP/4.0_FP
    !u3 = 5.0_FP/4.0_FP
    !u4 = 1.0_FP

    !! in simple wave solutions, u = a. So u = F(x-2ut) is solved
    !! in burgers' equation, u = F(x-ut) is solved
    !wspeed = 0.0_FP ! 1 for burgers', 2 for simple wave
    !x1 = -waveLen + u1*wspeed*t
    !x2 = -0.5_FP*waveLen + u2*wspeed*t
    !x3 =  0.5_FP*waveLen + u3*wspeed*t
    !x4 =  waveLen + u4*wspeed*t
    !! density
    !if ( x < x1 ) then 
    !    entropyWave(1) = 1.0_FP 
    !else if ( x >= x1 .and. x < x2 ) then
    !    A = 1.0_FP/16.0_FP
    !    B = 0.5_FP
    !    C = 2.0_FP
    !    ! for Burgers' equation
    !    !entropyWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
    !    !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))
    !    entropyWave(1) = 1.0_FP*(A*(x-uInf*t)*(x-uInf*t) + B*(x-uInf*t) + C)
    !else if ( x >= x2 .and. x < x3 ) then
    !    A = -1.0_FP/16.0_FP
    !    B = 0.0_FP
    !    C = 3.0_FP/2.0_FP
    !    ! for Burgers' equation
    !    !entropyWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
    !    !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))
    !    entropyWave(1) = 1.0_FP*(A*(x-uInf*t)*(x-uInf*t) + B*(x-uInf*t) + C)
    !else if ( x >= x3 .and. x < x4 ) then
    !    A = 1.0_FP/16.0_FP
    !    B = -0.5_FP
    !    C = 2.0_FP        
    !    ! for Burgers' equation
    !    !entropyWave(1) = 2.0_FP*(A*x*x + B*x + C)/(2.0_FP*A*x*t + 1.0_FP*B*t + 1.0_FP + &
    !    !    sqrt(-4.0_FP*A*C*t*t + 1.0_FP*B*B*t*t + 4.0_FP*A*t*x + 2.0_FP*B*t + 1.0_FP))
    !    entropyWave(1) = 1.0_FP*(A*(x-uInf*t)*(x-uInf*t) + B*(x-uInf*t) + C)
    !else
    !    entropyWave(1) = 1.0_FP
    !end if
    !entropyWave(2) = uInf
    !entropyWave(3) = vInf
    !entropyWave(4) = pInf

end function entropyWave
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D vortex transport for full Euler equations
!>      slow vortex
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 December 2015 - Initial Creation
!>
function slowVortex(nEqns,x,y)

    use solverVars, only: pi
    use physics, only: Rgas, gam 
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y !< input coordinates

    ! Function variable
    real(FP) :: slowVortex(nEqns)

    ! Local variables
    real(FP), parameter :: xc = 0.05_FP,        & !< center of vortex
                           yc = 0.05_FP,        & !< center of vortex
                           pInf = 1.0e+05,    & !< uniform pressure, [N/m^2]
                           TInf = 300.0_FP,    & !< temperature, [K]
                           MInf = 0.05_FP,     & !< Mach number
                           RChar = 0.005_FP,   & !< characteristic radius
                           beta = 1.0_FP/50.0_FP !< strength

    real(FP) :: deltaU, & !< change in velocity
                deltaV, & !< change in velocity
                deltaT, & !< change in temperature
                uInf,   & !< freestream velocity
                rhoInf, & !< uniform density
                rho0,   & !< density
                u0,     & !< velocity
                v0,     & !< velocity
                p0,     & !< pressure
                T0,     & !< temperature
                rad,    & !< radius
                Cp        !< specific heat constant pressure

    uInf = MInf*sqrt(gam*Rgas*Tinf)
    rad = sqrt((x-xc)**2.0_FP + (y-yc)**2.0_FP)/RChar
    Cp = gam/(gam-1.0_FP)*Rgas

    deltaU = -(uInf*beta)*(y-yc)/RChar*exp(-0.5_FP*rad**2.0_FP)
    deltaV =  (uInf*beta)*(x-xc)/RChar*exp(-0.5_FP*rad**2.0_FP)
    deltaT = 1.0_FP/(2.0_FP*Cp)*(uInf*beta)**2.0_FP*exp(-rad**2.0_FP)

    
    u0 = uInf + deltaU
    v0 = deltaV
    T0 = TInf - deltaT 
    rhoInf = pInf/(Rgas*TInf)
    rho0 = rhoInf*(T0/TInf)**(1.0_FP/(gam-1.0_FP))
    p0 = rho0*Rgas*T0 

    slowVortex(1) = rho0
    slowVortex(2) = u0
    slowVortex(3) = v0 
    slowVortex(4) = p0 
    
end function slowVortex
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D vortex transport for full Euler equations
!>      fast vortex
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 December 2015 - Initial Creation
!>
function fastVortex(nEqns,x,y)

    use solverVars, only: pi
    use physics, only: Rgas, gam 
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y !< input coordinates

    ! Function variable
    real(FP) :: fastVortex(nEqns)

    ! Local variables
    real(FP), parameter :: xc = 0.05_FP,       & !< center of vortex
                           yc = 0.05_FP,       & !< center of vortex
                           pInf = 1.0e+05,     & !< uniform pressure, [N/m^2]
                           TInf = 300.0_FP,    & !< temperature, [K]
                           MInf = 0.5_FP,      & !< Mach number
                           RChar = 0.005_FP,   & !< characteristic radius
                           beta = 1.0_FP/5.0_FP  !< strength

    real(FP) :: deltaU, & !< change in velocity
                deltaV, & !< change in velocity
                deltaT, & !< change in temperature
                uInf,   & !< freestream velocity
                rhoInf, & !< uniform density
                rho0,   & !< density
                u0,     & !< velocity
                v0,     & !< velocity
                p0,     & !< pressure
                T0,     & !< temperature
                rad,    & !< radius
                Cp        !< specific heat constant pressure

    uInf = MInf*sqrt(gam*Rgas*Tinf)
    rad = sqrt((x-xc)**2.0_FP + (y-yc)**2.0_FP)/RChar
    Cp = gam/(gam-1.0_FP)*Rgas

    deltaU = -(uInf*beta)*(y-yc)/RChar*exp(-0.5_FP*rad**2.0_FP)
    deltaV =  (uInf*beta)*(x-xc)/RChar*exp(-0.5_FP*rad**2.0_FP)
    deltaT = 1.0_FP/(2.0_FP*Cp)*(uInf*beta)**2.0_FP*exp(-rad**2.0_FP)
    
    u0 = uInf + deltaU
    v0 = deltaV
    T0 = TInf - deltaT 
    rhoInf = pInf/(Rgas*TInf)
    rho0 = rhoInf*(T0/TInf)**(1.0_FP/(gam-1.0_FP))
    p0 = rho0*Rgas*T0 

    fastVortex(1) = rho0
    fastVortex(2) = u0
    fastVortex(3) = v0 
    fastVortex(4) = p0 
    
end function fastVortex
!-------------------------------------------------------------------------------
!> @purpose 
!>  2D acoustic test 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  13 May 2015 - Initial Creation
!>
function acousticTest(nEqns,x,y)

    use physics, only: gam, isentropicConst
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns !< equation number

    real(FP), intent(in) :: x, y !< input coordinates

    ! Function variable
    real(FP) :: acousticTest(nEqns)

    ! Local variables
    real(FP) :: xi,     &
                V,      &
                a,      &
                Amp,    &
                k,      &
                uInf

    uInf = 0.0_FP
    Amp = 1.0_FP
    k = 0.5_FP

    !a = isentropicConst*p/rho
    ! Burgers' equation
    !V = u + a
    !acousticTest(1) = (1.0_FP + 2.0_FP/(gam+1.0_FP)*V)**(2.0_FP/(gam-1.0_FP))
    acousticTest(1) = uInf + 2.0_FP/(gam+1.0_FP)*V 
    acousticTest(2) = uInf + 2.0_FP/(gam+1.0_FP)*V 
    acousticTest(3) = uInf + 2.0_FP/(gam+1.0_FP)*V 
    acousticTest(4) = uInf + 2.0_FP/(gam+1.0_FP)*V 

end function acousticTest
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for solid body rotation in 2d
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 August 2015 - Initial Creation
!>
function solidRotation(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y     !< y coord

    ! Function variable
    real(FP) :: solidRotation(nEqns)

    ! Local variables
    real(FP) :: xc, yc    !< center coord

    real(FP) :: r,      & !< radius 
                d,      & !< distance
                d2,     & !< distance
                k,      & !< decay coefficient
                kk,     & !< decay coefficient 2
                vMag,   & !< magnitude of velocity
                theta     !< angle

    xc = x!-0.0125_FP
    yc = y!-0.0125_FP

    d = 4.0_FP
    r = sqrt((xc)**2.0_FP + (yc)**2.0_FP)
    k = 0.5_FP
    kk = d

    solidRotation(1) = 1.0_FP
    solidRotation(4) = 2.0_FP

    ! with free bc
    solidRotation(2) = -k*(yc)
    solidRotation(3) =  k*(xc)

    !if ( r >= d ) then
    !    solidRotation(2) = 0.0_FP 
    !    solidRotation(3) = 0.0_FP
    !else
    !    solidRotation(2) = -k*(y)*tanh(kk*(r-d)**2.0_FP)
    !    solidRotation(3) =  k*(x)*tanh(kk*(r-d)**2.0_FP)
    !end if

end function solidRotation
!-------------------------------------------------------------------------------
!> @purpose 
!>  Pressureless euler systems exact solutions for two dimensional expansion case. 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 February 2015 - Initial Creation
!>
function plEulerExact2d(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  & !< y coord
                            t     !< simulation time

    ! Function variable
    real(FP) :: plEulerExact2d(nEqns)

    ! Local variables
    real(FP) :: xVal, yVal
    
    !xVal = x+6.0_FP
    xVal = x-6.0_FP
    yVal = y+6.0_FP

    plEulerExact2d(1) = (((xVal**2.0_FP+yVal**2.0_FP)/(5.0_FP+5.0_FP*t)**2.0_FP)**4.0_FP)/&
                           (5.0_FP+5.0_FP*t)**2.0_FP
    plEulerExact2d(2) = 5.0_FP/(5.0_FP+5.0_FP*t)*(xVal)
    plEulerExact2d(3) = 5.0_FP/(5.0_FP+5.0_FP*t)*(yVal)
    plEulerExact2d(4) = 1.0_FP

end function plEulerExact2d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for pressureless euler system with velocity divergence  
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial Creation
!>
function plEuler2d_v1(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x,       & !< x coord
                            y          !< y coord

    ! Function variable
    real(FP) :: plEuler2d_v1(nEqns)

    ! Local variable
    real(FP) :: k      !< decay coefficient 

    k = 0.1_FP
    
    !plEuler2d_v1(1) = 1.0_FP*exp( -k*(x**2.0_FP+y**2.0_FP) )
    plEuler2d_v1(1) = 1.0_FP
    plEuler2d_v1(2) = -1.0_FP/3.0_FP*(cos(pi*(x)/(5.0_FP))+2.0_FP)
    plEuler2d_v1(3) = 1.0_FP/3.0_FP*(sin(pi*(y)/(5.0_FP))+2.0_FP)
    plEuler2d_v1(4) = 1.0_FP

    !plEuler2d_v1(2) = -1.0_FP*(exp(-k*(x-y)))
    !plEuler2d_v1(3) = 1.0_FP*(exp(-k*(x-y)))

end function plEuler2d_v1
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialization for pressureless euler system 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  30 January 2015 - Initial Creation
!>
function plEuler2d_v2(nEqns,x,y)

    use solverVars, only: pi
    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns      !< equation number

    real(FP), intent(in) :: x, y       !< x+y coord

    ! Function variable
    real(FP) :: plEuler2d_v2(nEqns)
    
    plEuler2d_v2(1) = exp(sin(pi*(x+y)/(10.0_FP*sqrt(2.0_FP))))
    plEuler2d_v2(2) = -1.0_FP/3.0_FP*(cos(pi*(x+y)/(5.0_FP))+2.0_FP)
    plEuler2d_v2(3) =  1.0_FP/3.0_FP*(sin(pi*(x+y)/(5.0_FP))+2.0_FP)
    plEuler2d_v2(4) = 1.0_FP 

end function plEuler2d_v2
!-------------------------------------------------------------------------------
!> @purpose 
!>  1D Sod shock tube problem 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  13 March 2017 - Initial Creation
!>
function sodProblem1D(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Function variable
    real(FP) :: sodProblem1D(nEqns)

    ! Local variable 
    real(FP) :: x0,     & !< 
                x1,     & !<
                y0        !< 

    x0 = 0.0_FP
    x1 = 0.25_FP
    y0 = 0.0_FP

    if ( x <= x0 ) then
        sodProblem1D(1) = 1.0_FP 
        sodProblem1D(2) = 0.0_FP 
        sodProblem1D(3) = 0.0_FP 
        sodProblem1D(4) = 1.0_FP 
    else if ( ( x > x0 ) .and. ( x < x1 ) ) then
        sodProblem1D(1) = 0.625_FP 
        sodProblem1D(2) = 0.0_FP 
        sodProblem1D(3) = 0.0_FP 
        sodProblem1D(4) = 0.6_FP 
    else
        sodProblem1D(1) = 0.25_FP 
        sodProblem1D(2) = 0.0_FP 
        sodProblem1D(3) = 0.0_FP 
        sodProblem1D(4) = 0.2_FP 
    end if

end function sodProblem1D
!-------------------------------------------------------------------------------
!> @purpose 
!>  1D Sod shock tube problem 
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  13 March 2017 - Initial Creation
!>
function sodProblem2D(nEqns,x,y,t)

    implicit none

    ! Interface variables
    integer, intent(in) :: nEqns  !< equation number

    real(FP), intent(in) :: x,  & !< x coord
                            y,  &   !< y coord
                            t

    ! Function variable
    real(FP) :: sodProblem2D(nEqns)

    ! Local variable 
    real(FP) :: r,      & !< radius 
                r0,     & !< radius reference 
                x0,     & !< initial location
                y0        !< initial location

    r0 = 2.0_FP 
    r = sqrt(x**2.0_FP+y**2.0_FP)

    if ( r <= r0 ) then
        sodProblem2D(1) = 1.0_FP 
        sodProblem2D(2) = 0.0_FP 
        sodProblem2D(3) = 0.0_FP 
        sodProblem2D(4) = 1.0_FP 
    else
        sodProblem2D(1) = 0.125_FP 
        sodProblem2D(2) = 0.0_FP 
        sodProblem2D(3) = 0.0_FP 
        sodProblem2D(4) = 0.1_FP 
    end if

end function sodProblem2D
!-------------------------------------------------------------------------------
end module analyticFunctions
