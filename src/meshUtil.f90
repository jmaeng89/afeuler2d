!-------------------------------------------------------------------------------
!> @purpose 
!>  Mesh related routines
!>
!> @author
!>  Timothy A. Eymann
!>  
!> @history
!>  8 May 2015 - Added acNodeCells to be used in acoustic solver (Maeng)
!>  10 May 2015 - Added acFaceCells acEdgeCells (Maeng)
!>
module meshUtil

    use solverVars, only: FP
    implicit none

    integer :: nDim,            & !< number of dimensions
               nCells,          & !< number of physical cells
               nGhostCells,     & !< number of ghost cells
               nAllCells,       & !< total number of cells
               physCellStart,   & !< starting index of physical cells
               physCellEnd,     & !< ending index of physical cells
               nNodes,          & !< number of physical nodes
               nAllNodes,       & !< total number of nodes
               physNodeStart,   & !< starting index of physical nodes
               physNodeEnd,     & !< ending index of physical nodes
               nZones,          & !< number of zones in mesh
               nPatches,        & !< number of boundary patches in mesh
               nFaces,          & !< number of faces in mesh
               nEdges,          & !< number of edges in mesh
               maxNodesPerFace, & !< maximum number of nodes per face
               maxFacesPerCell, & !< maximum number of faces per cell
               maxNodesPerCell    !< maximum number of nodes per cell

    integer, allocatable :: cellFaces(:,:),     & !< faces of a cell
                            cellNodes(:,:),     & !< nodes in a cell
                            faceNodes(:,:),     & !< nodes in a face
                            faceCells(:,:),     & !< cells sharing face
                            edgeCell(:,:),      & !< cell connected to edge
                            nodeCell(:,:)         !< cell connected to node
                            !acEdgeCells(:,:,:), & !< cells sharing edge, acoustcis
                            !acFaceCells(:,:,:), & !< cells sharing face, acoustics
                            !acNodeCells(:,:,:)    !< cells sharing node, acoustics
                                ! acoustics connectivity contains local edge index

    real(FP), allocatable :: cellCentroid(:,:), & !< cell centroid coordinates
                             cellVolume(:),     & !< cell volume ( area / width )
                             cellDetJac(:),     & !< determinant of Jacobian
                             cellInvJac(:,:,:), & !< inverse Jacobian
                             cellJac(:,:,:),    & !< Jacobian
                             cellAngles(:,:),   & !< interior angles for cell
                             faceArea(:),       & !< face area (edge length)
                             faceNormal(:,:),   & !< face normal (points to right cell)
                             edgeCoord(:,:),    & !< coordinate of edge midpoint
                             nodeCoord(:,:),    & !< node coordinates
                             xiNode(:,:),       & !< node coord in ref. space
                             xiEdge(:,:),       & !< edge coord in ref. space
                             xMax(:),           & !< maximum extent of grid
                             xMin(:)              !< minimum extent of grid

    real(FP) :: totalVolume     !< total volume of domain

    ! mesh/geometry variable
    real(FP) :: lMin !< minimum distance in mesh

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Fill connectivity (mapping) arrays and calculate mesh geometry
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  8 March 2012 - Initial Creation
!>  8 May 2012 - Add 1-D connectivity
!>  8 May 2015 - Added acNodeCells to be used in acoustic solver (Maeng)
!>  10 May 2015 - Added acEdgeCells acFaceCells (Maeng)
!>
subroutine connect

    use solverVars, only: iLeft, iRight, dtIn, pi, eps, &
                    truncDecPts, SHORTINT, LONGINT
    use mathutil, only: pointInTri
#ifdef FORT2K8
    use mathutil, only: norm2
#endif
    implicit none

    ! Local variables
    integer :: iCell,   & !< cell index
               jCell,   & !< cell index
               iFace,   & !< face index
               jFace,   & !< face index
               iNode,   & !< node index
               jNode,   & !< node index
               node1,   &
               node2,   &
               node3,   & !< ordered nodes
               face1,   &
               face2,   &
               face3,   & !< ordered faces
               tFace,   & !< temporary face storage
               iDir,    & !< direction index
               iSide ,  & !< left/right index
               iP1,     & !< i+1 index
               iM1       !< i-1 index

    real(FP) :: coordSum,           & !< sum of coordinate values (for average)
                xN(nDim),           & !< 
                xVert(nDim,maxNodesPerCell), &
                xP1(nDim),          & !< x(i+1)
                xM1(nDim),          & !< x(i-1)
                x(maxNodesPerCell), & !< x-coordinates of cell nodes
                y(maxNodesPerCell), & !< y-coordinates of cell nodes
                xDiff(nDim),        & !< difference in node coordinates
                tVec(nDim)            !< tangential vector

    real(FP), parameter :: ds = 1.0e-13_FP ! slightly positive number
    real(FP), parameter :: coordTol = 1.0e-13_FP ! geometric coordinate tolerance

    totalVolume = 0.0_FP
    xMax(:) = -1.0e6_FP
    xMin(:) = 1.0e6_FP

    ! fill cell to face mapping array ( not ordered )
    do iFace = 1, nFaces
        do iSide = iLeft, iRight
            jCell = faceCells(iSide,iFace)

            ! only operate on physical cells
            if ( jCell > 0 ) then
                if ( cellFaces(1,jCell) == 0 ) then
                    cellFaces(1,jCell) = iFace
                else
                    ! find next open spot in array
                    jFace = 2
                    do while ( abs(cellFaces(jFace,jCell)) > 0 ) 
                        jFace = jFace + 1
                    end do
                    cellFaces(jFace,jCell) = iFace
                end if
            end if
        end do
    end do

    ! loop over cells to order nodes, faces, and calculate Jacobian
    select case ( nDim )

        case (1)

            do iCell = 1, nCells
                do iNode = 1, maxNodesPerCell
                    cellNodes(iNode,iCell) = iCell + (iNode-1)
                    cellFaces(iNode,iCell) = iCell + (iNode-1)
                end do
                
                node1 = iCell
                node2 = iCell+1
                x(1) = nodeCoord(1,node1)
                x(2) = nodeCoord(1,node2)
                
                cellJac(1,1,iCell) = x(2) - x(1)
                cellDetJac(iCell) = cellJac(1,1,iCell)
                cellInvJac(1,1,iCell) = 1.0_FP/cellJac(1,1,iCell)
                cellVolume(iCell) = cellJac(1,1,iCell)
                cellCentroid(:,iCell) = 0.5_FP*sum(x(:))

                ! store grid extents
                xMax(1) = max(xMax(1),maxVal(x(:)))
                xMin(1) = min(xMin(1),minVal(x(:)))

                ! calculate total volume of domain
                totalVolume = totalVolume + cellVolume(iCell)
            end do

        case (2)

            do iCell = 1, nCells
              
                ! assume cell nodes/faces are properly ordered
                face1 = cellFaces(1,iCell)
                node1 = faceNodes(1,face1)
                node2 = faceNodes(2,face1)
                
                do iFace = 2,maxFacesPerCell
                    if ((faceNodes(1,cellFaces(iFace,iCell))==node1).or. &
                        (faceNodes(2,cellFaces(iFace,iCell))==node1)) then
                        face3 = cellFaces(iFace,iCell)
                        if ( faceNodes(1,face3) == node1 ) then
                            node3 = faceNodes(2,face3)
                        else
                            node3 = faceNodes(1,face3)
                        end if
                    else
                        face2 = cellFaces(iFace,iCell)
                    end if
                end do
 
                ! enforce ccw ordering 
                if ( (nodeCoord(1,node2)-nodeCoord(1,node1)) * &
                     (nodeCoord(2,node3)-nodeCoord(2,node1)) - &
                     (nodeCoord(1,node3)-nodeCoord(1,node1)) * &
                     (nodeCoord(2,node2)-nodeCoord(2,node1)) < 0.0_FP ) then
                   
                    ! use node 1 for temp storage to flip nodes 2,3
                    node1 = node2
                    node2 = node3
                    node3 = node1
                    node1 = faceNodes(1,face1)

                    ! flip faces 1,3
                    tFace = face1
                    face1 = face3
                    face3 = tFace
                end if
                cellNodes(:,iCell) = (/node1,node2,node3/)

                ! order faces so that cellFaces(1,iCell) is opposite "node 1"
                cellFaces(:,iCell) = (/face2,face3,face1/)

                ! find Jacobian
                x(:) = nodeCoord(1,cellNodes(:,iCell))
                y(:) = nodeCoord(2,cellNodes(:,iCell))

                ! shift the difference and round in order to reduce round
                ! off error - I don't think this is a good idea
                cellJac(1,1,iCell) = x(2) - x(1)
                cellJac(1,2,iCell) = x(3) - x(1)
                cellJac(2,1,iCell) = y(2) - y(1)
                cellJac(2,2,iCell) = y(3) - y(1)
                !cellDetJac(iCell) = (x(2)-x(1))*(y(3)-y(1)) - &
                !                    (x(3)-x(1))*(y(2)-y(1))
                cellDetJac(iCell) = (cellJac(1,1,iCell)*cellJac(2,2,iCell)) - &
                                    (cellJac(1,2,iCell)*cellJac(2,1,iCell))

                cellInvJac(1,1,iCell) = y(3)-y(1)
                cellInvJac(1,2,iCell) = x(1)-x(3)
                cellInvJac(2,1,iCell) = y(1)-y(2)
                cellInvJac(2,2,iCell) = x(2)-x(1)
                cellInvJac(:,:,iCell) = (1.0_FP/cellDetJac(iCell)) * &
                                        cellInvJac(:,:,iCell)
                !write(*,*) iCell, cellInvJac(1,1,iCell), cellInvJac(1,2,iCell), &
                !    cellInvJac(2,1,iCell), cellInvJac(2,2,iCell)
                !if ( iCell == 90 ) write(*,*) cellInvJac(1,:,iCell)

                cellVolume(iCell) = 0.5_FP*cellDetJac(iCell)
                cellCentroid(:,iCell) = 1.0_FP/3.0_FP*(/sum(x(:)),sum(y(:))/)

                !write(*,*) cellVolume(iCell)
                !if ( iCell == 22 .or. iCell == 24 ) then
                !write(*,*) iCell, cellVolume(iCell), cellInvJac(:,:,iCell), cellDetJac(iCell)
                !end if

                ! store grid extents
                xMax(1) = max(xMax(1),maxVal(x(:)))
                xMin(1) = min(xMin(1),minVal(x(:)))
                xMax(2) = max(xMax(2),maxVal(y(:)))
                xMin(2) = min(xMin(2),minVal(y(:)))

                ! check for global node and edge for the cell
                !if ( iCell == 29 .or. iCell == 27 .or.  &
                !    iCell == 19 .or. iCell == 23 ) then
                !    write(*,*) 'cell: ', iCell
                !    write(*,*) 'node: ', cellNodes(:,iCell)
                !    write(*,*) 'edge: ', cellFaces(:,iCell)
                !end if

                ! calculate total volume of domain
                totalVolume = totalVolume + cellVolume(iCell)

            end do

    end select

    ! Loop over faces to calculate normals and coordinates
    do iFace = 1, nFaces

        ! calculate edge coordinate
        do iDir = 1, nDim
            coordSum = 0.0_FP
            do iNode = 1, maxNodesPerFace
                coordSum = coordSum + nodeCoord(iDir,faceNodes(iNode,iFace))
            end do
            ! in 1D and 2D, faces and edges are the same thing...
            !edgeCoord(iDir,iFace) = coordSum / maxNodesPerFace
            edgeCoord(iDir,iFace) = coordSum / real(maxNodesPerFace,FP)
        end do

        ! only need faceCells for 2d solver, but edge array needed for 3d
        edgeCell(1,iFace) = faceCells(iLeft,iFace)

        ! calculate face normals and face area
        select case ( nDim )

            case (1)
                if ( iFace == 1 ) then
                    faceNormal(:,iFace) = -1.0_FP
                else
                    faceNormal(:,iFace) = 1.0_FP
                end if
                faceArea(iFace) = 1.0_FP

            case (2)
                xDiff(:) = nodeCoord(:,faceNodes(2,iFace)) - &
                           nodeCoord(:,faceNodes(1,iFace))
                tVec(:) = xDiff(:)/norm2(xDiff(:))
               
                faceNormal(1,iFace) = -tVec(2)
                faceNormal(2,iFace) =  tVec(1)

                ! make sure normal points away from "left" cell
                iCell = faceCells(iLeft,iFace)
                xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
                xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
                !xN(:) = edgeCoord(:,iFace) - dtIn*faceNormal(:,iFace)
                xN(:) = edgeCoord(:,iFace) - ds*faceNormal(:,iFace)
                if ( .not. pointInTri(xN,xVert) ) then
#ifdef VERBOSE
                    write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
#endif
                    !write(*,'(a,i0)') 'WARNING: Flipping normal for face ',iFace
                    faceNormal(:,iFace) = -1.0_FP*faceNormal(:,iFace)
                end if

                faceArea(iFace) = norm2(xDiff(:))
        
        end select

    end do

    ! node to cell mapping (needed to start neighbor search)
    do iNode = 1, nNodes

        ! find first cell containing node
        do iCell = 1, nCells
            do jNode = 1, maxNodesPerCell
                if ( cellNodes(jNode,iCell) == iNode ) then
                    nodeCell(1,iNode) = iCell
                    exit
                end if
            end do
            if ( nodeCell(1,iNode) /= 0 ) exit
        end do

    end do

    inode = 0
    do iCell = 1, nCells
#ifdef VERBOSE
        ! check for negative Jacobians ( clockwise ordering )
        if ( cellDetJac(iCell) < 0 ) then
            inode = inode + 1
            write(*,*) ' cell: ',iCell
            write(*,*) 'nodes: ',cellNodes(:,iCell)
            write(*,*) 'faces: ',cellFaces(:,iCell)
            read(*,*)
        end if
#endif
        ! calculate angles in each cell
        if ( nDim == 2 ) then
            do iNode = 1, 3
                iP1 = iNode + 1; if ( iP1 > 3 ) iP1 = 1
                iM1 = iNode - 1; if ( iM1 < 1 ) iM1 = 3

                xP1 = nodeCoord(:,cellNodes(ip1,iCell))
                xM1 = nodeCoord(:,cellNodes(im1,iCell))
                xN = nodeCoord(:,cellNodes(iNode,iCell))
                cellAngles(iNode,iCell) = &
                    acos( dot_product(xP1-xN,xM1-xN) / &
                          ( faceArea(cellFaces(iM1,iCell)) * &
                            faceArea(cellFaces(iP1,iCell)) ) )
            end do
#ifdef VERBOSE
            if ( abs(sum(cellAngles(:,iCell))-pi) > 4.0*eps ) then
                write(*,*) 'WARNING: cell angle sum incorrect.'
                write(*,*)abs(sum(cellAngles(:,iCell))-pi) 
            end if
#endif
        end if
    end do

    ! node and edge coordinates in reference space
    select case ( nDim )
        case (1)
            xiNode(:,1) = 0.0_FP
            xiNode(:,2) = 1.0_FP

            ! set the same as xiNode, in 1D xiNode = xiEdge
            xiEdge(:,1) = 0.0_FP
            xiEdge(:,2) = 1.0_FP

        case (2)
            xiNode(:,1) = (/0.0_FP, 0.0_FP/)
            xiNode(:,2) = (/1.0_FP, 0.0_FP/)
            xiNode(:,3) = (/0.0_FP, 1.0_FP/)

            xiEdge(:,3) = (/0.5_FP, 0.0_FP/)
            xiEdge(:,1) = (/0.5_FP, 0.5_FP/)
            xiEdge(:,2) = (/0.0_FP, 0.5_FP/)
    end select

end subroutine connect
!-------------------------------------------------------------------------------
!> @purpose 
!>  Evaluate face normal vector and face area for a specific face and cell
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 January 2016 - Initial creation
!>
subroutine faceNormalArea(nDim,iFace,iCell,fNormal,fArea)

    use solverVars, only: iLeft, iRight, dtIn, pi, eps
    use mathutil, only: pointInTri

    implicit none

    !> Interface variables
    integer :: nDim,    & !< problem dimension
               iFace,   & !< global face index
               iCell      !< global cell index

    real(FP), intent(out) :: fNormal(nDim),    & !< face normal vector
                             fArea               !< face area

    !> Local variables
    real(FP), parameter :: ds = 1.0e-13_FP ! slightly positive number

    real(FP) :: xN(nDim),                    & !< 
                xVert(nDim,maxNodesPerCell), &
                xDiff(nDim),                 & !< difference in node coordinates
                tVec(nDim)                     !< tangential vector

    ! calculate face normals and face area
    select case ( nDim )

        case (1)
            if ( iFace == 1 ) then
                fNormal(:) = -1.0_FP
            else
                fNormal(:) = 1.0_FP
            end if
            fArea = 1.0_FP

        case (2)
            xDiff(:) = nodeCoord(:,faceNodes(2,iFace)) - &
                       nodeCoord(:,faceNodes(1,iFace))
            tVec(:) = xDiff(:)/norm2(xDiff(:))
           
            fNormal(1) = -tVec(2)
            fNormal(2) =  tVec(1)

            ! make sure normal points away from "left" cell
            xVert(1,:) = nodeCoord(1,cellNodes(:,iCell))
            xVert(2,:) = nodeCoord(2,cellNodes(:,iCell))
            !xN(:) = edgeCoord(:,iFace) - dtIn*fNormal(:)
            xN(:) = edgeCoord(:,iFace) - ds*fNormal(:)
            if ( .not. pointInTri(xN,xVert) ) then
                fNormal(:) = -1.0_FP*fNormal(:)
            end if

            fArea = norm2(xDiff(:))
    
    end select
end subroutine faceNormalArea
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find node angle
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  26 August 2016 - Initial Creation
!>
function nodeAngle(nDim,iNode,iCell)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iNode,   & !< local node index
                           iCell      !< cell index

    ! Function output
    real(FP) :: nodeAngle
    
    ! Local variables
    integer :: iEdge,   & !< edge index
               lEdge,   & !< edge index
               rEdge    

    if ( nDim < 2 ) then
        write(*,*) 'ERROR: no angles for 1 dimensional problem'
        stop
    end if

    ! local edge index
    iEdge = iNode
    lEdge = mod(iNode+1,3)
    if ( lEdge == 0 ) lEdge = 3
    rEdge = mod(iNode+2,3)
    if ( rEdge == 0 ) rEdge = 3
    
    ! node angle
    nodeAngle = acos( (faceArea(cellFaces(lEdge,iCell))**2.0_FP + &
                   faceArea(cellFaces(rEdge,iCell))**2.0_FP - &
                   faceArea(cellFaces(iEdge,iCell))**2.0_FP)  / &
        (2.0_FP*faceArea(cellFaces(lEdge,iCell))*faceArea(cellFaces(rEdge,iCell))) )

end function nodeAngle
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert reference coordinates to Cartesian coordinates
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation
!>
function ref2cart(nDim,iCell,xi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< simulation dimension
                           iCell      !< cell index

    real(FP), intent(in) :: xi(nDim) !< reference coordinates

    ! Function variable
    real(FP) :: ref2cart(nDim)

    ! Local variables
    integer :: j !< column index

    real(FP) :: Jcol(nDim), & !< column of cell Jacobian
                Jprod(nDim)   !< product of Jacobian and reference vector
    

    Jprod = 0.0_FP
    do j = 1, nDim
        Jcol(:) = cellJac(:,j,iCell)
        Jprod(:) = Jprod(:) + xi(j)*Jcol(:)
    end do

    ref2cart(:) = nodeCoord(:,abs(cellNodes(1,iCell))) + Jprod(:)

end function ref2cart
!-------------------------------------------------------------------------------
!> @purpose 
!>  Convert Cartesian coordinates to reference coordinates
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation
!>
function cart2ref(nDim,iCell,x)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< simulation dimension
                           iCell      !< cell index

    real(FP), intent(in) :: x(nDim) !< reference coordinates

    ! Function variable
    real(FP) :: cart2ref(nDim)

    ! Local variables
    integer :: j !< column index

    real(FP) :: xDiff(nDim),    & !< difference of Cartesian coordinates
                Jcol(nDim),     & !< column of cell inverse Jacobian
                Jprod(nDim)       !< product of inverse Jacobian and Cart. vector
 

    xDiff(:) = x(:) - nodeCoord(:,abs(cellNodes(1,iCell)))

    Jprod = 0.0_FP
    do j = 1, nDim
        Jcol(:) = cellInvJac(:,j,iCell)
        Jprod(:) = Jprod(:) + xDiff(j)*Jcol(:)
    end do

    cart2ref(:) = Jprod(:)

end function cart2ref
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find global minimum distance for time step constraint
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 April 2016 - Initial creation
!>
subroutine minDistance(minLen)
    
    !use solverVars, only: lMin

    implicit none

    !< Interface variable
    real(FP), intent(out) :: minLen

    !< Local variable
    integer :: iCell,   & !< cell index
               iEdge,   & !< edge index
               gEdge      !< global edge index

    minLen = 1.0e06

    select case ( nDim )

        case ( 1 )
            do iCell = 1,nCells
                minLen = min( minLen, cellVolume(iCell) )
            end do

        case ( 2 )
            do iCell = 1,nCells
                do iEdge = 1, maxFacesPerCell
                    gEdge = cellFaces(iEdge,iCell)
                    minLen = min( minLen, cellVolume(iCell)/faceArea(gEdge) )
                end do
            end do

        case default
            write(*,*) 'Error: 3D not implemented'
            stop

    end select

end subroutine minDistance
!-------------------------------------------------------------------------------
!> @purpose 
!>  Find cell containing Cartesian coordinate.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 March 2012 - Initial Creation\n
!>  8 May 2012 - Extend to 1-D cases
!>
recursive function findCell(nDim,iCell,x) result (cellIndex)

    use solverVars, only : iLeft, iRight
    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< dimensions
                           iCell      !< starting point of neighbor search

    real(FP), intent(in) :: x(nDim) !< coordinate

    ! Function variable
    integer :: cellIndex

    ! Local variables
    integer :: jCell,   & !< next cell to search
               iFace      !< reference face to cross for neighbor walk

    real(FP) :: xi(nDim) !< search point in reference coordinates
  

    xi(:) = cart2ref(nDim,iCell,x)
   
    select case ( nDim )

        case (1)
            if ( ( xi(1) >= 0.0_FP ) .and. ( xi(1) <= 1.0_FP ) ) then
                cellIndex = iCell
                return
            elseif ( xi(1) < 0.0_FP ) then
                iFace = 1
            elseif ( xi(1) > 1.0_FP ) then
                iFace = 2
            else
                write(*,*) 'ERROR: problem with neighbor walk logic.'
                stop
            end if

        case (2)
            if ( (( xi(2) <= 1.0_FP ) .and. ( xi(2) >= 0.0_FP )) .and. &
                 (( xi(1) >= 0.0_FP ) .and. ( xi(1) <= 1.0_FP-xi(2))) ) then 
                
                ! coordinate is inside cell
                cellIndex = iCell
                return

            elseif ( xi(2) < 0.0_FP ) then ! look across face 3
                iFace = 3
            
            elseif ( xi(1) > 1.0_FP - xi(2) ) then ! look across face 1
                iFace = 1

            elseif ( xi(1) < 0.0_FP ) then ! look across face 2
                iFace = 2

            else
                write(*,*) 'ERROR: problem with neighbor walk logic.'
                stop
            end if
    end select

    ! move to neighbor cell
    if ( faceCells(iLeft,cellFaces(iFace,iCell)) == iCell ) then
        jCell = faceCells(iRight,abs(cellFaces(iFace,iCell)))
    else    
        jCell = faceCells(iLeft,abs(cellFaces(iFace,iCell)))
    end if

    if ( jCell <= 0 ) then
        write(*,*) 'Negative or zero cell index encountered in neighbor walk...'
        write(*,*) 'jCell: ',jCell
        write(*,*) 'iCell: ',iCell
        write(*,*)
        write(*,*) '  cell face: ',iFace
        write(*,*) 'global face: ',cellFaces(iFace,iCell)
        write(*,*) '     neighs: ',faceCells(:,cellFaces(iFace,iCell))
        read(*,*)
    end if
    cellIndex = findCell(nDim,jCell,x)

end function findCell
!-------------------------------------------------------------------------------
end module meshUtil
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Return neighboring cell information for a specific global edge
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  16 February 2016 - Initial creation
!!>
!subroutine faceNeighborInfo(nDim,iFace,iCell,fNormal,fArea)
!
!    implicit none
!
!    !> Interface variables
!    integer :: nDim,    & !< problem dimension
!               iFace,   & !< global face index
!               iCell      !< global cell index
!
!    real(FP), intent(out) :: fNormal(nDim),    & !< face normal vector
!                             fArea               !< face area
!
!    lCell = faceCells(iLeft,iEdge)
!    rCell = faceCells(iRight,iEdge) ! ghost cell or physical periodic cell
!    node1 = faceNodes(iLeft,iEdge)
!    node2 = faceNodes(iRight,iEdge)
!    if ( rCell < 0 ) then 
!        ! for periodic BC, negative cell index
!        rCell = abs(faceCells(iRight,iEdge))
!        pEdge = pEdgePair(iEdge)
!    end if
!
!    ! find residual surrounding edge, two from lCell two from rCell
!    ! left cell
!    do jEdge = 1,maxFacesPerCell
!        gEdge = cellFaces(jEdge,lCell)
!        if ( gEdge == iEdge ) then
!            node3 = cellNodes(jEdge,lCell)
!            cycle
!        end if
!
!    end do
!
!    ! right cell
!    do jEdge = 1,maxFacesPerCell
!        gEdge = cellFaces(jEdge,rCell)
!        if ( edgeBC(iEdge) > 0 ) then
!            ! periodic boundary edge 
!            ! TODO: ghost cell needed 
!            if ( gEdge == pEdge ) then
!                node4 = cellNodes(jEdge,rCell) ! not sure completely
!                cycle
!            end if
!        else
!            if ( gEdge == iEdge ) then 
!                node4 = cellNodes(jEdge,rCell)
!                cycle
!            end if
!        end if 
!
!    end do   
!
!end subroutine faceNeighborInfo
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Fill acoustics node/edge connectivity arrays, with no regards for boundary
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  8 May 2015 - Initial creation.
!!>  10 May 2015 - Added acEdgeCells acFaceCells (Maeng)
!!>
!subroutine setAcNodeEdge
!
!    use solverVars, only: iLeft, iRight
!
!    implicit none
!
!    ! Local variables
!    integer :: iCell,   & !< cell index
!               jCell,   & !< cell index
!               iFace,   & !< face index
!               jFace,   & !< face index
!               iNode,   & !< node index
!               jNode,   & !< node index
!               iCount     !< counter
!
!    ! Loop over faces to find acEdgeCells, acFaceCells
!    do iFace = 1, nFaces
!
!        ! find acEdgeCells acFaceCells to be used in ACOUSTICS edge update
!        select case ( nDim )
!
!            case (1)
!            ! 2 cells sharing the edge at all times
!                iCount = 1
!                do iCell = 1,nCells
!                    do jFace = 1, maxFacesPerCell
!                        if ( cellFaces(jFace,iCell) == iFace ) then
!                            ! global cell number 
!                            acEdgeCells(1,iFace,iCount) = iCell
!                            ! local edge index
!                            acEdgeCells(2,iFace,iCount) = jFace
!                            iCount = iCount + 1
!                            ! there are only two neighboring cells
!                            if ( iCount > 2 ) exit
!                        end if
!                    end do
!                end do
!
!            case (2)
!            ! 2 cells sharing the edge at all times
!                iCount = 1 ! local index 
!                do iCell = 1,nCells
!                    do jFace = 1, maxFacesPerCell
!                        if ( cellFaces(jFace,iCell) == iFace ) then
!                            ! global cell number 
!                            acEdgeCells(1,iFace,iCount) = iCell
!                            ! local edge index
!                            acEdgeCells(2,iFace,iCount) = jFace
!                            iCount = iCount + 1
!                            ! there are only two neighboring cells
!                            if ( iCount > 2 ) exit
!                        end if
!                    end do
!                end do
!
!        end select
!        
!    end do
!    ! 1 and 2d acFaceCells == acEdgeCells
!    acFaceCells(:,:,:) = acEdgeCells(:,:,:) 
!
!    ! node to cell mapping (needed to start neighbor search)
!    do iNode = 1, nNodes
!
!        ! find acNodeCells to be used in ACOUSTICS nodal update
!        select case ( nDim )
!
!            case (1)
!            ! 2 cells sharing the node at all times
!                iCount = 1
!                do iCell = 1,nCells
!                    do jNode = 1, maxNodesPerCell
!                        if ( cellNodes(jNode,iCell) == iNode ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,iCount) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,iCount) = jNode
!                            iCount = iCount + 1
!                            ! there are only two neighboring cells
!                            if ( iCount > 2 ) exit
!                        end if
!                    end do
!                end do
!
!            case (2)
!                ! many cells sharing the node
!                ! TODO: how many cells share the node? 
!                iCount = 1 ! local index 
!                do iCell = 1,nCells
!                    do jNode = 1, maxNodesPerCell
!                        if ( cellNodes(jNode,iCell) == iNode ) then
!                            ! global cell number 
!                            acNodeCells(1,iNode,iCount) = iCell
!                            ! local node index
!                            acNodeCells(2,iNode,iCount) = jNode
!                            iCount = iCount + 1
!                        end if
!                    end do
!                end do
!
!        end select
!
!    end do
!
!    !! debug: write to file the connectivities
!    !open (unit=43210,file="acEdge.dat",action="write",status="replace")
!    !do iFace = 1,nFaces
!    !write (43210,'(i4,i4,i4,i4,i4)') iFace, acEdgeCells(1,iFace,1),acEdgeCells(1,iFace,2)
!    !end do 
!    !close (43210)
!
!    !! debug: write to file the connectivities
!    !open (unit=43211,file="acNode.dat",action="write",status="replace")
!    !do iNode = 1,nNodes
!    !    write (43211,'(i4,10i4)') iNode, acNodeCells(1,iNode,:)
!    !end do 
!    !close (43211)
!
!end subroutine setAcNodeEdge
