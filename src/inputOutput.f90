!-------------------------------------------------------------------------------
!> @purpose 
!>  Provides input and output functions
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  10 April 2015 - Added additional visualization routines (Maeng)
!>
module inputOutput

    implicit none

    integer, parameter :: fileLength = 200 !< length of a file string

    character(fileLength) :: filePath,        & !< base path for files
                             drvFileName,     & !< driver file name
                             solFileName,     & !< solution file name
                             reconFileName,   & !< combined mesh/sol file name
                             inputFileName,   & !< combined mesh/sol file name = input
                             meshFileName,    & !< mesh file
                             bcFileName,      & !< boundary condition file
                             histFileName,    & !< history file
                             residFileName,   & !< residual history file name
                             auxFileName,     & !< auxiliary data file 
                             auxFileName2,    & !< auxiliary data file 
                             dumpReconName,   & !< reconstruction dump file name
                             dumpSolName,     & !< solution dump file name
                             dumpPrompt,      & !< data dump file prompt
                             shutDownPrompt     !< simulation shut down prompt

    character(fileLength) :: reconFileName_copy,  & !< reconstruction file name copy
                             solFileName_copy       !< solution file name copy

    integer, parameter :: drvFile = 10,        & !< input file unit
                          solFile = 20,        & !< solution file unit
                          meshFile = 30,       & !< mesh file unit
                          reconFile = 40,      & !< reconstruction file unit
                          inputFile = 1100,    & !< solution recon. input file unit
                          bcFile = 50,         & !< initialization file
                          histFile = 60,       & !< history file unit
                          residFile = 90,      & !< residual history file unit
                          auxFile = 70,        & !< aux file unit
                          auxFile2 = 80,       & !< aux file unit
                          dumpReconFile = 124, & !< reconstruction dump file unit
                          dumpPromptFile = 121,& !< solution dump file prompt unit
                          shutDownPromptFile = 135 !< simulation shut down prompt unit

    logical :: solInput = .false.      !< solution recon. input file flag. Default = false

    integer :: solOutputFreq        !< frequency of solution output

contains
!----------------------------------------------------------------------
!> @purpose 
!>  Gather driver (input) file from command line; make sure it exists
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 July 2010 - Initial Creation
!>
subroutine getDriverFile

    use solverVars
    use meshUtil
    implicit none

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    integer :: nArgs    !< number of command line arguments
    
    character(fileLength)   :: executable   !< name of program

    call getarg(0,executable)
    
    ! Check for proper command line arguments
    nArgs = iargc()
    if ( nArgs .lt. 1 ) then  
        write(*,*)
        write(*,10) 'ERROR: Too few arguments.'
        write(*,*) 
        write(*,10) 'Usage:'
        write(*,20) trim(adjustl(executable)),' driverFile.drv'
        write(*,*)
        stop
    else
        call getarg(1, drvFileName)
       
        ! Make sure file exists
        inquire( file = trim(adjustl(drvFileName)), exist = isFile )
        if ( .not. isFile ) then
            write(*,*)
            write(*,'(3A)') "ERROR: Driver file '",                        &
                              trim(adjustl(drvFileName)), "' does not exist."
            write(*,*)
            stop
        end if
    end if

 10 format ( 1X, A )
 20 format ( 3X, 2A )

end subroutine
!-------------------------------------------------------------------------------
!> @purpose 
!>  Parse input file 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 July 2010 - Initial creation
!>  7 March 2012 - Adapted from burgSolv version
!>  27 April 2016 - Replaced dtIn, nIter in time parameter with cfl, tf
!>
subroutine parseDriverFile

    use solverVars
    use meshUtil, only: nDim
    use update, only: eqFlag, ADVECTION_EQ, ACOUSTIC_EQ, PLESSEULER_EQ, &
                      ISENTEULER_EQ, EULER_EQ

    implicit none

    ! Local variables
    character(1)          :: header   !< Buffer for comments/whitespace in file
    character(fileLength) :: tmpFile  !< Place holder for file name 
    integer               :: line,  & !< Counter to skip lines in file
                             iErr     !< error status
    
    open( unit = drvFile, file = trim(adjustl(drvFileName)), &
          form = 'formatted' )
 
    ! File names
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,filePath)

    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,solFileName)
    solFileName_copy = solFileName
    solFileName = trim(adjustl(filePath))//'/'//trim(adjustl(solFileName))

    ! set data dump file name
    dumpPrompt = trim(adjustl(filePath))//'/dump'
    ! set simulation shut down prompt name 
    shutDownPrompt = trim(adjustl(filePath))//'/shutdown'
  
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,reconFileName)
    reconFileName_copy = reconFileName
    ! reconstruction file name
    reconFileName = trim(adjustl(filePath))//'/'//trim(adjustl(reconFileName))

    ! set residual file name 
    auxFileName = trim(adjustl(filePath))//'/aux'//trim(adjustl(reconFileName_copy))
    auxFileName2 = trim(adjustl(filePath))//'/aux2'//trim(adjustl(reconFileName_copy))
    residFileName = trim(adjustl(filePath))//'/resid'//trim(adjustl(reconFileName_copy))
    
    ! assume mesh and bc file are specified by full path
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,meshFileName)
    meshFileName = trim(adjustl(meshFileName))
    
    read(drvFile,'(A100)') tmpFile; call stripInputString(tmpFile,bcFileName)
    bcFileName = trim(adjustl(bcFileName))

    histFileName = trim(adjustl(filePath))//'/'//'history.dat'

    ! Solution parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) govEqn
    ! parse mesh once governing equation is known (also allocates variables)
    call readMeshFile
    select case ( trim(adjustl(govEqn)) )
        case ( 'advectio' )
            eqFlag = ADVECTION_EQ

        case ( 'acoustic' )
            eqFlag = ACOUSTIC_EQ

        case ( 'pleuler' )
            eqFlag = PLESSEULER_EQ

        case ( 'isenteul' )
            eqFlag = ISENTEULER_EQ
        
        case ( 'euler' )
            eqFlag = EULER_EQ

        case default
            write(*,'(3a)') 'ERROR: governing equation "', &
                            trim(adjustl(govEqn)),'" undefined.'
            stop
    end select
    read(drvFile,*) initSolnType
    
    ! Time-step parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) tFinal
    read(drvFile,*) cfl
    !read(drvFile,*) dtIn
    !read(drvFile,*) nIter
 
    ! Output parameters
    read(drvFile,'(A1)',advance='yes') (header, line = 1,4)
    read(drvFile,*) solOutputFreq
    if ( solOutputFreq == 0 ) solOutputFreq = 1

    ! Solution input file parameters -------------------------------------------------
    ! TODO: I guess I can also take mesh information as well?
    ! For continuing simulation from an existing reconstruction file
    ! read specified file. File name MUST include path as well 
    read(drvFile,'(A1)',advance='yes',iostat=iErr) (header, line = 1,4)
    if ( iErr < 0 ) then
        ! end of file 
        GO TO 108
    end if

    read(drvFile,'(A100)',iostat=iErr) tmpFile; call stripInputString(tmpFile,inputFileName)
    if ( iErr > 0 ) then
        ! error reading file 
        write(*,*) 
        write(*,*) 'ERROR: reading input file name'
        write(*,*)
        stop
    else if ( iErr < 0 ) then
        ! end of file 
        GO TO 108
    else
        call readInputFile(inputFile,inputFileName,solInput)
        ! TODO: tFinal in this case is not reflective of tSim /= 0 
    end if

108 close(drvFile)
    ! Solution input file parameters -------------------------------------------------

    !close(drvFile)

end subroutine parseDriverFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Parse boundary condition file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 March 2012 - Initial creation
!>
subroutine parseBCfile

    use solverVars, only: inLength, iLeft, iRight, initSolnType
    use meshUtil, only: nFaces, faceCells, cellNodes, cellFaces, &
                        maxNodesPerCell, maxFacesPerCell, maxNodesPerFace, &
                        faceNodes, nPatches
    use boundaryConditions
    use meshUtil, only: edgeCoord
    implicit none

    ! Local variables
    character(1) :: header !< Buffer for headers in file
    character(inLength) :: patchName !< patch name
    character(fileLength) :: idList !< list of patch ids
    
    integer :: line,    & !< counter for line skipping
               iErr,    & !< error code
               i,       & !< loop index
               id0,     & !< starting index for id string
               idF,     & !< ending index for id string
               iFace,   & !< face index
               iNode,   & !< node index
               bcID       !< boundary condition id

    logical :: isFile !< file exist flag

    ! Make sure file exists
    inquire( file = trim(adjustl(bcFileName)), exist = isFile )
    if ( .not. isFile ) then
        write(*,*)
        write(*,'(3A)') "ERROR: Boundary file '",                        &
                          trim(adjustl(bcFileName)), "' does not exist."
        write(*,*)
        stop
    end if
    
    open(unit=bcFile,file=trim(adjustl(bcFileName)),form='formatted')
    read(bcFile,'(a)',advance='yes') (header, line = 1,4)

    iErr = 0
    i = 0
    pNodePair(:,:) = 0
    pEdgePair = 0
    do while ( ( iErr == 0 ) )
        !read(bcFile,'(<fileLength>a)',iostat=iErr) idList(:)
        read(bcFile,'(*(a))',iostat=iErr) idList(:)
        read(bcFile,*,iostat=iErr) header
        read(bcFile,*,iostat=iErr) patchName

        !write(*,*) patchName, bcID
        ! Tasks done once end of the file is detected
        if ( iErr /= 0 ) then
            
            ! mark edges and nodes connected to boundary with bc type
            do iFace = 1, nFaces
                if ( ( faceCells(iRight,iFace) < 0 ) .and. &
                     ( edgeBC(iFace) /= periodicID ) ) then
                    ! store patch id
                    edgePatch(iFace) = abs(faceCells(iRight,iFace))

                    do i = 1, nPatches
                        if ( patchID(i) == abs(faceCells(iRight,iFace)) ) then
                            bcID = patchType(i)
                        end if
                    end do
                    edgeBC(iFace) = bcID
                    do iNode = 1, maxNodesPerFace
                        nodeBC(faceNodes(iNode,iFace)) = bcID
                    end do
                end if
            end do
            
            call testPeriodicAssignment
            return
        end if
 
        ! parse patch id list (full line)
        id0 = 1
        idF = 1
        do while ( ( idF < fileLength ) .and. ( iErr == 0 ) )
           
            idF = index(idList,' ')

            if ( idF > 1 ) then
                i = i + 1
                read(idList(1:idF),*,iostat=iErr) patchID(i)

                select case ( trim(adjustl(patchName)) )
                    case ('periodic') 
                        patchType(i) = periodicID

                    case ('outflow')
                        patchType(i) = outflowID
                    
                    case ('wall')
                        patchType(i) = wallID

                    case ('specFunc')
                        patchType(i) = specFuncID

                    case ('internal')
                        patchType(i) = internalID

                    case ('initSoln')
                        patchType(i) = initSolnID

                    case default
                        write(*,'(3a)') 'ERROR: ',trim(adjustl(patchName)),&
                                        ' unknown patch type.'
                        stop
                end select
            else
                exit
            end if

            ! remove last patch number from list
            id0 = idF+1
            idList = idList(id0:fileLength-id0+1)

        end do

        ! find "neighbor" across domain for periodic pair 
        if ( patchType(i) == periodicID  ) then
            ! assumes only two patches on the line
            call setPeriodicNeighbor(patchID(i-1:i))
        elseif ( patchType(i) == specFuncID ) then
            read(bcFile,*) bcFunction
        elseif ( patchType(i) == initSolnID ) then
            ! set bcFunction to initial solution type, convenient
            bcFunction = initSolnType
        end if

        read(bcFile,*,iostat=iErr) header
   end do

end subroutine parseBCfile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Strip off commented text from string input
!>
!> @author
!>  Timothy A. Eymann (adapted from routine by David McDaniel)
!>
!> @history
!>  21 May 2010 - Initial creation
!>
subroutine stripInputString(strin,strout)

    implicit none

    ! Local variables
    character(*)  :: strin, strout
    integer       :: i

    i = index(strin,'#')
    if ( i .gt. 0 ) then
        strout = trim(strin(1:i-1))
    else
        strout = trim(strin)
    end if

end subroutine
!----------------------------------------------------------------------
!> @purpose 
!>  Check if data dump is required. Checks for data dump file prompt 
!>   in the driver file directory called 'dump' without the apostrophes.
!>   Outputs binary file containing solution at the particular iteration
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  28 March 2016 - Initial Creation
!>
subroutine shutDownInquiry(switch)

    !use solverVars
    !use meshUtil

    implicit none

    ! Interface variables
    logical, intent(out) :: switch

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge,   & !< edge index
               iErr       !< error code

    ! shut down switch
    switch = .false.

    ! Make sure file exists
    inquire( file = trim(adjustl(shutDownPrompt)), exist = isFile )
    if ( isFile ) then
        ! open-close and delete dump file
        open( unit = shutDownPromptFile, file = trim(adjustl(shutDownPrompt)), &
              status = 'OLD' )
        close ( unit = shutDownPromptFile, status = 'delete' )

        ! activate shut down switch
        switch = .true.
        write(*,*)
        write(*,'(1A)') 'Simulation shut down requested.'
        write(*,'(3A)') 'Driver file is "', trim(adjustl(drvFileName)), '" '
        write(*,*)
        
    end if

end subroutine shutDownInquiry
!----------------------------------------------------------------------
!> @purpose 
!>  Check if data dump is required. Checks for data dump file prompt 
!>   in the driver file directory called 'dump' without the apostrophes.
!>   Outputs binary file containing solution at the particular iteration
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  28 March 2016 - Initial Creation
!>  12 August 2016 - Implemented switch 
!>
subroutine dumpDataInquiry(iter,t,switch)

    use solverVars
    use meshUtil
    use update

    implicit none

    !include 'tecio.f90'

    ! Interface variables
    integer, intent(in) :: iter

    real(FP), intent(in) :: t 

    logical, intent(out) :: switch

    ! Local variables
    logical :: isFile   !< flag specifying file existence

    character(fileLength) :: varfmt,    & !< 
                             temp

    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge,   & !< edge index
               iErr       !< error code

    ! dump data switch
    switch = .false.

    ! Make sure file exists
    inquire( file = trim(adjustl(dumpPrompt)), exist = isFile )
    if ( isFile ) then
        ! open-close and delete dump file
        open( unit = dumpPromptFile, file = trim(adjustl(dumpPrompt)), &
              status = 'OLD' )
        close ( unit = dumpPromptFile, status = 'delete' )

        ! activate dump data switch
        switch = .true.

        ! change dump file name according to iteration number
        reconFileName_copy = adjustr(reconFileName_copy)
        temp = reconFileName_copy(:len(reconFileName_copy)-4) ! trim '.dat' from the end of file name 
        write(varfmt,'(2a,i0,a)') trim(adjustl(temp)),'.dump', iter, '.dat'
        write(*,*)
        write(*,'(1A)') "Solution dump file found."
        write(*,'(2A)') "Driver file is ", trim(adjustl(drvFileName))
        write(*,*)
        write(*,'(3A)') "Solution dump file '", trim(adjustl(varfmt)), "' is created."
        write(*,*)

        dumpReconName = trim(adjustl(filePath))//'/'//trim(adjustl(varfmt))

        ! open dump file and write binary output data
        open( unit = dumpReconFile, file = trim(adjustl(dumpReconName)) , &
              form = 'unformatted', access = 'stream', status = 'REPLACE', &
              iostat = iErr )
        if ( iErr /= 0 ) then
            write(*,*) 'ERROR: opening ', trim(adjustl(dumpReconName))
            stop
        end if

        ! store binary data in the new dump file
        write(dumpReconFile) nDim, nEqns, nIter
        write(dumpReconFile) maxNodesPerCell, maxFacesPerCell, nNodes, nEdges, nCells   

        ! mesh information
        do iNode = 1,nNodes
            write(dumpReconFile) nodeCoord(:,iNode)
        end do

        do iEdge = 1,nEdges
            write(dumpReconFile) edgeCoord(:,iEdge)
        end do

        ! cell nodes and faces
        do iCell = 1, nCells
            write(dumpReconFile) cellNodes(:,iCell), cellFaces(:,iCell)
        end do

        ! indicate iteration and time for data
        write(dumpReconFile) iter, t 

        ! conserved cell averages
        do iCell = 1, nCells
            write(dumpReconFile) cellAvgN(:,iCell)
        end do

        ! primitive cell averages
        do iCell = 1, nCells
            write(dumpReconFile) primAvgN(:,iCell)
        end do

        ! node values
        do iNode = 1, nNodes
            write(dumpReconFile) nodeDataN(:,iNode)
        end do

        ! edge values
        do iEdge = 1, nEdges
            write(dumpReconFile) edgeDataN(:,iEdge)
        end do

        close ( dumpReconFile )

        ! result visualization at iteration
        solFileName_copy = adjustr(solFileName_copy)
        temp = solFileName_copy(:len(solFileName_copy)-4) ! trim '.plt' from the end of file name 
        write(varfmt,'(2a,i0,a)') trim(adjustl(temp)),'.dump', iter, '.plt'
        write(*,'(3A)') 'TecPlot file "', trim(adjustl(varfmt)), '" is created.'
        write(*,*)

        ! create a new TecPlot file name 
        dumpSolName = trim(adjustl(filePath))//'/'//trim(adjustl(varfmt))
        
    end if

end subroutine dumpDataInquiry
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read solution input file
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  11 April 2016 - Initial creation
!>
subroutine readInputFile(fileUnit,fileName,updated)
    
    use solverVars, only: tSim

    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    character(fileLength), intent(in) :: fileName !< file name

    logical, intent(out) :: updated

    ! Local variables
    integer :: maxIter,     & !< maximum iteration number
               iter,        & !< iteration counter
               iErr           !< error 

    ! open solution input file 
    open( unit = fileUnit, file = trim(adjustl(fileName)), form = 'unformatted', &
        access = 'stream', status = 'OLD', iostat = iErr , action = 'read' )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening solution reconstruction input file'
        write(*,*) 'Instead, solution will be initialized from its initial condition.'
        updated = .false.
        return
    end if
    write(*,*)
    write(*,*) 'WARNING: Solution initialized from "', trim(adjustl(fileName)), '".'

    ! signal that solution is initialized from input file
    updated = .true.

    ! read the most recent reconstruction data file
    maxIter = getIterNum(fileUnit)
    rewind(fileUnit)
    call skipConnectivity(fileUnit)
    do iter = 1, maxIter-1  
        iErr = skipData(fileUnit)
    end do
    call readData(fileUnit,iter,tSim)
    write(*,'(a,e12.4e2)') '      Starting time: ', tSim

    ! TODO: put a dimension check so if a wrong input file is called, quit program.
    ! check nCells, nNodes, nEdges

    ! close the file
    close(fileUnit)

end subroutine readInputFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read a AVUS/Cobalt formatted (*.grd) mesh from a file.  Allocates data
!>  structures associated with mesh variables
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 March 2012 - Initial creation
!>
subroutine readMeshFile

    use meshUtil
    use boundaryConditions
    use solverVars
    use update
    implicit none

    ! Local variables
    integer :: iFace,       & !< face index
               iNode,       & !< node index
               iDir,        & !< coordinate direction
               iErr,        & !< error code
               nodesPerFace   !< number of nodes on face
    real(FP) :: nodeTempX,     &
                nodeTempY

    ! guess that file is formatted
    open( unit = meshFile, file=trim(adjustl(meshFileName)), form='formatted')
    read(meshFile,*,iostat=iErr) nDim, nZones, nPatches

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: mesh file must be formatted (ascii)'
        stop
    end if

    read(meshFile,*) nNodes, nFaces, nCells, maxNodesPerFace, maxFacesPerCell

    ! handle special case of 3d quad element
    if ( ( maxFacesPerCell > 4 ) .and. ( nDim > 2 ) ) then
        maxNodesPerCell = 8
    else
        maxNodesPerCell = maxFacesPerCell
    end if

    ! calculate number of edges in mesh
    if ( nDim <= 2 ) then
        nEdges = nFaces
    end if

    ! initialize arrays
    call allocateMeshVars()

    ! store node coordinates
    !write(*,*) 'WARNING: mesh domain has been modified for fast vortex'
    do iNode = 1,nNodes
        read(meshFile,*) nodeCoord(:,iNode)

        !! modify domain size for fast vortex
        !nodeTempX = nodeCoord(1,iNode)
        !nodeTempY = nodeCoord(2,iNode)
        !nodeCoord(1,iNode) = (nodeTempX+10.0_FP)/200.0_FP
        !nodeCoord(2,iNode) = (nodeTempY+10.0_FP)/200.0_FP

        do iDir = 1, nDim
            xMax(iDir) = max( xMax(iDir), nodeCoord(iDir,iNode) )
            xMin(iDir) = min( xMin(iDir), nodeCoord(iDir,iNode) )
        end do
    end do

    ! store face connectivity information
    do iFace = 1,nFaces
        read(meshFile,*) nodesPerFace, faceNodes(1:nodesPerFace,iFace), &
                         faceCells(:,iFace)
    end do

    close(meshFile)

end subroutine readMeshFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return number of iterations in afSolver binary file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 July 2012 - Initial Creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
function getIterNum(fileUnit)

    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit for binary data

    ! Function variable
    integer :: getIterNum

    ! Local variables
    integer :: iErr !< error code

    iErr = 0
    getIterNum = 0
    
    rewind(fileUnit)

    call skipConnectivity(fileUnit)
    do while ( iErr == 0 )
        iErr = skipData(fileUnit)
        if ( iErr == 0 ) getIterNum = getIterNum + 1
    end do

end function getIterNum
!-------------------------------------------------------------------------------
!> @purpose 
!>  Skip connectivity data in binary file
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 July 2012 - Initial Creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
subroutine skipConnectivity(fileUnit)

    use meshUtil, only: nDim, nNodes, nEdges, nCells, maxNodesPerCell
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit for binary data

    ! Local variables
    integer :: i,       & !< counter
               iNode,   & !< node index
               iEdge,   & !< edge index
               iCell,   & !< cell index
               dumInt     !< dummy integer variable

    real :: dumReal !< dummy real variable

    read(fileUnit) (dumInt, i=1,8) 
    do iNode = 1, nNodes
        read(fileUnit) (dumReal, i=1,nDim)
    end do
    do iEdge = 1, nEdges
        read(fileUnit) (dumReal, i=1,nDim)
    end do
    do iCell = 1, nCells
        read(fileUnit) (dumInt, i=1,2*maxNodesPerCell)
    end do

end subroutine skipConnectivity
!-------------------------------------------------------------------------------
!> @purpose 
!>  Skip iteration data from an output file produced by afSolver
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
function skipData(fileUnit)

    use solverVars, only: nEqns
    use meshUtil, only: nCells, nEdges, nNodes
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    ! Function variable
    integer :: skipData

    ! Local variables
    integer :: dumInt,  & !< dummy integer variable
               iCell,   & !< cell index
               iNode,   & !< node index
               iEdge,   & !< edge index
               iEq        !< equation index

    real :: dumReal !< dummy real variable

    read(fileUnit, iostat=skipData) dumInt, dumReal
    
    do iCell = 1, nCells
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
    end do

    do iCell = 1, nCells
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns) 
    end do
    
    do iNode = 1, nNodes
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
    end do
    
    do iEdge = 1, nEdges
        read(fileUnit, iostat=skipData) (dumReal, iEq=1,nEqns)
    end do

end function skipData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Read data from an output file produced by afSolver
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 April 2012 - Initial creation
!>  11 April 2016 - Relocated to inputOutput.f90 from postProc.f90 (Maeng)
!>
subroutine readData(fileUnit,solIter,solTime)

    use solverVars, only: nEqns
    use update, only: primAvgN, cellAvgN, & 
                      nodeDataN, edgeDataN
    use meshUtil, only: nCells, nNodes, nEdges
    implicit none

    ! Interface variables
    integer, intent(in) :: fileUnit !< file unit number

    integer, intent(out), optional :: solIter !< solution iteration

    real, intent(out), optional :: solTime 

    ! Local variables
    integer :: dumInt,  & !< dummy integer variable
               iCell,   & !< cell index
               iNode,   & !< node index
               iEdge,   & !< edge index
               iEq        !< equation index

    real :: dumReal !< dummy real variable

    if ( (present(solIter)) .and. (present(solTime)) ) then
        read(fileUnit) solIter, solTime
    else
        read(fileUnit) dumInt, dumReal
    end if

    do iCell = 1, nCells
        read(fileUnit) (cellAvgN(iEq,iCell), iEq=1,nEqns) 
    end do
    
    do iCell = 1, nCells
        read(fileUnit) (primAvgN(iEq,iCell), iEq=1,nEqns) 
    end do
    
    do iNode = 1, nNodes
        read(fileUnit) (nodeDataN(iEq,iNode), iEq=1,nEqns)
    end do
    
    do iEdge = 1, nEdges
        read(fileUnit) (edgeDataN(iEq,iEdge), iEq=1,nEqns)
    end do

end subroutine readData
!-------------------------------------------------------------------------------
!> @purpose 
!>  Allocate variables dimensioned with mesh parameters
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  3 April 2012 - Initial creation
!>
subroutine allocateMeshVars

    use solverVars
    use meshUtil
    use boundaryConditions
    use update
    use reconstruction

    implicit none

    allocate( cellCentroid(nDim,nCells), &
              cellVolume(nCells), &
              cellDetJac(nCells), &
              cellInvJac(nDim,nDim,nCells), &
              cellJac(nDim,nDim,nCells), &
              cellAngles(maxNodesPerCell,nCells), &
              cellAvg(nEqns,nCells), &
              cellAvgN(nEqns,nCells), & 
              primAvg(nEqns,nCells), &
              primAvgN(nEqns,nCells), & 
              cellFaces(maxFacesPerCell,nCells), &
              cellNodes(maxNodesPerCell,nCells), & 
              faceNodes(maxNodesPerFace,nFaces), &
              faceArea(nFaces), &
              faceCells(2,nFaces), &
              faceNormal(nDim,nFaces), &
              edgeCoord(nDim,nEdges), &
              edgeData(nEqns,nEdges), &
              edgeDataH(nEqns,nEdges), &
              edgeDataN(nEqns,nEdges), &
              edgeCell(1,nEdges), &
              nodeCoord(nDim,nNodes), &
              nodeData(nEqns,nNodes), &
              nodeDataH(nEqns,nNodes), &
              nodeDataN(nEqns,nNodes),  &
              nodeCell(1,nNodes), &
              patchID(nPatches), &
              patchType(nPatches), &
              nodeBC(nNodes), &
              edgeBC(nEdges), &
              nodeCellCount(nNodes), &
              edgeCellCount(nEdges), &
              xMax(nDim), xMin(nDim), &
              reconData(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells), &
              reconDataH(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells), &
              reconDataF(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim)),nCells), &
              pNodePair(nDim,nNodes), pEdgePair(nEdges), edgePatch(nEdges), &
              xiNode(nDim,maxNodesPerCell), xiEdge(nDim,maxFacesPerCell), &
              residMax(nEqns), residNorm(nEqns), primMin(nEqns), &
              primNorm(nEqns), conservNorm(nEqns), & 
              edgeChOrg(nDim,nEdges), nodeChOrg(nDim,nNodes) )

    cellCentroid(:,:) = 0.0_FP
    cellVolume(:) = 0.0_FP
    cellDetJac(:) = 0.0_FP
    cellInvJac(:,:,:) = 0.0_FP
    cellJac(:,:,:) = 0.0_FP
    cellAvg(:,:) = 0.0_FP
    cellAvgN(:,:) = cellAvg(:,:)
    primAvg(:,:) = 0.0_FP
    primAvgN(:,:) = primAvg(:,:)
    cellFaces(:,:) = 0
    cellNodes(:,:) = 0
    cellAngles(:,:) = 0.0_FP
    faceNodes(:,:) = 0
    faceArea(:) = 0.0_FP
    faceCells(:,:) = 0
    faceNormal(:,:) = 0.0_FP
    nodeCoord(:,:) = 0.0_FP
    edgeData(:,:) = 0.0_FP
    edgeDataH(:,:) = 0.0_FP
    edgeDataN(:,:) = 0.0_FP
    edgeCoord(:,:) = 0.0_FP
    edgeCell(:,:) = 0
    nodeData(:,:) = 0.0_FP
    nodeDataH(:,:) = 0.0_FP
    nodeDataN(:,:) = 0.0_FP
    nodeCell(:,:) = 0
    patchID(:) = 0
    patchType(:) = internalID
    nodeBC(:) = 0
    edgeBC(:) = 0
    nodeCellCount(:) = 0
    edgeCellCount(:) = 0
    xMax(:) = -1.0e6_FP
    xMin(:) = 1.0e6_FP
    reconData(:,:,:) = 0.0_FP
    reconDataH(:,:,:) = 0.0_FP
    reconDataF(:,:,:) = 0.0_FP
    pNodePair(:,:) = 0
    pEdgePair(:) = 0
    edgePatch(:) = 0
    xiNode(:,:) = 0.0_FP
    xiEdge(:,:) = 0.0_FP
    residMax(:) = 0.0_FP
    residNorm(:) = 0.0_FP
    primNorm(:) = 0.0_FP
    primMin(:) = 0.0_FP
    conservNorm(:) = 0.0_FP
    edgeChOrg(:,:) = 0.0_FP
    nodeChOrg(:,:) = 0.0_FP

end subroutine allocateMeshVars
!-------------------------------------------------------------------------------
!> @purpose 
!>  Deallocate variables dimensioned with mesh parameters
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  7 March 2012 - Initial creation
!>
subroutine deallocateMeshVars

    use solverVars
    use meshUtil
    use boundaryConditions
    use update
    use reconstruction

    implicit none

    deallocate( cellCentroid, cellVolume, cellDetJac, cellInvJac, cellJac, &
                cellAvg, cellAvgN, primAvg, primAvgN, &
                edgeData, edgeDataN, edgeDataH, nodeData, nodeDataN, nodeDataH, &
                cellFaces, cellNodes, cellAngles, &
                faceNodes, faceArea, faceCells, faceNormal, &
                edgeCoord, edgeCell, &
                nodeCoord, nodeCell, &
                patchID, patchType, nodeBC, edgeBC, nodeCellCount, edgeCellCount, &
                xMax, xMin, reconData, reconDataH, reconDataF, &
                pNodePair, pEdgePair, edgePatch, xiNode, xiEdge,  &
                residMax, residNorm, primMin, primNorm, conservNorm, &
                edgeChOrg, nodeChOrg )

end subroutine deallocateMeshVars
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output binary file containing vertex, edge, and centroid data which can be
!>  used with external visualization routines
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>  10 April 2015 - Added primitive variable cell average (Maeng)
!>
subroutine outputBinary(iter, t)

    use solverVars, only: FP, nEqns
    use meshUtil, only: nDim, nCells, nNodes, nEdges
    use update, only: cellAvgN, primAvgN, &
                      nodeDataN, edgeDataN
    implicit none
    
    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< current time

    ! Local variables
    integer :: iNode,   & !< node index
               iCell,   & !< cell index
               iEdge      !< edge index

    ! indicate iteration and time for data
    write(reconFile) iter, t 

    ! conserved cell averages
    do iCell = 1, nCells
        write(reconFile) cellAvgN(:,iCell)
    end do

    ! primitive cell averages
    do iCell = 1, nCells
        write(reconFile) primAvgN(:,iCell)
    end do

    ! node values
    do iNode = 1, nNodes
        write(reconFile) nodeDataN(:,iNode)
    end do

    ! edge values
    do iEdge = 1, nEdges
        write(reconFile) edgeDataN(:,iEdge)
    end do

end subroutine outputBinary
!-------------------------------------------------------------------------------
!> @purpose 
!>  Open files for writing 
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>
subroutine openFiles

    use meshUtil
    use reconstruction
    use solverVars, only: nEqns, nIter

    implicit none

    ! Local variables
    integer :: i,           & !< counter
               iNode,       & !< node index
               iEdge,       & !< edge index
               iCell,       & !< cell index
               iErr,        & !< error code
               dateVal(8)     !< array of date and time values


    ! open reconstruction file and write header information
    open( unit = reconFile, file = trim(adjustl(reconFileName)), &
          form = 'unformatted', access = 'stream', status = 'REPLACE', &
          iostat = iErr)
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(reconFileName))
        stop
    end if

    write(reconFile) nDim, nEqns, nIter
    write(reconFile) maxNodesPerCell, maxFacesPerCell, nNodes, nEdges, nCells   

    ! mesh information
    do iNode = 1,nNodes
        write(reconFile) nodeCoord(:,iNode)
    end do

    do iEdge = 1,nEdges
        write(reconFile) edgeCoord(:,iEdge)
    end do

    ! cell nodes and faces
    do iCell = 1, nCells
        write(reconFile) cellNodes(:,iCell), cellFaces(:,iCell)
    end do

    ! set up history file and metadata
    open( unit = histFile, file = trim(adjustl(histFileName)), &
          form = 'formatted', position = 'append', iostat = iErr)
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(histFileName))
        stop
    end if
    call date_and_time( VALUES = dateVal(:) )
    write(histFile,'(a)',advance='no') '#'
    write(histFile,'(78a)',advance='no') ('-',i=1,78)
    write(histFile,'(a)') '#'
    write(histFile, '(a)') '#'
    write(histFile, '(a,i0,a,i0,a,i0,1x,i2.0,a,i0.2)' ) &
        '#     Job started: ',dateVal(2),'/',dateVal(3),'/',dateVal(1),&
         dateVal(5),':',dateVal(6)
    write(histFile, '(2a)') '#            Mesh: ',trim(adjustl(meshFileName))
    write(histFile, '(2a)') '#         BC File: ',trim(adjustl(bcFileName))
    write(histFile, '(a)') '#'

    !! open residual file
    !open ( unit = residFile, file=trim(adjustl(residFileName)), form='formatted', &
    !       status = 'REPLACE', iostat = iErr )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(residFileName))
    !    stop
    !end if

    !! open aux file
    !open ( unit = auxFile, file=trim(adjustl(auxFileName)), form='formatted', &
    !       status = 'REPLACE', iostat = iErr )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(auxFileName))
    !    stop
    !end if

    !! open aux file
    !open ( unit = auxFile2, file=trim(adjustl(auxFileName2)), form='formatted', &
    !       status = 'REPLACE', iostat = iErr )
    !if ( iErr /= 0 ) then
    !    write(*,*) 'ERROR: opening ', trim(adjustl(auxFileName2))
    !    stop
    !end if

end subroutine openFiles
!-------------------------------------------------------------------------------
!> @purpose 
!>  Close open files
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 March 2012 - Initial creation
!>
subroutine closeFiles

    implicit none

    !include 'tecio.f90'

    ! Local variables
    integer :: iErr !< Error code

    close( reconFile )
    close( histFile )
    !close( residFile )

    !close( auxFile )
    !close( auxFile2 )

end subroutine closeFiles
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output file containing auxiliary data. Contains residual norm over all
!>  cells
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 December 2013 - Initial creation
!>
subroutine outputResidual(iter,t)

    use solverVars, only: FP, nEqns
    use update, only: residMax, residNorm, &
                      primMin, primNorm, conservNorm
    implicit none

    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< simulation time

    ! Local variable
    character(fileLength) :: varfmt

    ! indicate iteration and time for data
    write(varfmt,'(a,i0,a,i0,a)') '(i0,e12.4e2,',2*nEqns,'(e24.16e2))'
    write(residFile,varfmt) iter, t, primMin(:), conservNorm(:)

end subroutine outputResidual
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output file containing auxiliary data. Contains residual norm over all
!>  cells
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  4 December 2013 - Initial creation
!>
subroutine outputAux(iter,t)

    use solverVars, only: FP, nEqns
    use reconstruction

    implicit none

    ! Interface variables
    integer, intent(in) :: iter !< iteration number

    real(FP), intent(in) :: t !< simulation time

    ! Local variable
    integer :: iEdge
    
    character(fileLength) :: varfmt
    
    !write(varfmt,'(a,i0,a,i0,a)') '(i0,e12.4e2,',2*nEqns,'(e24.16e2))'
    !write(auxFile2,varfmt) iter, tSim, deltaNodeL1(:), deltaEdgeL1(:)

end subroutine outputAux
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output ASCII file containing useful data for one dimensional problems
!>  one-to-one relationship between data is implied
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  20 February 2015 - Initial creation
!>
subroutine outputASCII(fileName,nDim,nPts,nEqns,nCells,gridData,cellData)

    use solverVars, only: FP
    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,     & !< number of dimension
                           nPts,     & !< number of quad point
                           nEqns,    & !< number of quad point
                           nCells      !< number of cells

    real(FP), intent(in) :: gridData(nDim,nPts,nCells),     & !< file data
                            cellData(nEqns,nPts,nCells)

    character(80), intent(in) :: fileName   !< file name 

    ! Local variables
    integer :: iCell,           & !< cell index
               iPt,             & !< point index
               iErr,            & !< error code
               fileUnit = 1900    !< file unit

    character(80) :: varfmt          

    !write(varfmt,'(a,i0,a)') '(e24.15e2,', ni, 'e24.15e2)'
    write(varfmt,'(a,i0,a,i0,a)') '(',nDim,'e24.15e2, ',nEqns, 'e24.15e2)'

    ! open file
    open( unit = fileUnit, file = trim(adjustl(fileName)), form = 'formatted', &
          iostat = iErr )
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: opening ', trim(adjustl(fileName))
        stop
    end if
    write(fileUnit, '(a)') 'TITLE = "solution" '
    write(fileUnit, '(a)') 'VARIABLES = "x", "q1", "q2" '
    write(fileUnit, '(a)') 'ZONE T = "'//trim(adjustl(fileName))//'" '
    write(fileUnit, '(a,i0,a)') 'I = ', nPts*nCells, ', J = 1, F = POINT '

    do iCell = 1, nCells 
        do iPt = 1, nPts
            write(fileUnit,varfmt) gridData(:,iPt,iCell), cellData(:,iPt,iCell) 
        end do
    end do

    close( fileUnit )

end subroutine outputASCII
!-------------------------------------------------------------------------------
end module inputOutput
!-------------------------------------------------------------------------------

!!-------------------------------------------------------------------------------
!! TECPLOT output 
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Output TecPlot binary file switching routine.  
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  10 April 2015 - Initial creation
!!>
!subroutine switchTecOutput(whichFile)
!
!    implicit none
!
!    include 'tecio.f90'
!    
!    ! Interface variable
!    integer, intent(in) :: whichFile
!
!    integer :: iErr               !< error flag
!
!    iErr = TecFil142(whichFile)
!
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem switching TecPlot output files'
!        stop
!    end if
!
!end subroutine switchTecOutput
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Initialize TecPlot file header. Optional fileName can be used to dump out
!!>   data when needed. 'nVar' and 'variables' are defined in this routine.
!!>
!!> @author
!!>  J. Brad Maeng
!!>  
!!> @history
!!>  28 March 2015 - Initial creation
!!>
!subroutine initTecPlotFile(fileName)
!
!    use solverVars
!    use meshUtil
!
!    implicit none
!
!    include 'tecio.f90'
!    
!    ! Interface variable
!    character(80), intent(in), optional :: fileName
!
!    ! Local variables
!    character(1) :: nullChar !< null character for terminating strings
!
!    character(80) :: dataTitle,     & !< data set title
!                     fName            !< name of file to create
!
!    character(400) :: variables !< variables in data set
!
!    integer, parameter :: fullSoln = 0,     & !< flag for full solution file
!                          fileFmt = 0,      & !< flag for file format, 0-.plt, 1-szplt
!                          noDebug = 0,      & !< flag for debugging, 0-no, 1-yes
!                          dblPrec = 1         !< flag for double precision
!
!    pointer (nullPtr,null) !< null pointer for TecPlot calls
!    integer*4 null(*)
!
!    integer :: iErr               !< error flag
!    
!    nullChar = char(0)
!    nullPtr = 0
!    dataTitle = 'Solution Reconstrution'
!
!    select case (nDim)
!        case (1)
!            variables = 'x'  ! coordinate
!            variables = trim(adjustl(variables)) // &
!                ' <greek>r</greek> u '//nullChar
!
!            nVars = nDim + nEqns 
!        case (2)
!            variables = 'x y' ! coordinate
!            variables = trim(adjustl(variables)) // & 
!                ' <greek>r</greek> u v p s &
!                rCons ruCons rvCons rECons '//nullChar
!                !rIncons uIncons vIncons pIncons '//nullChar
!                !rR ruR rvR rER' //nullChar
!
!            !nVars = nDim + nEqns + 16
!            nVars = nDim + 9 ! + 4 !+ 4
!
!    end select
!
!    ! check if filename is given
!    if ( .not. present(fileName) ) then
!        fName = solFileName ! has path included in variable
!    else
!        fName = trim(adjustl(filePath))//'/'//trim(adjustl(fileName))   
!    end if
!
!    !! open file and create header information
!    !iErr = TecIni112(trim(adjustl(dataTitle))//nullChar, &
!    !                 trim(adjustl(variables))//nullChar, &
!    !                 trim(adjustl(fName))//nullChar, &
!    !                 trim(adjustl(filePath))//nullChar, &
!    !                 fullSoln, noDebug, dblPrec )
!    iErr = TecIni142(trim(adjustl(dataTitle))//nullChar, &
!                     trim(adjustl(variables))//nullChar, &
!                     trim(adjustl(fName))//nullChar, &
!                     trim(adjustl(filePath))//nullChar, &
!                     fileFmt, fullSoln, noDebug, dblPrec )
!
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem creating TecPlot file'
!        stop
!    end if
!
!end subroutine initTecPlotFile
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Output TecPlot binary file containing visualization mesh.  Each cell writes
!!>  full internal state to account for solution discontinutities at interfaces
!!>  ( no sharing of node/edge data )
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  12 June 2012 - Initial creation
!!>  28 March 2016 - Initialization of TecPlot header moved to initTecPlotFile(Maeng)
!!>
!subroutine outputVizMesh()
!
!    use solverVars
!    use meshUtil
!    implicit none
!
!    include 'tecio.f90'
!    
!    ! Local variables
!    character(1) :: nullChar !< null character for terminating strings
!
!    character(80) :: dataTitle,     & !< data set title
!                     zoneTitle,     & !< zone title
!                     auxTitle,      & !< auxiliary data title
!                     auxData,       & !< auxiliary data value
!                     fName            !< name of file to create
!
!    integer, parameter :: dblPrec = 1,      & !< flag for double precision
!                          iCellMax = 0,     & !< future use flag
!                          jCellMax = 0,     & !< fugure use flag
!                          kCellMax = 0,     & !< future use flag
!                          strandID = 0,     & !< static zone flag
!                          parentZn = 0,     & !< zone has no parent
!                          isBlock = 1,      & !< block data format
!                          nFaceConnect = 0, & !< number of face connections
!                          faceNbMode = 0,   & !< face neighbor mode (local 1:1)
!                          shareConnect = 0    !< no connectivity sharing
!
!    pointer (nullPtr,null) !< null pointer for TecPlot calls
!    integer*4 null(*)
!
!    integer :: iErr,            & !< error flag
!               iDir,            & !< direction index
!               iNode,           & !< node index
!               iCell,           & !< global cell index
!               vCell,           & !< viz cell index
!               i, j,            & !< ref. coordinate indices
!               cellType,        & !< flag specifying element type
!               vNodePerCell,    & !< number of viz nodes per cell
!               nVizCells          !< number of visualization subcells
!
!    integer, allocatable :: vNodeConnect(:,:),  & !< viz node connectivity
!                            ij2node(:,:),       & !< i,j to node index array
!                            passiveVars(:),     & !< passive variable array
!                            varLocation(:)        !< variable location
!
!    real(FP) :: dXi,        & !< xi spacing
!                xi(nDim)      !< reference coordinates
!
!    real(DP) :: solTime !< solution time
!
!    real(DP), allocatable :: xVizNode(:,:) !< viz node coordinate
!    
!    
!    nullChar = char(0)
!    nullPtr = 0
!    !dataTitle = 'Solution Reconstrution'
!    zoneTitle = 'Viz Mesh'
!    solTime = 0.0_FP
!
!    dXi = 1.0_FP / real(nSubCells)
!
!    select case (nDim)
!        case (1)
!            cellType = 1 ! FE linear element
!            vNodePerCell = 2*nSubCells+1
!            nVizCells = 2*nSubCells
!            allocate( ij2node(2*nSubCells+1,1) ) 
!
!        case (2)
!            cellType = 2 ! FE triangular element
!            vNodePerCell = (nSubCells+1)*(nSubCells+2)/2
!            nVizCells = nSubCells**2
!            allocate( ij2node(nSubCells+1,nSubCells+1) ) 
!
!    end select
! 
!    allocate( vNodeConnect(nDim+1,nCells*nVizCells), &
!              xVizNode(nDim,vNodePerCell*nCells),   &
!              passiveVars(nVars), varLocation(nVars) )
!
!    ! all data at nodes, node centered value representation
!    varLocation(1:nVars) = 1
!
!    ! make coordinates active variables, all other passive
!    passiveVars(1:nDim) = 0
!    passiveVars(nDim+1:nVars) = 1
!
!    ! zone information, write *.plt binary file
!    iErr = TecZne142(trim(adjustl(zoneTitle))//nullChar, &
!                     cellType, vNodePerCell*nCells, nCells*nVizCells, nFaces, &
!                     iCellMax, jCellMax, kCellMax, &
!                     solTime, strandID, parentZn, isBlock, &
!                     nFaceConnect, faceNbMode, &
!                     0, 0, 0, & ! values only used for polygonal elements
!                     passiveVars(:), varLocation(:), null, shareConnect)
!
!    !write(*,*) 'zone 0'
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem creating TecPlot mesh zone'
!        stop
!    end if
!
!    ! write time and iteration for zone as auxiliary data
!    auxTitle = 'Iteration'
!    write(auxData,'(i0)') -1
!    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
!                         trim(adjustl(auxData))//nullChar)
!    
!    auxTitle = 'Time'
!    write(auxData,'(e12.4e2)') solTime
!    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
!                         trim(adjustl(auxData))//nullChar)
!
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem writing auxiliary data for zone'
!        stop
!    end if
!
!    ! Create new node lists and connectivity before writing to file
!    iNode = 0
!    vCell = 0
!
!    select case ( nDim )
!
!        case (1)
!            do iCell = 1, nCells
!                ! node coordinates
!                do i = 0, 2*nSubCells
!                    xi(1) = (0.5*dXi)*i
!                    iNode = iNode+1
!                    xVizNode(:,iNode) = ref2cart(nDim,iCell,xi(:))
!                    ij2node(i+1,1) = iNode
!                end do
!
!                ! connectivity
!                do i = 1, 2*nSubCells
!                    vCell = vCell + 1
!                    vNodeConnect(1,vCell) = ij2node(i,1)
!                    vNodeConnect(2,vCell) = ij2node(i+1,1)
!                end do
!            end do
!
!        case (2)
!            do iCell = 1, nCells
!                ! node coordinates
!                ij2node = 0
!                do j = 0, nSubCells
!                    do i = 0, nSubCells-j
!                        xi(1) = dXi*i
!                        xi(2) = dXi*j
!                        iNode = iNode+1
!                        xVizNode(:,iNode) = ref2cart(nDim,iCell,xi(:))
!
!                        ! store mapping from i,j to new node index
!                        ij2node(i+1,j+1) = iNode
!                    end do
!                end do
!
!                ! connectivity for lower triangles
!                do j = 1, nSubCells
!                    do i = 1, nSubCells-(j-1)
!                        vCell = vCell + 1
!                        vNodeConnect(1,vCell) = ij2node(i,j)
!                        vNodeConnect(2,vCell) = ij2node(i+1,j)
!                        vNodeConnect(3,vCell) = ij2node(i,j+1)
!                    end do
!                end do
!
!                ! connectivity for upper triangles
!                do j = 1, nSubCells-1
!                    do i = 2, nSubCells-(j-1) ! -1 left in for readability
!                        vCell = vCell + 1
!                        vNodeConnect(1,vCell) = ij2node(i,j)
!                        vNodeConnect(2,vCell) = ij2node(i,j+1)
!                        vNodeConnect(3,vCell) = ij2node(i-1,j+1)
!                    end do
!                end do
!
!            end do
!    end select
!
!    ! write out node coordinates 
!    do iDir = 1, nDim
!        iErr = TecDat142(vNodePerCell*nCells,xVizNode(iDir,:),dblPrec)
!        if ( iErr /= 0 ) then
!            write(*,*) 'ERROR: problem writing TecPlot mesh data'
!            stop
!        end if
!    end do
!
!    ! write out node connectivity information
!    iErr = TecNod142(vNodeConnect(:,:))
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem writing TecPlot connectivity data'
!        stop
!    end if
!
!    deallocate( vNodeConnect, xVizNode, ij2node, passiveVars, varLocation )
!
!end subroutine outputVizMesh
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Output TecPlot binary file containing internal cell reconstruction
!!>
!!> @author
!!>  Timothy A. Eymann
!!>
!!> @history
!!>  14 May 2013 - Initial creation
!!>
!subroutine outputVizSolution(iter, t)
!
!    use solverVars
!    use meshUtil
!    use analyticFunctions
!    use iterativeFunctions
!    use reconstruction
!    use update   
!    use physics, only: soundSpeed, gam
!    use mathUtil, only: symTriLoc, dunavantLoc, numAverage
!
!    implicit none
!
!    include 'tecio.f90'
!    
!    ! Interface variables
!    integer, intent(in) :: iter !< iteration
!
!    real(FP), intent(in) :: t !< current time
!
!    ! Local variables
!    character(1) :: nullChar !< null character for terminating strings
!
!    character(80) :: zoneTitle, & !< zone title
!                     auxTitle,  & !< auxiliary variable title
!                     auxData      !< auxiliary data value
!                     
!    integer, parameter :: meshFile = 1,     & !< flag for mesh data file
!                          fullSoln = 0,     & !< flag for full solution file
!                          noDebug = 0,      & !< flag for no debugging
!                          dblPrec = 1,      & !< flag for double precision
!                          iCellMax = 0,     & !< future use flag
!                          jCellMax = 0,     & !< fugure use flag
!                          kCellMax = 0,     & !< future use flag
!                          strandID = 0,     & !< static zone flag
!                          parentZn = 0,     & !< zone has no parent
!                          isBlock = 1,      & !< block data format
!                          nFaceConnect = 0, & !< number of face connections
!                          faceNbMode = 0,   & !< face neighbor mode (local 1:1)
!                          shareConnect = 1    !< share connectivity with zone 1
!
!    integer :: iErr,            & !< error code for TecPlot call
!               iNode,           & !< node index
!               iEdge,           & !< edge index
!               iCell,           & !< global cell index
!               iEq,             & !< equation index
!               i, j,            & !< ref. coord indices
!               vNodePerCell,    & !< viz nodes per cell
!               nVizCells,       & !< additional viz subcells per cell
!               cellType           !< flag specifying element type
!
!    real(FP) :: dXi,                & !< reference coordinate increment
!                xi(nDim),           & !< reference coordinate of node
!                xCart(nDim),        & !< phycial coordinate
!                gradU(nDim,nDim),   &
!                detJ,               &    
!                divU,               &
!                radialPos,          &
!                theta,              &
!                vMag,               &
!                speed,              &
!                qTemp(nEqns),       & !< temporary variable
!                qTemp2(nEqns),      & !< temporary variable
!                qExTemp(nEqns)        !< temporary variable
!
!    character(8) :: funcname 
!
!    real(FP) :: solTime !< solution time
!
!    real(FP), allocatable :: vizData(:,:) !< reconstruction data at nodes
!
!    integer, allocatable :: passiveVars(:), & !< passive variable array
!                            shareVar(:),    & !< array for data sharing
!                            varLocation(:)    !< variable location
!
!    real(FP) :: cTemp(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff
!
!    ! projection of exact solution onto quadratic + bubble (cubic)
!    integer :: iPt, rCell
!
!    real(FP) :: xProj(nDim,6),      & !< Lagrange points for exact solution  
!                qProjLag(nEqns,6),  & !< solutions on Lagrange locations
!                qProjDun(nEqns,6),  & !< dunavant point locations values
!                qProjAvg(nEqns,nCells),    & !< exact solution cell average
!                qProjAvgDiff(nEqns,nCells),    & !< exact solution cell average
!                cProj(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff
!
!    nullChar = char(0)
!    solTime = t
!
!    dXi = 1.0_FP/real(nSubCells)
!
!    select case (nDim)
!        case (1)
!            cellType = 1 ! FE linear element
!            vNodePerCell = 2*nSubCells+1
!            nVizCells = 2*nSubCells
!
!        case (2)
!            cellType = 2 ! FE triangular element
!            vNodePerCell = (nSubCells+1)*(nSubCells+2)/2
!            nVizCells = nSubCells**2
!            
!    end select
! 
!    allocate( passiveVars(nVars), shareVar(nVars), varLocation(nVars), &
!              vizData(nVars-nDim,vNodePerCell*nCells) )
!    write(zoneTitle,'(a,i0)') 'iter = ',iter
!
!    ! make node variables active, all other passive
!    ! 0 - active, 1 - passive
!    passiveVars(1:nDim) = 1 ! passive since it is shared with zone 1, see below
!    passiveVars(nDim+1:nVars) = 0 
!
!    shareVar(1:nDim) = 1 ! shared with zone 1
!    shareVar(nDim+1:nVars) = 0
!
!    ! all variables at nodes
!    varLocation(1:nVars) = 1
!
!    ! zone for cell data
!    iErr = TecZne142(trim(adjustl(zoneTitle))//nullChar, &
!                     cellType, vNodePerCell*nCells, nCells*nVizCells, nFaces, &
!                     iCellMax, jCellMax, kCellMax, &
!                     solTime, strandID, parentZn, isBlock, &
!                     nFaceConnect, faceNbMode, &
!                     0, 0, 0, & ! values only used for polygonal elements
!                     passiveVars(:), varLocation(:), shareVar(:), shareConnect)
!
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem creating TecPlot zone'
!        stop
!    end if
!
!    ! write time and iteration for zone as auxiliary data
!    auxTitle = 'Iteration'
!    write(auxData,'(i0)') iter
!    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
!                         trim(adjustl(auxData))//nullChar)
!    
!    auxTitle = 'Time'
!    write(auxData,'(e12.4e2)') solTime
!    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
!                         trim(adjustl(auxData))//nullChar)
!
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem writing auxiliary data for zone'
!        stop
!    end if
!
!    ! use latest values
!    call updateReconData()
!    ! fill in array of viz data
!    vizData(:,:) = 0.0_DP
!    select case (nDim)
!        case (1)
!            iNode = 0
!            do iCell = 1, nCells
!                do i = 0, 2*nSubCells
!                    iNode = iNode+1
!                    xi(1) = i*(0.5_FP*dXi)
!                    vizData(1,iNode) = reconValue(nDim,1,iCell,xi)
!                    vizData(2,iNode) = reconValue(nDim,2,iCell,xi)
!                    xCart = ref2cart(nDim,iCell,xi)
!                end do
!            end do
!        
!        case (2)
!
!            !do iCell = 1, nCells
!            !    ! Dunavant point values
!            !    xProj(:,:) = dunavantLoc(6)
!            !    do iPt = 1,6
!            !        xCart(:) = ref2cart(nDim,iCell,xProj(:,iPt))
!            !        qProjDun(:,iPt) = evalFunction(nDim,nEqns,xCart,t=solTime)
!            !    end do
!            !    do iEq = 1,nEqns
!            !        ! cell average
!            !        qProjAvg(iEq,iCell) = numAverage(nDim,6,qProjDun(iEq,:))
!            !        ! cell average difference - conservation error
!            !        qProjAvgDiff(iEq,iCell) = (qProjAvg(iEq,iCell)-primAvgN(iEq,iCell))
!            !    end do
!            !end do
!
!            iNode = 0
!            do iCell = 1, nCells
!                !----------------------------------------------------------
!                ! exact solution projection onto quadratic + cubic basis
!                ! Lagrange point values
!                !xProj(:,:) = symTriLoc(6)
!                !do iPt = 1,6
!                !    xCart(:) = ref2cart(nDim,iCell,xProj(:,iPt))
!                !    qProjLag(:,iPt) = evalFunction(nDim,nEqns,xCart,t=solTime)
!                !end do
!                !cProj(:,1) = qProjLag(:,1) 
!                !cProj(:,3) = qProjLag(:,3) 
!                !cProj(:,5) = qProjLag(:,5) 
!                !cProj(:,2) = qProjLag(:,2) 
!                !cProj(:,4) = qProjLag(:,4) 
!                !cProj(:,6) = qProjLag(:,6) 
!                !cProj(:,7) = (20.0_FP/9.0_FP)*(qProjAvg(:,iCell) - &
!                !             (1.0_FP/3.0_FP)*(qProjLag(:,2) + qProjLag(:,4) + &
!                !             qProjLag(:,6))) 
!                !cProj(:,8:10) = 0.0_FP
!
!                !cTemp(:,1) = qProjAvgDiff(:,iCell) 
!                !cTemp(:,3) = qProjAvgDiff(:,iCell) 
!                !cTemp(:,5) = qProjAvgDiff(:,iCell) 
!                !rCell = abs(faceCells(2,cellFaces(1,iCell)))
!                !if ( rCell == iCell ) rCell = abs(faceCells(1,cellFaces(1,iCell)))
!                !cTemp(:,2) = 0.5*(qProjAvgDiff(:,iCell) + qProjAvgDiff(:,rCell))
!                !rCell = abs(faceCells(2,cellFaces(2,iCell)))
!                !if ( rCell == iCell ) rCell = abs(faceCells(1,cellFaces(2,iCell)))
!                !cTemp(:,4) = 0.5*(qProjAvgDiff(:,iCell) + qProjAvgDiff(:,rCell))
!                !rCell = abs(faceCells(2,cellFaces(3,iCell)))
!                !if ( rCell == iCell ) rCell = abs(faceCells(1,cellFaces(3,iCell)))
!                !cTemp(:,6) = 0.5*(qProjAvgDiff(:,iCell) + qProjAvgDiff(:,rCell))
!                !cTemp(:,7:10) = 0.0_FP
!                !----------------------------------------------------------
!                 
!                do j = 0, nSubCells
!                    do i = 0, nSubCells-j
!                        iNode = iNode + 1
!                        xi(1) = i*dXi
!                        xi(2) = j*dXi
!                        ! physical coordinate related
!                        xCart(:) = ref2cart(nDim,iCell,xi)
!                        radialPos = sqrt(xCart(1)**2.0_FP + xCart(2)**2.0_FP)
!                        if ( abs(xCart(2)) <= eps .and. abs(xCart(1)) <= eps ) then
!                            theta = 0.0_FP
!                        else
!                            theta = atan2(xCart(2),xCart(1))
!                        end if
!
!                        ! Interpolation solution
!                        qTemp(:) = reconState(nDim,nEqns,iCell,xi)
!                        vizData(1,iNode) = qTemp(1)
!                        vizData(2,iNode) = qTemp(2)
!                        vizData(3,iNode) = qTemp(3)
!                        vizData(4,iNode) = qTemp(4)
!                        if ( qTemp(1) < eps .or. qTemp(4) < eps  ) then
!                            vizData(5,iNode) = 0.0_FP
!                        else
!                            vizData(5,iNode) = log(qTemp(4)) - gam*log(qTemp(1))
!                        end if
!
!                        qTemp(:) = evalFunction(nDim,nEqns,xCart,solTime)
!                        vizData(6,iNode) = qTemp(1)  
!                        vizData(7,iNode) = qTemp(2) 
!                        vizData(8,iNode) = qTemp(3) 
!                        vizData(9,iNode) = qTemp(4)
!
!                        !vizData(6,iNode) = cellAvgN(1,iCell)  
!                        !vizData(7,iNode) = cellAvgN(2,iCell) 
!                        !vizData(8,iNode) = cellAvgN(3,iCell) 
!                        !vizData(9,iNode) = cellAvgN(4,iCell)
!
!                    end do
!                end do
!            end do
!                
!    end select
!
!    ! write out nodal values
!    do iEq = 1, nVars-nDim
!        iErr = TecDat142(vNodePerCell*nCells,vizData(iEq,:),dblPrec)
!    end do
!    if ( iErr /= 0 ) then
!        write(*,*) 'ERROR: problem writing TecPlot variable data'
!        stop
!    end if
!
!    deallocate( passiveVars, shareVar, varLocation, vizData )
!
!end subroutine outputVizSolution
!!-------------------------------------------------------------------------------
!!> @purpose 
!!>  Output TecPlot binary file containing characteristic line paths
!!>
!!> @author
!!>  J. Brad Maeng
!!>
!!> @history
!!>  1 October 2015 - Initial creation
!!>  
!subroutine outputVizChOrg(iZone,dt)
!
!    use solverVars
!    use meshUtil
!    use analyticFunctions
!    use reconstruction
!    use update
!    use boundaryConditions, only: nodeBC, edgeBC
!
!    implicit none
!
!    include 'tecio.f90'
!    
!    ! Interface variables
!    integer, intent(in) :: iZone !< zone counter
!
!    real(FP), intent(in) :: dt !< time step
!
!    ! Local variables
!    character(1) :: nullChar !< null character for terminating strings
!
!    integer :: iEq,             & !< equation
!               iErr,            & !< error code for TecPlot call
!               iCell,           & !< cell index
!               iNode,           & !< node index
!               iEdge,           & !< edge index
!               gNode,           & !< global node index
!               gEdge,           & !< global edge index
!               gFace,           & !< global face index
!               onEdge             !< edge containing char. origin
!
!    real(FP) :: xi(nDim),           & !< node in ref space 
!                xi0(nDim),          & !< char origin in ref space
!                cVel(nDim),         &
!                xCartN(nDim),       &
!                xCartH(nDim),       &
!                xCart(nDim)
!
!    nullChar = char(0)
!
!    ! add characteristic line geometry
!    ! more real time approach
!    do iNode = 1, nNodes
!        xCartN(:) = nodeCoord(:,iNode) !< node coordinate
!        xCartH(:) = xCartN(:)
!        if ( nodeBC(iNode) > 0 ) cycle
!        xCart(:) = nodeChOrg(:,iNode)
!        iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!            1, 1, 0, & !< Color, FillColor, IsFilled, 
!            0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!            (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!            (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!            (/0.0_SP/), nullChar )
!        iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!            3, 3, 0, & !< Color, FillColor, IsFilled, 
!            3, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!            (/real(0.001,SP)/), &
!            (/0.0_SP/), &
!            (/0.0_SP/), nullChar )
!        !! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        !xCartH(:) = nodeChOrg(:,iNode)
!        !xCart(:) = nodeChOrgL(:,iNode)
!        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
!        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!        !    (/0.0_SP/), nullChar )
!        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
!        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/real(0.001,SP)/), &
!        !    (/0.0_SP/), &
!        !    (/0.0_SP/), nullChar )
!        !xCartH(:) = nodeChOrg(:,iNode)
!        !xCart(:) = nodeChOrgR(:,iNode)
!        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
!        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!        !    (/0.0_SP/), nullChar )
!        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
!        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/real(0.001,SP)/), &
!        !    (/0.0_SP/), &
!        !    (/0.0_SP/), nullChar )
!        ! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        if ( iErr /= 0 ) then
!            write(*,*) 'ERROR: problem writing TecPlot variable data'
!            stop
!        end if
!    end do
!    do iEdge = 1, nEdges
!        xCartN(:) = edgeCoord(:,iEdge) !< edge coordinate
!        xCartH(:) = xCartN(:)
!        if ( edgeBC(iEdge) > 0 ) cycle
!        xCart(:) = edgeChOrg(:,iEdge)
!        iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!            1, 1, 0, & !< Color, FillColor, IsFilled, 
!            0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!            (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!            (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!            (/0.0_SP/), nullChar )
!        iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!            3, 3, 0, & !< Color, FillColor, IsFilled, 
!            3, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!            (/real(0.001,SP)/), &
!            (/0.0_SP/), &
!            (/0.0_SP/), nullChar )
!        !! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        !xCartH(:) = edgeChOrg(:,iEdge)
!        !xCart(:) = edgeChOrgL(:,iEdge)
!        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
!        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!        !    (/0.0_SP/), nullChar )
!        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
!        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/real(0.001,SP)/), &
!        !    (/0.0_SP/), &
!        !    (/0.0_SP/), nullChar )
!        !xCartH(:) = edgeChOrg(:,iEdge)
!        !xCart(:) = edgeChOrgR(:,iEdge)
!        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
!        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
!        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
!        !    (/0.0_SP/), nullChar )
!        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
!        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
!        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
!        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
!        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
!        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
!        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
!        !    (/real(0.001,SP)/), &
!        !    (/0.0_SP/), &
!        !    (/0.0_SP/), nullChar )
!        ! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        if ( iErr /= 0 ) then
!            write(*,*) 'ERROR: problem writing TecPlot variable data'
!            stop
!        end if
!    end do
!
!end subroutine outputVizChOrg
!-------------------------------------------------------------------------------

