!-------------------------------------------------------------------------------
!> @purpose 
!>  Handy math routines
!>
!> @author
!>  Timothy A. Eymann
!>
module mathUtil

    use solverVars, only: FP
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply symmetric quadrature location for triangle in 2D
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  3 December 2015 - Initial Creation
!>
function symTriLoc(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: symTriLoc(2,nPts) !< first index is dimension (hardcode to 2)

    select case (nPts)

        case (6)
            ! 2nd order. Supply locations on node/edge
            symTriLoc(:,1) = (/0.0_FP, 0.0_FP/)
            symTriLoc(:,3) = (/1.0_FP, 0.0_FP/)
            symTriLoc(:,5) = (/0.0_FP, 1.0_FP/)

            symTriLoc(:,2) = (/0.5_FP, 0.5_FP/)
            symTriLoc(:,4) = (/0.0_FP, 0.5_FP/)
            symTriLoc(:,6) = (/0.5_FP, 0.0_FP/)

        case (7)
            ! 3rd order. Supply locations on node/edge
            symTriLoc(:,1) = (/0.0_FP, 0.0_FP/)
            symTriLoc(:,3) = (/1.0_FP, 0.0_FP/)
            symTriLoc(:,5) = (/0.0_FP, 1.0_FP/)

            symTriLoc(:,2) = (/0.5_FP, 0.5_FP/)
            symTriLoc(:,4) = (/0.0_FP, 0.5_FP/)
            symTriLoc(:,6) = (/0.5_FP, 0.0_FP/)

            symTriLoc(:,7) = 1.0_FP/3.0_FP

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop
 
    end select
end function symTriLoc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply weights for symmetric quadrature weights for triangle in 2D
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  3 December 2015 - Initial Creation
!>
function symTriWt(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: symTriWt(nPts) 

    select case (nPts) 

        case ( 6 )
            ! 2nd order. Supply locations on node/edge
            symTriWt(1) = 0.0_FP
            symTriWt(3) = 0.0_FP
            symTriWt(5) = 0.0_FP

            symTriWt(2) = 1.0_FP/3.0_FP  
            symTriWt(4) = 1.0_FP/3.0_FP 
            symTriWt(6) = 1.0_FP/3.0_FP 

        case ( 7 )
            ! 3rd order. Supply locations on node/edge
            symTriWt(1) = 1.0_FP/20.0_FP
            symTriWt(3) = 1.0_FP/20.0_FP
            symTriWt(5) = 1.0_FP/20.0_FP

            symTriWt(2) = 2.0_FP/15.0_FP  
            symTriWt(4) = 2.0_FP/15.0_FP 
            symTriWt(6) = 2.0_FP/15.0_FP 

            symTriWt(7) = 9.0_FP/20.0_FP 

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop
 
    end select

end function symTriWt
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply locations for numerical integration using Dunavant points
!>
!>  NOTE: These WILL NOT give answers to machine precision because of some 
!>        have form 0.0**** which means they are missing a significant figure.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 June 2012 - Initial Creation
!>
function dunavantLoc(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: dunavantLoc(2,nPts) !< first index is dimension (hardcode to 2)

    select case (nPts)
    
        case (4) !< 3rd order
            dunavantLoc(:,1) = 1.0_FP/3.0_FP
            dunavantLoc(:,2) = (/0.6_FP,0.2_FP/)
            dunavantLoc(:,3) = 0.2_FP
            dunavantLoc(:,4) = (/0.2_FP,0.6_FP/)

        case (6) !< 4th order
            ! 4th order dunavant points
            dunavantLoc(:,1) = (/0.108103018168070_FP, 0.445948490915965_FP/)
            dunavantLoc(:,2) = (/0.445948490915965_FP, 0.445948490915965_FP/)
            dunavantLoc(:,3) = (/0.445948490915965_FP, 0.108103018168070_FP/)
            dunavantLoc(:,4) = (/0.816847572980459_FP, 0.091576213509771_FP/)
            dunavantLoc(:,5) = (/0.091576213509771_FP, 0.091576213509771_FP/)
            dunavantLoc(:,6) = (/0.091576213509771_FP, 0.816847572980459_FP/)

        case (12) !< 6th order
            dunavantLoc(:,1) = (/0.501426509658179_FP, 0.249286745170910_FP/)
            dunavantLoc(:,2) = (/0.249286745170910_FP, 0.249286745170910_FP/)
            dunavantLoc(:,3) = (/0.249286745170910_FP, 0.501426509658179_FP/) 
            dunavantLoc(:,4) = (/0.873821971016996_FP, 0.063089014491502_FP/)
            dunavantLoc(:,5) = (/0.063089014491502_FP, 0.063089014491502_FP/) 
            dunavantLoc(:,6) = (/0.063089014491502_FP, 0.873821971016996_FP/)
            dunavantLoc(:,7) = (/0.053145049844817_FP, 0.310352451033784_FP/) 
            dunavantLoc(:,8) = (/0.310352451033784_FP, 0.636502499121399_FP/)
            dunavantLoc(:,9) = (/0.636502499121399_FP, 0.053145049844817_FP/)
            dunavantLoc(:,10) = (/0.310352451033784_FP, 0.053145049844817_FP/)
            dunavantLoc(:,11) = (/0.636502499121399_FP, 0.310352451033784_FP/) 
            dunavantLoc(:,12) = (/0.053145049844817_FP, 0.636502499121399_FP/)  
        
        case (16) !< 8th order
            dunavantLoc(:,1) = (/0.333333333333333_FP, 0.333333333333333_FP/)
            dunavantLoc(:,2) = (/0.081414823414554_FP, 0.459292588292723_FP/)
            dunavantLoc(:,3) = (/0.459292588292723_FP, 0.459292588292723_FP/)
            dunavantLoc(:,4) = (/0.459292588292723_FP, 0.081414823414554_FP/)
            dunavantLoc(:,5) = (/0.658861384496480_FP, 0.170569307751760_FP/)
            dunavantLoc(:,6) = (/0.170569307751760_FP, 0.170569307751760_FP/)
            dunavantLoc(:,7) = (/0.170569307751760_FP, 0.658861384496480_FP/)
            dunavantLoc(:,8) = (/0.898905543365938_FP, 0.050547228317031_FP/)
            dunavantLoc(:,9) = (/0.050547228317031_FP, 0.050547228317031_FP/)
            dunavantLoc(:,10) = (/0.050547228317031_FP, 0.898905543365938_FP/)
            dunavantLoc(:,11) = (/0.008394777409958_FP, 0.263112829634638_FP/)
            dunavantLoc(:,12) = (/0.263112829634638_FP, 0.728492392955404_FP/)
            dunavantLoc(:,13) = (/0.728492392955404_FP, 0.008394777409958_FP/)
            dunavantLoc(:,14) = (/0.263112829634638_FP, 0.008394777409958_FP/)
            dunavantLoc(:,15) = (/0.728492392955404_FP, 0.263112829634638_FP/)
            dunavantLoc(:,16) = (/0.008394777409958_FP, 0.728492392955404_FP/)

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop

    end select

end function dunavantLoc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply weights for numerical integration using Dunavant points.  
!>
!>  NOTE: These WILL NOT give answers to machine precision because of some 
!>        have form 0.0**** which means they are missing a significant figure.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  27 June 2012 - Initial Creation
!>  18 March 2016 - Weights sum up to 1.0_FP (Maeng)
!>
function dunavantWt(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: dunavantWt(nPts) 


    select case (nPts)

        case (4) ! 3rd order
            dunavantWt(1) = -9.0_FP/16.0_FP
            dunavantWt(2:4) = 25.0_FP/48.0_FP
        
        case (6) ! 4th order
            ! 4th order dunavant weights
            dunavantWt(1:3) = 0.223381589678011_FP
            dunavantWt(4:6) = 0.109951743655322_FP

        case (12) ! 6th order
            dunavantWt(1:3) = 0.116786275726379_FP
            dunavantWt(4:6) = 0.050844906370207_FP
            dunavantWt(7:12) = 0.082851075618374_FP

        case (16) ! 8th order
            dunavantWt(:) = (/0.144315607677787_FP, 0.095091634267285_FP, &
                              0.095091634267285_FP, 0.095091634267285_FP, &
                              0.103217370534718_FP, 0.103217370534718_FP, &
                              0.103217370534718_FP, 0.032458497623198_FP, &
                              0.032458497623198_FP, 0.032458497623198_FP, &
                              0.027230314174435_FP, 0.027230314174435_FP, &
                              0.027230314174435_FP, 0.027230314174435_FP, &
                              0.027230314174435_FP, 0.027230314174435_FP/)

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop

    end select

end function dunavantWt
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply locations for numerical integration using Gaussian quadrature.
!>  Accurate for polynomials up to order 2n-1, where n is number of points.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  9 July 2012 - Initial Creation
!>
function gaussQuadLoc(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: gaussQuadLoc(nPts) 


    select case (nPts)

        case (4) 
 
            gaussQuadLoc(1) = sqrt((3.0_FP-2.0_FP*sqrt(1.2_FP))/7.0_FP)
            gaussQuadLoc(2) = -gaussQuadLoc(1)
            gaussQuadLoc(3) = sqrt((3.0_FP+2.0_FP*sqrt(1.2_FP))/7.0_FP)
            gaussQuadLoc(4) = -gaussQuadLoc(3)
        
        case (5)

            gaussQuadLoc(1) = 1.0_FP/3.0_FP*sqrt(5.0_FP-2.0_FP*sqrt(10.0_FP/7.0_FP))
            gaussQuadLoc(2) = -gaussQuadLoc(1)
            gaussQuadLoc(3) = 1.0_FP/3.0_FP*sqrt(5.0_FP+2.0_FP*sqrt(10.0_FP/7.0_FP))
            gaussQuadLoc(4) = -gaussQuadLoc(3)
            gaussQuadLoc(5) = 0.0_FP

        case (6)

            gaussQuadLoc(1) = 0.6612093864662645_FP 
            gaussQuadLoc(2) = -gaussQuadLoc(1) 
            gaussQuadLoc(3) = 0.2386191860831969_FP
            gaussQuadLoc(4) = -gaussQuadLoc(3)
            gaussQuadLoc(5) = 0.9324695142031521_FP 
            gaussQuadLoc(6) = -gaussQuadLoc(5)

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop

    end select
end function gaussQuadLoc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Supply weights for numerical integration using Gaussian quadrature.
!>  Accurate for polynomials up to order 2n-1, where n is number of points.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  9 July 2012 - Initial Creation
!>
function gaussQuadWt(nPts)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nPts

    ! Function variable
    real(FP) :: gaussQuadWt(nPts) 

    select case (nPts)

        case (4) 
            gaussQuadWt(1:2) = 0.5_FP + sqrt(30.0_FP)/36.0_FP
            gaussQuadWt(3:4) = 0.5_FP - sqrt(30.0_FP)/36.0_FP

        case (5) 
            gaussQuadWt(1:2) = (322.0_FP+13.0_FP*sqrt(70.0_FP))/900.0_FP
            gaussQuadWt(3:4) = (322.0_FP-13.0_FP*sqrt(70.0_FP))/900.0_FP 
            gaussQuadWt(5) = 128.0_FP/225.0_FP

        case (6)
            gaussQuadWt(1:2) = 0.3607615730481386_FP
            gaussQuadWt(3:4) = 0.4679139345726910_FP
            gaussQuadWt(5:6) = 0.1713244923791704_FP

        case default
            write(*,*) 'ERROR: invalid number of points.'
            stop

    end select

end function gaussQuadWt
!-------------------------------------------------------------------------------
!> @purpose 
!>  Calculate the L2 norm of a vector.  Only needed for versions of Fortran that
!>  do not support the 2008 standard.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  19 July 2012 - Initial Creation
!>
#ifdef FORT2K8
function norm2(vec)

    implicit none

    ! Interface variables
    real(FP), intent(in) :: vec(:) !< input vector
    
    ! Function variable
    real(FP) :: norm2

    ! Local variable
    integer :: iMax,    & !< length of vector
               i          !< index 
    
    real(FP) :: vecSum !< sum of vector elements

    iMax = size(vec)
    vecSum = 0.0_FP
    do i = 1, iMax
        vecSum = vecSum + vec(i)**2
    end do
    norm2 = sqrt(vecSum)

end function norm2
#endif
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use Barycentric coordinates to determine if point is in triangular cell
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  23 August 2012 - Initial Creation
!>
function pointInTri(x,xVert)

    implicit none
    
    ! Interface variables
    real(FP), intent(in) :: x(2),       & !< point to test
                            xVert(2,3)    !< vertices of triangle

    ! Function variable
    logical :: pointInTri

    ! Local variables
    integer, parameter :: ix = 1,   & !< x index
                          iy = 2      !< y index
    
    real(FP) :: u,  & !< parameterization along face 1
                v     !< parameterization along face 2


    u = ((x(ix)-xVert(ix,1))*(xVert(iy,1)-xVert(iy,3)) - &
         (x(iy)-xVert(iy,1))*(xVert(ix,1)-xVert(ix,3))) / &
        (xVert(ix,3)*(xVert(iy,2)-xVert(iy,1))- &
         xVert(ix,2)*(xVert(iy,3)-xVert(iy,1))+ &
         xVert(ix,1)*(xVert(iy,3)-xVert(iy,2)))
    
    v = ((x(ix)-xVert(ix,1))*(xVert(iy,2)-xVert(iy,1)) - &
         (x(iy)-xVert(iy,1))*(xVert(ix,2)-xVert(ix,1))) / &
        (-xVert(iy,3)*(xVert(ix,2)-xVert(ix,1))+ &
          xVert(iy,2)*(xVert(ix,3)-xVert(ix,1))- &
          xVert(iy,1)*(xVert(ix,3)-xVert(ix,2)))

    pointInTri = .false.
    if ( (( u >= 0.0_FP ).and.( u <= 1.0_FP )) .and. &
         (( v >= 0.0_FP ).and.( v <= 1.0_FP )) .and. &
         ( u + v <= 1.0_FP ) ) then
        pointInTri = .true.
    end if

end function
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use numerical integration to average a set of points
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  14 May 2013 - Initial Creation
!>  18 March 2016 - Weights for Dunavant points sum up to 1.0_FP (Maeng)
!>
function numAverage(nDim,nPts,samples)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nPts       !< number of samples

    real(FP), intent(in) :: samples(nPts)

    ! Function variable
    real(FP) :: numAverage

    ! Local variables
    integer :: iPt  !< loop variable

    real(FP) :: w(nPts),    & !< weights for numerical average
                factor,     & !< normalization factor
                quad          !< quadrature sum


    select case ( nDim )
        case (1)
            w(:) = gaussQuadWt(nPts)
            factor = 0.5_FP
        case (2)
            w(:) = dunavantWt(nPts)
            factor = 1.0_FP
        case default
            write(*,'(a)') 'ERROR: averages only supported up to two dimensions'
            stop
    end select

    quad = 0.0_FP
    do iPt = 1, nPts
        quad = quad + w(iPt)*samples(iPt)
    end do
    numAverage = factor*quad

end function numAverage
!-------------------------------------------------------------------------------
!> @purpose 
!>  Use second order accurate numerical integration to average a set of points
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  16 February 2016 - Initial Creation
!>
function numAverageSymTri(nDim,nPts,samples)

    implicit none

    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nPts       !< number of samples

    real(FP), intent(in) :: samples(nPts)

    ! Function variable
    real(FP) :: numAverageSymTri

    ! Local variables
    integer :: iPt  !< loop variable

    real(FP) :: w(nPts),    & !< weights for numerical average
                factor,     & !< normalization factor
                quad          !< quadrature sum


    select case ( nDim )

        case (2)
            w(:) = symTriWt(nPts)
            factor = 1.0_FP
        case default
            write(*,'(a)') 'ERROR: averages only supported up to two dimensions'
            stop

    end select

    quad = 0.0_FP
    do iPt = 1, nPts
        quad = quad + w(iPt)*samples(iPt)
    end do
    numAverageSymTri = factor*quad

end function numAverageSymTri
!-------------------------------------------------------------------------------
!> @purpose 
!>  One-dimensional Simpson's rule for numerical integraion\n\n
!>
!> @history
!>  17 May 2013 - Initial Creation (Eymann)
!>
function simpson1d( qData )

    implicit none

    ! Interface variables
    real(FP) :: qData(3) !< data at quadrature points

    ! Function variable
    real(FP) :: simpson1d


    simpson1d = (1.0_FP/6.0_FP)*(qData(1)+4.0_FP*qData(2)+qData(3))

end function
!-------------------------------------------------------------------------------
!> @purpose 
!>  Two-dimensional Simpson's rule for numerical integraion\n\n
!>
!> @history
!>  17 May 2013 - Initial Creation (Eymann)
!>
function simpson2d( qData )

    implicit none

    ! Interface variables
    real(FP) :: qData(3,3) !< data at quadrature points

    ! Function variable
    real(FP) :: simpson2d

    simpson2d = ( 1.0_FP/36.0_FP*( qData(1,1)+qData(1,3)+qData(3,1)+qData(3,3) ) + &
                  4.0_FP/36.0_FP*( qData(1,2)+qData(2,1)+qData(2,3)+qData(3,2) ) + &
                  16.0_FP/36.0_FP*( qData(2,2) ) ) 

end function
!-------------------------------------------------------------------------------
!> @purpose 
!>  Two-dimensional Trapezoidal rule for numerical integraion\n\n
!>
!> @history
!>  9 January 2017 - Initial Creation (Maeng)
!>
function trapez2d( qData )

    implicit none

    ! Interface variables
    real(FP) :: qData(2,2) !< data at quadrature points

    ! Function variable
    real(FP) :: trapez2d

    trapez2d = 1.0_FP/4.0_FP*( qData(1,1)+qData(1,2)+qData(2,1)+qData(2,2) )

end function trapez2d
!-------------------------------------------------------------------------------
!> @purpose 
!>  Sort vertices in order to create quadrilateral, convex hull of n = 4
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  16 February 2016 - Initial Creation
!>
subroutine sortQuadVertex(xVert,order)

    use solverVars, only: eps
    implicit none
    
    ! Interface variables
    integer, intent(inout) :: order(4)       !< order of nodes

    real(FP), intent(inout) :: xVert(2,4)    !< vertices of triangle
    
    ! Local variables
    integer :: nVert,   & !< number of vertex 
               iVert,   & !< vertex index
               jVert,   & !< vertex index
               kVert,   & !< vertex index
               lVert,   & !< vertex index
               j,       &
               lOrder(4)  !< local order 

    real(FP) :: xmax,   &
                xmin,   &
                ymax,   &
                ymin    

    real(FP) :: xVertOld(2,4),      & 
                xDiagVec1(2),       &
                xDiagVec2(2)

    real(FP) :: A(2,2), ADet, Ainv(2,2), coef(2), xInt(2)

    nVert = 4
    xVertOld = xVert
    lOrder = order 

    ! find max and min bound
    xmax = -1.0e+06
    ymax = -1.0e+06
    xmin = 1.0e+06
    ymin = 1.0e+06
    do iVert = 1,4
        xmax = max(xmax, xVert(1,iVert))
        ymax = max(ymax, xVert(2,iVert))
        xmin = min(xmin, xVert(1,iVert))
        ymin = min(ymin, xVert(2,iVert))
    end do

    iVertLoop: do iVert = 1, nVert-1 
        do j = iVert+1,nVert
            jVert = j
            xDiagVec1(:) = xVertOld(:,jVert) - xVertOld(:,iVert) 

            ! indices for the other diagonal line
            kVert = mod(iVert,nVert)+1
            if ( kVert == jVert ) kVert = mod(kVert,nVert)+1
            lVert = mod(kVert,nVert)+1 
            if ( lVert == iVert ) lVert = mod(lVert,nVert)+1
            if ( lVert == jVert ) lVert = mod(lVert,nVert)+1
            xDiagVec2(:) = xVertOld(:,lVert) - xVertOld(:,kVert) 
    
            ! construct matrix containing diagonal vectors
            A(:,1) = xDiagVec1(:)
            A(:,2) = -xDiagVec2(:)
            ADet = A(1,1)*A(2,2) - A(1,2)*A(2,1)

            if (abs(ADet) <= eps) then
                !write(*,*) 'Skip singular element case or parallel lines'
                cycle
            end if
            ! invert A
            Ainv(1,1) = A(2,2)
            Ainv(2,2) = A(1,1)
            Ainv(1,2) = -A(1,2)
            Ainv(2,1) = -A(2,1)
            Ainv = Ainv/ADet
             
            coef(:) = matmul(Ainv,(xVertOld(:,kVert)-xVertOld(:,iVert)))
            ! intersection point            
            xInt(:) = xVertOld(:,iVert) + coef(1)*xDiagVec1(:) 
            !xInt(:) = xVertOld(:,kVert) + coef(2)*xDiagVec2(:) 
             
            if ( (xInt(1) > xmin) .and. (xInt(1) < xmax) .and. &
                 (xInt(2) > ymin) .and. (xInt(2) < ymax) ) then
                ! check cross product for positive area
                if ( (xDiagVec1(1)*xDiagVec2(2) - xDiagVec2(1)*xDiagVec1(2)) >= eps ) then
                    xVert(:,1) = xVertOld(:,iVert)
                    xVert(:,3) = xVertOld(:,jVert)
                    xVert(:,2) = xVertOld(:,kVert)
                    xVert(:,4) = xVertOld(:,lVert)
                    order(1) = lOrder(iVert)
                    order(2) = lOrder(kVert)
                    order(3) = lOrder(jVert)
                    order(4) = lOrder(lVert)
                    !write(*,*) iVert, jVert, kVert, lVert, xInt
                else
                    xVert(:,1) = xVertOld(:,iVert)
                    xVert(:,3) = xVertOld(:,jVert)
                    xVert(:,2) = xVertOld(:,lVert)
                    xVert(:,4) = xVertOld(:,kVert)
                    order(1) = lOrder(iVert)
                    order(2) = lOrder(lVert)
                    order(3) = lOrder(jVert)
                    order(4) = lOrder(kVert)
                end if
                exit iVertLoop
            end if
        end do
    end do iVertLoop

end subroutine sortQuadVertex
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return bilinear isoparametric quadrilateral mapping given four vertex of 
!>  quadrilateral element
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 January 2016 - Initial Creation
!>
function bilinQuadriLoc(nDim,xNodes,xi)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim !< problem dimension

    real(FP), intent(in) :: xNodes(nDim,4), & !< function coefficients
                            xi(nDim)          !< Reference coordinate

    ! Function variables
    real(FP) :: bilinQuadriLoc(nDim)

    ! Local variables
    integer :: iPoint

    real(FP) :: phi(4)       !< shape function

    phi(1) = 0.25_FP*(1.0_FP-xi(1))*(1.0_FP-xi(2))
    phi(2) = 0.25_FP*(1.0_FP+xi(1))*(1.0_FP-xi(2))
    phi(3) = 0.25_FP*(1.0_FP+xi(1))*(1.0_FP+xi(2))
    phi(4) = 0.25_FP*(1.0_FP-xi(1))*(1.0_FP+xi(2))

    bilinQuadriLoc(:) = 0.0_FP
    do iPoint = 1, 4
        bilinQuadriLoc(1) = bilinQuadriLoc(1) + xNodes(1,iPoint)*phi(iPoint)  
        bilinQuadriLoc(2) = bilinQuadriLoc(2) + xNodes(2,iPoint)*phi(iPoint) 
    end do

end function bilinQuadriLoc
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return bilinear isoparametric quadrilateral mapping Jacobian
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  17 January 2016 - Initial Creation
!>
function bilinQuadriJac(nDim,xNodes,xi)

    use solverVars, only: eps
    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim !< problem dimension

    real(FP), intent(in) :: xNodes(nDim,4), & !< function coefficients
                            xi(nDim)          !< Reference coordinate

    ! Function variables
    real(FP) :: bilinQuadriJac(nDim,nDim)

    ! Local variables
    integer :: iPoint

    real(FP) :: phixi(nDim,4)      !< xi derivative

    phixi(1,1) = -0.25_FP*(1.0_FP-xi(2))
    phixi(1,2) =  0.25_FP*(1.0_FP-xi(2))
    phixi(1,3) =  0.25_FP*(1.0_FP+xi(2))
    phixi(1,4) = -0.25_FP*(1.0_FP+xi(2))

    phixi(2,1) = -0.25_FP*(1.0_FP-xi(1))
    phixi(2,2) = -0.25_FP*(1.0_FP+xi(1))
    phixi(2,3) =  0.25_FP*(1.0_FP+xi(1))
    phixi(2,4) =  0.25_FP*(1.0_FP-xi(1))
    
    bilinQuadriJac(:,:) = 0.0_FP
    do iPoint = 1, 4
        bilinQuadriJac(1,1) = bilinQuadriJac(1,1) + phixi(1,iPoint)*xNodes(1,iPoint) 
        bilinQuadriJac(1,2) = bilinQuadriJac(1,2) + phixi(1,iPoint)*xNodes(2,iPoint) 
        bilinQuadriJac(2,1) = bilinQuadriJac(2,1) + phixi(2,iPoint)*xNodes(1,iPoint) 
        bilinQuadriJac(2,2) = bilinQuadriJac(2,2) + phixi(2,iPoint)*xNodes(2,iPoint) 
    end do

end function bilinQuadriJac
!-----------------------------------------------------------------------------
end module mathUtil

