!-------------------------------------------------------------------------------
!> @purpose 
!>  Provides visualization functions
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  3 May 2016 - Initial creation
!>
module tecplotVisualUtil

    use inputOutput, only: fileLength, filePath, solFileName

    implicit none

    ! TecPlot parameter
    integer, parameter :: nSubCells = 2  !< number of sub-cells per side of standard triangle

    integer :: nVars                !< TecPlot number of variables, only set once

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output TecPlot binary file switching routine.  
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  10 April 2015 - Initial creation
!>
subroutine switchTecPlotFile(whichFile)

    implicit none

    include 'tecio.f90'
    
    ! Interface variable
    integer, intent(in) :: whichFile

    integer :: iErr               !< error flag

    iErr = TecFil142(whichFile)

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem switching TecPlot output files'
        stop
    end if

end subroutine switchTecPlotFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Close whichever TecPlot binary file is currently open and being editted.  
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  12 August 2016 - Initial creation
!>
subroutine closeTecPlotFile()

    implicit none

    include 'tecio.f90'
    
    ! local variable
    integer :: iErr               !< error flag

    iErr = TecEnd142()

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem closing TecPlot output file'
        stop
    end if

end subroutine closeTecPlotFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize TecPlot file header. Optional fileName can be used to dump out
!>   data when needed. 'nVar' and 'variables' are defined in this routine.
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  28 March 2015 - Initial creation
!>
subroutine initTecPlotFile(fileName)

    use solverVars
    use meshUtil

    implicit none

    include 'tecio.f90'
    
    ! Interface variable
    character(fileLength), intent(in), optional :: fileName

    ! Local variables
    character(1) :: nullChar !< null character for terminating strings

    character(80) :: dataTitle,     & !< data set title
                     fName            !< name of file to create including file path

    character(400) :: variables !< variables in data set

    integer, parameter :: fileFmt = 0,      & !< flag for file format, 0-.plt, 1-szplt
                          fullSoln = 0,     & !< flag for full solution file
                          noDebug = 0,      & !< flag for debugging, 0-no, 1-yes
                          dblPrec = 1         !< flag for double precision

    pointer (nullPtr,null) !< null pointer for TecPlot calls
    integer*4 null(*)

    integer :: iErr               !< error flag
    
    nullChar = char(0)
    nullPtr = 0
    dataTitle = 'Solution Reconstrution'

    select case (nDim)
        case (1)
            variables = 'x'  ! coordinate
            variables = trim(adjustl(variables)) // &
                ' <greek>r</greek> u '//nullChar

            nVars = nDim + nEqns 

        case (2)
            variables = 'x y' ! coordinate
            variables = trim(adjustl(variables)) // & 
                ' <greek>r</greek> u v p &
                rCons ruCons rvCons rECons ' //nullChar
                !rBF uBF vBF pBF s ' //nullChar

            nVars = nDim + 4 + 4 ! + 4 + 1

    end select

    ! check if filename is given
    if ( .not. present(fileName) ) then
        fName = solFileName ! has path included in variable
    else
        fName = fileName 
    end if

    ! open file and create header information
    iErr = TecIni142(trim(adjustl(dataTitle))//nullChar, &
                     trim(adjustl(variables))//nullChar, &
                     trim(adjustl(fName))//nullChar, &
                     trim(adjustl(filePath))//nullChar, &
                     fileFmt, fullSoln, noDebug, dblPrec )

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem creating TecPlot file'
        stop
    end if

end subroutine initTecPlotFile
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output TecPlot binary file containing visualization mesh.  Each cell writes
!>  full internal state to account for solution discontinutities at interfaces
!>  ( no sharing of node/edge data )
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  12 June 2012 - Initial creation
!>  28 March 2016 - Initialization of TecPlot header moved to initTecPlotFile(Maeng)
!>
subroutine outputTecPlotMesh()

    use solverVars
    use meshUtil
    implicit none

    include 'tecio.f90'
    
    ! Local variables
    character(1) :: nullChar !< null character for terminating strings

    character(80) :: zoneTitle,     & !< zone title
                     auxTitle,      & !< auxiliary data title
                     auxData,       & !< auxiliary data value
                     fName            !< name of file to create

    integer, parameter :: dblPrec = 1,      & !< flag for double precision
                          iCellMax = 0,     & !< future use flag
                          jCellMax = 0,     & !< fugure use flag
                          kCellMax = 0,     & !< future use flag
                          strandID = 0,     & !< static zone flag
                          parentZn = 0,     & !< zone has no parent
                          isBlock = 1,      & !< block data format
                          nFaceConnect = 0, & !< number of face connections
                          faceNbMode = 0,   & !< face neighbor mode (local 1:1)
                          shareConnect = 0    !< no connectivity sharing

    pointer (nullPtr,null) !< null pointer for TecPlot calls
    integer*4 null(*)

    integer :: iErr,            & !< error flag
               iDir,            & !< direction index
               iNode,           & !< node index
               iCell,           & !< global cell index
               vCell,           & !< viz cell index
               i, j,            & !< ref. coordinate indices
               cellType,        & !< flag specifying element type
               vNodePerCell,    & !< number of viz nodes per cell
               nVizCells          !< number of visualization subcells

    integer, allocatable :: vNodeConnect(:,:),  & !< viz node connectivity
                            ij2node(:,:),       & !< i,j to node index array
                            passiveVars(:),     & !< passive variable array
                            varLocation(:)        !< variable location

    real(FP) :: dXi,        & !< xi spacing
                xi(nDim)      !< reference coordinates

    real(FP) :: solTime !< solution time

    real(FP), allocatable :: xVizNode(:,:) !< viz node coordinate
    
    
    nullChar = char(0)
    nullPtr = 0
    zoneTitle = 'Viz Mesh'
    solTime = 0.0_FP

    dXi = 1.0_FP / real(nSubCells)

    select case (nDim)
        case (1)
            cellType = 1 ! FE linear element
            vNodePerCell = 2*nSubCells+1
            nVizCells = 2*nSubCells
            allocate( ij2node(2*nSubCells+1,1) ) 

        case (2)
            cellType = 2 ! FE triangular element
            vNodePerCell = (nSubCells+1)*(nSubCells+2)/2
            nVizCells = nSubCells**2
            allocate( ij2node(nSubCells+1,nSubCells+1) ) 

    end select
 
    allocate( vNodeConnect(nDim+1,nCells*nVizCells), &
              xVizNode(nDim,vNodePerCell*nCells),   &
              passiveVars(nVars), varLocation(nVars) )

    ! all data at nodes, node centered value representation
    varLocation(1:nVars) = 1

    ! make coordinates active variables, all other passive
    passiveVars(1:nDim) = 0
    passiveVars(nDim+1:nVars) = 1

    ! zone information, write *.plt binary file
    iErr = TecZne142(trim(adjustl(zoneTitle))//nullChar, &
                     cellType, vNodePerCell*nCells, nCells*nVizCells, nFaces, &
                     iCellMax, jCellMax, kCellMax, &
                     solTime, strandID, parentZn, isBlock, &
                     nFaceConnect, faceNbMode, &
                     0, 0, 0, & ! values only used for polygonal elements
                     passiveVars(:), varLocation(:), null, shareConnect)

    !write(*,*) 'zone 0'
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem creating TecPlot mesh zone'
        stop
    end if

    ! write time and iteration for zone as auxiliary data
    auxTitle = 'Iteration'
    write(auxData,'(i0)') -1
    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
                         trim(adjustl(auxData))//nullChar)
    
    auxTitle = 'Time'
    write(auxData,'(e12.4e2)') solTime
    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
                         trim(adjustl(auxData))//nullChar)

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem writing auxiliary data for zone'
        stop
    end if

    ! Create new node lists and connectivity before writing to file
    iNode = 0
    vCell = 0

    select case ( nDim )

        case (1)
            do iCell = 1, nCells
                ! node coordinates
                do i = 0, 2*nSubCells
                    xi(1) = (0.5*dXi)*i
                    iNode = iNode+1
                    xVizNode(:,iNode) = ref2cart(nDim,iCell,xi(:))
                    ij2node(i+1,1) = iNode
                end do

                ! connectivity
                do i = 1, 2*nSubCells
                    vCell = vCell + 1
                    vNodeConnect(1,vCell) = ij2node(i,1)
                    vNodeConnect(2,vCell) = ij2node(i+1,1)
                end do
            end do

        case (2)
            do iCell = 1, nCells
                ! node coordinates
                ij2node = 0
                do j = 0, nSubCells
                    do i = 0, nSubCells-j
                        xi(1) = dXi*i
                        xi(2) = dXi*j
                        iNode = iNode+1
                        xVizNode(:,iNode) = ref2cart(nDim,iCell,xi(:))

                        ! store mapping from i,j to new node index
                        ij2node(i+1,j+1) = iNode
                    end do
                end do

                ! connectivity for lower triangles
                do j = 1, nSubCells
                    do i = 1, nSubCells-(j-1)
                        vCell = vCell + 1
                        vNodeConnect(1,vCell) = ij2node(i,j)
                        vNodeConnect(2,vCell) = ij2node(i+1,j)
                        vNodeConnect(3,vCell) = ij2node(i,j+1)
                    end do
                end do

                ! connectivity for upper triangles
                do j = 1, nSubCells-1
                    do i = 2, nSubCells-(j-1) ! -1 left in for readability
                        vCell = vCell + 1
                        vNodeConnect(1,vCell) = ij2node(i,j)
                        vNodeConnect(2,vCell) = ij2node(i,j+1)
                        vNodeConnect(3,vCell) = ij2node(i-1,j+1)
                    end do
                end do

            end do
    end select

    ! write out node coordinates 
    do iDir = 1, nDim
        iErr = TecDat142(vNodePerCell*nCells,xVizNode(iDir,:),dblPrec)
        if ( iErr /= 0 ) then
            write(*,*) 'ERROR: problem writing TecPlot mesh data'
            stop
        end if
    end do

    ! write out node connectivity information
    iErr = TecNod142(vNodeConnect(:,:))
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem writing TecPlot connectivity data'
        stop
    end if

    deallocate( vNodeConnect, xVizNode, ij2node, passiveVars, varLocation )

end subroutine outputTecPlotMesh
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output TecPlot binary file containing internal cell reconstruction
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  14 May 2013 - Initial creation
!>
subroutine outputTecPlotSolution(iter, t)

    use solverVars
    use meshUtil
    use analyticFunctions
    use iterativeFunctions
    use reconstruction
    use update   
    use physics, only: soundSpeed, gam

    implicit none

    include 'tecio.f90'
    
    ! Interface variables
    integer, intent(in) :: iter !< iteration

    real(FP), intent(in) :: t !< current time

    ! Local variables
    character(1) :: nullChar !< null character for terminating strings

    character(80) :: zoneTitle, & !< zone title
                     auxTitle,  & !< auxiliary variable title
                     auxData      !< auxiliary data value
                     
    integer, parameter :: meshFile = 1,     & !< flag for mesh data file
                          fullSoln = 0,     & !< flag for full solution file
                          noDebug = 0,      & !< flag for no debugging
                          dblPrec = 1,      & !< flag for double precision
                          iCellMax = 0,     & !< future use flag
                          jCellMax = 0,     & !< fugure use flag
                          kCellMax = 0,     & !< future use flag
                          strandID = 0,     & !< static zone flag
                          parentZn = 0,     & !< zone has no parent
                          isBlock = 1,      & !< block data format
                          nFaceConnect = 0, & !< number of face connections
                          faceNbMode = 0,   & !< face neighbor mode (local 1:1)
                          shareConnect = 1    !< share connectivity with zone 1

    integer :: iErr,            & !< error code for TecPlot call
               iNode,           & !< node index
               iEdge,           & !< edge index
               iCell,           & !< global cell index
               iEq,             & !< equation index
               i, j,            & !< ref. coord indices
               vNodePerCell,    & !< viz nodes per cell
               nVizCells,       & !< additional viz subcells per cell
               cellType           !< flag specifying element type

    real(FP) :: dXi,                & !< reference coordinate increment
                xi(nDim),           & !< reference coordinate of node
                xCart(nDim),        & !< phycial coordinate
                gradU(nDim,nDim),   &
                detJ,               &    
                divU,               &
                radialPos,          &
                theta,              &
                vMag,               &
                speed,              &
                qTemp(nEqns),       & !< temporary variable
                qTemp2(nEqns),      & !< temporary variable
                qExTemp(nEqns)        !< temporary variable

    character(8) :: funcname 

    real(FP) :: solTime !< solution time

    real(FP), allocatable :: vizData(:,:) !< reconstruction data at nodes

    integer, allocatable :: passiveVars(:), & !< passive variable array
                            shareVar(:),    & !< array for data sharing
                            varLocation(:)    !< variable location

    !real(FP) :: cTemp(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    ! projection of exact solution onto quadratic + bubble (cubic)
    integer :: iPt, rCell

    real(FP) :: xProj(nDim,6),      & !< Lagrange points for exact solution  
                qProjLag(nEqns,6),  & !< solutions on Lagrange locations
                qProjDun(nEqns,6),  & !< dunavant point locations values
                qProjAvg(nEqns,nCells),    & !< exact solution cell average
                qProjAvgDiff(nEqns,nCells),    & !< exact solution cell average
                cProj(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< reconstruction coeff

    nullChar = char(0)
    solTime = t

    dXi = 1.0_FP/real(nSubCells)

    select case (nDim)
        case (1)
            cellType = 1 ! FE linear element
            vNodePerCell = 2*nSubCells+1
            nVizCells = 2*nSubCells

        case (2)
            cellType = 2 ! FE triangular element
            vNodePerCell = (nSubCells+1)*(nSubCells+2)/2
            nVizCells = nSubCells**2
            
    end select
 
    allocate( passiveVars(nVars), shareVar(nVars), varLocation(nVars), &
              vizData(nVars-nDim,vNodePerCell*nCells) )
    write(zoneTitle,'(a,i0)') 'iter = ',iter

    ! make node variables active, all other passive
    ! 0 - active, 1 - passive
    passiveVars(1:nDim) = 1 ! passive since it is shared with zone 1, see below
    passiveVars(nDim+1:nVars) = 0 

    shareVar(1:nDim) = 1 ! shared with zone 1
    shareVar(nDim+1:nVars) = 0

    ! all variables at nodes
    varLocation(1:nVars) = 1

    ! zone for cell data
    iErr = TecZne142(trim(adjustl(zoneTitle))//nullChar, &
                     cellType, vNodePerCell*nCells, nCells*nVizCells, nFaces, &
                     iCellMax, jCellMax, kCellMax, &
                     solTime, strandID, parentZn, isBlock, &
                     nFaceConnect, faceNbMode, &
                     0, 0, 0, & ! values only used for polygonal elements
                     passiveVars(:), varLocation(:), shareVar(:), shareConnect)

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem creating TecPlot zone'
        stop
    end if

    ! write time and iteration for zone as auxiliary data
    auxTitle = 'Iteration'
    write(auxData,'(i0)') iter
    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
                         trim(adjustl(auxData))//nullChar)
    
    auxTitle = 'Time'
    write(auxData,'(e12.4e2)') solTime
    iErr = TecZAuxStr142(trim(adjustl(auxTitle))//nullChar, &
                         trim(adjustl(auxData))//nullChar)

    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem writing auxiliary data for zone'
        stop
    end if

    ! use latest values
    call updateReconData()

    ! fill in array of viz data
    vizData(:,:) = 0.0_FP
    select case (nDim)
        case (1)
            iNode = 0
            do iCell = 1, nCells
                do i = 0, 2*nSubCells
                    iNode = iNode+1
                    xi(1) = i*(0.5_FP*dXi)
                    vizData(1,iNode) = reconValue(nDim,1,iCell,xi)
                    vizData(2,iNode) = reconValue(nDim,2,iCell,xi)
                    xCart = ref2cart(nDim,iCell,xi)
                end do
            end do
        
        case (2)

            iNode = 0
            do iCell = 1, nCells
                do j = 0, nSubCells
                    do i = 0, nSubCells-j
                        iNode = iNode + 1
                        xi(1) = i*dXi
                        xi(2) = j*dXi
                        ! physical coordinate related
                        xCart(:) = ref2cart(nDim,iCell,xi)
                        radialPos = sqrt(xCart(1)**2.0_FP + xCart(2)**2.0_FP)
                        if ( abs(xCart(2)) <= eps .and. abs(xCart(1)) <= eps ) then
                            theta = 0.0_FP
                        else
                            theta = atan2(xCart(2),xCart(1))
                        end if

                        ! Interpolation solution
                        do iEq = 1, nEqns
                            qTemp(iEq) = reconValue(nDim,iEq,iCell,xi)
                        end do
                        vizData(1,iNode) = qTemp(1)
                        vizData(2,iNode) = qTemp(2)
                        vizData(3,iNode) = qTemp(3)
                        vizData(4,iNode) = qTemp(4)
                                                
                        vizData(5,iNode) = cellAvgN(1,iCell)  
                        vizData(6,iNode) = cellAvgN(2,iCell) 
                        vizData(7,iNode) = cellAvgN(3,iCell) 
                        vizData(8,iNode) = cellAvgN(4,iCell)

                        !vizData(9,iNode) = reconData(1,7,iCell)  
                        !vizData(10,iNode) = reconData(2,7,iCell) 
                        !vizData(11,iNode) = reconData(3,7,iCell) 
                        !vizData(12,iNode) = reconData(4,7,iCell)

                        !if ( abs(vizData(1,iNode)) <= 1000.0*eps ) then
                        !    vizData(13,iNode) = 0.0_FP
                        !else
                        !    vizData(13,iNode) = log(qTemp(4)/qTemp(1)**gam)
                        !end if

                    end do
                end do
            end do
                
    end select

    ! write out nodal values
    do iEq = 1, nVars-nDim
        iErr = TecDat142(vNodePerCell*nCells,vizData(iEq,:),dblPrec)
    end do
    if ( iErr /= 0 ) then
        write(*,*) 'ERROR: problem writing TecPlot variable data'
        stop
    end if

    deallocate( passiveVars, shareVar, varLocation, vizData )

end subroutine outputTecPlotSolution
!-------------------------------------------------------------------------------
!> @purpose 
!>  Output TecPlot binary file containing characteristic line paths
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  1 October 2015 - Initial creation
!>  
subroutine outputTecPlotChOrg(iZone,dt)

    use solverVars
    use meshUtil
    use analyticFunctions
    use reconstruction
    use update
    use boundaryConditions, only: nodeBC, edgeBC

    implicit none

    include 'tecio.f90'
    
    ! Interface variables
    integer, intent(in) :: iZone !< zone counter

    real(FP), intent(in) :: dt !< time step

    ! Local variables
    character(1) :: nullChar !< null character for terminating strings

    integer :: iEq,             & !< equation
               iErr,            & !< error code for TecPlot call
               iCell,           & !< cell index
               iNode,           & !< node index
               iEdge,           & !< edge index
               gNode,           & !< global node index
               gEdge,           & !< global edge index
               gFace,           & !< global face index
               onEdge             !< edge containing char. origin

    real(FP) :: xi(nDim),           & !< node in ref space 
                xi0(nDim),          & !< char origin in ref space
                cVel(nDim),         &
                xCartN(nDim),       &
                xCartH(nDim),       &
                xCart(nDim)

    nullChar = char(0)

    ! add characteristic line geometry
    ! more real time approach
    do iNode = 1, nNodes
        xCartN(:) = nodeCoord(:,iNode) !< node coordinate
        xCartH(:) = xCartN(:)
        if ( nodeBC(iNode) > 0 ) cycle
        xCart(:) = nodeChOrg(:,iNode)
        iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
            1, 1, 0, & !< Color, FillColor, IsFilled, 
            0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
            (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
            (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
            (/0.0_SP/), nullChar )
        iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
            3, 3, 0, & !< Color, FillColor, IsFilled, 
            3, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
            (/real(0.001,SP)/), &
            (/0.0_SP/), &
            (/0.0_SP/), nullChar )
        !! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !xCartH(:) = nodeChOrg(:,iNode)
        !xCart(:) = nodeChOrgL(:,iNode)
        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
        !    (/0.0_SP/), nullChar )
        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/real(0.001,SP)/), &
        !    (/0.0_SP/), &
        !    (/0.0_SP/), nullChar )
        !xCartH(:) = nodeChOrg(:,iNode)
        !xCart(:) = nodeChOrgR(:,iNode)
        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
        !    (/0.0_SP/), nullChar )
        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/real(0.001,SP)/), &
        !    (/0.0_SP/), &
        !    (/0.0_SP/), nullChar )
        ! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if ( iErr /= 0 ) then
            write(*,*) 'ERROR: problem writing TecPlot variable data'
            stop
        end if
    end do
    do iEdge = 1, nEdges
        xCartN(:) = edgeCoord(:,iEdge) !< edge coordinate
        xCartH(:) = xCartN(:)
        if ( edgeBC(iEdge) > 0 ) cycle
        xCart(:) = edgeChOrg(:,iEdge)
        iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
            1, 1, 0, & !< Color, FillColor, IsFilled, 
            0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
            (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
            (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
            (/0.0_SP/), nullChar )
        iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
            0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
            3, 3, 0, & !< Color, FillColor, IsFilled, 
            3, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
            0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
            0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
            1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
            (/real(0.001,SP)/), &
            (/0.0_SP/), &
            (/0.0_SP/), nullChar )
        !! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !xCartH(:) = edgeChOrg(:,iEdge)
        !xCart(:) = edgeChOrgL(:,iEdge)
        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
        !    (/0.0_SP/), nullChar )
        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/real(0.001,SP)/), &
        !    (/0.0_SP/), &
        !    (/0.0_SP/), nullChar )
        !xCartH(:) = edgeChOrg(:,iEdge)
        !xCart(:) = edgeChOrgR(:,iEdge)
        !iErr = TecGeo142(xCartH(1), xCartH(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    1, 1, 0, & !< Color, FillColor, IsFilled, 
        !    0, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/0.0_SP,real((xCart(1)-xCartH(1)),SP)/), &
        !    (/0.0_SP,real((xCart(2)-xCartH(2)),SP)/), &
        !    (/0.0_SP/), nullChar )
        !iErr = TecGeo142(xCart(1), xCart(2), 0.0, & !< coordinate data
        !    0, 1, iZone, & !< PosCoordMode, AttachToZone, Zone
        !    3, 3, 0, & !< Color, FillColor, IsFilled, 
        !    2, 0, 100.0_FP, & !< GeomType, LinePattern, PatternLength
        !    0.2_FP, 20, 0, & !< LineThickness, NumEllipsePts, ArrowheadStyle
        !    0, 0.0_FP, 0.0_FP, & !< ArrowheadAttachment, ArrowheadSize, ArrowheadAngle
        !    1, 1, 1, (/2/), & !< Scope, Clipping, NumSegments, NumSegPts
        !    (/real(0.001,SP)/), &
        !    (/0.0_SP/), &
        !    (/0.0_SP/), nullChar )
        ! to plot cross wind diffusion origins !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        if ( iErr /= 0 ) then
            write(*,*) 'ERROR: problem writing TecPlot variable data'
            stop
        end if
    end do

end subroutine outputTecPlotChOrg
!-------------------------------------------------------------------------------
end module tecplotVisualUtil
!-------------------------------------------------------------------------------
