!------------------------------------------------------------------------------
!> @purpose 
!>  Routines that reconstruct solution within cell
!>
!> @author
!>  Timothy A. Eymann
!>  
!> @history
!>  10 April 2015 - Added gradient and second order gradient evaluation (Maeng)
!>
module reconstruction
    
    use solverVars, only : FP
    implicit none

    real(FP), allocatable :: reconData(:,:,:),       & !< recon coef at "n"
                             reconDataH(:,:,:),      & !< recon coef at half time
                             reconDataF(:,:,:)         !< recon coef at full time

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return full reconstruction state at a specified coordinate in 
!>  reference space.  Probably want to combine this with reconValue...
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  26 June 2012 - Initial Creation
!>
function reconState(nDim,nEqns,iCell,xi,cIn)

    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           nEqns,   & !< reconstruciton index
                           iCell      !< cell containing point

    real(FP), intent(in) :: xi(nDim) !< Reference coordinate

    real(FP), intent(in), optional :: cIn(nEqns,(nDim+2)*(nDim+3)/(2*(3-nDim))) !< function coefficients

    ! Function variables
    real(FP) :: reconState(nEqns)

    ! Local variables
    integer :: iEq !< equation index

    reconState(:) = 0.0_FP
    do iEq = 1, nEqns
        reconState(iEq) = reconValue(nDim,iEq,iCell,xi,cIn(iEq,:))
    end do

end function reconState
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return reconstruction value at a specified coordinate in reference space\n
!>  Contains basis function definition.
!>
!> @author
!>  Timothy A. Eymann
!>
!> @history
!>  29 March 2012 - Initial Creation
!>
function reconValue(nDim,iEq,iCell,xi,cIn)

    use solverVars, only: eps
    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iCell,   & !< cell containing point
                           iEq        !< reconstruciton index

    real(FP), intent(in) :: xi(nDim) !< Reference coordinate

    real(FP), intent(in), optional :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim))) 
                                        !< function coefficients

    ! Function variables
    real(FP) :: reconValue

    ! Local variables
    integer :: nBasis,  & !< total number of basis functions
               basis      !< index for basis function

    real(FP) :: c((nDim+2)*(nDim+3)/(2*(3-nDim))),  & !< reconstruction coeff
                phi((nDim+2)*(nDim+3)/(2*(3-nDim))),& !< basis functions
                s1,                                 & !< area of sub triangles
                s2,                                 &
                s3

    nBasis = (nDim+2)*(nDim+3)/(2*(3-nDim))
    if ( .not. present(cIn) ) then
        c(:) = reconData(iEq,:,iCell)
    else
        c(:) = cIn(:)
    end if
    phi(:) = 0.0_FP

    select case ( nDim ) 

        case (1)
            reconValue = c(1)*(1.0_FP-2.0_FP*xi(1))*(1.0_FP-xi(1)) + &
                         c(2)*4.0_FP*xi(1)*(1.0_FP-xi(1)) + &
                         c(3)*xi(1)*(2.0_FP*xi(1)-1.0_FP)

        case (2)
            s1 = 1.0_FP - (xi(1)+xi(2))
            s2 = xi(1)
            s3 = xi(2)
            
            ! quadratic
            phi(1) = s1*(2.0_FP*s1-1.0_FP)
            phi(2) = 4.0_FP*s2*s3
            phi(3) = s2*(2.0_FP*s2-1.0_FP)
            phi(4) = 4.0_FP*s3*s1
            phi(5) = s3*(2.0_FP*s3-1.0_FP)
            phi(6) = 4.0_FP*s1*s2
            ! bubble
            phi(7) = 27.0_FP*s1*s2*s3
            phi(8:10) = 0.0_FP

            reconValue = 0.0_FP
            do basis = 1, nBasis
                reconValue = reconValue + c(basis)*phi(basis)
            end do

    end select

end function reconValue
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return reconstruction value at a specified coordinate in reference space.
!>  Apply diffusion in bubble functions by interpolating at two cross-wind 
!>  locations and averaging them
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  29 March 2012 - Initial Creation
!>
function reconValueBFdiff(nDim,iEq,iCell,lCell,rCell,xi,xiL,xiR)

    use solverVars, only: eps
    implicit none
    
    ! Interface variables
    integer, intent(in) :: nDim,    & !< problem dimension
                           iCell,   & !< cell containing point
                           lCell,   & !< cell containing point
                           rCell,   & !< cell containing point
                           iEq        !< reconstruciton index

    real(FP), intent(in) :: xi(nDim),   & !< Reference coordinate
                            xiL(nDim),  & !< left cross-wind reference coord 
                            xiR(nDim)     !< right cross-wind reference coord 

    ! Function variables
    real(FP) :: reconValueBFdiff

    ! Local variables
    integer :: nBasis,  & !< total number of basis functions
               basis      !< index for basis function

    real(FP) :: c((nDim+2)*(nDim+3)/(2*(3-nDim))),  & !< reconstruction coeff
                phi((nDim+2)*(nDim+3)/(2*(3-nDim))),& !< basis functions
                phiL((nDim+2)*(nDim+3)/(2*(3-nDim))), &
                phiR((nDim+2)*(nDim+3)/(2*(3-nDim))), &
                s1,                                 & !< area of sub triangles
                s2,                                 &
                s3


    nBasis = (nDim+2)*(nDim+3)/(2*(3-nDim))
    c(:) = reconData(iEq,:,iCell)
    phi(:) = 0.0_FP

    reconValueBFdiff = 0.0_FP
    select case ( nDim ) 

        case (1)
            reconValueBFdiff = c(1)*(1.0_FP-2.0_FP*xi(1))*(1.0_FP-xi(1)) + &
                         c(2)*4.0_FP*xi(1)*(1.0_FP-xi(1)) + &
                         c(3)*xi(1)*(2.0_FP*xi(1)-1.0_FP)
        case (2)

            s1 = 1.0_FP - (xi(1)+xi(2))
            s2 = xi(1)
            s3 = xi(2)
            phi(1) = s1*(2.0_FP*s1-1.0_FP)
            phi(2) = 4.0_FP*s2*s3
            phi(3) = s2*(2.0_FP*s2-1.0_FP)
            phi(4) = 4.0_FP*s3*s1
            phi(5) = s3*(2.0_FP*s3-1.0_FP)
            phi(6) = 4.0_FP*s1*s2
            !phi(7) = 27.0_FP*s1*s2*s3
            phi(7:10) = 0.0_FP

            s1 = 1.0_FP - (xiL(1)+xiL(2))
            s2 = xiL(1)
            s3 = xiL(2)
            phiL(1) = s1*(2.0_FP*s1-1.0_FP)
            phiL(2) = 4.0_FP*s2*s3
            phiL(3) = s2*(2.0_FP*s2-1.0_FP)
            phiL(4) = 4.0_FP*s3*s1
            phiL(5) = s3*(2.0_FP*s3-1.0_FP)
            phiL(6) = 4.0_FP*s1*s2
            !phiL(7) = 27.0_FP*s1*s2*s3
            phiL(7:10) = 0.0_FP

            s1 = 1.0_FP - (xiR(1)+xiR(2))
            s2 = xiR(1)
            s3 = xiR(2)
            phiR(1) = s1*(2.0_FP*s1-1.0_FP)
            phiR(2) = 4.0_FP*s2*s3
            phiR(3) = s2*(2.0_FP*s2-1.0_FP)
            phiR(4) = 4.0_FP*s3*s1
            phiR(5) = s3*(2.0_FP*s3-1.0_FP)
            phiR(6) = 4.0_FP*s1*s2
            !phiR(7) = 27.0_FP*s1*s2*s3
            phiR(7:10) = 0.0_FP

            do basis = 1, nBasis
                reconValueBFdiff = reconValueBFdiff + &
                    1.0_FP/3.0_FP*c(basis)*(phi(basis)+phiL(basis)+phiR(basis))
                !reconValueBFdiff = reconValueBFdiff + 0.5_FP*c(basis)*(phiL(basis)+phiR(basis))
            end do

            !! average bubble function 
            !reconValueBFdiff = reconValueBFdiff + &
            !    0.5_FP*(reconData(iEq,7,lCell)*phi7L + &
            !            reconData(iEq,7,rCell)*phi7R)

    end select

end function reconValueBFdiff
!-------------------------------------------------------------------------------
!> @purpose 
!>  Spatial gradient operator for AF reconstruction
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  27 January 2015 - Initial Creation 
!>  22 April 2016 - Implimented cellInvJac to return gradients in physical coord
!>
function gradVal( nDim,iEq,iCell,xi,cIn )

    use solverVars, only: eps
    use meshUtil, only: cellInvJac
    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,    & !< number of dimensions
                           iEq,     & !< equation index
                           iCell      !< cell index

    real(FP), intent(in) :: xi(nDim)  !< eval location

    real(FP), intent(in), optional :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim))) !< recon coeffs
                
    !< Function variable
    real(FP) :: gradVal(nDim)

    !< Local variable
    integer :: nBasis,  & !< total number of basis functions
               basis      !< index for basis function

    real(FP) :: phix((nDim+2)*(nDim+3)/(2*(3-nDim))),   & !< xi grad basis functions
                phiy((nDim+2)*(nDim+3)/(2*(3-nDim))),   & !< eta grad basis functions
                c((nDim+2)*(nDim+3)/(2*(3-nDim))),      & !< recon coeffs
                J11, J12, J21, J22,                     & !< inverse transformation jacobian
                gradx,                                  &
                grady,                                  &
                s1,                                     &
                s2,                                     &
                s3

    nBasis = (nDim+2)*(nDim+3)/(2*(3-nDim))
    if ( .not. present(cIn) ) then
        c(:) = reconData(iEq,:,iCell)
    else
        c(:) = cIn(:)
    end if
 
    gradVal(:) = 0.0_FP
    select case ( nDim ) 
        case (1)
            J11 = cellInvJac(1,1,iCell)

            gradx = c(1)*(4.0_FP*xi(1)-3.0_FP) +  &
                         c(2)*(-8.0_FP*xi(1)+4.0_FP) + &
                         c(3)*(4.0_FP*xi(1)-1.0_FP)

            ! du/dx
            gradVal(1) = gradx*J11

        case (2)
            J11 = cellInvJac(1,1,iCell)
            J12 = cellInvJac(1,2,iCell)
            J21 = cellInvJac(2,1,iCell)
            J22 = cellInvJac(2,2,iCell)

            s1 = 1.0_FP - (xi(1)+xi(2))
            s2 = xi(1)
            s3 = xi(2)

            ! grad_xi basis 
            phix(1) = 1.0_FP-4.0_FP*s1 
            phix(2) = 4.0_FP*s3 
            phix(3) = 4.0_FP*s2-1.0_FP 
            phix(4) = -4.0_FP*s3 
            phix(5) = 0.0_FP 
            phix(6) = 4.0_FP*(s1-s2)
            !! bubble
            !phix(7) = 27.0_FP*((s1*s3)-1.0_FP*s2*s3)
            !phix(8:10) = 0.0_FP
            ! nonconservative
            phix(7:10) = 0.0_FP

            ! grad_eta basis 
            phiy(1) = 1.0_FP-4.0_FP*s1
            phiy(2) = 4.0_FP*s2 
            phiy(3) = 0.0_FP 
            phiy(4) = 4.0_FP*(s1-s3) 
            phiy(5) = 4.0_FP*s3-1.0_FP 
            phiy(6) = -4.0_FP*s2 
            !! bubble
            !phiy(7) = 27.0_FP*((s1*s2)-1.0_FP*s2*s3)
            !phiy(8:10) = 0.0_FP
            ! nonconservative
            phiy(7:10) = 0.0_FP

            ! gradient in reference coordinate
            gradx = 0.0_FP
            grady = 0.0_FP
            do basis = 1, nBasis
                gradx = gradx + c(basis)*phix(basis)
                grady = grady + c(basis)*phiy(basis)
            end do

            ! du/dx, du/dy in physical coordinate
            gradVal(1) = J11*gradx + J21*grady
            gradVal(2) = J12*gradx + J22*grady

    end select

end function gradVal
!-------------------------------------------------------------------------------
!> @purpose 
!>  Second order spatial gradient operator for AF reconstruction
!>
!> @author
!>  J. Brad Maeng
!>  
!> @history
!>  27 February 2015 - Initial Creation 
!>
function secondGradVal( nDim,iEq,iCell,xi,cIn )

    use meshUtil, only: cellInvJac
    implicit none

    !< Interface variables
    integer, intent(in) :: nDim,    & !< number of dimensions
                           iEq,     & !< equation index
                           iCell      !< cell index

    real(FP), intent(in) :: xi(nDim)  !< eval location

    real(FP), intent(in), optional :: cIn((nDim+2)*(nDim+3)/(2*(3-nDim))) !< recon coeffs
                
    !< Function variable
    real(FP) :: secondGradVal(nDim,nDim)

    !< Local variable
    integer :: nBasis,  & !< total number of basis functions
               basis      !< index for basis function

    real(FP) :: phixx((nDim+2)*(nDim+3)/(2*(3-nDim))),   & !< xi grad basis functions
                phixy((nDim+2)*(nDim+3)/(2*(3-nDim))),   & !< eta grad basis functions
                phiyy((nDim+2)*(nDim+3)/(2*(3-nDim))),   & !< eta grad basis functions
                c((nDim+2)*(nDim+3)/(2*(3-nDim))),      &
                J11, J12, J21, J22, &
                gradxx,             &
                gradyy,             &
                gradxy,             &
                s1,                 &
                s2,                 &
                s3

    nBasis = (nDim+2)*(nDim+3)/(2*(3-nDim))
    if ( .not. present(cIn) ) then
        c(:) = reconData(iEq,:,iCell)
    else
        c(:) = cIn(:)
    end if
 
    secondGradVal(:,:) = 0.0_FP
    select case ( nDim ) 
        case (1)
            secondGradVal(1,:) = 4.0_FP*(c(1) - 2.0_FP*c(2) + c(3)) 

        case (2)
            J11 = cellInvJac(1,1,iCell)
            J12 = cellInvJac(1,2,iCell)
            J21 = cellInvJac(2,1,iCell)
            J22 = cellInvJac(2,2,iCell)
            
            s1 = 1.0_FP - (xi(1)+xi(2))
            s2 = xi(1)
            s3 = xi(2)

            ! all basis functions are hard coded 
            ! grad_x^2 basis 
            phixx(1) =  J11*J11*4.0_FP + J11*J21*4.0_FP + J21*J11*4.0_FP + J21*J21*4.0_FP  
            phixx(2) =  J11*J11*0.0_FP + J11*J21*4.0_FP + J21*J11*4.0_FP + J21*J21*0.0_FP  
            phixx(3) =  J11*J11*4.0_FP + J11*J21*0.0_FP + J21*J11*0.0_FP + J21*J21*0.0_FP  
            phixx(4) =  J11*J11*0.0_FP - J11*J21*4.0_FP - J21*J11*4.0_FP - J21*J21*8.0_FP  
            phixx(5) =  J11*J11*0.0_FP + J11*J21*0.0_FP + J21*J11*0.0_FP + J21*J21*4.0_FP  
            phixx(6) = -J11*J11*8.0_FP - J11*J21*4.0_FP - J21*J11*4.0_FP + J21*J21*0.0_FP  
            phixx(7:10) = 0.0_FP

            ! grad_xy basis 
            phixy(1) =  J11*J12*4.0_FP + J11*J22*4.0_FP + J21*J12*4.0_FP + J21*J22*4.0_FP 
            phixy(2) =  J11*J12*0.0_FP + J11*J22*4.0_FP + J21*J12*4.0_FP + J21*J22*0.0_FP 
            phixy(3) =  J11*J12*4.0_FP + J11*J22*0.0_FP + J21*J12*0.0_FP + J21*J22*0.0_FP 
            phixy(4) =  J11*J12*0.0_FP - J11*J22*4.0_FP - J21*J12*4.0_FP - J21*J22*8.0_FP 
            phixy(5) =  J11*J12*0.0_FP + J11*J22*0.0_FP + J21*J12*0.0_FP + J21*J22*4.0_FP 
            phixy(6) = -J11*J12*8.0_FP - J11*J22*4.0_FP - J21*J12*4.0_FP + J21*J22*0.0_FP 
            phixy(7:10) = 0.0_FP

            ! grad_y^2 basis 
            phiyy(1) =  J12*J12*4.0_FP + J12*J22*4.0_FP + J22*J12*4.0_FP + J22*J22*4.0_FP 
            phiyy(2) =  J12*J12*0.0_FP + J12*J22*4.0_FP + J22*J12*4.0_FP + J22*J22*0.0_FP 
            phiyy(3) =  J12*J12*4.0_FP + J12*J22*0.0_FP + J22*J12*0.0_FP + J22*J22*0.0_FP 
            phiyy(4) =  J12*J12*0.0_FP - J12*J22*4.0_FP - J22*J12*4.0_FP - J22*J22*8.0_FP 
            phiyy(5) =  J12*J12*0.0_FP + J12*J22*0.0_FP + J22*J12*0.0_FP + J22*J22*4.0_FP 
            phiyy(6) = -J12*J12*8.0_FP - J12*J22*4.0_FP - J22*J12*4.0_FP + J22*J22*0.0_FP 
            phiyy(7:10) = 0.0_FP

            gradxx = 0.0_FP
            gradxy = 0.0_FP
            gradyy = 0.0_FP
            do basis = 1, nBasis
                gradxx = gradxx + c(basis)*phixx(basis)
                gradxy = gradxy + c(basis)*phixy(basis)
                gradyy = gradyy + c(basis)*phiyy(basis)
            end do

            secondGradVal(1,1) = gradxx 
            secondGradVal(1,2) = gradxy 
            secondGradVal(2,1) = gradxy 
            secondGradVal(2,2) = gradyy 

    end select

end function secondGradVal
!-------------------------------------------------------------------------------
end module reconstruction
!-------------------------------------------------------------------------------
