!-------------------------------------------------------------------------------
!> @purpose 
!>  Routines related to time step control
!>
!> @history
!>  27 April 2016 - Initial creation (Maeng)
!>  
module timeControl

    use solverVars, only: FP, dtMax, tFinal
    implicit none

contains
!-------------------------------------------------------------------------------
!> @purpose 
!>  Initialize time step related variables
!>
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 April 2016 - Initial Creation
!>  7 February 2017 - Make total iteration number even numbered
!>
subroutine initTimeStep(nIter,cfl,dt)

    use meshUtil, only: lMin, minDistance

    implicit none

    ! Interface variables
    integer, intent(out) :: nIter        !< number of iteration 

    real(FP), intent(inout) :: cfl,    & !< initial cfl
                               dt        !< initial dt

    ! Local variables
    real(FP) :: temp      !< temporary variable

    ! set minimum distance for time step calculation
    call minDistance(lMin)

    ! maximum allowable time step 
    call maxDt(dtMax)
    !write(*,*) dtMax

    ! override tFinal for fast vortex problem
    !tFinal = 1.0_FP*0.1_FP/(0.5_FP*sqrt(1.4_FP*287.15*300.0_FP)) 

    ! calculate dtIn based on initial CFL
    call updateDt(cfl,dt)
    nIter = int(tFinal/dt) ! initial estimate
    if ( nIter <= 0 ) nIter = 1

    ! 2/7/2017: make nIter even numbered
    if ( mod(nIter,2) /= 0 ) nIter = nIter + 1
    
    ! re-calculate dtIn based on nIter
    dt = dble(tFinal/nIter)
    cfl = dt/dtMax ! new updated cfl

end subroutine initTimeStep
!-------------------------------------------------------------------------------
!> @purpose 
!>  Time step calculation. 
!>  With each iteration, time step may need to be re-adjusted.
!>  
!> @author
!>  J. Brad Maeng
!>
!> @history
!>  27 April 2016 - Initial Creation
!>
subroutine updateDt(cfl,dt)

    use physics, only: UVEL, VVEL, soundSpeed
    use meshUtil, only: lMin, nDim, nCells, nNodes, nEdges
    use update

    implicit none

    !> Interface variables
    real(FP), intent(in) :: cfl

    real(FP), intent(inout) :: dt 

    !> Local variables
    integer :: iCell,   & !< local cell index
               iEdge,   & !< local edge index
               iNode,   & !< node edge index
               gNode,   & !< global node index
               gEdge      !< global edge index

    real(FP) :: acSpeed,    & !< acoustic speed
                lamEdge,    & !< temporary wave speed
                lamNode,    & !< temporary wave speed
                lamMax        !< maximum wave speed

    lamMax = 1.0e-06

    select case ( eqFlag ) 

        case ( ADVECTION_EQ, PLESSEULER_EQ )
            ! linear advection and pressureless Euler
            if ( nDim == 2 ) then
                do iNode = 1,nNodes
                    lamNode = norm2(nodeDataN(UVEL:VVEL,iNode)) 
                    lamMax = max(lamMax,abs(lamNode))
                end do
                do iEdge = 1,nEdges
                    lamEdge = norm2(edgeDataN(UVEL:VVEL,iEdge)) 
                    lamMax = max(lamMax,abs(lamEdge))
                end do
            else
                ! nDim == 1
                do iNode = 1,nNodes
                    lamNode = nodeDataN(UVEL,iNode) 
                    lamMax = max(lamMax,abs(lamNode)) 
                end do
            end if

        case ( ISENTEULER_EQ, EULER_EQ )
            ! Isentropic Euler and Euler equations
            do iNode = 1,nNodes
                acSpeed = soundSpeed(nEqns,nodeDataN(:,iNode))
                lamNode = norm2(nodeDataN(UVEL:VVEL,iNode)) 
                lamMax = max(lamMax,abs(lamNode+acSpeed)) 
            end do
            do iEdge = 1,nEdges
                acSpeed = soundSpeed(nEqns,edgeDataN(:,iEdge))
                lamEdge = norm2(edgeDataN(UVEL:VVEL,iEdge)) 
                lamMax = max(lamMax,abs(lamEdge+acSpeed)) 
            end do

        case ( ACOUSTIC_EQ ) 
            do iNode = 1,nNodes
                acSpeed = soundSpeed(nEqns,nodeDataN(:,iNode))
                lamMax = max(lamMax,abs(acSpeed)) 
            end do
            do iEdge = 1,nEdges
                acSpeed = soundSpeed(nEqns,edgeDataN(:,iEdge))
                lamMax = max(lamMax,abs(acSpeed)) 
            end do

        case default
            write(*,'(a)') 'ERROR: Unspecified equation type.'
            stop

    end select

    dt = cfl*lMin/lamMax

end subroutine updateDt
!-------------------------------------------------------------------------------
!> @purpose 
!>  Return maximum allowable time step 
!>
!> @history
!>  18 May 2013 - Initial Creation (Eymann)
!>  6 September 2013 - modified for 2D linear advection (Maeng)
!>  12 January 2015 - modified for 2D pressure-less Euler equations (Maeng) 
!>  7 May 2015 - added acoustic speed (Maeng)
!>  7 February 2016 - eqFlag for time step update (Maeng)
!>
subroutine maxDt(maximumDt)

    use physics, only: UVEL, VVEL, soundSpeed
    use meshUtil, only: lMin, nDim, nCells, nNodes, nEdges
    use update

    implicit none
    !> Interface variables
    real(FP), intent(out) :: maximumDt

    !> Local variables
    integer :: iCell,   & !< cell index
               iEdge,   & !< local edge index
               iNode,   & !< local node index
               gNode,   & !< global node index
               gEdge      !< global edge index

    real(FP) :: acSpeed,        & !< acoustic speed
                lamNode,        & !< estimate of advection speed in cell
                lamEdge,        & !< estimate of advection speed in cell
                lamMax            !< maximum wave speed

    maximumDt = 1.0e6

    lamMax = 1.0e-06
    lamNode = lamMax
    lamEdge = lamMax

    select case ( eqFlag ) 

        case ( ADVECTION_EQ, PLESSEULER_EQ )
            ! linear advection and pressureless Euler
            if ( nDim == 2 ) then
                do iNode = 1,nNodes
                    lamNode = norm2(nodeDataN(UVEL:VVEL,iNode)) 
                    lamMax = max(lamMax,abs(lamNode))
                end do
                do iEdge = 1,nEdges
                    lamEdge = norm2(edgeDataN(UVEL:VVEL,iEdge)) 
                    lamMax = max(lamMax,abs(lamEdge))
                end do
            else
                ! nDim == 1
                do iNode = 1,nNodes
                    lamNode = nodeDataN(UVEL,iNode) 
                    lamMax = max(lamMax,abs(lamNode)) 
                end do
            end if

        case ( ISENTEULER_EQ, EULER_EQ )
            ! Isentropic Euler and Euler equations
            do iNode = 1,nNodes
                acSpeed = soundSpeed(nEqns,nodeDataN(:,iNode))
                lamNode = norm2(nodeDataN(UVEL:VVEL,iNode)) 
                lamMax = max(lamMax,abs(lamNode+acSpeed)) 
            end do
            do iEdge = 1,nEdges
                acSpeed = soundSpeed(nEqns,edgeDataN(:,iEdge))
                lamEdge = norm2(edgeDataN(UVEL:VVEL,iEdge)) 
                lamMax = max(lamMax,abs(lamEdge+acSpeed)) 
            end do

        case ( ACOUSTIC_EQ ) 
            do iNode = 1,nNodes
                acSpeed = soundSpeed(nEqns,nodeDataN(:,iNode))
                lamMax = max(lamMax,abs(acSpeed)) 
            end do
            do iEdge = 1,nEdges
                acSpeed = soundSpeed(nEqns,edgeDataN(:,iEdge))
                lamMax = max(lamMax,abs(acSpeed)) 
            end do

        case default
            write(*,'(a)') 'ERROR: Unspecified equation type.'
            stop

    end select

    maximumDt = min( maximumDt, lMin/lamMax )
   
end subroutine maxDt
!-------------------------------------------------------------------------------
end module timeControl
!-------------------------------------------------------------------------------
